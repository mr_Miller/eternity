<?php

namespace Eternity\Http\Contracts;

use Eternity\Http\Dto\ErrorDto;

/**
 * Interface Error
 * @package Eternity\Http\Contracts
 */
interface Error
{
    /**
     * Get errors from response
     * @return array
     */
    public function getErrors(): array;

    /**
     * @return \Eternity\Http\Dto\ErrorDto|null
     */
    public function getError(): ?ErrorDto;

    /**
     * Get original error trace
     *
     * Use when debugging
     *
     * @return mixed
     */
    public function getTrace(): ?array;
}