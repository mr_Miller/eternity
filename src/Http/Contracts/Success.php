<?php

namespace Eternity\Http\Contracts;

/**
 * Interface Success
 * @package Eternity\Http\Contracts
 */
interface Success
{
    /**
     * Get whole payload items
     * @return array
     */
    public function getPayloadItems(): array;

    /**
     * Get single (first) item from payload
     *
     * Use for obtaining first item from payload
     * @return array
     */
    public function getPayloadItem(): array;

    /**
     * Get links
     * @return array
     */
    public function getLinks(): array;

    /**
     * Get metadata
     * @return array
     */
    public function getMetadata(): array;
}