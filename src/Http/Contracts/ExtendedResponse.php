<?php

namespace Eternity\Http\Contracts;

use Psr\Http\Message\ResponseInterface;

/**
 * Interface ExtendedResponse
 * @package Eternity\Http\Contracts
 */
interface ExtendedResponse extends Error, Success
{
    /**
     * Return the original response class
     * @return mixed
     */
    public function getOriginalResponse(): ResponseInterface;

    /**
     * Return headers
     * @return array
     */
    public function getHeaders(): array;

    /**
     * Check if response has forbidden error
     * @return bool
     */
    public function isForbidden(): bool;

    /**
     * Check if response is success
     * @return bool
     */
    public function isSuccess(): bool;

    /**
     * Check if response is error
     * @return bool
     */
    public function isError(): bool;

    /**
     * Check response type is application/json
     * @return bool
     */
    public function isApplicationJson(): bool;

    /**
     * Return response body as array
     * @return array
     */
    public function getBody(): ?array;

    /**
     * Return http status code
     */
    public function getHttpStatus(): int;

}