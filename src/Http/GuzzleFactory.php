<?php

namespace Eternity\Http;

use GuzzleHttp\Client as GuzzleHttp;
use GuzzleHttp\Exception\ConnectException;
use GuzzleHttp\Exception\TransferException;
use GuzzleHttp\HandlerStack;
use GuzzleHttp\Middleware;
use Psr\Http\Message\RequestInterface;
use Psr\Http\Message\ResponseInterface;

/**
 * This is the PSR-7 factory class.
 */
final class GuzzleFactory
{
    /**
     * The default connect timeout.
     * @var int
     */
    const CONNECT_TIMEOUT = 10;

    /**
     * The default transport timeout.
     * @var int
     */
    const TIMEOUT = 15;

    /**
     * The default backoff multiplayer
     */
    const BACKOFF = 1000;

    /**
     * The default 4xx retry codes.
     * @var int[]
     */
    const CODES = [429];

    /**
     * Create a new guzzle client.
     *
     * @param array $options
     * @param int|null $backoff
     * @param int[]|null $codes
     *
     * @return Client
     */
    public static function make(array $options = [], int $backoff = null, array $codes = null): Client
    {
        $options['http_errors'] = false; // Guzzle will not throw exceptions when status 4xx or 5xx is received
        $config = array_merge(['connect_timeout' => self::CONNECT_TIMEOUT, 'timeout' => self::TIMEOUT], $options);
        // todo it is disabled temporary because of very long server response time on error
        // $config['handler'] = self::handler($backoff, $codes, $options['handler'] ?? null);

        return new Client(new GuzzleHttp($config));
    }

    /**
     * Create a new guzzle handler stack.
     *
     * @param int|null $backoff
     * @param int[]|null $codes
     * @param \GuzzleHttp\HandlerStack|null $stack
     *
     * @return \GuzzleHttp\HandlerStack
     */
    public static function handler(int $backoff = null, array $codes = null, HandlerStack $stack = null): HandlerStack
    {
        $stack = $stack ?: HandlerStack::create();
        $stack->push(Middleware::retry(static function (
            $retries,
            RequestInterface $request,
            ResponseInterface $response = null,
            TransferException $exception = null
        ) use ($codes) {
            return $retries < 3 && ($exception instanceof ConnectException || ($response && ($response->getStatusCode() >= 500 || in_array($response->getStatusCode(), $codes ?? self::CODES, true))));
        }, static function ($retries) use ($backoff) {
            return (int)pow(2, $retries) * ($backoff ?? self::BACKOFF);
        }));

        return $stack;
    }
}
