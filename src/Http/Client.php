<?php

namespace Eternity\Http;

use Eternity\Http\Contracts\ExtendedResponse;
use Eternity\Http\Exceptions\RequestBodyFormatException;
use Eternity\Http\Exceptions\RequestException;
use Eternity\Http\Exceptions\ResponseException;
use Eternity\Http\Response\Response;
use GuzzleHttp\ClientInterface;
use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\Exception\RequestException as GuzzleRequestException;
use GuzzleHttp\Psr7\Request as Psr7Request;
use Psr\Http\Message\RequestInterface;

/**
 * Http Client
 *
 * Class Client
 * @package Eternity\Http
 */
class Client
{
    /**
     * @var ClientInterface
     */
    private $httpClient;

    /**
     * Client constructor.
     *
     * @param ClientInterface $client
     */
    public function __construct(ClientInterface $client)
    {
        $this->httpClient = $client;
    }

    /**
     * Handle undefined methods from httpClient
     *
     * @param $method
     * @param $args
     *
     * @return mixed
     */
    public function __call($method, $args)
    {
        return $this->httpClient->{$method}(...$args);
    }

    /**
     * @return ClientInterface
     */
    public function client(): ClientInterface
    {
        return $this->httpClient;
    }

    /**
     * Check response status code is equal to expected status
     *
     * @param int $expectedStatus
     * @param \Eternity\Http\Contracts\ExtendedResponse $response
     *
     * @return bool
     */
    public function expected($expectedStatus, ExtendedResponse $response): bool
    {
        return $expectedStatus === $response->getHttpStatus();
    }

    /**
     * Create and send GET request to endpoint
     *
     * @param \Psr\Http\Message\UriInterface|string $uri
     * @param int $expectedStatus
     * @param array $headers
     * @param array|string|null $body
     *
     * @return \Eternity\Http\Contracts\ExtendedResponse
     * @throws \Eternity\Http\Exceptions\RequestBodyFormatException
     * @throws \Eternity\Http\Exceptions\RequestException
     * @throws \Eternity\Http\Exceptions\ResponseException
     */
    public function get($uri, $expectedStatus = 200, $headers = [], $body = null): ExtendedResponse
    {
        return $this->send($this->buildRequest('GET', $uri, $headers, $body), $expectedStatus);
    }

    /**
     * Create and send PUT request to endpoint
     *
     * @param \Psr\Http\Message\UriInterface|string $uri
     * @param int $expectedStatus
     * @param array $headers
     * @param array|string|null $body
     *
     * @return \Eternity\Http\Contracts\ExtendedResponse
     * @throws \Eternity\Http\Exceptions\RequestBodyFormatException
     * @throws \Eternity\Http\Exceptions\RequestException
     * @throws \Eternity\Http\Exceptions\ResponseException
     */
    public function put($uri, $expectedStatus = 204, $headers = [], $body = null): ExtendedResponse
    {
        return $this->send($this->buildRequest('PUT', $uri, $headers, $body), $expectedStatus);
    }

    /**
     * Create and send POST request to endpoint
     *
     * @param \Psr\Http\Message\UriInterface|string $uri
     * @param int $expectedStatus
     * @param array $headers
     * @param array|string|null $body
     *
     * @return \Eternity\Http\Contracts\ExtendedResponse
     * @throws \Eternity\Http\Exceptions\RequestBodyFormatException
     * @throws \Eternity\Http\Exceptions\RequestException
     * @throws \Eternity\Http\Exceptions\ResponseException
     */
    public function post($uri, $expectedStatus = 200, $headers = [], $body = null): ExtendedResponse
    {
        return $this->send($this->buildRequest('POST', $uri, $headers, $body), $expectedStatus);
    }

    /**
     * Create and send PATCH request to endpoint
     *
     * @param \Psr\Http\Message\UriInterface|string $uri
     * @param int $expectedStatus
     * @param array $headers
     * @param array|string|null $body
     *
     * @return \Eternity\Http\Contracts\ExtendedResponse
     * @throws \Eternity\Http\Exceptions\RequestBodyFormatException
     * @throws \Eternity\Http\Exceptions\RequestException
     * @throws \Eternity\Http\Exceptions\ResponseException
     */
    public function patch($uri, $expectedStatus = 204, $headers = [], $body = null): ExtendedResponse
    {
        return $this->send($this->buildRequest('PATCH', $uri, $headers, $body), $expectedStatus);
    }

    /**
     * Create and send DELETE request to endpoint
     *
     * @param \Psr\Http\Message\UriInterface|string $uri
     * @param int $expectedStatus
     * @param array $headers
     * @param array|string|null $body
     *
     * @return \Eternity\Http\Contracts\ExtendedResponse
     * @throws \Eternity\Http\Exceptions\RequestBodyFormatException
     * @throws \Eternity\Http\Exceptions\RequestException
     * @throws \Eternity\Http\Exceptions\ResponseException
     */
    public function delete($uri, $expectedStatus = 204, $headers = [], $body = null): ExtendedResponse
    {
        return $this->send($this->buildRequest('DELETE', $uri, $headers, $body), $expectedStatus);
    }

    /**
     * Create and send HEAD request to endpoint
     *
     * @param \Psr\Http\Message\UriInterface|string $uri
     * @param int $expectedStatus
     * @param array $headers
     * @param array|string|null $body
     *
     * @return \Eternity\Http\Contracts\ExtendedResponse
     * @throws \Eternity\Http\Exceptions\RequestBodyFormatException
     * @throws \Eternity\Http\Exceptions\RequestException
     * @throws \Eternity\Http\Exceptions\ResponseException
     */
    public function head($uri, $expectedStatus = 200, $headers = [], $body = null): ExtendedResponse
    {
        return $this->send($this->buildRequest('HEAD', $uri, $headers, $body), $expectedStatus);
    }

    /**
     * Create and send OPTIONS request to endpoint
     *
     * @param string $uri
     * @param int $expectedStatus
     * @param array $headers
     * @param array|string|null $body
     *
     * @return \Eternity\Http\Contracts\ExtendedResponse
     * @throws \Eternity\Http\Exceptions\RequestBodyFormatException
     * @throws \Eternity\Http\Exceptions\RequestException
     * @throws \Eternity\Http\Exceptions\ResponseException
     */
    public function options($uri, $expectedStatus = 200, $headers = [], $body = null): ExtendedResponse
    {
        return $this->send($this->buildRequest('OPTIONS', $uri, $headers, $body), $expectedStatus);
    }

    /**
     * Prepare request
     *
     * With Psr7Request
     * @param string $requestType
     * @param string $uri
     * @param array $headers
     * @param array|string|null $body
     * @return \Psr\Http\Message\RequestInterface
     * @throws \Eternity\Http\Exceptions\RequestBodyFormatException
     */
    protected function buildRequest(
        string $requestType,
        string $uri,
        array $headers = [],
        $body = null
    ): RequestInterface {
        return new Psr7Request($requestType, $uri, $headers, $this->prepareBody($body));
    }

    /**
     * Prepare body
     * @param array|string|null $body
     * @return string
     * @throws \Eternity\Http\Exceptions\RequestBodyFormatException
     */
    protected function prepareBody($body): ?string
    {
        if ($body === null) {
            return null;
        } elseif (is_array($body)) {
            return json_encode($body);
        } elseif (strval($body)) {
            return $body;
        }

        throw new RequestBodyFormatException();
    }

    /**
     * Create and send request to api endpoint
     *
     * @param \Psr\Http\Message\RequestInterface $request
     * @param int $expectedStatus
     * @return \Eternity\Http\Contracts\ExtendedResponse
     * @throws \Eternity\Http\Exceptions\RequestException
     * @throws \Eternity\Http\Exceptions\ResponseException
     */
    protected function send(RequestInterface $request, int $expectedStatus): ExtendedResponse
    {
        try {
            $response = new Response($this->httpClient->send($request));
        } catch (GuzzleRequestException|GuzzleException $e) {
            // In case if request not send
            throw new RequestException(get_class($e), $e->getMessage(), $e);
        }
        if (!$this->expected($expectedStatus, $response)) {
            throw new ResponseException(
                $response,
                'Response error',
                'Response has unexpected status: ' . $response->getHttpStatus() . ' in stead of ' . $expectedStatus
            );
        }

        return $response;
    }

}

