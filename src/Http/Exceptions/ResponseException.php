<?php

namespace Eternity\Http\Exceptions;

use Eternity\Exceptions\ServerException;
use Eternity\Http\Contracts\ExtendedResponse;
use Throwable;

/**
 * Exception is thrown if response contains errors
 *
 * Class ResponseErrorException
 * @package Eternity\Http\Exceptions
 */
class ResponseException extends ServerException
{
    /**
     * @var ExtendedResponse
     */
    protected $response;

    /**
     * ResponseErrorException constructor.
     * @param \Eternity\Http\Contracts\ExtendedResponse $response
     * @param string|null $title
     * @param string|null $detail
     * @param \Throwable|null $previous
     */
    public function __construct(
        ExtendedResponse $response,
        string $title = null,
        string $detail = null,
        Throwable $previous = null
    ) {
        parent::__construct($title, $detail, $previous);
        $this->response = $response;
        $this->type = 'ResponseException';
    }

    /**
     * Returns the response with error
     */
    public function getResponse(): ExtendedResponse
    {
        return $this->response;
    }

}