<?php

namespace Eternity\Http\Exceptions;

use Eternity\Exceptions\ServerException;
use Throwable;

/**
 * Class RequestBodyFormatException
 * @package Eternity\Http\Exceptions
 */
class RequestBodyFormatException extends ServerException
{
    /**
     * RequestBodyFormatException constructor.
     * @param string|null $title
     * @param string|null $detail
     * @param \Throwable|null $previous
     */
    public function __construct(
        string $title = 'Body format error',
        string $detail = 'Request body format is invalid',
        Throwable $previous = null
    ) {
        parent::__construct($title, $detail, $previous);
        $this->type = 'RequestBodyFormatException';
    }
}