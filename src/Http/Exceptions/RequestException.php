<?php

namespace Eternity\Http\Exceptions;

use Eternity\Exceptions\EternityException;

/**
 * Request exception
 *
 * Thrown if request is not sent
 *
 * Class RequestException
 * @package Eternity\Http\Exceptions
 */
class RequestException extends EternityException
{
    public function __construct(string $title = null, string $detail = null, \Throwable $previous = null)
    {
        parent::__construct($title, $detail, $previous);
        $this->type = 'RequestException';
    }
}
