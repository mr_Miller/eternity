<?php

namespace Eternity\Http\Dto;

use Eternity\Contracts\Arrayable;
use Eternity\Traits\FromArray;

/**
 * Error DTO
 *
 * Dto for error transferring between the layers
 *
 * Class ErrorDto
 * @package Eternity\Http\Dto
 */
class ErrorDto implements Arrayable
{
    use FromArray;

    /**
     * @var int A unique identifier for this particular occurrence of the problem.
     */
    public $id;

    /**
     * @var int An application-specific error code, expressed as a string value.
     */
    public $code;

    /**
     * @var int The HTTP status code applicable to this problem, expressed as a string value
     */
    public $status;

    /**
     * @var string A short, human-readable summary of the problem. It SHOULD NOT change from occurrence to occurrence of the problem, except for purposes of localization.
     */
    public $title;

    /**
     * @var string A human-readable explanation specific to this occurrence of the problem.
     */
    public $type;

    /**
     * @var array Associated resources, which can be dereferenced from the request document.
     */
    public $links;

    /**
     * @var string A human-readable explanation specific to this occurrence of the problem.
     */
    public $detail;

    /**
     * @var array
     */
    public $validation_errors;

    /**
     * @var string The relative path to the relevant attribute within the associated resource(s).
     * Only appropriate for problems that apply to a single resource or type of resource.
     */
    public $path;

    /**
     * Transforms object into array
     *
     * @return array
     */
    public function toArray(): array
    {
        return [
            'id'                => $this->id,
            'code'              => $this->code,
            'status'            => $this->status,
            'title'             => $this->title,
            'type'              => $this->type,
            'links'             => $this->links,
            'detail'            => $this->detail,
            'validation_errors' => $this->validation_errors,
        ];
    }
}