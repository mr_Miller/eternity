# HttpClient
This is component is a part of Eternity framework components.

## Usage
Http client initializing:
```php
use Eternity\Http\GuzzleFactory;

$httpClient = GuzzleFactory::make([
    'headers' => [
        'Accept' => 'application/json',
        'Content-Type' => 'application/json',
    ],
]);
```

Example of usage in code:
```php
use Eternity\Http\GuzzleFactory;

$httpClient = GuzzleFactory::make([
    'headers' => [
        'Accept' => 'application/json',
        'Content-Type' => 'application/json',
    ],
]);

try{
    $response = $httpClient->get('http://animal-id/product/1');
    // In case if need to transmit success data further
    $successResponse = $response->getBody(); // send this data to client directly
} catch (\Eternity\Http\Exceptions\ResponseException $exception) {
    // In case if need to handle external microservice error
    $error = $exception->getResponse()->getError();
    // In case if need to transmit error response further
    $errorResponse = $exception->getResponse()->getBody(); // send error data to client directly
}
```
