<?php

namespace Eternity\Http\Response;

use Eternity\Http\Contracts\ExtendedResponse;
use Eternity\Http\Dto\ErrorDto;
use Psr\Http\Message\ResponseInterface;

/**
 * Response
 *
 * Decorator over classic response. Provides method for easy access and operation of response data
 *
 * Interface Response
 * @package Eternity\Http\Response
 */
class Response implements ExtendedResponse
{
    /**
     * @var ResponseInterface
     */
    private $originalResponse;

    /**
     * @var array|null Decoded json body of response
     */
    private $body;

    /**
     * @var array
     */
    private $headers = [];

    /**
     * @var array
     */
    private $errors = [];

    /**
     * @var array
     */
    private $payload = [];

    /**
     * @var string|null
     */
    private $contentType;

    /**
     * Response constructor.
     * @param \Psr\Http\Message\ResponseInterface $sourceResponse
     */
    public function __construct(ResponseInterface $sourceResponse)
    {
        $this->originalResponse = $sourceResponse;
        $this->initHeaders();
        $this->initBody();
        $this->initErrors();
        $this->initPayload();
    }

    /**
     * Initialize response body
     */
    private function initBody(): void
    {
        if ($this->isApplicationJson()) {
            $this->body = json_decode($this->originalResponse->getBody()->getContents(), true);
        }
    }

    /**
     * Initialize errors
     */
    private function initErrors(): void
    {
        $this->errors = $this->getBodyProperty('errors', []);
    }

    /**
     * Initialize Payload
     */
    private function initPayload(): void
    {
        $this->payload = $this->getBodyProperty('payload', []);
    }

    /**
     * Initialize headers
     */
    private function initHeaders(): void
    {
        $this->headers = $this->originalResponse->getHeaders();
        $this->contentType = $this->originalResponse->getHeaderLine('Content-Type');
    }

    /**
     * Check response type is application/json
     * @return bool
     */
    public function isApplicationJson(): bool
    {
        return strpos($this->getContentType(), 'application/json') !== false;
    }

    /**
     * Init response body
     * @return array|null
     */
    public function getBody(): ?array
    {
        return $this->body;
    }

    /**
     * Get response body property
     * @param string $propertyName
     * @param $default
     * @return mixed|null
     */
    protected function getBodyProperty(string $propertyName, $default = null)
    {
        if ($this->body === null || !isset($this->body[$propertyName])) {
            // if body is null OR property not set
            return $default;
        }

        return $this->body[$propertyName];
    }

    /**
     * Get first response error
     * @return \Eternity\Contracts\Arrayable|\Eternity\Http\Dto\ErrorDto
     */
    public function getError(): ?ErrorDto
    {
        if (isset($this->getErrors()[0])) {
            return ErrorDto::fromArray($this->getErrors()[0]);
        }

        return null;
    }

    /**
     * Return the original response class
     * @return mixed
     */
    public function getOriginalResponse(): ResponseInterface
    {
        return $this->originalResponse;
    }

    /**
     * Return headers
     * @return array
     */
    public function getHeaders(): array
    {
        return $this->headers;
    }

    /**
     * Check if response is error
     * @return bool
     */
    public function isError(): bool
    {
        return $this->getHttpStatus() >= 400;
    }

    /**
     * Check if response has forbidden error
     * @return bool
     */
    public function isForbidden(): bool
    {
        return $this->getHttpStatus() === 403;
    }

    /**
     * Check if response is success
     * @return bool
     */
    public function isSuccess(): bool
    {
        return $this->getHttpStatus() < 400;
    }

    /**
     * Get whole payload
     * @return array
     */
    public function getPayloadItems(): array
    {
        return $this->payload;
    }

    /**
     * Get single entity from payload
     *
     * Use for single entity in payload
     * @return array
     */
    public function getPayloadItem(): array
    {
        if (!isset($this->payload[0])) {
            return [];
        }

        return $this->payload[0];
    }

    /**
     * Get links
     * @return array
     */
    public function getLinks(): array
    {
        return $this->getBodyProperty('links', []);
    }

    /**
     * Get metadata
     * @return array
     */
    public function getMetadata(): array
    {
        return $this->getBodyProperty('metadata', []);
    }

    /**
     * Get errors from response
     * @return array
     */
    public function getErrors(): array
    {
        return $this->errors;
    }

    /**
     * Get original error trace
     *
     * Use when debugging
     *
     * @return mixed
     */
    public function getTrace(): ?array
    {
        return $this->getBodyProperty('trace', null);
    }

    /**
     * Return http status code
     */
    public function getHttpStatus(): int
    {
        return $this->originalResponse->getStatusCode();
    }

    /**
     * Returns content type
     * @return string|null
     */
    public function getContentType(): ?string
    {
        return $this->contentType;
    }
}