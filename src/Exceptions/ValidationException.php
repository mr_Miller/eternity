<?php

namespace Eternity\Exceptions;

/**
 * Exception can be thrown if input data is invalid
 *
 * Class ValidationException
 * @package App\Application\Exceptions\Eternity\Exceptions
 */
class ValidationException extends UserException
{
    /**
     * ValidationException constructor.
     * @param array|null $validation_errors
     * @param string $title
     * @param string|null $detail
     * @param \Throwable|null $previous
     */
    public function __construct(
        array $validation_errors = null,
        string $title = 'Validation error',
        string $detail = null,
        \Throwable $previous = null
    ) {
        parent::__construct($title, $detail, $previous);
        $this->status = 422;
        $this->type = 'ValidationException';
        $this->validation_errors = $validation_errors;
    }
}
