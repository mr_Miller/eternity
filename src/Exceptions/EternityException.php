<?php

namespace Eternity\Exceptions;

/**
 * Eternity base exception class
 *
 * Responsible for building proper errors structure
 * --- Use: ---
 * Extend this class and set default properties for current exception type in constructor
 *
 * Class EternityException
 * @package App\Application\Exceptions\Eternity\Exceptions
 */
class EternityException extends \Exception
{
    /**
     * @var int A unique identifier for this particular occurrence of the problem.
     */
    protected $id;

    /**
     * @var int The HTTP status code applicable to this problem, expressed as a string value
     */
    protected $status;

    /**
     * @var string|null A short, human-readable summary of the problem. It SHOULD NOT change from occurrence to occurrence of the problem, except for purposes of localization.
     */
    protected $title;

    /**
     * @var string A human-readable explanation specific to this occurrence of the problem.
     */
    protected $type;

    /**
     * @var array Associated resources, which can be dereferenced from the request document.
     */
    protected $links;

    /**
     * @var string|null A human-readable explanation specific to this occurrence of the problem.
     */
    protected $detail;

    /**
     * @var array
     */
    protected $validation_errors;

    /**
     * @var string The relative path to the relevant attribute within the associated resource(s).
     * Only appropriate for problems that apply to a single resource or type of resource.
     */
    protected $path;

    /**
     * @var string
     */
    protected $level = ExceptionLevel::CRITICAL;

    /**
     * EternityException constructor.
     * @param string|null $title
     * @param string|null $detail
     * @param \Throwable|null $previous
     */
    public function __construct(string $title = null, string $detail = null, \Throwable $previous = null)
    {
        // todo title + detail for supporting exceptions in old animal ID
        parent::__construct($title . ($detail ? '. ' . $detail : ''), 0, $previous);
        $this->title = $title;
        $this->detail = $detail;
        $this->status = 500;
        $this->type = 'EternityException';
    }

    /**
     * @return string
     */
    public function getLevel(): string
    {
        return $this->level;
    }

    /**
     * @return bool
     */
    public function isCritical(): bool
    {
        return $this->getLevel() === ExceptionLevel::CRITICAL;
    }

    /**
     * @return bool
     */
    public function isLogging(): bool
    {
        return $this->getLevel() === ExceptionLevel::LOGGING;
    }

    /**
     * Code setter
     * @param int $code
     */
    public function setCode(int $code): void
    {
        $this->code = $code;
    }

    /**
     * Http status setter
     * @param int $status
     */
    public function setStatus(int $status): void
    {
        $this->status = $status;
    }

    /**
     * Returns validation errors
     * @return array
     */
    public function getValidationErrors(): array
    {
        return $this->validation_errors;
    }

    /**
     * Returns id
     * @return int
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getType(): string
    {
        return $this->type;
    }

    /**
     * Returns status
     * @return int
     */
    public function getStatus(): int
    {
        return $this->status;
    }

    /**
     * Returns title
     * @return string
     */
    public function getTitle(): ?string
    {
        return $this->title;
    }

    /**
     * Returns detail
     * @return string
     */
    public function getDetail(): ?string
    {
        return $this->detail;
    }

    /**
     * Returns links
     * @return array
     */
    public function getLinks(): array
    {
        return $this->links;
    }

    /**
     * Returns path
     * @return string
     */
    public function getPath(): ?string
    {
        return $this->path;
    }

    /**
     * Exception serialization
     * @return array
     */
    final public function serialize(): array
    {
        return [
            'id'                => $this->id,
            'code'              => $this->code,
            'status'            => $this->status,
            'title'             => $this->title,
            'type'              => $this->type,
            'links'             => $this->links,
            'detail'            => $this->detail,
            'validation_errors' => $this->validation_errors,
        ];
    }
}
