<?php

namespace Eternity\Exceptions;

/**
 * Exception must be thrown if error is occurred because of User
 *
 * Class UserException
 * @package App\Application\Exceptions\Eternity\Exceptions
 */
class UserException extends EternityException
{
    /**
     * UserException constructor.
     * @param string|null $title
     * @param string|null $detail
     * @param \Throwable|null $previous
     */
    public function __construct(string $title = null, string $detail = null, \Throwable $previous = null)
    {
        parent::__construct($title, $detail, $previous);
        $this->type = 'UserException';
        $this->status = 400;
    }
}
