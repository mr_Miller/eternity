<?php

namespace Eternity\Exceptions;

use Throwable;

/**
 * Exception must be thrown if error is occurred because of Server
 *
 * Class ServerException
 * @package App\Application\Exceptions\Eternity\Exceptions\Server
 */
class ServerException extends EternityException
{
    /**
     * ServerBaseException constructor.
     * @param string|null $title
     * @param string|null $detail
     * @param \Throwable|null $previous
     */
    public function __construct(string $title = null, string $detail = null, Throwable $previous = null)
    {
        parent::__construct($title, $detail, $previous);
        $this->type = 'ServiceException';
        $this->status = 500;
    }
}
