<?php

namespace Eternity\Exceptions;

/**
 * Forbidden exception
 *
 * Throws if request is not authorized to use the resource.
 * For instance not enough rights
 *
 * Class ForbiddenException
 * @package Eternity\Exceptions
 */
class ForbiddenException extends EternityException
{
    /**
     * ForbiddenException constructor.
     * @param string|null $title
     * @param string|null $detail
     * @param \Throwable|null $previous
     */
    public function __construct(string $title = 'Authorization error', string $detail = 'Access forbidden', \Throwable $previous = null)
    {
        parent::__construct($title, $detail, $previous);
        $this->status = 403;
        $this->type = 'ForbiddenException';
    }
}