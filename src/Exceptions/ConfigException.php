<?php

namespace Eternity\Exceptions;

use Throwable;

/**
 * Class ConfigException
 * @package Eternity\Exceptions
 */
class ConfigException extends ServerException
{
    /**
     * ConfigException constructor.
     * @param string|null $title
     * @param string|null $detail
     * @param \Throwable|null $previous
     */
    public function __construct(string $title = null, string $detail = null, Throwable $previous = null)
    {
        parent::__construct($title, $detail, $previous);
        $this->type = 'ConfigException';
    }
}