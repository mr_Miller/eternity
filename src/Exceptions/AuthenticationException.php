<?php

namespace Eternity\Exceptions;

/**
 * Authentication exception
 *
 * Throws if authentication is failed
 * For instance if failed to authenticate the user
 *
 * Class AuthenticationException
 * @package Eternity\Exceptions
 */
class AuthenticationException extends EternityException
{
    /**
     * AuthenticationException constructor.
     * @param string|null $title
     * @param string|null $detail
     * @param \Throwable|null $previous
     */
    public function __construct(string $title = 'Authentication error', string $detail = null, \Throwable $previous = null)
    {
        parent::__construct($title, $detail, $previous);
        $this->type = 'AuthenticationException';
        $this->status = 401;
    }
}