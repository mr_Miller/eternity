## EternityException
Class reference
```
Eternity\Exceptions\EternityException
```
For purpose of maintaining to single error response structure and exception error documentation base exception class `ExternityException` created.

Learn more about [error structure](https://animal-id.atlassian.net/wiki/spaces/A/pages/3538945/API+Reference+v1.0#Errors).

These exceptions are parsed properly by `Eternity\LaravelBase\Exceptions\Handler` class.

#### Usage
Inherit `EternityException` and set needed properties in constructor

```php
use Eternity\Exceptions\EternityException;

class NotFoundException extends EternityException
{
    public function __construct(string $title = null, string $detail = null, Throwable $previous = null)
    {
        parent::__construct($title, $detail, $previous);
        $this->setStatus = 404;
        $this->type = 'NotFoundException';
    }
}
```

#### Example in code

Example shows Eternity exception throwing flow.

```php
use Eternity\Exceptions\ValidationException;
use Illuminate\Foundation\Http\Request;
use Illuminate\Support\Facades\Validator;

class Controller 
{
    public function create(Request $request)
    {
        $validator = Validator::make($request->all());
        if ($validator->fails()) {
            throw new ValidationException($validator->getErrors()); // usage
        }
        $model->fill($request->all);
        $model->saveOrFail();
        return new JsonResource($model->toArray());
    }
}
```

ValidationException accepts errors in constructor.
