<?php

namespace Eternity\Exceptions\Entity;

use Eternity\Exceptions\ServerException;
use Throwable;

/**
 * In case if entity not saved
 *
 * For:
 *   - create;
 *   - update;
 *   - delete.
 *
 * Class EntityNotSavedException
 * @package App\Infrastructure\Exceptions
 */
class EntityNotSavedException extends ServerException
{
    /**
     * EntityNotSavedException constructor.
     * @param string|null $title
     * @param string|null $detail
     * @param \Throwable|null $previous
     */
    public function __construct(string $title = null, string $detail = null, Throwable $previous = null)
    {
        parent::__construct($title, $detail, $previous);
        $this->type = 'EntityNotSaved';
    }
}
