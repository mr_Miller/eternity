<?php

namespace Eternity\Exceptions\Entity;

use Eternity\Exceptions\UserException;
use Throwable;

/**
 * In case if entity not found
 * Class EntityNotFoundException
 * @package App\Infrastructure\Exceptions
 */
class EntityNotFoundException extends UserException
{
    /**
     * EntityNotFoundException constructor.
     * @param string|null $title
     * @param string|null $detail
     * @param \Throwable|null $previous
     */
    public function __construct(string $title = null, string $detail = null, Throwable $previous = null)
    {
        parent::__construct($title, $detail, $previous);
        $this->status = 404;
        $this->type = 'EntityNotFound';
    }
}
