<?php

namespace Eternity\Exceptions;

/**
 * Describes Eternity error codes
 *
 * Extend this class
 *
 * Class ExceptionCodes
 * @package Eternity\Exceptions
 */
abstract class ErrorCodes
{
    // Internal server errors
    // Leading digit - 1
    // Common errors (10000 - 10250):
    public const LOCK_ERROR = 10032;

    // HTTP errors (10251 - 10300):
    public const HTTP_BAD_REQUEST = 10251; // 400
    public const HTTP_UNAUTHORIZED = 10252; // 401
    public const HTTP_NOT_FOUND = 10253; // 404
    public const HTTP_NOT_ALLOWED = 10254; // 403
    public const HTTP_CONFLICT = 10255; // 409
    public const HTTP_UNPROCESSABLE_ENTITY = 10256; // 422
    public const HTTP_INTERNAL_SERVER_ERROR = 10257; // 500
    public const HTTP_NOT_IMPLEMENTED = 10258; // 4**
    public const HTTP_SERVICE_UNAVAILABLE = 10259; // 503

    // Database errors (10301 - 10400):
    public const DB_QUERY_ERROR = 10301;
    public const DB_WRITE_ERROR = 10302;
    public const DB_CONNECTION_ERROR = 10303;
    public const DB_TRIGGER_ERROR = 10304;
    public const DB_CONSTRAINT_ERROR = 10305;

    // Entity errors (10451 - 10500):
    public const ENTITY_HAS_DEPENDENCIES = 10451;
    public const ENTITY_UPDATE_FAILED = 10452;
    public const ENTITY_CREATION_FAILED = 10453;
    public const ENTITY_DELETION_FAILED = 10454;
    public const ENTITY_OBTAINING_ID_FAILED = 10455;
    public const ENTITY_IS_IN_INVALID_STATE = 10456;
    public const ENTITY_CANNOT_BE_SAVED = 10457;

    // Validation errors
    // Leading digit 2
    public const VALIDATION_FAILED = 20000;
    public const EMPTY_REQUEST = 20001;
    public const REQUIRED = 20002;

    // Informal message codes
    // Leading digit 4
    public const REQUEST = 40001;
    public const REQUEST_HEADERS = 40002;
    public const REQUEST_BODY = 40003;
    public const TRACE_ID_GENERATION_OVERRIDE = 40004;
    public const TRACE_ID_INIT_OVERRIDE = 40005;
    public const TRACE_ID_HEADER_MISSING = 40006;
    public const TRACE_ID_INIT_ERROR = 40007;
    public const LOGGER_CONFIG_HANDLERS_EMPTY = 40008;

    // Other non-categorized
    public const METADATA_VALIDATION_ERROR = 50001;
    public const THUMBNAIL_GENERATOR_ERROR = 50002;
    public const FILE_UPLOAD_ERROR = 50003;
    public const FILE_CONTENT_ERROR = 50004;
    public const FILE_READ_ERROR = 50005;
    public const FILE_NOT_DELETED_ERROR = 50006;
    public const DIRECTORY_NOT_CREATED_ERROR = 50007;
    public const DIRECTORY_NOT_DELETED_ERROR = 50008;
    public const LANGUAGE_NOT_EXISTS_ERROR = 50009;
    public const REGION_ERROR = 50010;
    public const REGION_NOT_SET_ERROR = 50011;
    public const REGION_NOT_EXISTS_ERROR = 50012;
    public const REGION_LANGUAGES_NOT_SET_ERROR = 50013;
    public const REGION_DEFAULT_LANGUAGE_NOT_SET_ERROR = 50014;
    public const REGION_COUNTRIES_NOT_SET_ERROR = 50015;
    public const COUNTRY_PHONE_IS_NOT_SET_ERROR = 50016;
}