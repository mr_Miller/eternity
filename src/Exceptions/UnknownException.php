<?php

namespace Eternity\Exceptions;

/**
 * Class UnknownException
 * @package Eternity\Exceptions
 */
class UnknownException extends EternityException
{
    /**
     * UnknownException constructor.
     * @param string|null $title
     * @param string|null $detail
     * @param \Throwable|null $previous
     */
    public function __construct(string $title = null, string $detail = null, \Throwable $previous = null)
    {
        parent::__construct($title, $detail, $previous);
        $this->type = 'UnknownException';
        $this->status = 500;
    }
}
