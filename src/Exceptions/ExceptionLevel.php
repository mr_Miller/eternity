<?php

namespace Eternity\Exceptions;

use Monolog\Logger;

/**
 * Class ExceptionLevel
 * @package Eternity\Exceptions
 */
class ExceptionLevel
{
    /**
     * Logging
     */
    public const LOGGING = 'logging';

    /**
     * Warning
     */
    public const WARNING = 'warning';

    /**
     * Critical
     */
    public const CRITICAL = 'critical';

    /**
     * Eternity logger
     */
    private const LOG_LEVEL_MAP = [
        self::LOGGING  => Logger::INFO,
        self::WARNING  => Logger::WARNING,
        self::CRITICAL => Logger::ERROR,
    ];

    /**
     * @param string $exceptionLevel
     * @return int
     * @throws \Eternity\Exceptions\EternityException
     */
    public static function logLevel(string $exceptionLevel): int
    {
        if (!isset(self::LOG_LEVEL_MAP[$exceptionLevel])) {
            throw new EternityException("Level $exceptionLevel is not set");
        }

        return self::LOG_LEVEL_MAP[$exceptionLevel];
    }
}