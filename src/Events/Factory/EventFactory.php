<?php

namespace Eternity\Events\Factory;

use Eternity\Events\Contracts\Event;
use Eternity\Events\Events;

/**
 * Class EventFactory
 * @package Eternity\Events\Factory
 */
abstract class EventFactory
{
    /**
     * Creates event object
     * @param string $type
     * @param string $serialized
     * @return \Eternity\Events\Contracts\Event
     * @throws \Eternity\Events\Exceptions\EventException
     */
    public static function create(string $type, string $serialized): Event
    {
        $class = Events::getClass($type);
        /** @var \Eternity\Events\Contracts\Event $event */
        $event = new $class();
        $event->unserialize($serialized);

        return $event;
    }
}