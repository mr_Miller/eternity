<?php

namespace Eternity\Events;

use Eternity\Events\Exceptions\EventException;
use Eternity\Events\Microservices\AnimalId\AddedMicrochipToPet;
use Eternity\Events\Microservices\AnimalId\FirstPassportActivated;
use Eternity\Events\Microservices\AnimalId\PetDeleted;
use Eternity\Events\Microservices\AnimalId\PetDied;
use Eternity\Events\Microservices\AnimalId\PetRegistered;
use Eternity\Events\Microservices\AnimalId\ReferralPromoCreated;
use Eternity\Events\Microservices\AnimalId\ReferralPromoUsed;
use Eternity\Events\Microservices\Gateway\UserDeleted;
use Eternity\Events\Microservices\Gateway\UserRegistered;
use Eternity\Events\Microservices\Notification\Push;
use Eternity\Events\Microservices\Notification\PushMultiple;
use Eternity\Events\Microservices\PushNotification;
use Eternity\Events\Microservices\Referral\PassportActivationCompleted;
use Eternity\Events\Microservices\Referral\PromoUsageCompleted;
use Eternity\Events\Microservices\Referral\ReferralCreated;
use Eternity\Events\Microservices\Subscription\SubscribedToMembership;
use Eternity\Events\Microservices\Subscription\SubscriptionStatusChanged;
use Eternity\Events\Microservices\Subscription\UnsubscribedToMembership;
use Eternity\Events\Microservices\TestEvent;

/**
 * Class Map
 * @package Eternity\Events\Factory
 */
abstract class Events
{
    /* Animal ID */
    public const AID_PET_REGISTERED = 'animal-id-pet-registered';
    public const AID_ADDED_MICROCHIP_TO_PET = 'animal-id-pet-chipped';
    public const AID_FIRST_PASSPORT_ACTIVATED = 'animal-id-first-passport-activated';
    public const AID_REFERRAL_PROMO_CREATED = 'animal-id-referral-promo-created';
    public const AID_REFERRAL_PROMO_USED = 'animal-id-referral-promo-used';
    public const AID_PET_DELETED = 'animal-id-pet-deleted';
    public const AID_PET_DIED = 'animal-id-pet-died';

    /* Marketplace */

    /* Gateway */
    public const GW_USER_REGISTERED = 'gateway-user-registered';
    public const GW_USER_DELETED = 'gateway-user-deleted';

    /* Referral */
    public const RF_REFERRAL_CREATED = 'referral-referral-created';
    public const RF_PROMO_USAGE_COMPLETED = 'referral-promo-usage-completed';
    public const RF_PASSPORT_ACTIVATION_COMPLETED = 'referral-passport-activation-completed';

    /* Subscription */
    public const SC_SUBSCRIPTION_STATUS_CHANGED = 'subscription-subscription-status-changed';
    public const SC_SUBSCRIPTION_MEMBERSHIP_PURCHASED = 'subscription-subscription-purchased';
    public const SC_SUBSCRIPTION_MEMBERSHIP_EXPIRED = 'subscription-subscription-expired';

    /*
     * Special
     *
     * Can be thrown by any microservice
    */
    public const TEST = 'special-test';
    public const PUSH_NOTIFICATION = 'special-push-notification';
    public const PUSH_ID_NOTIFICATION = 'special-push-id-notification'; // Sends notification by ID from Notification MS
    public const MULTIPLE_PUSH_ID_NOTIFICATION = 'special-multiple-push-id-notification'; // Send multiple push notifications by ID from Notification MS

    /**
     * Specifies Event Class for event type
     */
    public const EVENT_TYPE_MAP = [
        /* Animal ID */
        self::AID_FIRST_PASSPORT_ACTIVATED         => FirstPassportActivated::class,
        self::AID_REFERRAL_PROMO_CREATED           => ReferralPromoCreated::class,
        self::AID_REFERRAL_PROMO_USED              => ReferralPromoUsed::class,
        self::AID_PET_DELETED                      => PetDeleted::class,
        self::AID_PET_DIED                         => PetDied::class,
        self::AID_PET_REGISTERED                   => PetRegistered::class,
        self::AID_ADDED_MICROCHIP_TO_PET           => AddedMicrochipToPet::class,

        /* Marketplace */

        /* Gateway */
        self::GW_USER_REGISTERED                   => UserRegistered::class,
        self::GW_USER_DELETED                      => UserDeleted::class,

        /* Referral */
        self::RF_REFERRAL_CREATED                  => ReferralCreated::class,
        self::RF_PROMO_USAGE_COMPLETED             => PromoUsageCompleted::class,
        self::RF_PASSPORT_ACTIVATION_COMPLETED     => PassportActivationCompleted::class,

        /* Subscription */
        self::SC_SUBSCRIPTION_STATUS_CHANGED       => SubscriptionStatusChanged::class,
        self::SC_SUBSCRIPTION_MEMBERSHIP_PURCHASED => SubscribedToMembership::class,
        self::SC_SUBSCRIPTION_MEMBERSHIP_EXPIRED   => UnsubscribedToMembership::class,

        /* Special */
        self::TEST                                 => TestEvent::class, // This class needed for testing
        self::PUSH_NOTIFICATION                    => PushNotification::class,
        self::PUSH_ID_NOTIFICATION                 => Push::class,
        self::MULTIPLE_PUSH_ID_NOTIFICATION        => PushMultiple::class,
    ];

    /**
     * Returns Event Class by Type
     * @param string $type
     * @return string
     * @throws \Eternity\Events\Exceptions\EventException
     */
    public static function getClass(string $type): string
    {
        if (!isset(self::EVENT_TYPE_MAP[$type])) {
            throw new EventException('Event error', "Event class in not defined for type '$type'");
        }

        return self::EVENT_TYPE_MAP[$type];
    }

    /**
     * Returns Event type by Class
     * @param string $class
     * @return string
     * @throws \Eternity\Events\Exceptions\EventException
     */
    public static function getType(string $class): string
    {
        $map = array_flip(self::EVENT_TYPE_MAP);
        if (!isset($map[$class])) {
            throw new EventException('Event error', "Event type for class '$class' is not defined");
        }

        return $map[$class];
    }
}