<?php

namespace Eternity\Events;

use Eternity\Events\Exceptions\EventException;
use Eternity\Events\Microservices\AnimalId\AddedMicrochipToPet;
use Eternity\Events\Microservices\AnimalId\FirstPassportActivated;
use Eternity\Events\Microservices\AnimalId\PetDeleted;
use Eternity\Events\Microservices\AnimalId\PetDied;
use Eternity\Events\Microservices\AnimalId\PetRegistered;
use Eternity\Events\Microservices\AnimalId\ReferralPromoCreated;
use Eternity\Events\Microservices\AnimalId\ReferralPromoUsed;
use Eternity\Events\Microservices\Gateway\UserDeleted;
use Eternity\Events\Microservices\Gateway\UserRegistered;
use Eternity\Events\Microservices\Notification\Push;
use Eternity\Events\Microservices\Notification\PushMultiple;
use Eternity\Events\Microservices\PushNotification;
use Eternity\Events\Microservices\Referral\PassportActivationCompleted;
use Eternity\Events\Microservices\Referral\PromoUsageCompleted;
use Eternity\Events\Microservices\Referral\ReferralCreated;
use Eternity\Events\Microservices\Subscription\SubscribedToMembership;
use Eternity\Events\Microservices\Subscription\SubscriptionStatusChanged;
use Eternity\Events\Microservices\Subscription\UnsubscribedToMembership;
use Eternity\Microservices;

/**
 * Declares Event consumer microservices
 *
 * Class Subscribers
 * @package App\Infrastructure\Services\Bus
 */
class Consumer
{
    private const EVENT_CONSUMERS = [
        /* Animal ID */
        PetRegistered::class               => [
            Microservices::PET,
        ],
        AddedMicrochipToPet::class         => [
            Microservices::PET,
        ],
        FirstPassportActivated::class      => [
            Microservices::REFERRAL,
        ],
        ReferralPromoCreated::class        => [
            Microservices::REFERRAL,
        ],
        ReferralPromoUsed::class           => [
            Microservices::REFERRAL,
        ],
        PetDeleted::class                  => [
            Microservices::CALENDAR,
            Microservices::PET,
        ],
        PetDied::class                     => [
            Microservices::CALENDAR,
            Microservices::PET,
        ],
        /* Gateway */
        UserRegistered::class              => [
            Microservices::REFERRAL,
            Microservices::NOTIFICATION,
        ],
        UserDeleted::class                 => [
            Microservices::SUBSCRIPTION,
            Microservices::PET,
        ],
        /* Referral */
        ReferralCreated::class             => [
            Microservices::GATEWAY, // NOTE: This events must be consumed by Animal ID via Gateway HTTP
        ],
        PromoUsageCompleted::class         => [
            Microservices::MARKETPLACE,
        ],
        PassportActivationCompleted::class => [
            Microservices::MARKETPLACE,
        ],
        /* Subscription */
        SubscriptionStatusChanged::class   => [
            Microservices::GATEWAY,
        ],
        SubscribedToMembership::class      => [
            Microservices::PET,
        ],
        UnsubscribedToMembership::class    => [
            Microservices::PET,
        ],
        /* Special */
        PushNotification::class            => [
            Microservices::NOTIFICATION,
        ],
        Push::class                        => [
            Microservices::NOTIFICATION,
        ],
        PushMultiple::class                => [
            Microservices::NOTIFICATION,
        ],
    ];

    /**
     * @var array
     */
    private $eventConsumers;

    /**
     * Consumers constructor.
     * @param array|null $eventConsumers
     */
    public function __construct(array $eventConsumers = null)
    {
        if (!$eventConsumers) {
            $eventConsumers = self::EVENT_CONSUMERS;
        }

        $this->eventConsumers = $eventConsumers;
    }

    /**
     * Returns event consumers
     *
     * @param string $type
     * @return array
     * @throws \Eternity\Events\Exceptions\EventException
     */
    public function get(string $type): array
    {
        if (!isset($this->eventConsumers[$type])) {
            throw new EventException("Consumers are not set for event '$type'");
        }

        return $this->eventConsumers[$type];
    }
}
