<?php

namespace Eternity\Events\Contracts;

/**
 * Event interface
 *
 * This interface is for events that must be delivered to center Event Bus (Event-Driven Architecture)
 * This interface used in Event-Driven Architecture
 *
 * Interface Event
 * @package Eternity\Events
 */
interface Event extends \Serializable
{
}