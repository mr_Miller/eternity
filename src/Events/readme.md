### Usage
How to create new events for Event-Bus:
1. Create Event Class in `src/Events/Services` in the directory same as microservice name,
class must implement `Eternity\Events\Contracts\Event` interface.
2. Define event type and class reference (created in p. 1) in `Eternity\Events\Events`.
3. Specify event consumer microservices in `Eternity\Events\Consumers`

> Attention !!!
>
> When create events hardly recommended to use **PROTECTED** or **PUBLIC** properties for correct serialization. 
> Private properties will not be serialized.

Use `camelCase` for event property naming - it's a good practice.