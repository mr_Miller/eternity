<?php

namespace Eternity\Events\Microservices\Subscription;

use Eternity\Events\Microservices\AbstractEvent;

/**
 * Event that indicated about Subscription Status change
 *
 * Class SubscriptionStatusChanged
 * @package Eternity\Events\Microservices\Subscription
 */
class SubscriptionStatusChanged extends AbstractEvent
{
    /**
     * @var int $uid Subscription owner User ID
     */
    protected $uid;

    /**
     * @var int $id Subscription ID
     */
    protected $id;

    /**
     * @var int $status New subscription status
     */
    protected $status;

    /**
     * @param int $id
     * @param int $uid
     * @param int $status
     * @return \Eternity\Events\Microservices\Subscription\SubscriptionStatusChanged
     */
    public static function create(int $id, int $uid, int $status): self
    {
        $event = new static;
        $event->fromArray([
            'id'     => $id,
            'uid'    => $uid,
            'status' => $status,
        ]);

        return $event;
    }

    /**
     * Transforms object into array
     *
     * @return array
     */
    public function toArray(): array
    {
        return [
            'id'     => $this->id,
            'uid'    => $this->uid,
            'status' => $this->status,
        ];
    }

    /**
     * @return int
     */
    public function uid(): int
    {
        return $this->uid;
    }

    /**
     * @return int
     */
    public function id(): int
    {
        return $this->id;
    }

    /**
     * @return int
     */
    public function status(): int
    {
        return $this->status;
    }

}