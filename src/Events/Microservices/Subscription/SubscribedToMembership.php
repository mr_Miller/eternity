<?php

namespace Eternity\Events\Microservices\Subscription;

use Eternity\Events\Microservices\AbstractEvent;

/**
 * Class SubscribedToMembership
 * @package Eternity\Events\Microservices\Subscription
 */
class SubscribedToMembership extends AbstractEvent
{
    protected $uid;
    protected $sub_id;
    protected $status;
    protected $purchased_at;
    protected $expires_at;

    /**
     * Creates a new instance of the class using the given parameters.
     *
     * @param int $uid The user ID of the event.
     * @param int $subId The subscription ID of the event.
     * @param int $status The status of the event.
     * @param string $purchased_at The purchased_at date of the event.
     * @param string $expires_at The expires_at date of the event.
     * @return self A new instance of the class.
     */
    public static function create(int $uid, int $subId, int $status, string $purchased_at, string $expires_at): self
    {
        $event = new static();
        $event->fromArray([
            'uid'          => $uid,
            'sub_id'       => $subId,
            'status'       => $status,
            'purchased_at' => $purchased_at,
            'expires_at'   => $expires_at,
        ]);

        return $event;
    }

    /**
     * Converts the object to an array representation.
     *
     * @return array The array representation of the object.
     */
    public function toArray(): array
    {
        return [
            'uid'          => $this->uid,
            'sub_id'       => $this->sub_id,
            'status'       => $this->status,
            'purchased_at' => $this->purchased_at,
            'expires_at'   => $this->expires_at
        ];
    }

    /**
     * @return int
     */
    public function uid(): int
    {
        return $this->uid;
    }

    /**
     * @return int
     */
    public function subId(): int
    {
        return $this->sub_id;
    }

    /**
     * @return int
     */
    public function status(): int
    {
        return $this->status;
    }

    /**
     * @return string
     */
    public function purchasedAt(): string
    {
        return $this->purchased_at;
    }

    /**
     * @return string
     */
    public function expiresAt(): string
    {
        return $this->expires_at;
    }
}