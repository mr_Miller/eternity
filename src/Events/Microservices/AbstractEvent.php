<?php

namespace Eternity\Events\Microservices;

use Eternity\Contracts\Arrayable;
use Eternity\Events\Contracts\Event;

/**
 * Class AbstractEvent
 * @package Eternity\Events\Microservices
 */
abstract class AbstractEvent implements Event, Arrayable
{
    /**
     * Creates event from array
     * @param array $data
     */
    public function fromArray(array $data): void
    {
        foreach ($data as $property => $propertyValue) {
            if (property_exists($this, $property)) {
                $this->$property = $propertyValue;
            }
        }
    }

    /**
     * String representation of object
     * @link https://php.net/manual/en/serializable.serialize.php
     * @return string the string representation of the object or null
     */
    public function serialize(): string
    {
        return json_encode($this->toArray());
    }

    /**
     * Constructs the object
     * @link https://php.net/manual/en/serializable.unserialize.php
     * @param string $serialized <p>
     * The string representation of the object.
     * </p>
     * @return void
     */
    public function unserialize($serialized): void
    {
        $this->fromArray(json_decode($serialized, true));
    }
}