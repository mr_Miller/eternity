<?php

namespace Eternity\Events\Microservices\Referral;

use Eternity\Events\Microservices\AbstractEvent;

/**
 * Promo usage stage is completed on Referral
 *
 * Class PromoUsageStageCompleted
 * @package Eternity\Events\Microservices\Referral
 */
class PromoUsageCompleted extends AbstractEvent
{
    /**
     * @var int $uid
     */
    protected $uid;

    /**
     * @var string
     */
    protected $language;

    /**
     * @var float The amount of bonuses that referent get for this activation
     */
    protected $bonus;

    /**
     * @var string
     */
    protected $promoCode;

    /**
     * @var int
     */
    protected $referralPromoCodeId;

    /**
     * @var int
     */
    protected $stage;

    /**
     * @var int
     */
    protected $referentUid;

    /**
     * ReferralPromoUsed constructor.
     * @param int $uid
     * @param int $referentUid
     * @param string $language
     * @param string $promoCode
     * @param int $referralPromoCodeId
     * @param int $stage
     * @param float $bonus
     * @return static
     */
    public static function create(
        int $uid,
        int $referentUid,
        string $language,
        string $promoCode,
        int $referralPromoCodeId,
        int $stage,
        float $bonus
    ): self {
        $event = new static();
        $event->fromArray([
            'uid'                 => $uid,
            'referentUid'         => $referentUid,
            'language'            => $language,
            'stage'               => $stage,
            'bonus'               => $bonus,
            'promoCode'           => $promoCode,
            'referralPromoCodeId' => $referralPromoCodeId
        ]);

        return $event;
    }

    /**
     * Transforms object into array
     *
     * @return array
     */
    public function toArray(): array
    {
        return [
            'uid'                 => $this->uid,
            'referentUid'         => $this->referentUid,
            'language'            => $this->language,
            'stage'               => $this->stage,
            'bonus'               => $this->bonus,
            'promoCode'           => $this->promoCode,
            'referralPromoCodeId' => $this->referralPromoCodeId
        ];
    }

    /**
     * @return int
     */
    public function getReferentUid(): int
    {
        return $this->referentUid;
    }

    /**
     * @return int
     */
    public function getReferralPromoCodeId(): int
    {
        return $this->referralPromoCodeId;
    }

    /**
     * @return string
     */
    public function getPromoCode(): string
    {
        return $this->promoCode;
    }

    /**
     * @return int
     */
    public function getStage(): int
    {
        return $this->stage;
    }

    /**
     * @return float
     */
    public function getBonus(): float
    {
        return $this->bonus;
    }

    /**
     * @return int
     */
    public function getUid(): int
    {
        return $this->uid;
    }

    /**
     * @return string
     */
    public function getLanguage(): string
    {
        return $this->language;
    }
}