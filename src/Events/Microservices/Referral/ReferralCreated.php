<?php

namespace Eternity\Events\Microservices\Referral;

use Eternity\Events\Microservices\AbstractEvent;

/**
 * Class ReferralCreated
 * @package Eternity\Events\Microservices\Referral
 */
class ReferralCreated extends AbstractEvent
{
    /**
     * @var int $uid Referral UID - user which came by Referent HASH
     */
    protected $uid;

    /**
     * @var int $uid
     */
    protected $referralId;

    /**
     * @var string
     */
    protected $language;

    /**
     * ReferralCreated constructor.
     * @param int $uid
     * @param int $referralId
     * @param string $language
     * @return static
     */
    public static function create(int $uid, int $referralId, string $language): self
    {
        $event = new static();
        $event->fromArray([
            'uid'        => $uid,
            'referralId' => $referralId,
            'language'   => $language
        ]);

        return $event;
    }

    /**
     * Transforms object into array
     *
     * @return array
     */
    public function toArray(): array
    {
        return [
            'uid'        => $this->uid,
            'referralId' => $this->referralId,
            'language'   => $this->language
        ];
    }

    /**
     * @return int
     */
    public function getUid(): int
    {
        return $this->uid;
    }

    /**
     * @return int
     */
    public function getReferralId(): int
    {
        return $this->referralId;
    }

    /**
     * @return string
     */
    public function getLanguage(): string
    {
        return $this->language;
    }
}
