<?php

namespace Eternity\Events\Microservices;

/**
 * !!! Event need for testing !!!
 * Class TestEvent
 * @package Eternity\Events\Microservices
 */
class TestEvent extends AbstractEvent
{
    /**
     * @var int
     */
    protected $uid;

    /**
     * @var string
     */
    protected $language;

    /**
     * TestEvent constructor.
     * @param int $uid
     * @param string $language
     * @return static
     */
    public static function create(int $uid, string $language): self
    {
        $event = new static();
        $event->fromArray([
            'uid'      => $uid,
            'language' => $language,
        ]);

        return $event;
    }

    /**
     * Transforms object into array
     *
     * @return array
     */
    public function toArray(): array
    {
        return [
            'uid'      => $this->uid,
            'language' => $this->language,
        ];
    }

    /**
     * @return int
     */
    public function getUid(): int
    {
        return $this->uid;
    }

    /**
     * @return string
     */
    public function getLanguage(): string
    {
        return $this->language;
    }
}