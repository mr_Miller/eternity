<?php

namespace Eternity\Events\Microservices\Notification;

use Eternity\Events\Microservices\AbstractEvent;

/**
 * Push Notification by ID
 *
 * Notification will be built on Notification service, this event does NOT fires on Notification MS.
 * It's is an exception. Other events are located in folder of Microservice they are fired.
 *
 * Class Push
 * @package Eternity\Events\Microservices\Notification
 */
class Push extends AbstractEvent
{
    /**
     * @var string
     */
    protected $id;

    /**
     * @var int
     */
    protected $uid;

    /**
     * @var array
     */
    protected $params;

    /**
     * @var string|null
     */
    protected $priority;

    /**
     * @var array
     */
    protected $data;

    /**
     * @var array
     */
    protected $android_config;

    /**
     * @var array
     */
    protected $apple_config;

    /**
     * Push constructor.
     * @param string $id
     * @param int $uid
     * @param array $params
     * @param string|null $priority
     * @param array $data
     * @param array $androidConfig
     * @param array $appleConfig
     * @return \Eternity\Events\Microservices\Notification\Push
     */
    public static function create(
        string $id,
        int $uid,
        array $params = [],
        string $priority = null,
        array $data = [],
        array $androidConfig = [],
        array $appleConfig = []
    ) {
        $event = new static;
        $event->fromArray([
            'id'             => $id,
            'uid'            => $uid,
            'params'         => $params,
            'priority'       => $priority,
            'data'           => $data,
            'android_config' => $androidConfig,
            'apple_config'   => $appleConfig,
        ]);

        return $event;
    }

    /**
     * @return string
     */
    public function id(): string
    {
        return $this->id;
    }

    /**
     * @return int
     */
    public function uid(): int
    {
        return $this->uid;
    }

    /**
     * @return array
     */
    public function params(): array
    {
        return $this->params;
    }

    /**
     * @return string|null
     */
    public function priority(): ?string
    {
        return $this->priority;
    }

    /**
     * @return array
     */
    public function data(): array
    {
        return $this->data;
    }

    /**
     * @return array
     */
    public function androidConfig(): array
    {
        return $this->android_config;
    }

    /**
     * @return array
     */
    public function appleConfig(): array
    {
        return $this->apple_config;
    }

    /**
     * Transforms object into array
     *
     * @return array
     */
    public function toArray(): array
    {
        return [
            'id'             => $this->id,
            'uid'            => $this->uid,
            'params'         => $this->params,
            'priority'       => $this->priority,
            'data'           => $this->data,
            'android_config' => $this->android_config,
            'apple_config'   => $this->apple_config,
        ];
    }
}