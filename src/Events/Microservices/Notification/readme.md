# Instruction how to use Push-notifications by ID

The main advantage for this notification is only user ID and message ID is required to deliver them to user device with 
proper translation, chosen automatically.

There two type of Push-notification available:
- Push notification by ID;
- Multiple push notification by ID;

## Push notification by ID
```Eternity\Events\Microservices\Notification\Push```

Allows sending Push-notifications to users by only providing user ID and message ID. 
Messages go via Event-Bus to Notification MS. 
This microservice is responsible for translation and delivery to user devices.

### Usage 
1. Create needed push notification on Notification MS with translations and set its ID, lets call this **Message ID**.
2. Create the instance of class `Eternity\Events\Microservices\Notification\Push` and provide **User ID** and **Message ID** 
   (Other Push-notification date can be provided as well)
3. Set params as array to replace placeholders (example :placeholder) in translations.   
4. Send this event to Event-Bus

> Translation will be set depending on user chosen notification language. You do not need to worry about.

### Example
```php 
$uid = 1;
$messageId = 'some_pretty_push_nt';
$params = [
    'count' => 25,  // The paramere in message title and body :count will be replaced by value 25 
];

$push = Push::create($uid, $messageId, $params);

// send to event-bus
```

## Multiple push notification by ID
```Eternity\Events\Microservices\Notification\PushMultiple```
This event contains multiple Push-notifications. Is used for sending huge number of Push-notifications at once. 
One event can contain up to 500 different notifications. Event push-notification will be handled properly by Notification MS 
and delivered to its recipient.