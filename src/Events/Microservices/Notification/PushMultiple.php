<?php

namespace Eternity\Events\Microservices\Notification;

use Eternity\Events\Microservices\AbstractEvent;
use Exception;
use Traversable;

/**
 * This push can be used to send multiple pushes to firebase at once
 *
 * Class MultiplePush
 * @package Eternity\Events\Microservices\Notification
 */
class PushMultiple extends AbstractEvent implements \IteratorAggregate
{
    /**
     * @var array
     */
    protected $serializedPushes = [];

    /**
     * Add multiple Pushes
     *
     * Caution: Recommended to use maximum array of 100 items
     *
     * @param array $pushes
     */
    public function addMultiple(array $pushes): void
    {
        foreach ($pushes as $push) {
            $this->add($push);
        }
    }

    /**
     * Add push
     * @param \Eternity\Events\Microservices\Notification\Push $push
     */
    public function add(Push $push): void
    {
        $this->serializedPushes[] = $push->serialize();
    }

    /**
     * @return int
     */
    public function count(): int
    {
        return count($this->serializedPushes);
    }

    /**
     * Transforms object into array
     *
     * @return array
     */
    public function toArray(): array
    {
        return [
            'serializedPushes' => $this->serializedPushes,
        ];
    }

    /**
     * Retrieve an external iterator
     * @link https://php.net/manual/en/iteratoraggregate.getiterator.php
     * @return Traversable An instance of an object implementing <b>Iterator</b> or
     * <b>Traversable</b>
     * @throws Exception on failure.
     */
    public function getIterator()
    {
        foreach ($this->serializedPushes as $serializedPush) {
            $push = new Push();
            $push->unserialize($serializedPush);
            yield $push;
        }
    }
}