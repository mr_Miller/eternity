<?php

namespace Eternity\Events\Microservices\Gateway;

use Eternity\Events\Microservices\AbstractEvent;

/**
 * Class UserDeleted
 * @package Eternity\Events\Microservices\Gateway
 */
class UserDeleted extends AbstractEvent
{
    /** @var int $userId */
    protected $userId;

    /** @var string|null */
    protected $deletedAt;

    /**
     * @param int $uid
     * @param string|null $deletedAt
     * @return self
     */
    public static function create(int $uid, ?string $deletedAt = null): self
    {
        $event = new static();
        $event->fromArray([
            'userId'    => $uid,
            'deletedAt' => $deletedAt,
        ]);

        return $event;
    }

    /**
     * Transforms object into array
     *
     * @return array
     */
    public function toArray(): array
    {
        return [
            'userId'    => $this->userId,
            'deletedAt' => $this->deletedAt,
        ];
    }

    /**
     * Returns the user ID.
     *
     * @return int The user ID.
     */
    public function uid(): int
    {
        return $this->userId;
    }

    /**
     * Returns the deletion timestamp.
     *
     * @return string|null The deletion timestamp or null if not deleted.
     */
    public function deletedAt(): ?string
    {
        return $this->deletedAt;
    }
}
