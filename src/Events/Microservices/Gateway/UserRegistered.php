<?php

namespace Eternity\Events\Microservices\Gateway;

use Eternity\Events\Microservices\AbstractEvent;

/**
 * Class UserRegistered
 * @package Eternity\Events\Microservices\Gateway
 */
class UserRegistered extends AbstractEvent
{
    /**
     * @var int $uid
     */
    protected $uid;

    /**
     * @var string|null $email
     */
    protected $email;

    /**
     * @var int|null $phone
     */
    protected $phone;

    /**
     * @var string|null $referralHash
     */
    protected $referralHash;

    /**
     * @var string
     */
    protected $language;

    /**
     * UserRegistered constructor.
     * @param int $uid
     * @param string $language
     * @param string|null $email
     * @param string|null $phone
     * @param string|null $referralHash
     * @return static
     */
    public static function create(
        int $uid,
        string $language,
        ?string $email = null,
        ?string $phone = null,
        ?string $referralHash = null
    ): self {
        $event = new static();
        $event->fromArray([
            'uid'          => $uid,
            'email'        => $email,
            'phone'        => $phone,
            'referralHash' => $referralHash,
            'language'     => $language
        ]);

        return $event;
    }

    /**
     * Transforms object into array
     *
     * @return array
     */
    public function toArray(): array
    {
        return [
            'uid'          => $this->uid,
            'email'        => $this->email,
            'phone'        => $this->phone,
            'referralHash' => $this->referralHash,
            'language'     => $this->language
        ];
    }

    /**
     * @return int
     */
    public function getUid(): int
    {
        return $this->uid;
    }

    /**
     * @return string|null
     */
    public function getReferralHash(): ?string
    {
        return $this->referralHash;
    }

    /**
     * @return int|null
     */
    public function getPhone(): ?int
    {
        return $this->phone;
    }

    /**
     * @return string|null
     */
    public function getEmail(): ?string
    {
        return $this->email;
    }

    /**
     * @return string
     */
    public function getLanguage(): string
    {
        return $this->language;
    }
}