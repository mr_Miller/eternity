<?php

namespace Eternity\Events\Microservices\AnimalId;

use Eternity\Events\Microservices\AbstractEvent;

/**
 * Class AddedMicrochipToPet
 * @package Eternity\Events\Microservices\AnimalId
 */
class AddedMicrochipToPet extends AbstractEvent
{
    /** @var int */
    protected $pet_id;

    /** @var int */
    protected $microchip_id;

    /** @var string */
    protected $chipping_date;

    /**
     * @param int $petId
     * @param int $microchipId
     * @param string $chippingDate
     * @return static
     */
    public static function create(int $petId, int $microchipId, string $chippingDate): self
    {
        $event = new static();
        $event->pet_id = $petId;
        $event->microchip_id = $microchipId;
        $event->chipping_date = $chippingDate;

        return $event;
    }

    /**
     * Transforms object into array
     *
     * @return array
     */
    public function toArray(): array
    {
        return [
            'pet_id'        => $this->pet_id,
            'microchip_id'  => $this->microchip_id,
            'chipping_date' => $this->chipping_date,
        ];
    }

    /**
     * @return int
     */
    public function getPetId(): int
    {
        return $this->pet_id;
    }

    /**
     * @return int
     */
    public function getMicrochipId(): int
    {
        return $this->microchip_id;
    }

    /**
     * @return string
     */
    public function getChippingDate(): string
    {
        return $this->chipping_date;
    }
}