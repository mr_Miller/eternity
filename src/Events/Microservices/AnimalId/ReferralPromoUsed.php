<?php

namespace Eternity\Events\Microservices\AnimalId;

use Eternity\Events\Microservices\AbstractEvent;

/**
 * Class ReferralPromoUsed
 * @package Eternity\Events\Microservices\AnimalId
 */
class ReferralPromoUsed extends AbstractEvent
{
    /**
     * @var string $promoCode
     */
    protected $promoCode;

    /**
     * @var int $uid
     */
    protected $uid;

    /**
     * @var string
     */
    protected $language;

    /**
     * ReferralPromoUsed constructor.
     * @param int $uid
     * @param string $promoCode
     * @param string $language
     * @return static
     */
    public static function create(int $uid, string $promoCode, string $language): self
    {
        $event = new static();
        $event->fromArray([
            'promoCode' => $promoCode,
            'uid'       => $uid,
            'language'  => $language
        ]);

        return $event;
    }

    /**
     * Transforms object into array
     *
     * @return array
     */
    public function toArray(): array
    {
        return [
            'promoCode' => $this->promoCode,
            'uid'       => $this->uid,
            'language'  => $this->language
        ];
    }

    /**
     * @return int
     */
    public function getUid(): int
    {
        return $this->uid;
    }

    /**
     * @return string
     */
    public function getPromoCode(): string
    {
        return $this->promoCode;
    }

    /**
     * @return string
     */
    public function getLanguage(): string
    {
        return $this->language;
    }
}