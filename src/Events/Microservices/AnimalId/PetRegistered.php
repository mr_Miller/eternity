<?php

namespace Eternity\Events\Microservices\AnimalId;

use Eternity\Events\Microservices\AbstractEvent;
use Eternity\Events\Microservices\AnimalId\Dto\PetRegisteredDto;

/**
 * Class PetRegistered
 * @package Eternity\Events\Microservices\AnimalId
 */
class PetRegistered extends AbstractEvent
{
    /** @var int */
    protected $pet_id;

    /** @var int|null */
    protected $microchip_id;

    /** @var string|null */
    protected $chipping_date;

    /** @var int */
    protected $species_id;

    /** @var string|null */
    protected $breed;

    /** @var string|null */
    protected $color;

    /** @var string|null */
    protected $nickname;

    /** @var int|null */
    protected $gender;

    /** @var bool|null */
    protected $is_sterilization;

    /** @var int|null */
    protected $owner_id;

    /** @var int */
    protected $created_by;

    /** @var string */
    protected $created_at;

    /**
     * @return static
     */
    public static function create(PetRegisteredDto $dto): self
    {
        $event = new static();
        $event->fromArray($dto->toArray());

        return $event;
    }

    /**
     * Transforms object into array
     *
     * @return array
     */
    public function toArray(): array
    {
        return [
            'pet_id'           => $this->pet_id,
            'microchip_id'     => $this->microchip_id,
            'chipping_date'    => $this->chipping_date,
            'species_id'       => $this->species_id,
            'breed'            => $this->breed,
            'color'            => $this->color,
            'nickname'         => $this->nickname,
            'gender'           => $this->gender,
            'is_sterilization' => $this->is_sterilization,
            'owner_id'         => $this->owner_id,
            'created_by'       => $this->created_by,
            'created_at'       => $this->created_at,
        ];
    }

    /**
     * @return int
     */
    public function getPetId(): int
    {
        return $this->pet_id;
    }

    /**
     * @return int
     */
    public function getMicrochipId(): ?int
    {
        return $this->microchip_id;
    }

    /**
     * @return string|null
     */
    public function getChippingDate(): ?string
    {
        return $this->chipping_date;
    }

    /**
     * @return int
     */
    public function getSpeciesId(): int
    {
        return $this->species_id;
    }

    /**
     * @return string|null
     */
    public function getBreed(): ?string
    {
        return $this->breed;
    }

    /**
     * @return string|null
     */
    public function getColor(): ?string
    {
        return $this->color;
    }

    /**
     * @return string|null
     */
    public function getNickname(): ?string
    {
        return $this->nickname;
    }

    /**
     * @return int|null
     */
    public function getGender(): ?int
    {
        return $this->gender;
    }

    /**
     * @return bool|null
     */
    public function isSterilization(): ?bool
    {
        return $this->is_sterilization;
    }

    /**
     * @return int
     */
    public function getCreatedBy(): int
    {
        return $this->created_by;
    }

    /**
     * @return string
     */
    public function getCreatedAt(): string
    {
        return $this->created_at;
    }

    /**
     * @return int|null
     */
    public function getOwnerId(): ?int
    {
        return $this->owner_id;
    }
}