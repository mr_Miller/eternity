<?php

namespace Eternity\Events\Microservices\AnimalId;

use Eternity\Events\Microservices\AbstractEvent;

/**
 * Class FirstPassportActivated
 * @package Eternity\Events\Microservices\AnimalId
 */
class FirstPassportActivated extends AbstractEvent
{
    /**
     * @var string $passport
     */
    protected $passport;

    /**
     * @var string $promo
     */
    protected $promo;

    /**
     * @var int $uid
     */
    protected $uid;

    /**
     * @var string
     */
    protected $language;

    /**
     * FirstPassportActivated constructor.
     * @param string $passport
     * @param string $promo
     * @param int $uid
     * @param string $language
     * @return static
     */
    public static function create(string $passport, string $promo, int $uid, string $language): self
    {
        $event = new static();
        $event->fromArray([
            'passport' => $passport,
            'promo'    => $promo,
            'uid'      => $uid,
            'language' => $language
        ]);

        return $event;
    }

    /**
     * Transforms object into array
     *
     * @return array
     */
    public function toArray(): array
    {
        return [
            'passport' => $this->passport,
            'promo'    => $this->promo,
            'uid'      => $this->uid,
            'language' => $this->language
        ];
    }

    /**
     * @return int
     */
    public function getUid(): int
    {
        return $this->uid;
    }

    /**
     * @return string
     */
    public function getPromo(): string
    {
        return $this->promo;
    }

    /**
     * @return string
     */
    public function getPassport(): string
    {
        return $this->passport;
    }

    /**
     * @return string
     */
    public function getLanguage(): string
    {
        return $this->language;
    }
}