<?php

namespace Eternity\Events\Microservices\AnimalId\Dto;

use Eternity\Contracts\Arrayable;
use Eternity\Traits\FromArray;

/**
 * Class PetRegisteredDto
 * @package Eternity\Events\Microservices\AnimalId\Dto
 */
class PetRegisteredDto implements Arrayable
{
    use FromArray;

    /** @var int */
    public $pet_id;

    /** @var int */
    public $microchip_id;

    /** @var string|null ISO 8601 */
    public $chipping_date;

    /** @var int */
    public $species_id;

    /** @var string|null */
    public $breed;

    /** @var string|null */
    public $color;

    /** @var string|null */
    public $nickname;

    /** @var int|null */
    public $gender;

    /** @var bool|null */
    public $is_sterilization;

    /** @var int|null */
    public $owner_id;

    /** @var int */
    public $created_by;

    /** @var string ISO 8601 */
    public $created_at;

    /**
     * Transforms object into array
     *
     * @return array
     */
    public function toArray(): array
    {
        return [
            'pet_id'           => $this->pet_id,
            'microchip_id'     => $this->microchip_id,
            'chipping_date'    => $this->chipping_date,
            'species_id'       => $this->species_id,
            'breed'            => $this->breed,
            'color'            => $this->color,
            'nickname'         => $this->nickname,
            'gender'           => $this->gender,
            'is_sterilization' => $this->is_sterilization,
            'owner_id'         => $this->owner_id,
            'created_by'       => $this->created_by,
            'created_at'       => $this->created_at,
        ];
    }
}