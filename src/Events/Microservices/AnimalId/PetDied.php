<?php

namespace Eternity\Events\Microservices\AnimalId;

use Eternity\Events\Microservices\AbstractEvent;

/**
 * Class PetDied
 * @package Eternity\Events\Microservices\AnimalId
 */
class PetDied extends AbstractEvent
{
    /** @var int */
    protected $petId;

    /** @var string */
    protected $diedAt;

    /** @var int|null User ID, who mark about pet died */
    protected $updatedByUid;

    /**
     * PetDied constructor
     * @param int $petId
     * @param string $diedAt
     * @param int|null $updatedByUid
     * @return static
     */
    public static function create(int $petId, string $diedAt, ?int $updatedByUid): self
    {
        $event = new static();
        $event->fromArray([
            'petId'        => $petId,
            'diedAt'       => $diedAt,
            'updatedByUid' => $updatedByUid,
        ]);

        return $event;
    }

    /**
     * Transforms object into array
     *
     * @return array
     */
    public function toArray(): array
    {
        return [
            'petId'        => $this->petId,
            'diedAt'       => $this->diedAt,
            'updatedByUid' => $this->updatedByUid,
        ];
    }

    /**
     * @return int
     */
    public function getPetId(): int
    {
        return $this->petId;
    }

    /**
     * @return string
     */
    public function getDiedAt(): string
    {
        return $this->diedAt;
    }

    /**
     * @return int|null
     */
    public function getUpdatedByUid(): ?int
    {
        return $this->updatedByUid;
    }
}