<?php

namespace Eternity\Events\Microservices\AnimalId;

use Eternity\Events\Microservices\AbstractEvent;

/**
 * Class PetDeleted
 * @package Eternity\Events\Microservices\AnimalId
 */
class PetDeleted extends AbstractEvent
{
    /** @var int */
    protected $petId;

    /** @var string */
    protected $deletedAt;

    /** @var int|null User ID, who mark about pet deleted */
    protected $updatedByUid;

    /**
     * PetDeleted constructor.
     * @param int $petId
     * @param string $deletedAt
     * @param int|null $updatedByUid
     * @return static
     */
    public static function create(int $petId, string $deletedAt, ?int $updatedByUid): self
    {
        $event = new static();
        $event->fromArray([
            'petId'        => $petId,
            'deletedAt'    => $deletedAt,
            'updatedByUid' => $updatedByUid,
        ]);

        return $event;
    }

    /**
     * Transforms object into array
     *
     * @return array
     */
    public function toArray(): array
    {
        return [
            'petId'        => $this->petId,
            'deletedAt'    => $this->deletedAt,
            'updatedByUid' => $this->updatedByUid,
        ];
    }

    /**
     * @return int
     */
    public function getPetId(): int
    {
        return $this->petId;
    }

    /**
     * @return string
     */
    public function getDeletedAt(): string
    {
        return $this->deletedAt;
    }

    /**
     * @return int|null
     */
    public function getUpdatedByUid(): ?int
    {
        return $this->updatedByUid;
    }
}