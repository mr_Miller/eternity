<?php

namespace Eternity\Events\Microservices;

/**
 * Push event
 * Sends push notification is handled by Notification service
 *
 * Provide config if needed to add any logic for specific device OS (Android/iOS).
 *
 * Class SendPush
 * @package Eternity\Events\Microservices\Notification
 */
class PushNotification extends AbstractEvent
{
    /**
     * @var int
     */
    protected $uid;

    /**
     * @var string
     */
    protected $language;

    /**
     * @var string
     */
    protected $title;

    /**
     * @var string
     */
    protected $body;

    /**
     * @var string|null
     */
    protected $priority;

    /**
     * @var array
     */
    protected $androidConfig;

    /**
     * @var array
     */
    protected $appleConfig;

    /**
     * @var array
     */
    protected $data;

    /**
     * SendPush constructor.
     * @param int $uid
     * @param string $language
     * @param string $title
     * @param string $body
     * @param string|null $priority
     * @param array $data
     * @param array $androidConfig
     * @param array $appleConfig
     * @return static
     */
    public static function create(
        int $uid,
        string $language,
        string $title,
        string $body,
        string $priority = null,
        array $data = [],
        array $androidConfig = [],
        array $appleConfig = []
    ): self {
        $event = new static();
        $event->fromArray([
            'uid'           => $uid,
            'language'      => $language,
            'title'         => $title,
            'body'          => $body,
            'priority'      => $priority,
            'data'          => $data,
            'androidConfig' => $androidConfig,
            'appleConfig'   => $appleConfig,
        ]);

        return $event;
    }

    /**
     * Transforms object into array
     *
     * @return array
     */
    public function toArray(): array
    {
        return [
            'uid'           => $this->uid,
            'language'      => $this->language,
            'title'         => $this->title,
            'body'          => $this->body,
            'priority'      => $this->priority,
            'data'          => $this->data,
            'androidConfig' => $this->androidConfig,
            'appleConfig'   => $this->appleConfig,
        ];
    }

    /**
     * @return int
     */
    public function getUid(): int
    {
        return $this->uid;
    }

    /**
     * @return string
     */
    public function getLanguage(): string
    {
        return $this->language;
    }

    /**
     * @return string
     */
    public function getTitle(): string
    {
        return $this->title;
    }

    /**
     * @return string
     */
    public function getBody(): string
    {
        return $this->body;
    }

    /**
     * @return string|null
     */
    public function getPriority(): ?string
    {
        return $this->priority;
    }

    /**
     * @return array
     */
    public function getAndroidConfig(): array
    {
        return $this->androidConfig;
    }

    /**
     * @return array
     */
    public function getAppleConfig(): array
    {
        return $this->appleConfig;
    }

    /**
     * @return array
     */
    public function getData(): array
    {
        return $this->data;
    }
}