<?php

namespace Eternity\Events\Exceptions;

use Eternity\Exceptions\ServerException;

/**
 * Class EventException
 * @package Eternity\Events\Exceptions
 */
class EventException extends ServerException
{
}