<?php

namespace Eternity\Contracts;

/**
 * Responsible for request authorization
 * Use this policy to declare special access rules for specific situations
 *
 * Class AuthorizationPolicy
 * @package Eternity\Contracts
 */
interface AuthorizationPolicy
{
    /**
     * Check policy
     * @param array $params
     * @return mixed
     */
    public function check(array $params): void;
}