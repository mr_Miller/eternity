<?php

namespace Eternity\Contracts;

/**
 * Arrayable interface
 *
 * Class Arrayable
 * @package Eternity\Contracts
 */
interface Arrayable
{
    /**
     * Transforms object into array
     *
     * @return array
     */
    public function toArray(): array;
}