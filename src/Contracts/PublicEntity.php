<?php

namespace Eternity\Contracts;

/**
 * Class PublicEntity
 * @package Eternity\Contracts
 */
interface PublicEntity
{
    /**
     * Returns entity public Id
     * @return string
     */
    public function publicId(): string;

    /**
     * Returns entity public type
     * @return string
     */
    public function publicType(): string;
}