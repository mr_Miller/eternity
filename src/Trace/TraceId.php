<?php

namespace Eternity\Trace;

use Eternity\Exceptions\ErrorCodes;
use Eternity\Messages\Messages;
use Eternity\Trace\Exceptions\TraceIdException;

/**
 * Trace ID
 *
 * Responsible for maintaining unique request ID
 * Prefix is used for identifying the component. To cut the long story short it's component ID.
 *
 * Trace ID logic for different types of application call:
 *     CONSOLE commands - must be generated manually before executing command
 *     HTTP request
 *          Core - generated automatically
 *          Other - must be provided in request header
 *     QUEUE task handler - must be provided
 *     CRON task - must be generated before execution manually.
 *
 * Class Trace
 * @package Eternity\Logger
 */
final class TraceId
{
    private const DEFAULT_PREFIX = 'dev';

    /**
     * @var string $value Request trace ID
     */
    private $value;

    /**
     * Returns current trace ID
     * @return mixed
     * @throws \Eternity\Trace\Exceptions\TraceIdException
     */
    public function value(): string
    {
        if ($this->value === null) {
            throw new TraceIdException(
                ErrorCodes::TRACE_ID_INIT_ERROR,
                'Trace Id Error',
                Messages::message(ErrorCodes::TRACE_ID_INIT_ERROR)
            );
        }

        return $this->value;
    }

    /**
     * Initializes Trace ID with provided value.
     *
     * Can be reinitialized
     *
     * @param string $value
     */
    public function init(string $value): void
    {
        $this->value = $value;
    }

    /**
     * Generates value
     *
     * Value can all be generated once. Exception will be thrown on trying to generate twice.
     *
     * @param string|null $prefix
     * @return void
     * @throws \Eternity\Trace\Exceptions\TraceIdException
     * @throws \Exception
     */
    public function generate(string $prefix = self::DEFAULT_PREFIX): void
    {
        if ($this->value !== null) {
            throw new TraceIdException(
                ErrorCodes::TRACE_ID_GENERATION_OVERRIDE,
                'Trace Id Error',
                Messages::message(ErrorCodes::TRACE_ID_GENERATION_OVERRIDE)
            );
        }
        $this->value = $prefix . '_' . hash("md5", time() . random_bytes(8));
    }

    /**
     * To string method
     * @return string
     * @throws \Eternity\Trace\Exceptions\TraceIdException
     */
    public function __toString(): string
    {
        return $this->value();
    }
}