<?php

namespace Eternity\Trace\Exceptions;

use Eternity\Exceptions\EternityException;

/**
 * Class TraceIdException
 * @package Eternity\Trace\Exceptions
 */
class TraceIdException extends EternityException
{
    public function __construct(
        int $code,
        string $title = 'Trace ID error',
        string $detail = null,
        \Throwable $previous = null
    ) {
        $this->code = $code;
        parent::__construct($title, $detail, $previous);
    }
}