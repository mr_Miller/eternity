<?php

namespace Eternity\Metadata;

use Eternity\Metadata\Contracts\MetadataInterface;
use Eternity\Resource\Objects\Filter\FilterCollection;
use Eternity\Resource\Objects\Order;
use Eternity\Resource\Objects\Page;
use Eternity\Resource\Objects\Pagination;

/**
 * Metadata used for methods with pagination
 *
 * Class PaginationMetadata
 * @package Eternity\Metadata
 */
class PaginationMetadata implements MetadataInterface
{
    /**
     * @var \Eternity\Resource\Objects\Filter\FilterCollection
     */
    private $filter;

    /**
     * @var \Eternity\Resource\Objects\Order
     */
    private $order;

    /**
     * @var \Eternity\Resource\Objects\Page
     */
    private $page;

    /**
     * @var \Eternity\Resource\Objects\Pagination
     */
    private $pagination;

    /**
     * Metadata constructor.
     * @param \Eternity\Resource\Objects\Filter\FilterCollection $filter
     * @param \Eternity\Resource\Objects\Order $order
     * @param \Eternity\Resource\Objects\Page $page
     * @param \Eternity\Resource\Objects\Pagination $pagination
     */
    public function __construct(FilterCollection $filter, Order $order, Page $page, Pagination $pagination)
    {
        $this->filter = $filter;
        $this->order = $order;
        $this->page = $page;
        $this->pagination = $pagination;
    }

    /**
     * @return \Eternity\Resource\Objects\Filter\FilterCollection
     */
    public function filter(): FilterCollection
    {
        return $this->filter;
    }

    /**
     * @return \Eternity\Resource\Objects\Order
     */
    public function order(): Order
    {
        return $this->order;
    }

    /**
     * @return \Eternity\Resource\Objects\Page
     */
    public function page(): Page
    {
        return $this->page;
    }

    /**
     * @return \Eternity\Resource\Objects\Pagination
     */
    public function pagination(): Pagination
    {
        return $this->pagination;
    }

    /**
     * Transforms object into array
     *
     * @return array
     */
    public function toArray(): array
    {
        return [
            'order'      => $this->order()->toArray(),
            'filter'     => $this->filter()->toArray(),
            'page'       => $this->page()->toArray(),
            'pagination' => $this->pagination()->toArray(),
        ];
    }
}