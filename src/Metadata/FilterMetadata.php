<?php

namespace Eternity\Metadata;

use Eternity\Metadata\Contracts\MetadataInterface;
use Eternity\Metadata\Objects\Null\Order;
use Eternity\Metadata\Objects\Null\Page;
use Eternity\Metadata\Objects\Null\Pagination;
use Eternity\Resource\Objects\Filter\FilterCollection;

/**
 * Class FilterMetadata
 * @package Eternity\Metadata
 */
class FilterMetadata implements MetadataInterface
{
    /**
     * @var \Eternity\Resource\Objects\Filter\FilterCollection
     */
    private $filterCollection;

    /**
     * AnalyticsMetadata constructor.
     * @param \Eternity\Resource\Objects\Filter\FilterCollection $filterCollection
     */
    public function __construct(FilterCollection $filterCollection)
    {
        $this->filterCollection = $filterCollection;
    }

    /**
     * Return Filter
     * @return \Eternity\Resource\Objects\Filter\FilterCollection
     */
    public function filter(): FilterCollection
    {
        return $this->filterCollection;
    }

    /**
     * Transforms object into array
     *
     * @return array
     */
    public function toArray(): array
    {
        return [
            'order'      => (new Order)->toArray(),
            'filter'     => $this->filter()->toArray(),
            'page'       => (new Page)->toArray(),
            'pagination' => (new Pagination)->toArray(),
        ];
    }
}