<?php

namespace Eternity\Metadata\Objects\Null;

use Eternity\Contracts\Arrayable;

/**
 * Class Pages
 * @package Eternity\Metadata\Objects\Null
 */
class Pages implements Arrayable
{
    /**
     * Transforms object into array
     *
     * @return array
     */
    public function toArray(): array
    {
        return [
            'first' => null,
            'last'  => null,
            'prev'  => null,
            'next'  => null,
        ];
    }

}