<?php

namespace Eternity\Metadata\Objects\Null;

use Eternity\Contracts\Arrayable;

/**
 * Class Page
 * @package Eternity\Metadata\Objects\Null
 */
class Page implements Arrayable
{
    /**
     * Transforms object into array
     *
     * @return array
     */
    public function toArray(): array
    {
        return [
            'size'    => null,
            'current' => null,
        ];
    }

}