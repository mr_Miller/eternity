<?php

namespace Eternity\Metadata\Objects\Null;

use Eternity\Contracts\Arrayable;

/**
 * Class Pagination
 * @package Eternity\Metadata\Objects\Null
 */
class Pagination implements Arrayable
{
    /**
     * Transforms object into array
     *
     * @return array
     */
    public function toArray(): array
    {
        return [
            'pages_total' => null,
            'items_total' => null,
            'pages'       => (new Pages)->toArray(),
        ];
    }
}