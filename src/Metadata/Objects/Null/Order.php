<?php

namespace Eternity\Metadata\Objects\Null;

use Eternity\Contracts\Arrayable;

/**
 * Null Order
 *
 * Class Order
 * @package Eternity\Metadata\Objects\Null
 */
class Order implements Arrayable
{
    /**
     * Transforms object into array
     *
     * @return array
     */
    public function toArray(): array
    {
        return [
            'direction' => null,
            'by'        => null,
        ];
    }

}