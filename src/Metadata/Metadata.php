<?php

namespace Eternity\Metadata;

use Eternity\Metadata\Contracts\MetadataInterface;
use Eternity\Metadata\Objects\Null\Pagination;
use Eternity\Resource\Objects\Filter\FilterCollection;
use Eternity\Resource\Objects\Order;
use Eternity\Resource\Objects\Page;

/**
 * Metadata
 *
 * This common Metadata class is used to obtain request metadata parameters
 * This metadata can be transformed to a PaginationMetadata
 *
 * Class Metadata
 * @package Eternity\Metadata
 */
class Metadata implements MetadataInterface
{
    /**
     * @var \Eternity\Resource\Objects\Filter\FilterCollection
     */
    private $filter;

    /**
     * @var \Eternity\Resource\Objects\Order
     */
    private $order;

    /**
     * @var \Eternity\Resource\Objects\Page
     */
    private $page;

    /**
     * Metadata constructor.
     * @param \Eternity\Resource\Objects\Filter\FilterCollection $filter
     * @param \Eternity\Resource\Objects\Order $order
     * @param \Eternity\Resource\Objects\Page $page
     */
    public function __construct(FilterCollection $filter, Order $order, Page $page)
    {
        $this->filter = $filter;
        $this->order = $order;
        $this->page = $page;
    }

    /**
     * @return \Eternity\Resource\Objects\Filter\FilterCollection
     */
    public function filter(): FilterCollection
    {
        return $this->filter;
    }

    /**
     * @return \Eternity\Resource\Objects\Order
     */
    public function order(): Order
    {
        return $this->order;
    }

    /**
     * @return \Eternity\Resource\Objects\Page
     */
    public function page(): Page
    {
        return $this->page;
    }

    /**
     * Transforms object into array
     *
     * @return array
     */
    public function toArray(): array
    {
        return [
            'order'      => $this->order()->toArray(),
            'filter'     => $this->filter()->toArray(),
            'page'       => $this->page()->toArray(),
            'pagination' => (new Pagination)->toArray(),
        ];
    }
}