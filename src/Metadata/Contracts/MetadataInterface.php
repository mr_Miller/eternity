<?php

namespace Eternity\Metadata\Contracts;

use Eternity\Contracts\Arrayable;

/**
 * Metadata interface
 *
 * Class MetadataInterface
 * @package Eternity\Resource\Contract
 */
interface MetadataInterface extends Arrayable
{
}