<?php

namespace Eternity;

use Eternity\Components\Definition\AbstractBaseDefinition;
use Eternity\Components\Definition\Exceptions\DefinitionException;

/**
 * List of all microservices
 *
 * Metadata of microservices
 *
 * Class Microservices
 * @package Eternity
 */
class Microservices extends AbstractBaseDefinition
{
    public const MARKETPLACE = 'marketplace';
    public const GATEWAY = 'gateway';
    public const NOTIFICATION = 'notification';
    public const ANIMAL_ID = 'animal-id';
    public const EVENT = 'event';
    public const REFERRAL = 'referral';
    public const SUBSCRIPTION = 'subscription';
    public const LOCATION = 'location';
    public const CALENDAR = 'calendar';
    public const PET = 'pet';
    public const ANALYTIC = 'analytic';
    public const ACHIEVEMENT = 'achievement';

    /**
     * Method builds the whole definition structure
     *
     * Every array item must have next structure:
     *  - id
     *  - title
     *  - translation
     *
     * For example:
     *  [
     *      [
     *          'id' => 5,
     *          'title' => 'String description',
     *          'translation_key' => 'Key for user readable translation',
     *      ],
     *      ...
     *  ]
     *
     * @return array
     */
    public static function getDefinitionsList(): array
    {
        return [
            [
                'id'    => self::MARKETPLACE,
                'title' => 'Marketplace',
                'short' => 'mp',
                'desc'  => 'Operates products, QR-codes, accounts',
            ],
            [
                'id'    => self::ANIMAL_ID,
                'title' => 'Animal ID',
                'short' => 'a-id',
                'desc'  => 'Operates animals',
            ],
            [
                'id'    => self::GATEWAY,
                'title' => 'Gateway',
                'short' => 'gw',
                'desc'  => 'Core microservice',
            ],
            [
                'id'    => self::NOTIFICATION,
                'title' => 'Notification',
                'short' => 'nc',
                'desc'  => 'Push notifications',
            ],
            [
                'id'    => self::EVENT,
                'title' => 'Event',
                'short' => 'ev',
                'desc'  => 'Event bus',
            ],
            [
                'id'    => self::REFERRAL,
                'title' => 'Referral',
                'short' => 'rf',
                'desc'  => 'Operates referrals',
            ],
            [
                'id'    => self::SUBSCRIPTION,
                'title' => 'Subscription',
                'short' => 'sc',
                'desc'  => 'Operates subscriptions',
            ],
            [
                'id'    => self::LOCATION,
                'title' => 'Location',
                'short' => 'lc',
                'desc'  => 'Operates locations',
            ],
            [
                'id'    => self::CALENDAR,
                'title' => 'Calendar',
                'short' => 'ca',
                'desc'  => 'Recurrent events with notifications',
            ],
            [
                'id'    => self::PET,
                'title' => 'Pet',
                'short' => 'pe',
                'desc'  => 'Manages pets',
            ],
            [
                'id'    => self::ANALYTIC,
                'title' => 'Analytic',
                'short' => 'an',
                'desc'  => 'Collecting analytics',
            ],
            [
                'id'    => self::ACHIEVEMENT,
                'title' => 'Achievement',
                'short' => 'ach',
                'desc'  => 'Achievement and awards',
            ],
        ];
    }

    /**
     * Returns desc
     *
     * Description is not required
     *
     * @param string $const
     * @return string
     */
    public static function desc(string $const): ?string
    {
        $array = array_column(static::getDefinitionsList(), 'desc', 'id');
        if (!isset($array[$const])) {
            return null;
        }

        return $array[$const];
    }

    /**
     * Returns short name
     * @param string $const
     * @return string
     * @throws \Eternity\Components\Definition\Exceptions\DefinitionException
     */
    public static function short(string $const): string
    {
        $array = array_column(static::getDefinitionsList(), 'short', 'id');
        if (!isset($array[$const])) {
            throw new DefinitionException('Definition error', 'Microservice short name is required');
        }

        return $array[$const];
    }
}