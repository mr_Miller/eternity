<?php

namespace Eternity\Mutex;

use Eternity\Mutex\Contracts\Storage;

/**
 * Class MutexFactory
 * @package Eternity\Mutex
 */
final class MutexFactory
{
    /**
     * @var \Eternity\Mutex\Contracts\Storage
     */
    private $storage;

    /**
     * MutexFactory constructor.
     *
     * @param  \Eternity\Mutex\Contracts\Storage  $storage
     */
    public function __construct(Storage $storage)
    {
        $this->storage = $storage;
    }

    /**
     * Creates a lock for the given resource.
     *
     * @param string $name The resource to lock
     * @param int $timeout Maximum expected lock duration in seconds
     *
     * @return \Eternity\Mutex\Mutex
     * @throws \Exception
     */
    public function make(string $name, int $timeout = 10): Mutex
    {
        return new Mutex($this->storage, new Token($name, $timeout));
    }
}
