<?php

namespace Eternity\Mutex\Contracts;

use Eternity\Mutex\Token;

/**
 * Interface Storage
 * @package Eternity\Mutex\Contracts
 */
interface Storage
{
    /**
     * Stores the resource if it's not locked by someone else.
     *
     * @param Token $token
     * @return void
     * @throws \Eternity\Mutex\Exceptions\LockStorageException
     * @throws \Eternity\Mutex\Exceptions\LockAcquiringException
     */
    public function store(Token $token): void;

    /**
     * Removes a resource from the storage.
     *
     * @param Token $token
     * @return void
     */
    public function delete(Token $token): void;

    /**
     * Updates an expiration timeout for the key
     * @param  \Eternity\Mutex\Token  $token
     * @throws \Eternity\Mutex\Exceptions\LockStorageException
     */
    public function update(Token $token): void;

    /**
     * Returns whether or not the resource exists in the storage.
     *
     * @param Token $token
     * @return bool
     */
    public function exists(Token $token): bool;

    /**
     * Returns the remaining time to live of a key that has a timeout.
     * @param  \Eternity\Mutex\Token  $token
     *
     * @return int|null
     */
    public function ttl(Token $token): ?int;
}
