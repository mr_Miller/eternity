<?php

namespace Eternity\Mutex\Contracts;

use Closure;

interface Retriable
{
    /**
     * Performs loop executions
     *
     * @param \Closure $callable
     *
     * @return mixed|null
     */
    public function execute(Closure $callable);

    /**
     * Stop loop execution
     * @return void
     */
    public function end(): void;
}
