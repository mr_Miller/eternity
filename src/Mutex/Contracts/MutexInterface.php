<?php

namespace Eternity\Mutex\Contracts;

/**
 * MutexInterface defines an interface to manipulate the status of a mutex.
 */
interface MutexInterface
{
    /**
     * Acquires the lock. If the lock is acquired by someone else, the parameter `blocking` determines whether or not
     * the call should block until the release of the lock.
     *
     * @param  int|null  $timeout Maximum expected timeout in seconds during acquiring
     *
     * @return bool whether or not the lock had been acquired
     */
    public function acquire(?int $timeout = null): bool;

    /**
     * Release the lock.
     * @throws \Eternity\Mutex\Exceptions\LockReleasingException If the lock can not be released
     */
    public function release();

    /**
     * Increase the duration of an acquired lock.
     *
     * @param int $timeout Maximum expected lock duration in seconds
     *
     * @throws \Eternity\Mutex\Exceptions\LockStorageException If the lock is acquired by someone else
     * @throws \Eternity\Mutex\Exceptions\LockAcquiringException  If the lock can not be refreshed
     */
    public function refresh(int $timeout): void;

    /**
     * Returns whether or not the lock is acquired.
     * @return bool
     */
    public function acquired(): bool;

    /**
     * Check whether or not the mutex is expired
     * @return bool
     */
    public function expired(): bool;

    /**
     * Returns the remaining lifetime.
     * @return int|null Remaining lifetime in seconds. Null when the lock won't expire.
     */
    public function ttl(): ?int;
}
