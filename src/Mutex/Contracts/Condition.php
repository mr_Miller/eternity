<?php

namespace Eternity\Mutex\Contracts;

use Closure;

interface Condition
{
    /**
     * Callback that decides if the lock should be acquired
     * and if the critical code callback should be executed
     * after acquiring the lock
     *
     * @param \Closure $condition User defined callback
     *
     * @return mixed
     */
    public function try(Closure $condition);

    /**
     * Executes a synchronized callback only after the check callback passes
     * before and after acquiring the lock.
     *
     * If then returns boolean boolean false, the check did not pass before or
     * after acquiring the lock. A boolean false can also be returned from the
     * critical code callback to indicate that processing did not occurred or has
     * failed. It is up to the user to decide the last point.
     *
     * @param \Closure $callable User defined callback
     *
     * @return mixed
     */
    public function then(Closure $callable);

    /**
     * Executes when lock can not be acquired
     * @param \Closure $callable User defined callback
     *
     * @return mixed
     */
    public function fail(Closure $callable);
}
