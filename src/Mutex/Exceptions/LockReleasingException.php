<?php

namespace Eternity\Mutex\Exceptions;

/**
 * Lock release exception.
 *
 * Failed to release the lock. Take this exception very serious. Failing to
 * release a lock might have the potential to introduce deadlocks. Also the
 * critical code was executed i.e. side effects may have happened.
 */
class LockReleasingException extends MutexException
{

}
