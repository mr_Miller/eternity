<?php

namespace Eternity\Mutex\Exceptions;

use Eternity\Exceptions\EternityException;

/**
 * Mutex exception.
 *
 * Generic exception for any other not covered reason. Usually extended by child classes.
 */
abstract class MutexException extends EternityException
{

}
