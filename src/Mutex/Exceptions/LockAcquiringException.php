<?php

namespace Eternity\Mutex\Exceptions;

/**
 * Lock acquire exception.
 *
 * Used when the lock could not be acquired. This exception implies that the
 * critical code was not executed, or at least had no side effects.
 */
class LockAcquiringException extends MutexException
{

}
