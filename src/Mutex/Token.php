<?php

namespace Eternity\Mutex;

use Eternity\Mutex\Exceptions\TimeoutException;

/**
 * Class Token
 * @package Eternity\Mutex
 */
final class Token
{
    /**
     * Base key name
     * @var string
     */
    private $name;

    /**
     * Generated token value
     * @var string
     */
    private $value;

    /**
     * Calculated deadline timestamp
     * @var float
     */
    private $deadline;

    /**
     * Expected duration in seconds
     * @var float
     */
    private $timeout;

    /**
     * Token constructor.
     *
     * @param  string  $name
     * @param  float  $timeout
     *
     * @throws \Exception
     */
    public function __construct(string $name, float $timeout = 10)
    {
        $this->name = "mutex:{$name}";
        $this->value = $this->generate();

        $this->refresh($timeout);
    }

    /**
     * @return string
     */
    public function __toString()
    {
        return $this->value();
    }

    /**
     * @return string
     */
    public function name(): string
    {
        return $this->name;
    }

    /**
     * @return string
     */
    public function value(): string
    {
        return $this->value;
    }

    /**
     * Initial remaining lifetime
     * @return float
     */
    public function timeout(): ?float
    {
        return $this->timeout;
    }

    /**
     * Reset token lifetime, make it infinite
     */
    public function reset(): void
    {
        $this->timeout = null;
        $this->deadline = null;
    }

    /**
     * Returns the remaining lifetime.
     * @return float|null Remaining lifetime in seconds. Null when the key won't expire.
     */
    public function ttl(): ?float
    {
        return null === $this->deadline ? null : $this->deadline - microtime(true);
    }

    /**
     * Check if deadline is expired
     * @return bool
     */
    public function expired(): bool
    {
        return null !== $this->deadline && $this->deadline < microtime(true);
    }

    /**
     * Calculates new deadline
     *
     * @param  float  $timeout  the expiration delay for lock in seconds
     *
     * @throws \Eternity\Mutex\Exceptions\TimeoutException
     */
    public function refresh(float $timeout): void
    {
        if ($timeout < 0)
        {
            throw new TimeoutException('Timeout cannot be less than 0');
        }

        $this->timeout = $timeout;
        $this->deadline =  microtime(true) + $timeout;
    }

    /**
     * Generates uniques token for specified key
     *
     * @return string
     * @throws \Exception
     */
    private function generate(): string
    {
        return base64_encode(random_bytes(32));
    }
}
