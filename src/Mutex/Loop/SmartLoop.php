<?php

namespace Eternity\Mutex\Loop;

use Closure;
use UnexpectedValueException;
use Eternity\Mutex\Exceptions\TimeoutException;
use Eternity\Mutex\Contracts\Retriable;

/**
 * Class SmartLoop
 * @package Eternity\Mutex\Loop
 */
class SmartLoop implements Retriable
{
    /**
     * Minimum time that we want to wait, between lock checks. In micro seconds.
     * @var double
     */
    private const MINIMUM_WAIT_US = 1e4;

    /**
     * Maximum time that we want to wait, between lock checks. In micro seconds.
     * @var double
     */
    private const MAXIMUM_WAIT_US = 1e6;

    /**
     * @var float The timeout in seconds.
     */
    protected $timeout;

    /**
     * @var bool True while code execution is repeating.
     */
    protected $looping = false;

    /**
     * Sets the timeout. The default is 10 seconds.
     *
     * @param  float  $timeout  The timeout in seconds. The default is 10 seconds.
     *
     * @throws \UnexpectedValueException The timeout must be greater than 0.
     */
    public function __construct(float $timeout = 1)
    {
        if ($timeout <= 0) {
            throw new UnexpectedValueException(
                sprintf(
                    'The timeout must be greater than 0. %d was given.',
                    $timeout
                )
            );
        }

        $this->timeout = $timeout;
    }

    public function __destruct()
    {
        if ($this->looping) {
            $this->end();
        }
    }

    /**
     * Notifies that this was the last iteration
     * @return void
     */
    public function end(): void
    {
        $this->looping = false;
    }

    /**
     * Repeats executing a code until it was successful.
     * The code has to be designed in a way that it can be repeated without any
     * side effects. When execution was successful it should notify that event
     * by calling {@link \Eternity\Mutex\Contracts\Retriable::end()}. I.e. the only side
     * effects of the code may happen after a successful execution.
     * If the code throws an exception it will stop repeating the execution.
     *
     * @param  \Closure  $callable  The to be executed code callback.
     *
     * @return mixed The return value of the executed code callback.
     * @throws \Eternity\Mutex\Exceptions\TimeoutException The timeout has been reached.
     * @throws \Exception The execution callback threw an exception.
     */
    public function execute(Closure $callable)
    {
        $this->looping = true;
        // At this time, the lock will time out.
        $deadline = microtime(true) + $this->timeout;
        $result   = null;
        for ($i = 0; $this->looping && microtime(true) < $deadline; ++$i) {
            $result = $callable();

            if ( ! $this->looping) {
                break;
            }

            // Calculate max time remaining, don't sleep any longer than that.
            $remaining = (int)(($deadline - microtime(true)) * 1e6);

            // We've ran out of time.
            if ($remaining <= 0) {
                throw TimeoutException::create($this->timeout);
            }

            $min     = min(
                (int)self::MINIMUM_WAIT_US * 1.5 ** $i,
                self::MAXIMUM_WAIT_US
            );
            $max     = min($min * 2, self::MAXIMUM_WAIT_US);
            $toSleep = min($remaining, random_int((int)$min, (int)$max));

            usleep($toSleep);
        }

        if (microtime(true) >= $deadline) {
            throw TimeoutException::create($this->timeout);
        }

        return $result;
    }
}
