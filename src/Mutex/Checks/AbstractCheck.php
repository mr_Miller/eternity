<?php

namespace Eternity\Mutex\Checks;

use Closure;
use Eternity\Mutex\Contracts\Condition;
use Eternity\Mutex\Contracts\MutexInterface;

abstract class AbstractCheck implements Condition
{
    /**
     * @var \Eternity\Mutex\Contracts\MutexInterface
     */
    protected $mutex;

    /**
     * @var Closure
     */
    protected $condition;

    /**
     * @var Closure
     */
    protected $failCallback;

    /**
     * Sets the lock.
     *
     * @param \Eternity\Mutex\Contracts\MutexInterface $mutex Provides logic for exclusive code execution
     */
    public function __construct(MutexInterface $mutex)
    {
        $this->mutex = $mutex;
    }

    /**
     * @inheritDoc
     */
    abstract public function then(Closure $callable);

    /**
     * @inheritDoc
     */
    public function try(Closure $condition)
    {
        $this->condition = $condition;
    }

    /**
     * @inheritDoc
     */
    public function fail(Closure $callable)
    {
        $this->failCallback = $callable;
    }

    /**
     * Executes the fail callback if it set
     * @return bool|mixed
     */
    protected function failed()
    {
        return $this->failCallback ? call_user_func($this->failCallback) : false;
    }
}
