<?php

namespace Eternity\Mutex\Checks;

use Closure;

/**
 * The double-checked locking pattern.
 * You should not instantiate this class directly. Use instead
 * {@link Mutex::condition()}
 */
class ChainedCondition extends AbstractCheck
{
    /**
     * @inheritDoc
     */
    public function then(Closure $callable)
    {
        if (! call_user_func($this->condition))
        {
            return $this->failed();
        }

        return $this->mutex->isolate(function () use ($callable) {
            return call_user_func($this->condition) ? $callable() : $this->failed();
        });
    }
}
