<?php

namespace Eternity\Mutex;

use Throwable;
use Eternity\Mutex\Checks\ChainedCondition;
use Eternity\Mutex\Contracts\Condition;
use Eternity\Mutex\Contracts\MutexInterface;
use Eternity\Mutex\Contracts\Storage;
use Eternity\Mutex\Exceptions\TimeoutException;
use Eternity\Mutex\Exceptions\LockAcquiringException;
use Eternity\Mutex\Exceptions\LockStorageException;
use Eternity\Mutex\Exceptions\LockReleasingException;

/**
 * Class Mutex
 * @package Eternity\Mutex
 */
class Mutex implements MutexInterface
{
    /**
     * @var \Eternity\Mutex\Token
     */
    private $token;

    /**
     * @var bool
     */
    private $acquired = false;

    /**
     * @var \Eternity\Mutex\Contracts\Storage
     */
    private $storage;

    /**
     * Mutex constructor.
     *
     * @param  \Eternity\Mutex\Contracts\Storage  $storage
     * @param  \Eternity\Mutex\Token  $token
     */
    public function __construct(Storage $storage, Token $token)
    {
        $this->token   = $token;
        $this->storage = $storage;
    }

    /**
     * Automatically releases the underlying lock when the object is destructed.
     */
    public function __destruct()
    {
        // stayed in storage
        if ($this->acquired()) {
            $this->release();
        }
    }

    /**
     * Returns whether or not the lock is acquired.
     * @return bool
     */
    public function acquired(): bool
    {
        return $this->acquired = $this->storage->exists($this->token);
    }

    /**
     * Check whether or not the mutex is expired
     * @return bool
     */
    public function expired(): bool
    {
        return $this->token->expired();
    }

    /**
     * Returns the remaining lifetime.
     * @return int|null
     */
    public function ttl(): ?int
    {
        return $this->storage->ttl($this->token);
    }

    /**
     * Acquires the lock.
     * This method blocks until the lock was acquired.
     *
     * @param  int|null  $timeout  Maximum expected timeout in seconds during acquiring
     *
     * @return bool
     * @throws \Eternity\Mutex\Exceptions\LockAcquiringException
     */
    public function acquire(?int $timeout = 0): bool
    {
        $delay = [50, 100, 150, 200];
        $start = microtime(true);

        do {
            try {
                $this->storage->store($this->token);
                $this->acquired = true;
                break;
            } catch (LockStorageException $e) {
                usleep($delay[random_int(0, 3)] * 1000);
                $this->acquired = false;
            } catch (Throwable $e) {
                throw new LockAcquiringException(sprintf('Failed to acquire the "%s" lock.', $this->token), null, $e);
            }
        } while (microtime(true) - $start < $timeout);

        return $this->acquired;
    }

    /**
     * Releases the lock.
     * @throws \Eternity\Mutex\Exceptions\LockReleasingException
     */
    public function release(): void
    {
        try {
            if ( ! $this->acquired) {
                throw new LockReleasingException(
                    'Mutex release failed',
                    sprintf('The resource "%s" does not locked.', $this->token)
                );
            }

            $this->storage->delete($this->token);
            $this->acquired = false;
        } catch (Throwable $e) {
            throw new LockReleasingException('Mutex release failed', 'Cannot delete from storage', $e);
        }
    }

    /**
     * @param  int  $timeout
     *
     * @throws \Eternity\Mutex\Exceptions\LockAcquiringException
     */
    public function refresh(int $timeout = 10): void
    {
        try {
            if ( ! $this->acquired) {
                throw new LockAcquiringException('Cannot refresh ttl for not acquired lock');
            }

            // bulletproof concept
            $this->acquired = true;

            $this->token->refresh($timeout);
            $this->storage->update($this->token);
        } catch (LockStorageException | TimeoutException $e) {
            $this->acquired = false;

            throw new LockAcquiringException(
                sprintf('Failed to define an expiration for the "%s" lock.', $this->token),
                null, $e
            );
        }
    }

    /**
     * Executes a block of code exclusively.
     * This method implements Java's synchronized semantic. I.e. this method
     * waits until a lock could be acquired, executes the code exclusively and
     * releases the lock.
     * The code block may throw an exception. In this case the lock will be
     * released as well.
     *
     * @param  callable  $callback  The synchronized execution callback.
     * @param  int  $timeout
     *
     * @return mixed The return value of the execution callback.
     * @throws \Eternity\Mutex\Exceptions\LockReleasingException The mutex could not be released, the code was already executed.
     * @throws \Throwable
     */
    public function isolate(callable $callback, int $timeout = 0)
    {
        try {
            $this->acquire($timeout);

            return $callback();
        } catch (Throwable $e) {
            throw $e;
        }
        finally {
            $this->release();
        }
    }

    /**
     * Performs a double-checked locking pattern.
     * Call {@link \Eternity\Mutex\Contracts\Condition::then()} on the
     * returned object.
     * Example:
     * <code>
     * $result = $mutex->check(function () use ($bankAccount, $amount) {
     *     return $bankAccount->getBalance() >= $amount;
     * })->then(function () use ($bankAccount, $amount) {
     *     return $bankAccount->withdraw($amount);
     * });
     * </code>
     *
     * @param  callable  $target  Callback that decides if the lock should be
     * acquired and if the synchronized callback should be executed after
     * acquiring the lock
     *
     * @return \Eternity\Mutex\Contracts\Condition The double-checked locking pattern
     */
    public function condition(callable $target): Condition
    {
        $check = new ChainedCondition($this);
        $check->try($target);

        return $check;
    }
}
