<?php

namespace Eternity\Helpers;

/**
 * Class Date
 *
 * @package App\Infrastructure\Helpers
 */
abstract class Date
{
    /**
     * Date format, this format is used while saving to DB, and getting from DB
     */
    public const POSTGRES_TIME_FORMAT = 'Y-m-d\TH:i:sP';
}
