<?php

namespace Eternity\Helpers;

/**
 * Class Uri
 * @package Eternity\Helpers
 */
class Uri
{
    /** @var string */
    private $path;

    /**
     * URI constructor
     *
     * Examples:
     *   $queryParams = [
     *       'status' => [1, 2, 10],        // ?status=1&status=2&status=10
     *       'name' => [                    // ?name[first_name]=Joe&name[last_name]=Bond
     *           'first_name' => 'Joe',
     *           'last_name' => 'Bond'
     *       ],
     *       'env' => 'production',         // ?env=production
     *   ]
     *
     * @param string $path
     * @param array $queryParams
     */
    public function __construct(string $path, array $queryParams = [])
    {
        $this->path = $this->buildPath($path, $queryParams);
    }

    /**
     * Builds a new instance of the class.
     *
     * @param string $path The path value for the instance.
     * @param array $queryParams The query parameters for the instance. Default is an empty array.
     * @return self The newly created instance of the class.
     */
    public static function build(string $path, array $queryParams = []): self
    {
        return new static($path, $queryParams);
    }

    /**
     * @return string
     */
    public function __toString(): string
    {
        return $this->path;
    }

    /**
     * Builds URI path
     *
     * @param string $path
     * @param array $queryParams
     * @return string
     */
    private function buildPath(string $path, array $queryParams = []): string
    {
        if (empty($queryParams)) {
            return $path;
        }

        $queryString = [];
        foreach ($queryParams as $key => $value) {
            if (is_array($value)) {
                $queryString[] = $this->buildNestedQueryString($key, $value);
            } else {
                $queryString[] = "$key=" . urlencode($value);
            }
        }

        $queryString = implode('&', $queryString);

        return $path . '?' . $queryString;
    }

    /**
     * Recursively builds a nested query string for an associative array.
     *
     * @param string $parentKey
     * @param array $data
     * @return string
     */
    private function buildNestedQueryString(string $parentKey, array $data): string
    {
        $queryString = [];
        foreach ($data as $key => $value) {
            $key = (is_int($key)) ? $parentKey : $parentKey . '[' . $key . ']';
            if (is_array($value)) {
                $queryString[] = $this->buildNestedQueryString($key, $value);
            } else {
                $queryString[] = "$key=" . urlencode($value);
            }
        }

        return implode('&', $queryString);
    }

    /**
     * @return string
     */
    public function path(): string
    {
        return $this->path;
    }
}