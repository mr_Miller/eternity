<?php

namespace Eternity\Helpers;

/**
 * Class NameHelper
 * @package Eternity\Helpers
 */
class NameHelper
{
    /**
     * Returns short classname without namespace
     * @param string $className
     * @return string
     */
    public static function shortClassName(string $className): string
    {
        return substr(strrchr($className, '\\'), 1) ?: $className;
    }
}