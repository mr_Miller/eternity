<?php

namespace Eternity\Helpers;

/**
 * Class Bonus
 * @package App\Infrastructure\Helpers
 */
abstract class Number
{
    /**
     * Formats a number with provided number of decimals WITHOUT rounding
     *
     * Example:
     * Bonus::f(100.9999);
     * Bonus::f(424.298);
     * Outputs:
     * "100.99"
     * "424.29"
     *
     * @param float $number
     * @param int $decimals
     * @return string
     */
    public static function decimals(float $number, int $decimals = 2): string
    {
        return bcdiv($number, 1, $decimals);
    }
}
