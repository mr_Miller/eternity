<?php

namespace Eternity\Traits;

/**
 * Allows to create object from array
 *
 * Allow to fill object properties from array
 *
 * Trait FromArray
 * @package Eternity\Traits
 */
trait FromArray
{
    /**
     * Fills dto properties from an array
     *
     * @param array $data
     */
    public function fill(array $data): void
    {
        foreach ($data as $property => $propertyValue)
        {
            if (property_exists($this, $property)) {
                $this->$property = $propertyValue;
            }
        }
    }

    /**
     * Creates object with properties from an array
     *
     * @param array $data
     * @return static
     */
    public static function fromArray(array $data): self
    {
        $object = new static();
        $object->fill($data);
        return $object;
    }
}