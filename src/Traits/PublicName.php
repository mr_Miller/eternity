<?php

namespace Eternity\Traits;

use Eternity\Helpers\NameHelper;

/**
 * Trait EntityType
 * @package Eternity\Traits
 */
trait PublicName
{
    /**
     * Returns entity name
     *
     * To set value init ENTITY constant
     *
     * @return string
     */
    public static function entityName(): string
    {
        return defined('static::PUBLIC_NAME') ? static::PUBLIC_NAME : NameHelper::shortClassName(static::class);
    }
}