<?php

namespace Eternity\Components\Entity;

/**
 * Class AbstractEntity
 * @package Eternity\Components\Entity
 */
abstract class AbstractEntity
{
    /**
     * @var array
     */
    private $original;

    /**
     * AbstractEntity constructor.
     */
    public function __construct(array $original = [])
    {
        $this->original = $original;
    }

    /**
     * Returns property
     *
     * If property is empty returns default value
     *
     * @param string $property
     * @param null $default
     * @return mixed|null
     */
    protected function property(string $property, $default = null)
    {
        return $this->original[$property] ?? $default;
    }

    /**
     * Returns original data
     * @return array
     */
    public function original(): array
    {
        return $this->original;
    }
}