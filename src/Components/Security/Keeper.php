<?php

namespace Eternity\Components\Security;

/**
 * Class Keeper
 * @package Eternity\Components\Security
 */
class Keeper
{
    /**
     * @var string
     */
    private $password;

    /**
     * @var string
     */
    private $username;

    /**
     * Keeper constructor.
     * @param string $username
     * @param string $password
     */
    public function __construct(string $username, string $password)
    {
        $this->username = $username;
        $this->password = $password;
    }

    /**
     * Verifying credentials
     * @param string|null $username
     * @param string|null $password
     *
     * @return bool
     */
    public function verify(?string $username, ?string $password): bool
    {
        if ($username === null || $password === null) {
            return false;
        }

        return password_verify($this->username, $username) && password_verify($this->password, $password);
    }

    /**
     * @return string
     */
    public function authorizationHeader(): string
    {
        return 'Basic ' . base64_encode($this->username . ':' . $this->password);
    }

    /**
     * @return string
     */
    public function usernameHash(): string
    {
        return password_hash($this->username, PASSWORD_DEFAULT);
    }

    /**
     * @return string
     */
    public function passwordHash(): string
    {
        return password_hash($this->password, PASSWORD_DEFAULT);
    }
}