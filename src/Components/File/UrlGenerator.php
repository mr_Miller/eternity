<?php

namespace Eternity\Components\File;

/**
 * Generates file paths for storage
 *
 * Class UrlGenerator
 * @package Eternity\Components\File
 */
class UrlGenerator
{
    /**
     * @var string
     */
    private $hostUrl;

    /**
     * UrlGenerator constructor.
     * @param string $hostUrl
     */
    public function __construct(string $hostUrl)
    {
        $this->hostUrl = $hostUrl;
    }

    /**
     * Returns host url
     * @return string
     */
    public function getHostUrl(): string
    {
        return $this->hostUrl;
    }

    /**
     * Returns absolute path
     * @param int $entityId
     * @param string $filename
     * @param string $entityPath
     * @return string
     */
    public function getUrl(int $entityId, string $filename, string $entityPath): string
    {
        return $this->getHostUrl() . '/' . static::getOriginalPath($entityId, $entityPath) . '/' . $filename;
    }

    /**
     * Returns absolute path for file without linked entity
     * @param string $filename
     * @param string $filePath
     * @return string
     */
    public function getUrlWithoutEntity(string $filename, string $filePath): string
    {
        return $this->getHostUrl() . '/' . static::getOriginalWithoutEntityPath($filePath) . '/' . $filename;
    }

    /**
     * Returns original path
     * @param int $entityId
     * @param string $entityPath
     * @return string
     */
    public static function getOriginalPath(int $entityId, string $entityPath): string
    {
        return $entityPath . '/' . $entityId;
    }

    /**
     * Returns path to original without specific entity
     * @param string $filepath
     * @return string
     */
    public static function getOriginalWithoutEntityPath(string $filepath): string
    {
        return $filepath;
    }
}