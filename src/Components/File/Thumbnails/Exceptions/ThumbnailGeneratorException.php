<?php

namespace Eternity\Components\File\Thumbnails\Exceptions;

use Eternity\Exceptions\EternityException;

/**
 * Class ThumbnailConfigurationException
 * @package Eternity\Components\File\Thumbnails\Exceptions
 */
class ThumbnailGeneratorException extends EternityException
{
    /**
     * ThumbnailConfigurationException constructor.
     * @param int $code
     * @param string|null $title
     * @param string|null $detail
     * @param \Throwable|null $previous
     */
    public function __construct(int $code, string $title = null, string $detail = null, \Throwable $previous = null)
    {
        parent::__construct($title, $detail, $previous);
        $this->type = 'ThumbnailGeneratorException';
        $this->code = $code;
    }
}