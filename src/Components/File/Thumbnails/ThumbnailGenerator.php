<?php

namespace Eternity\Components\File\Thumbnails;

use Eternity\Components\File\Thumbnails\Exceptions\ThumbnailGeneratorException;
use Eternity\Components\File\UrlGenerator;
use Eternity\Exceptions\ErrorCodes;

/**
 * Entity thumbnail generator
 *
 * Responsible for building proper absolute URL's for image thumbnails
 * Can be used across the microservices and in CORE-microservice (Gateway)
 *
 * Class ThumbnailsGenerator
 * @package Eternity\Components\File\Thumbnails
 */
class ThumbnailGenerator extends UrlGenerator
{
    /**
     * Prefix before thumbnail sizes when thumbnail array key builds
     */
    public const SIZE_PREFIX = 'size_';

    /**
     * List of thumbnails paths with lists of allowed sizes for every thumbnail path
     *
     * @var string
     */
    private $thumbnailPrefix;

    /**
     * @var array
     */
    private $thumbnailSizeList;

    /**
     * ThumbnailsGenerator constructor
     * @param string $hostUrl
     * @param array $thumbnailSizeList
     * @param string $thumbnailPrefix
     */
    public function __construct(string $hostUrl, array $thumbnailSizeList = [], string $thumbnailPrefix = 'resized')
    {
        parent::__construct($hostUrl);
        $this->thumbnailPrefix = $thumbnailPrefix;
        $this->thumbnailSizeList = $thumbnailSizeList;
    }

    /**
     * Returns thumbnails prefix
     * @return string
     */
    public function getThumbnailPrefix(): string
    {
        return $this->thumbnailPrefix;
    }

    /**
     * Returns allowed sizes for provided entity path
     *
     * @param string $entityPath
     * @return array
     * @throws \Eternity\Components\File\Thumbnails\Exceptions\ThumbnailGeneratorException
     */
    private function getSizes(string $entityPath): array
    {
        if (!isset($this->thumbnailSizeList[$entityPath])) {
            throw new ThumbnailGeneratorException(
                ErrorCodes::THUMBNAIL_GENERATOR_ERROR,
                'Thumbnail generator error',
                'Thumbnail sizes is not set for entity:' . $entityPath
            );
        }

        return $this->thumbnailSizeList[$entityPath];
    }

    /**
     * Returns thumbnails list
     * @param int $entityId
     * @param string $filename
     * @param string $entityPath
     * @return array
     * @throws \Eternity\Components\File\Thumbnails\Exceptions\ThumbnailGeneratorException
     */
    public function getThumbnails(int $entityId, string $filename, string $entityPath): array
    {
        $thumbnails = [];
        foreach ($this->getSizes($entityPath) as $size) {
            $thumbnails[$this->getThumbnailKey($size)] = $this->getThumbnailUrl($entityId, $size, $filename, $entityPath);
        }

        return $thumbnails;
    }

    /**
     * Returns thumbnail key
     *
     * Size must be in format {width}x{height}
     * For example 200x200
     *
     * @param string $size
     * @return string
     */
    private function getThumbnailKey(string $size): string
    {
        return self::SIZE_PREFIX . $size;
    }

    /**
     * Returns absolute url to thumbnail
     *
     * @param int $entityId
     * @param string $size
     * @param string $filename
     * @param string $entityPath
     * @return string
     */
    public function getThumbnailUrl(
        int $entityId,
        string $size,
        string $filename,
        string $entityPath
    ): string {
        return $this->getHostUrl()
            . '/' . $this->getThumbnailPrefix()
            . '/' . static::getThumbnailPath($entityId, $size, $entityPath)
            . '/' . $filename;
    }

    /**
     * Returns thumbnail relative path
     *
     * @param int $entityId
     * @param string $size
     * @param string $entityPath
     * @return string
     */
    public static function getThumbnailPath(
        int $entityId,
        string $size,
        string $entityPath
    ): string {
        return $size
            . '/' . $entityPath
            . '/' . $entityId;
    }
}