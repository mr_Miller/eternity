<?php

namespace Eternity\Components\Connector\Marketplace\SubConnectors\User;

use Eternity\Components\Connector\AbstractConnector;
use Eternity\Components\Connector\AnimalId\Definitions\HeadersDefinition;
use Eternity\Components\Connector\AnimalId\Dto\PaginationDto;
use Eternity\Components\Connector\Marketplace\Dto\User\UserDto;
use Eternity\Components\Connector\Marketplace\Responses\User\UserResponse;
use Eternity\Components\Connector\Marketplace\Responses\User\UserCollectionResponse;

/**
 * Class UserConnector
 *
 * Describes methods for users management in Marketplace
 */
class UserConnector extends AbstractConnector
{

    /**
     * Returns all users
     * @param \Eternity\Components\Connector\AnimalId\Dto\PaginationDto $paginationDto
     * @param int $userId
     * @param string $languageCode
     * @param string $expand
     * @return \Eternity\Components\Connector\Marketplace\Responses\User\UserCollectionResponse
     */
    public function getUsers(
        PaginationDto $paginationDto,
        int $userId,
        string $languageCode = null,
        ?string $expand = null
    ): UserCollectionResponse {
        $headers = [
            HeadersDefinition::LANG_CODE       => $languageCode,
            HeadersDefinition::ORDER_DIRECTION => $paginationDto->orderDirection,
            HeadersDefinition::ORDER_BY        => $paginationDto->orderBy,
            HeadersDefinition::PAGE_CURRENT    => $paginationDto->pageCurrent,
            HeadersDefinition::PAGE_SIZE       => $paginationDto->pageSize,
            HeadersDefinition::EXPAND          => $expand,
            HeadersDefinition::USER_ID         => $userId,
        ];
        $response = $this->get('v1/users', 200, array_filter($headers));

        return new UserCollectionResponse($response);
    }

    /**
     * Return User by id
     *
     * @param int $id view user with ID
     * @param int $userId Current user ID
     * @param string $languageCode
     * @param string|null $expand
     *
     * @return \Eternity\Components\Connector\Marketplace\Responses\User\UserResponse
     */
    public function getById(int $id, int $userId, string $languageCode, ?string $expand = null): UserResponse
    {
        $headers = [
            HeadersDefinition::EXPAND    => $expand,
            HeadersDefinition::USER_ID   => $userId,
            HeadersDefinition::LANG_CODE => $languageCode
        ];
        $response = $this->get('v1/users/' . $id, 200, array_filter($headers));

        return new UserResponse(UserDto::fromArray($response->getPayloadItem()), $response);
    }

    /**
     * Update User fields
     *
     * @param int $id
     * @param \Eternity\Components\Connector\Marketplace\Dto\User\UserDto $userDto
     * @param string $languageCode
     */
    public function updateUser(int $id, UserDto $userDto, string $languageCode): void
    {
        $headers = [
            HeadersDefinition::USER_ID   => $id,
            HeadersDefinition::LANG_CODE => $languageCode,
        ];
        $this->put('v1/users/' . $id, 204, $headers, array_filter($userDto->toArray()));
    }

    /**
     * Update delete_at field
     *
     * @param int $id Unique animal identifier
     *
     * @param string $languageCode
     * @return void
     */
    public function deleteById(int $id, string $languageCode): void
    {
        $headers = [
            HeadersDefinition::USER_ID => $id,
            HeadersDefinition::LANG_CODE => $languageCode,
        ];

        $this->delete('v1/users/' . $id, 204, $headers);
    }

    /**
     * Activate user
     *
     * For example when email or phone is confirmed
     *
     * @param int $id
     */
    public function activate(int $id): void
    {
        $this->put('v1/users/' . $id . '/activate', 204);
    }

    /**
     * Blocks active user
     *
     * @param int $id
     * @param string $statusNotice
     */
    public function block(int $id, string $statusNotice): void
    {
        $body = [
            'status_notice' => $statusNotice,
        ];
        $this->put('v1/users/' . $id, 204, [], $body);
    }
}
