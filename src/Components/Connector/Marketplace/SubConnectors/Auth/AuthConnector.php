<?php

namespace Eternity\Components\Connector\Marketplace\SubConnectors\Auth;

use Eternity\Components\Connector\AbstractConnector;
use Eternity\Components\Connector\Marketplace\Responses\User\UserCreateResponse;
use Eternity\Definitions\HeadersDefinition;

/**
 * AUTH
 *
 * Class AuthConnector
 * @package Eternity\Components\Connector\AnimalId\SubConnectors\Auth
 */
class AuthConnector extends AbstractConnector
{
    /**
     * Register user
     *
     * ATTENTION: This method does NOT check user authorization. It's completely internal service method.
     * Do NOT use this method directly from PUBLIC controller !!!
     *
     * @param string $languageCode
     * @return \Eternity\Components\Connector\Marketplace\Responses\User\UserCreateResponse
     */
    public function register(string $languageCode): UserCreateResponse
    {
        $headers = [
            HeadersDefinition::LANG_CODE => $languageCode
        ];
        $response = $this->post('v1/auth/register', 201, $headers);
        return new UserCreateResponse($response, $response->getPayloadItem()['id']);
    }

    /**
     * Activate user
     *
     * ATTENTION: This method does NOT check user authorization. It's completely internal service method.
     * Do NOT use this method directly from PUBLIC controller !!!
     *
     * @param int $userId
     */
    public function activate(int $userId): void
    {
        $this->put('v1/auth/activate/' . $userId, 204);
    }
}