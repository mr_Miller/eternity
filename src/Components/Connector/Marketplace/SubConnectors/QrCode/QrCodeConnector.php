<?php

namespace Eternity\Components\Connector\Marketplace\SubConnectors\QrCode;

use Eternity\Components\Connector\AbstractConnector;
use Eternity\Components\Connector\AnimalId\Definitions\HeadersDefinition;
use Eternity\Components\Connector\Marketplace\Dto\QrCode\CreateQrCodeDto;
use Eternity\Components\Connector\Marketplace\Dto\QrCode\QrCodeDto;
use Eternity\Components\Connector\Marketplace\Responses\QrCode\CustomQrCodeResponse;
use Eternity\Components\Connector\Marketplace\Responses\QrCode\QrCodeResponse;

/**
 * Qr Code method connector
 *
 * Class QrCodeConnector
 *
 * @package Eternity\Components\Connector\Marketplace\QrCode
 */
class QrCodeConnector extends AbstractConnector
{

    /**
     * Create batch qrCodes method
     *
     * @param \Eternity\Components\Connector\Marketplace\Dto\QrCode\CreateQrCodeDto $qrCode
     * @param int $userId
     * @param string $languageCode
     *
     * @return \Eternity\Components\Connector\Marketplace\Responses\QrCode\CustomQrCodeResponse
     */
    public function create(CreateQrCodeDto $qrCode, int $userId, string $languageCode): CustomQrCodeResponse
    {
        $headers = [
            HeadersDefinition::USER_ID => $userId,
            HeadersDefinition::LANG_CODE => $languageCode,
        ];

        $response = $this->post('v1/qr-codes', 201, $headers, array_filter($qrCode->toArray()));

        return new CustomQrCodeResponse($response->getPayloadItem(), $response);
    }

    /**
     * Get Single
     *
     * @param int $id
     * @param int $userId
     * @param string $languageCode
     *
     * @param string|null $expand
     * @return \Eternity\Components\Connector\Marketplace\Responses\QrCode\QrCodeResponse
     */
    public function getSingle(int $id, int $userId, string $languageCode, ?string $expand): QrCodeResponse
    {
        $headers = [
            HeadersDefinition::USER_ID => $userId,
            HeadersDefinition::LANG_CODE => $languageCode,
            HeadersDefinition::EXPAND => $expand,
        ];

        $response = $this->get('v1/qr-codes/' . $id, 200, $headers);

        return new QrCodeResponse(QrCodeDto::fromArray($response->getPayloadItem()), $response);
    }

    /**
     * Get By Batch
     *
     * @param int $batchId
     * @param int $userId
     * @param string $languageCode
     *
     * @return \Eternity\Components\Connector\Marketplace\Responses\QrCode\QrCodeResponse
     */
    public function getByBatch(int $batchId, int $userId, string $languageCode): QrCodeResponse
    {
        $headers = [
            HeadersDefinition::USER_ID => $userId,
            HeadersDefinition::LANG_CODE => $languageCode,
        ];

        $response = $this->get('v1/qr-codes/batch/' . $batchId, 200, $headers);

        return new QrCodeResponse(QrCodeDto::fromArray($response->getPayloadItem()), $response);
    }

    /**
     * Get By Lot
     *
     * @param int $lotId
     * @param int $userId
     * @param string $languageCode
     *
     * @return \Eternity\Components\Connector\Marketplace\Responses\QrCode\QrCodeResponse
     */
    public function getByLot(int $lotId, int $userId, string $languageCode): QrCodeResponse
    {
        $headers = [
            HeadersDefinition::USER_ID => $userId,
            HeadersDefinition::LANG_CODE => $languageCode,
        ];

        $response = $this->get('v1/qr-codes/lot/' . $lotId, 200, $headers);

        return new QrCodeResponse(QrCodeDto::fromArray($response->getPayloadItem()), $response);
    }

    /**
     * Get By Group
     *
     * @param int $groupId
     * @param int $userId
     * @param string $languageCode
     *
     * @return \Eternity\Components\Connector\Marketplace\Responses\QrCode\QrCodeResponse
     */
    public function getByGroup(int $groupId, int $userId, string $languageCode): QrCodeResponse
    {
        $headers = [
            HeadersDefinition::USER_ID => $userId,
            HeadersDefinition::LANG_CODE => $languageCode,
        ];

        $response = $this->get('v1/qr-codes/group/' . $groupId, 200, $headers);

        return new QrCodeResponse(QrCodeDto::fromArray($response->getPayloadItem()), $response);
    }

    /**
     * Delete
     *
     * @param int    $id
     * @param int    $userId
     * @param string $languageCode
     *
     * @return void
     */
    public function deleteById(int $id, int $userId, string $languageCode): void
    {
        $headers = [
            HeadersDefinition::USER_ID => $userId,
            HeadersDefinition::LANG_CODE => $languageCode,
        ];

        $this->delete('v1/qr-codes/' . $id, 204, $headers);
    }

    /**
     * Print
     *
     * @param int    $batchId
     * @param int    $userId
     * @param string $languageCode
     *
     * @return void
     */
    public function print(int $batchId, int $userId, string $languageCode): void
    {
        $headers = [
            HeadersDefinition::USER_ID => $userId,
            HeadersDefinition::LANG_CODE => $languageCode,
        ];

        $this->post('v1/qr-codes/print/' . $batchId, 204, $headers);
    }

    /**
     * Check Archive
     *
     * @param string $archiveName
     * @param int $userId
     * @param string $languageCode
     *
     * @return \Eternity\Components\Connector\Marketplace\Responses\QrCode\CustomQrCodeResponse
     */
    public function checkArchive(string $archiveName, int $userId, string $languageCode): CustomQrCodeResponse
    {
        $headers = [
            HeadersDefinition::USER_ID => $userId,
            HeadersDefinition::LANG_CODE => $languageCode,
        ];
        $body = [
            'archive_name' => $archiveName,
        ];

        $response = $this->post('v1/qr-codes/check-archive', 200, $headers, $body);

        return new CustomQrCodeResponse($response->getPayloadItem(), $response);
    }

    /**
     * Download Archive
     *
     * @param string $archiveName
     * @param int $userId
     * @param string $languageCode
     *
     * @return \Eternity\Components\Connector\Marketplace\Responses\QrCode\CustomQrCodeResponse
     */
    public function download(string $archiveName, int $userId, string $languageCode): CustomQrCodeResponse
    {
        $headers = [
            HeadersDefinition::USER_ID => $userId,
            HeadersDefinition::LANG_CODE => $languageCode,
        ];
        $body = [
            'filename' => $archiveName,
        ];

        $response = $this->post('v1/qr-codes/download', 200, $headers, $body);

        return new CustomQrCodeResponse($response->getPayloadItem(), $response);
    }

    /**
     * Matching qr code and product
     *
     * @param int    $batchId
     * @param int    $productId
     * @param int    $userId
     * @param string $languageCode
     *
     * @return void
     */
    public function matching(int $batchId, int $productId, int $userId, string $languageCode): void
    {
        $headers = [
            HeadersDefinition::USER_ID => $userId,
            HeadersDefinition::LANG_CODE => $languageCode,
        ];
        $body = [
            'batch_id' => $batchId,
            'product_id' => $productId,
        ];

        $this->post('v1/qr-codes/matching', 204, $headers, $body);
    }

    /**
     * Reserve
     *
     * @param string $secretCode
     *
     * @param int    $userId
     * @param string $languageCode
     *
     * @return void
     */
    public function reserve(string $secretCode, int $userId, string $languageCode): void
    {
        $headers = [
            HeadersDefinition::USER_ID => $userId,
            HeadersDefinition::LANG_CODE => $languageCode,
        ];
        $body = [
            'secret_code' => $secretCode,
        ];

        $this->post('v1/qr-codes/reserve', 204, $headers, $body);
    }

    /**
     * Activate codes
     *
     * @param int    $animalId
     *
     * @param int    $userId
     * @param string $languageCode
     *
     * @return void
     */
    public function activateCodes(int $animalId, int $userId, string $languageCode): void
    {
        $headers = [
            HeadersDefinition::USER_ID => $userId,
            HeadersDefinition::LANG_CODE => $languageCode,
        ];
        $body = [
            'animal_id' => $animalId,
        ];

        $this->post('v1/qr-codes/activate-reserved', 204, $headers, $body);
    }

    /**
     * Activate codes
     *
     * @param int    $animalId
     * @param string $secretCode
     * @param int    $userId
     * @param string $languageCode
     *
     * @return void
     */
    public function activateCode(int $animalId, string $secretCode, int $userId, string $languageCode): void
    {
        $headers = [
            HeadersDefinition::USER_ID => $userId,
            HeadersDefinition::LANG_CODE => $languageCode,
        ];
        $body = [
            'animal_id' => $animalId,
            'secret_code' => $secretCode,
        ];

        $this->post('v1/qr-codes/activate', 204, $headers, $body);
    }

    /**
     * Scan code
     *
     * @param string $secretCode
     * @param int    $userId
     * @param string $languageCode
     *
     * @return void
     */
    public function scan(string $secretCode, int $userId, string $languageCode): void
    {
        $headers = [
            HeadersDefinition::USER_ID => $userId,
            HeadersDefinition::LANG_CODE => $languageCode,
        ];
        $body = [
            'secret_code' => $secretCode,
        ];

        $this->post('v1/qr-codes/scan', 204, $headers, $body);
    }
}