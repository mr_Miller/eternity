<?php

namespace Eternity\Components\Connector\Marketplace\SubConnectors\Campaign;

use Eternity\Components\Connector\AbstractConnector;
use Eternity\Components\Connector\AnimalId\Definitions\HeadersDefinition;
use Eternity\Components\Connector\Marketplace\Dto\Campaign\CampaignDto;
use Eternity\Components\Connector\Marketplace\Dto\Campaign\CampaignProductsDto;
use Eternity\Components\Connector\Marketplace\Responses\Campaign\CampaignResponse;
use Eternity\Components\Connector\Marketplace\Responses\Campaign\CreatedCampaignResponse;

/**
 * Campaign method connector
 *
 * Class CampaignConnector
 *
 * @package Eternity\Components\Connector\Marketplace\Campaign
 */
class CampaignConnector extends AbstractConnector
{
    /**
     * Get Campaign
     *
     * @param int $id
     * @param int $userId
     *
     * @param string $languageCode
     *
     * @return \Eternity\Components\Connector\Marketplace\Responses\Campaign\CampaignResponse
     */
    public function getById(int $id, int $userId, string $languageCode): CampaignResponse
    {
        $headers = [
            HeadersDefinition::USER_ID => $userId,
            HeadersDefinition::LANG_CODE => $languageCode,
        ];
        $response = $this->get('v1/campaigns/' . $id, 200, $headers);

        return new CampaignResponse(CampaignDto::fromArray($response->getPayloadItem()), $response);
    }

    /**
     * Create Campaign
     *
     * @param \Eternity\Components\Connector\Marketplace\Dto\Campaign\CampaignDto $campaignDto
     * @param int $userId
     * @param string $languageCode
     *
     * @return \Eternity\Components\Connector\Marketplace\Responses\Campaign\CreatedCampaignResponse
     */
    public function create(CampaignDto $campaignDto, int $userId, string $languageCode): CreatedCampaignResponse
    {
        $headers = [
            HeadersDefinition::USER_ID => $userId,
            HeadersDefinition::LANG_CODE => $languageCode,
        ];
        $response = $this->post('v1/campaigns', 201, $headers, array_filter($campaignDto->toArray()));

        return new CreatedCampaignResponse($response->getPayloadItem()['id'], $response);
    }

    /**
     * Update Campaign
     *
     * @param int $id
     * @param \Eternity\Components\Connector\Marketplace\Dto\Campaign\CampaignDto $campaignDto
     * @param int $userId
     * @param string $languageCode
     *
     * @return void
     */
    public function update(int $id, CampaignDto $campaignDto, int $userId, string $languageCode): void
    {
        $headers = [
            HeadersDefinition::USER_ID => $userId,
            HeadersDefinition::LANG_CODE => $languageCode,
        ];
        $this->put('v1/campaigns/' . $id, 204, $headers, array_filter($campaignDto->toArray()));
    }

    /**
     * Delete Campaign
     *
     * @param int    $id
     * @param int    $userId
     * @param string $languageCode
     *
     * @return void
     */
    public function deleteById(int $id, int $userId, string $languageCode): void
    {
        $headers = [
            HeadersDefinition::USER_ID => $userId,
            HeadersDefinition::LANG_CODE => $languageCode,
        ];
        $this->delete('v1/campaigns/' . $id, 204, $headers);
    }

    /**
     * Activate Campaign
     *
     * @param int    $id
     * @param int    $userId
     * @param string $languageCode
     *
     * @return void
     */
    public function activate(int $id, int $userId, string $languageCode): void
    {
        $headers = [
            HeadersDefinition::USER_ID => $userId,
            HeadersDefinition::LANG_CODE => $languageCode,
        ];
        $this->put('v1/campaigns/' . $id . '/activate', 204, $headers);
    }

    /**
     * Block Campaign
     *
     * @param int    $id
     * @param int    $userId
     * @param string $languageCode
     *
     * @return void
     */
    public function block(int $id, int $userId, string $languageCode): void
    {
        $headers = [
            HeadersDefinition::USER_ID => $userId,
            HeadersDefinition::LANG_CODE => $languageCode,
        ];
        $this->put('v1/campaigns/' . $id . '/block', 204, $headers);
    }

    /**
     * Matching Product to Campaign
     *
     * @param int $id
     * @param \Eternity\Components\Connector\Marketplace\Dto\Campaign\CampaignProductsDto $dto
     * @param int $userId
     * @param string $languageCode
     *
     * @return void
     */
    public function matching(int $id, CampaignProductsDto $dto, int $userId, string $languageCode): void
    {
        $headers = [
            HeadersDefinition::USER_ID => $userId,
            HeadersDefinition::LANG_CODE => $languageCode,
        ];
        $this->post('v1/campaigns/' . $id . '/matching', 204, $headers, $dto->toArray());
    }
}