<?php

namespace Eternity\Components\Connector\Marketplace\SubConnectors\Product;

use Eternity\Components\Connector\AbstractConnector;
use Eternity\Components\Connector\AnimalId\Definitions\HeadersDefinition;
use Eternity\Components\Connector\AnimalId\Dto\PaginationDto;
use Eternity\Components\Connector\Marketplace\Dto\Product\TranslationCreateDto;
use Eternity\Components\Connector\Marketplace\Dto\Product\TranslationDto;
use Eternity\Components\Connector\Marketplace\Dto\Product\TranslationUpdateDto;
use Eternity\Components\Connector\Marketplace\Responses\Product\TranslationCollectionResponse;
use Eternity\Components\Connector\Marketplace\Responses\Product\TranslationCreateResponse;
use Eternity\Components\Connector\Marketplace\Responses\Product\TranslationResponse;

/**
 * Class TranslationConnector
 * @package Eternity\Components\Connector\Marketplace\SubConnectors\Product
 */
class TranslationConnector extends AbstractConnector
{
    /**
     * Returns product translation by ID
     * @param int $id
     * @param int $userId
     * @param string $languageCode
     * @return \Eternity\Components\Connector\Marketplace\Responses\Product\TranslationResponse
     */
    public function getById(int $id, int $userId, string $languageCode): TranslationResponse
    {
        $headers = [
            HeadersDefinition::USER_ID   => $userId,
            HeadersDefinition::LANG_CODE => $languageCode
        ];
        $response = $this->get('v1/products/translations/' . $id, 200, array_filter($headers));

        return new TranslationResponse($response, TranslationDto::fromArray($response->getPayloadItem()));
    }

    /**
     * Get list of translation by Product ID
     * @param \Eternity\Components\Connector\AnimalId\Dto\PaginationDto $paginationDto
     * @param int $productId
     * @param int $userId
     * @param string $languageCode
     * @return \Eternity\Components\Connector\Marketplace\Responses\Product\TranslationCollectionResponse
     */
    public function getList(
        PaginationDto $paginationDto,
        int $productId,
        int $userId,
        string $languageCode
    ): TranslationCollectionResponse {
        $headers = [
            HeadersDefinition::USER_ID         => $userId,
            HeadersDefinition::LANG_CODE       => $languageCode,
            HeadersDefinition::ORDER_DIRECTION => $paginationDto->orderDirection,
            HeadersDefinition::ORDER_BY        => $paginationDto->orderBy,
            HeadersDefinition::PAGE_CURRENT    => $paginationDto->pageCurrent,
            HeadersDefinition::PAGE_SIZE       => $paginationDto->pageSize,
        ];
        $response = $this->get(
            'v1/products/' . $productId . '/translations',
            200,
            array_filter($headers)
        );

        return new TranslationCollectionResponse($response);
    }

    /**
     * Create translation
     * @param \Eternity\Components\Connector\Marketplace\Dto\Product\TranslationCreateDto $createDto
     * @param int $userId
     * @param string $languageCode
     * @return \Eternity\Components\Connector\Marketplace\Responses\Product\TranslationCreateResponse
     */
    public function create(
        TranslationCreateDto $createDto,
        int $userId,
        string $languageCode
    ): TranslationCreateResponse {
        $headers = [
            HeadersDefinition::USER_ID   => $userId,
            HeadersDefinition::LANG_CODE => $languageCode
        ];
        $response = $this->post(
            'v1/products/translations',
            201,
            array_filter($headers),
            $createDto->toArray()
        );

        return new TranslationCreateResponse($response, $response->getPayloadItem()['id']);
    }

    /**
     * Update translation
     * @param int $translationId
     * @param \Eternity\Components\Connector\Marketplace\Dto\Product\TranslationUpdateDto $translationUpdateDto
     * @param int $userId
     * @param string $languageCode
     */
    public function update(int $translationId, TranslationUpdateDto $translationUpdateDto, int $userId,string $languageCode): void
    {
        $headers = [
            HeadersDefinition::USER_ID   => $userId,
            HeadersDefinition::LANG_CODE => $languageCode
        ];
        $this->put(
            'v1/products/translations/'. $translationId,
            204,
            array_filter($headers),
            $translationUpdateDto->toArray()
        );
    }

    /**
     * Delete product translation
     * @param int $translationId
     * @param int $userId
     * @param string $languageCode
     */
    public function deleteTranslation(int $translationId, int $userId, string $languageCode): void
    {
        $headers = [
            HeadersDefinition::USER_ID   => $userId,
            HeadersDefinition::LANG_CODE => $languageCode
        ];

        $this->delete('v1/products/translations/' . $translationId, 204, array_filter($headers));
    }
}