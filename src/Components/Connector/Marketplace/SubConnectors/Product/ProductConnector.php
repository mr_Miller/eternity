<?php

namespace Eternity\Components\Connector\Marketplace\SubConnectors\Product;

use Eternity\Components\Connector\AbstractConnector;
use Eternity\Components\Connector\AnimalId\Definitions\HeadersDefinition;
use Eternity\Components\Connector\AnimalId\Dto\PaginationDto;
use Eternity\Components\Connector\Marketplace\Dto\Product\ProductCreateDto;
use Eternity\Components\Connector\Marketplace\Dto\Product\ProductDto;
use Eternity\Components\Connector\Marketplace\Dto\Product\ProductUpdateDto;
use Eternity\Components\Connector\Marketplace\Responses\Product\ProductCollectionResponse;
use Eternity\Components\Connector\Marketplace\Responses\Product\ProductCreateResponse;
use Eternity\Components\Connector\Marketplace\Responses\Product\ProductResponse;

/**
 * Class ProductConnector
 * @package Eternity\Components\Connector\Marketplace\SubConnectors\Product
 */
class ProductConnector extends AbstractConnector
{
    /**
     * Get
     *
     * @param int $productId
     * @param int $userId
     * @param string $languageCode Language Code
     * @param string|null $expand Expands list in json string
     * @return \Eternity\Components\Connector\Marketplace\Responses\Product\ProductResponse
     */
    public function getProduct(int $productId, int $userId, string $languageCode, ?string $expand): ProductResponse
    {
        $headers = [
            HeadersDefinition::LANG_CODE => $languageCode,
            HeadersDefinition::USER_ID   => $userId,
            HeadersDefinition::EXPAND    => $expand
        ];
        $response = $this->get('v1/products/' . $productId, 200, array_filter($headers));

        return new ProductResponse(ProductDto::fromArray($response->getPayloadItem()), $response);
    }

    /**
     * Get paginated list of products
     * @param \Eternity\Components\Connector\AnimalId\Dto\PaginationDto $paginationDto
     * @param int $productCategoryId
     * @param int $userId
     * @param string $languageCode
     * @param string|null $expand
     * @return \Eternity\Components\Connector\Marketplace\Responses\Product\ProductCollectionResponse
     */
    public function getListByCategory(
        PaginationDto $paginationDto,
        int $productCategoryId,
        int $userId,
        string $languageCode,
        ?string $expand
    ): ProductCollectionResponse {
        $headers = [
            HeadersDefinition::LANG_CODE       => $languageCode,
            HeadersDefinition::USER_ID         => $userId,
            HeadersDefinition::ORDER_DIRECTION => $paginationDto->orderDirection,
            HeadersDefinition::ORDER_BY        => $paginationDto->orderBy,
            HeadersDefinition::PAGE_CURRENT    => $paginationDto->pageCurrent,
            HeadersDefinition::PAGE_SIZE       => $paginationDto->pageSize,
            HeadersDefinition::EXPAND          => $expand,
        ];
        $response = $this->get(
            'v1/product-categories/' . $productCategoryId . '/products',
            200,
            array_filter($headers)
        );

        return new ProductCollectionResponse($response);
    }

    /**
     * Create product
     *
     * @param \Eternity\Components\Connector\Marketplace\Dto\Product\ProductCreateDto $productCreateDto
     * @param int $userId Local marketplace user ID
     * @param string $languageCode
     * @return \Eternity\Components\Connector\Marketplace\Responses\Product\ProductCreateResponse
     */
    public function create(
        ProductCreateDto $productCreateDto,
        int $userId,
        string $languageCode
    ): ProductCreateResponse {
        $headers = [
            HeadersDefinition::LANG_CODE => $languageCode,
            HeadersDefinition::USER_ID   => $userId,
        ];
        $response = $this->post(
            'v1/products',
            201,
            array_filter($headers),
            $productCreateDto->toArray()
        );

        return new ProductCreateResponse($response->getPayloadItem()['id'], $response);
    }

    /**
     * Update product
     *
     * @param int $productId
     * @param \Eternity\Components\Connector\Marketplace\Dto\Product\ProductUpdateDto $productUpdateDto
     * @param int $userId
     * @param string $languageCode
     */
    public function update(int $productId, ProductUpdateDto $productUpdateDto, int $userId, string $languageCode): void
    {
        $headers = [
            HeadersDefinition::USER_ID   => $userId,
            HeadersDefinition::LANG_CODE => $languageCode,
        ];
        $this->put(
            'v1/products/' . $productId,
            204,
            $headers,
            $productUpdateDto->toArray()
        );
    }

    /**
     * Delete
     *
     * @param int $productId
     * @param int $userId
     * @param string $languageCode
     */
    public function remove(int $productId, int $userId, string $languageCode): void
    {
        $headers = [
            HeadersDefinition::USER_ID => $userId,
            HeadersDefinition::LANG_CODE => $languageCode
        ];
        $this->delete('v1/products/' . $productId, 204, $headers);
    }
}