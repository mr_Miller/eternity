<?php

namespace Eternity\Components\Connector\Marketplace\SubConnectors\Product;

use Eternity\Components\Connector\AbstractConnector;
use Eternity\Components\Connector\AnimalId\Definitions\HeadersDefinition;
use Eternity\Components\Connector\AnimalId\Dto\PaginationDto;
use Eternity\Components\Connector\Marketplace\Dto\Product\PhotoCreateDto;
use Eternity\Components\Connector\Marketplace\Responses\Product\PhotoCollectionResponse;
use Eternity\Components\Connector\Marketplace\Responses\Product\PhotoCreateResponse;

/**
 * Class PhotoConnector
 * @package Eternity\Components\Connector\Marketplace\SubConnectors\Product
 */
class PhotoConnector extends AbstractConnector
{
    /**
     * Update main photo of a product
     *
     * To unset main photo send null as value of $photoId
     *
     * @param int $photoId
     * @param int $productId
     * @param int $userId
     * @param string $languageCode
     * @return void
     */
    public function updateMain(?int $photoId, int $productId, int $userId, string $languageCode): void
    {
        $headers = [
            HeadersDefinition::LANG_CODE => $languageCode,
            HeadersDefinition::USER_ID   => $userId,
        ];
        $this->put('v1/products/' . $productId . '/main-photos', 204, array_filter($headers), [
            'photo_id' => $photoId
        ]);
    }

    /**
     * Add photo to product
     *
     * @param \Eternity\Components\Connector\Marketplace\Dto\Product\PhotoCreateDto $createDto
     * @param int $productId
     * @param int $userId
     * @param string $languageCode
     * @return \Eternity\Components\Connector\Marketplace\Responses\Product\PhotoCreateResponse
     */
    public function create(
        PhotoCreateDto $createDto,
        int $productId,
        int $userId,
        string $languageCode
    ): PhotoCreateResponse {
        $headers = [
            HeadersDefinition::LANG_CODE => $languageCode,
            HeadersDefinition::USER_ID   => $userId,
        ];
        $response = $this->post(
            'v1/products/' . $productId . '/photos',
            201,
            array_filter($headers),
            $createDto->toArray()
        );

        return new PhotoCreateResponse($response->getPayloadItem()['id'], $response);
    }

    /**
     * Delete photo
     *
     * @param int $photoId
     * @param int $productId
     * @param int $userId
     * @param string $languageCode
     * @return \Eternity\Http\Contracts\ExtendedResponse|void
     */
    public function deletePhoto(int $photoId, int $productId, int $userId, string $languageCode): void
    {
        $headers = [
            HeadersDefinition::LANG_CODE => $languageCode,
            HeadersDefinition::USER_ID   => $userId,
        ];
        $this->delete(
            'v1/products/' . $productId . '/photos/' . $photoId,
            204,
            array_filter($headers)
        );
    }

    /**
     * Get photos list for product
     *
     * @param \Eternity\Components\Connector\AnimalId\Dto\PaginationDto $paginationDto
     * @param int $productId
     * @param int $userId
     * @param string $languageCode
     * @return \Eternity\Components\Connector\Marketplace\Responses\Product\PhotoCollectionResponse
     */
    public function getList(
        int $productId,
        PaginationDto $paginationDto,
        int $userId,
        string $languageCode
    ): PhotoCollectionResponse {
        $headers = [
            HeadersDefinition::LANG_CODE       => $languageCode,
            HeadersDefinition::USER_ID         => $userId,
            HeadersDefinition::PAGE_CURRENT    => $paginationDto->pageCurrent,
            HeadersDefinition::PAGE_SIZE       => $paginationDto->pageSize,
            HeadersDefinition::ORDER_BY        => $paginationDto->orderBy,
            HeadersDefinition::ORDER_DIRECTION => $paginationDto->orderDirection
        ];
        $response = $this->get('v1/products/' . $productId . '/photos', 200, array_filter($headers));

        return new PhotoCollectionResponse($response);
    }
}