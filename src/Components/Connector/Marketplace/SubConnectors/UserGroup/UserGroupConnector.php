<?php

namespace Eternity\Components\Connector\Marketplace\SubConnectors\UserGroup;

use Eternity\Components\Connector\AbstractConnector;
use Eternity\Components\Connector\AnimalId\Definitions\HeadersDefinition;
use Eternity\Components\Connector\AnimalId\Dto\PaginationDto;
use Eternity\Components\Connector\Marketplace\Dto\UserGroup\UserGroupDto;
use Eternity\Components\Connector\Marketplace\Responses\UserGroup\CreatedUserGroupResponse;
use Eternity\Components\Connector\Marketplace\Responses\UserGroup\UserGroupCollectionResponse;
use Eternity\Components\Connector\Marketplace\Responses\UserGroup\UserGroupResponse;

/**
 * Class UserGroupConnector
 *
 * Describes methods for user group management in Marketplace
 */
class UserGroupConnector extends AbstractConnector
{

    /**
     * Returns all user groups
     * @param \Eternity\Components\Connector\AnimalId\Dto\PaginationDto $paginationDto
     * @param int $userId
     * @param string $languageCode
     * @param string $expand
     * @return \Eternity\Components\Connector\Marketplace\Responses\UserGroup\UserGroupCollectionResponse
     */
    public function getUserGroups(
        PaginationDto $paginationDto,
        int $userId,
        string $languageCode = null,
        ?string $expand = null
    ): UserGroupCollectionResponse {
        $headers = [
            HeadersDefinition::LANG_CODE       => $languageCode,
            HeadersDefinition::ORDER_DIRECTION => $paginationDto->orderDirection,
            HeadersDefinition::ORDER_BY        => $paginationDto->orderBy,
            HeadersDefinition::PAGE_CURRENT    => $paginationDto->pageCurrent,
            HeadersDefinition::PAGE_SIZE       => $paginationDto->pageSize,
            HeadersDefinition::EXPAND          => $expand,
            HeadersDefinition::USER_ID         => $userId,
        ];
        $response = $this->get('v1/user-groups', 200, array_filter($headers));

        return new UserGroupCollectionResponse($response);
    }

    /**
     * Get
     *
     * @param int $id
     * @param int $userId
     * @param string $languageCode
     *
     * @return \Eternity\Components\Connector\Marketplace\Responses\UserGroup\UserGroupResponse
     */
    public function getById(int $id, int $userId, string $languageCode): UserGroupResponse
    {
        $headers = [
            HeadersDefinition::LANG_CODE => $languageCode,
            HeadersDefinition::USER_ID   => $userId,
        ];
        $response = $this->get('v1/user-groups/' . $id, 200, $headers);

        return new UserGroupResponse(UserGroupDto::fromArray($response->getPayloadItem()), $response);
    }

    /**
     * Create
     *
     * @param \Eternity\Components\Connector\Marketplace\Dto\UserGroup\UserGroupDto $groupDto
     * @param int $userId
     * @param string $languageCode
     *
     * @return \Eternity\Components\Connector\Marketplace\Responses\UserGroup\CreatedUserGroupResponse
     */
    public function create(UserGroupDto $groupDto, int $userId, string $languageCode): CreatedUserGroupResponse
    {
        $headers = [
            HeadersDefinition::LANG_CODE => $languageCode,
            HeadersDefinition::USER_ID   => $userId,
        ];

        $response = $this->post('v1/user-groups', 201, $headers, array_filter($groupDto->toArray()));

        return new CreatedUserGroupResponse($response->getPayloadItem()['id'], $response);
    }

    /**
     * Update
     *
     * @param int $id
     * @param int $userId
     * @param \Eternity\Components\Connector\Marketplace\Dto\UserGroup\UserGroupDto $groupDto
     * @param string $languageCode
     *
     * @return void
     */
    public function update(int $id, int $userId, UserGroupDto $groupDto, string $languageCode): void
    {
        $headers = [
            HeadersDefinition::LANG_CODE => $languageCode,
            HeadersDefinition::USER_ID   => $userId,
        ];
        $this->put('v1/user-groups/' . $id, 204, $headers, array_filter($groupDto->toArray()));
    }

    /**
     * Delete
     *
     * @param int $id
     *
     * @return void
     */
    public function deleteById(int $id): void
    {
        $this->delete('v1/user-groups/' . $id, 204);
    }
}
