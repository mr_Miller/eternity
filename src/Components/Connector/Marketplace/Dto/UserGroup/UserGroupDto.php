<?php

namespace Eternity\Components\Connector\Marketplace\Dto\UserGroup;

use Eternity\Contracts\Arrayable;
use Eternity\Traits\FromArray;

/**
 * Class UserGroupDto
 *
 */
class UserGroupDto implements Arrayable
{
    use FromArray;

    /**
     * @var int $id ID
     */
    public $id;

    /**
     * @var int $owner_user_id Owner User Group
     */
    public $owner_user_id;

    /**
     * @var int $status Status
     */
    public $status;

    /**
     * @var string $status_notice Status Notice
     */
    public $status_notice;

    /**
     * @var int $country_id Country ID
     */
    public $country_id;

    /**
     * @var int $language_id Language ID
     */
    public $language_id;

    /**
     * @var string $title title group
     */
    public $title;

    /**
     * @var string $description description group
     */
    public $description;

    /**
     * @var string $city_name city group
     */
    public $city_name;

    /**
     * @var string $address Full address group
     */
    public $address;

    /**
     * Transforms object into array
     *
     * @return array
     */
    public function toArray(): array
    {
        return [
            'id' => $this->id,
            'owner_user_id' => $this->owner_user_id,
            'status' => $this->status,
            'status_notice' => $this->status_notice,
            'country_id' => $this->country_id,
            'language_id' => $this->language_id,
            'title' => $this->title,
            'description' => $this->description,
            'city_name' => $this->city_name,
            'address' => $this->address,
        ];
    }
}
