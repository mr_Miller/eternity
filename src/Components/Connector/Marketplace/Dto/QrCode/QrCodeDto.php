<?php

namespace Eternity\Components\Connector\Marketplace\Dto\QrCode;

use Eternity\Contracts\Arrayable;
use Eternity\Traits\FromArray;

/**
 * Qr Code Connector Dto
 *
 * Class QrCodeDto
 */
class QrCodeDto implements Arrayable
{
    use FromArray;

    /**
     * @var int $id ID
     */
    public $id;

    /**
     * @var string $secret_code Unique secret code that identifies the product (is planed to use UUID for this purpose)
     */
    public $secret_code;

    /**
     * @var string $serial_number Serial number of single produced QR code sticker. Make it according to some serial number standard.
     */
    public $serial_number;

    /**
     * @var int $batch ID of one package of QR codes. Is planned to have 100 QR codes in one package.
     */
    public $batch;

    /**
     * @var int $lot ID of one box of packages. Is planned to have 100 packages in one box.
     */
    public $lot;

    /**
     * @var int $status QR code status. Possible: New, producing confirmed (printed), attached to product, used.
     */
    public $status;

    /**
     * @var int $product_id Attached Product ID.
     */
    public $product_id;

    /**
     * @var int $created_at Creation timestamp
     */
    public $created_at;

    /**
     * @var int $updated_at Update timestamp
     */
    public $updated_at;

    /**
     * @var int $deleted_at Deletion timestamp
     */
    public $deleted_at;

    /**
     * @var int $created_by User ID that created a record
     */
    public $created_by;

    /**
     * @var int $updated_by User ID that created a record
     */
    public $updated_by;

    /**
     * @inheritDoc
     */
    public function toArray(): array
    {
        return [
            'id' => $this->id,
            'secret_code' => $this->secret_code,
            'serial_number' => $this->serial_number,
            'batch' => $this->batch,
            'lot' => $this->lot,
            'status' => $this->status,
            'product_id' => $this->product_id,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
            'deleted_at' => $this->deleted_at,
            'created_by' => $this->created_by,
            'updated_by' => $this->updated_by,
        ];
    }
}
