<?php


namespace Eternity\Components\Connector\Marketplace\Dto\QrCode;


use Eternity\Contracts\Arrayable;
use Eternity\Traits\FromArray;

class CreateQrCodeDto implements Arrayable
{
    use FromArray;

    /**
     * @var int $count QrCode Count
     */
    public $count;

    /**
     * @inheritDoc
     */
    public function toArray(): array
    {
        return [
            'count' => $this->count
        ];
    }
}