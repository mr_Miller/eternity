<?php

namespace Eternity\Components\Connector\Marketplace\Dto\User;

use Eternity\Contracts\Arrayable;
use Eternity\Traits\FromArray;

/**
 * Class UserDto
 *
 */
class UserDto implements Arrayable
{
    use FromArray;

    /**
     * @var int $id ID
     */
    public $id;

    /**
     * @var string first_name First name
     */
    public $first_name;

    /**
     * @var string $last_name Last name
     */
    public $last_name;

    /**
     * @var string $last_name Middle name or patronymic
     */
    public $middle_name;

    /**
     * @var int $birth_date Birth date in timestamp
     */
    public $birth_date;

    /**
     * @var int $sex Person sex. Male or Female.
     */
    public $sex;

    /**
     * @var int $country_id Country ID.
     */
    public $country_id;

    /**
     * @var int $language_id Chosen language ID.
     */
    public $language_id;

    /**
     * @var int $user_group_id User Group ID
     */
    public $user_group_id;

    /**
     * @var int $global_user_id Global User ID
     */
    public $global_user_id;

    /**
     * @var int $created_at Creation timestamp
     */
    public $created_at;

    /**
     * @var int $updated_at Update timestamp
     */
    public $updated_at;

    /**
     * @var
     */
    public $deleted_at;

    /**
     * @var
     */
    public $status;

    /**
     * @var
     */
    public $status_notice;

    /**
     * Transforms object into array
     *
     * @return array
     */
    public function toArray(): array
    {
        return [
            'id'            => $this->id,
            'first_name'    => $this->first_name,
            'last_name'     => $this->last_name,
            'middle_name'   => $this->middle_name,
            'birth_date'    => $this->birth_date,
            'sex'           => $this->sex,
            'country_id'    => $this->country_id,
            'language_id'   => $this->language_id,
            'user_group_id' => $this->user_group_id,
            'created_at'    => $this->created_at,
            'updated_at'    => $this->updated_at,
            'deleted_at'    => $this->deleted_at,
            'status'        => $this->status,
            'status_notice' => $this->status_notice
        ];
    }
}
