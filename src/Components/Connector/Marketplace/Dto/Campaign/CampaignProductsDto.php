<?php


namespace Eternity\Components\Connector\Marketplace\Dto\Campaign;


use Eternity\Contracts\Arrayable;
use Eternity\Traits\FromArray;

/**
 * Campaign Products Connector Dto
 *
 * Class CampaignProductsDto
 *
 * @package Eternity\Components\Connector\Marketplace\Campaign
 */
class CampaignProductsDto implements Arrayable
{
    use FromArray;

    /**
     * @var int $product_id Product ID
     */
    public $product_id;

    /**
     * @var int $product_count Product Count
     */
    public $product_count;

    /**
     * Transforms object into array
     *
     * @return array
     */
    public function toArray(): array
    {
        return [
            'product_id' => $this->product_id,
            'product_count' => $this->product_count,
        ];
    }
}