<?php

namespace Eternity\Components\Connector\Marketplace\Dto\Campaign;

use Eternity\Contracts\Arrayable;
use Eternity\Traits\FromArray;

/**
 * Campaign Connector Dto
 *
 * Class CampaignDto
 */
class CampaignDto implements Arrayable
{
    use FromArray;

    /**
     * @var string $title Title
     */
    public $title;

    /**
     * @var string $description Description
     */
    public $description;    

    /**
     * @var float $bonus_value Bonus Value
     */
    public $bonus_value;

    /**
     * @var int $bonus_type Bonus Type
     */
    public $bonus_type;

    /**
     * @var string $start_time Start Time
     */
    public $start_time;

    /**
     * @var string $end_time End Time
     */
    public $end_time;

    /**
     * @var int $max_points_amount Max points amount
     */
    public $max_points_amount;

    /**
     * Transforms object into array
     *
     * @return array
     */
    public function toArray(): array
    {
        return [
            'title' => $this->title,
            'description' => $this->description,
            'bonus_value' => $this->bonus_value,
            'bonus_type' => $this->bonus_type,
            'start_time' => $this->start_time,
            'end_time' => $this->end_time,
            'max_points_amount' => $this->max_points_amount,
        ];
    }
}
