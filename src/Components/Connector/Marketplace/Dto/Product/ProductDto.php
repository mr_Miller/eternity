<?php

namespace Eternity\Components\Connector\Marketplace\Dto\Product;

use Eternity\Contracts\Arrayable;
use Eternity\Traits\FromArray;

/**
 * Product DTO
 *
 * Class ProductDto
 * @package Eternity\Components\Connector\Marketplace\Dto\Product
 */
class ProductDto implements Arrayable
{
    use FromArray;

    /**
     * @var int $id ID
     */
    public $id;

    /**
     * @var float $price Price
     */
    public $price;

    /**
     * @var int $user_group_id User Group ID
     */
    public $user_group_id;

    /**
     * @var int $product_category_id Product Category ID
     */
    public $product_category_id;

    /**
     * @var int $created_at Creation timestamp
     */
    public $created_at;

    /**
     * @var int $updated_at Update timestamp
     */
    public $updated_at;

    /**
     * @var int $deleted_at Deletion timestamp
     */
    public $deleted_at;

    /**
     * @var int $created_by User ID that created a record
     */
    public $created_by;

    /**
     * @var int $updated_by User ID that created a record
     */
    public $updated_by;

    /**
     * @var int $main_photo_id Product main photo ID
     */
    public $main_photo_id;

    /**
     * @var array|null
     */
    public $category;

    /**
     * @var array|null
     */
    public $main_photo;

    /**
     * Transforms object into array
     *
     * @return array
     */
    public function toArray(): array
    {
        return [
            'id'                  => $this->id,
            'price'               => $this->price,
            'user_group_id'       => $this->user_group_id,
            'product_category_id' => $this->product_category_id,
            'main_photo_id'       => $this->main_photo_id,
            'created_at'          => $this->created_at,
            'updated_at'          => $this->updated_at,
            'deleted_at'          => $this->deleted_at,
            'created_by'          => $this->created_by,
            'updated_by'          => $this->updated_by,
            'category'            => $this->category,
            'main_photo'          => $this->main_photo,
        ];
    }
}