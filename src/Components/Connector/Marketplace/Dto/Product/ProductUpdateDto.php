<?php

namespace Eternity\Components\Connector\Marketplace\Dto\Product;

use Eternity\Contracts\Arrayable;
use Eternity\Traits\FromArray;

/**
 * Class ProductUpdateDto
 * @package Eternity\Components\Connector\Marketplace\Dto\Product
 */
class ProductUpdateDto implements Arrayable
{
    use FromArray;

    /**
     * @var float $price Price
     */
    public $price;

    /**
     * @var int $product_category_id Product Category ID
     */
    public $product_category_id;

    /**
     * @var integer
     */
    public $main_photo_id;

    /**
     * @var string $title Title
     */
    public $title;

    /**
     * @var string $short_desc Short description
     */
    public $short_desc;

    /**
     * @var string $description Description
     */
    public $description;

    /**
     * Transforms object into array
     *
     * @return array
     */
    public function toArray(): array
    {
        return [
            'price'               => $this->price,
            'product_category_id' => $this->product_category_id,
            'title'               => $this->title,
            'short_desc'          => $this->short_desc,
            'description'         => $this->description,
            'main_photo_id'       => $this->main_photo_id,
        ];
    }
}