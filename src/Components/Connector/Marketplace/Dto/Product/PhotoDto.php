<?php

namespace Eternity\Components\Connector\Marketplace\Dto\Product;

use Eternity\Contracts\Arrayable;
use Eternity\Traits\FromArray;

/**
 * Class PhotoDto
 * @package Eternity\Components\Connector\Marketplace\Dto\Product
 */
class PhotoDto implements Arrayable
{
    use FromArray;

    /**
     * @var int ID
     */
    public $id;

    /**
     * @var int File type
     */
    public $type;

    /**
     * @var string Caption
     */
    public $caption;

    /**
     * @var string File relative path
     */
    public $path;

    /**
     * @var string File name
     */
    public $name;

    /**
     * @var int Entity ID
     */
    public $entity_id;

    /**
     * @var string
     */
    public $created_at;

    /**
     * @var string
     */
    public $updated_at;

    /**
     * @var int
     */
    public $created_by;

    /**
     * @var int
     */
    public $updated_by;

    /**
     * @var string Url to original
     */
    public $origin;

    /**
     * @var array List of photo thumbnails
     */
    public $thumbnails;

    /**
     * Transforms object into array
     *
     * @return array
     */
    public function toArray(): array
    {
        return [
            'id'         => $this->id,
            'type'       => $this->type,
            'caption'    => $this->caption,
            'path'       => $this->path,
            'name'       => $this->name,
            'entity_id'  => $this->entity_id,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
            'created_by' => $this->created_by,
            'updated_by' => $this->updated_by,
            'origin'     => $this->origin,
            'thumbnails' => $this->thumbnails,
        ];
    }

}