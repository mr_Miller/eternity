<?php

namespace Eternity\Components\Connector\Marketplace\Dto\Product;

use Eternity\Contracts\Arrayable;
use Eternity\Traits\FromArray;

/**
 * Class TranslationCreateDto
 * @package Eternity\Components\Connector\Marketplace\Dto\Product
 */
class TranslationCreateDto implements Arrayable
{
    use FromArray;

    /**
     * @var string $title Title
     */
    public $title;

    /**
     * @var string $short_desc Short description
     */
    public $short_desc;

    /**
     * @var string $description Description
     */
    public $description;

    /**
     * @var int $product_id Product ID
     */
    public $product_id;

    /**
     * @var string $language Language code
     */
    public $language;

    /**
     * Transforms object into array
     *
     * @return array
     */
    public function toArray(): array
    {
        return [
            'title'       => $this->title,
            'short_desc'  => $this->short_desc,
            'description' => $this->description,
            'product_id'  => $this->product_id,
            'language'    => $this->language,
        ];
    }

}