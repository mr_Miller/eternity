<?php

namespace Eternity\Components\Connector\Marketplace\Dto\Product;

use Eternity\Contracts\Arrayable;
use Eternity\Traits\FromArray;

/**
 * Class TranslationUpdateDto
 * @package Eternity\Components\Connector\Marketplace\Dto\Product
 */
class TranslationUpdateDto implements Arrayable
{
    use FromArray;

    /**
     * @var string $title Title
     */
    public $title;

    /**
     * @var string $short_desc Short description
     */
    public $short_desc;

    /**
     * @var string $description Description
     */
    public $description;

    /**
     * Transforms object into array
     *
     * @return array
     */
    public function toArray(): array
    {
        return [
            'title'       => $this->title,
            'short_desc'  => $this->short_desc,
            'description' => $this->description,
        ];
    }
}