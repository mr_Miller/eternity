<?php

namespace Eternity\Components\Connector\Marketplace\Dto\Product;

use Eternity\Contracts\Arrayable;
use Eternity\Traits\FromArray;

/**
 * Class PhotoCreateDto
 * @package Eternity\Components\Connector\Marketplace\Dto\Product
 */
class PhotoCreateDto implements Arrayable
{
    use FromArray;

    /**
     * @var string
     */
    public $caption;

    /**
     * @var string
     */
    public $path;

    /**
     * @var string
     */
    public $name;

    /**
     * Transforms object into array
     *
     * @return array
     */
    public function toArray(): array
    {
        return [
            'caption' => $this->caption,
            'path'    => $this->path,
            'name'    => $this->name
        ];
    }

}