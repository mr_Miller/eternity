<?php

namespace Eternity\Components\Connector\Marketplace\Dto\Product;

use Eternity\Contracts\Arrayable;
use Eternity\Traits\FromArray;

/**
 * Class TranslationDto
 * @package Eternity\Components\Connector\Marketplace\Dto\Product
 */
class TranslationDto implements Arrayable
{
    use FromArray;

    /**
     * @var int $id ID
     */
    public $id;

    /**
     * @var string $language Language code
     */
    public $language;

    /**
     * @var int $product_id Product ID
     */
    public $product_id;

    /**
     * @var string $title Title
     */
    public $title;

    /**
     * @var string $short_desc Short description
     */
    public $short_desc;

    /**
     * @var string $description Description
     */
    public $description;

    /**
     * @var int $updated_at Update timestamp
     */
    public $updated_at;

    /**
     * @var int $created_at Creation timestamp
     */
    public $created_at;

    /**
     * @var int $deleted_at Deletion timestamp
     */
    public $deleted_at;

    /**
     * @var int $created_by User ID that created a record
     */
    public $created_by;

    /**
     * @var int $updated_by User ID that created a record
     */
    public $updated_by;

    /**
     * Transforms object into array
     *
     * @return array
     */
    public function toArray(): array
    {
        return [
            'id'          => $this->id,
            'language'    => $this->language,
            'product_id'  => $this->product_id,
            'title'       => $this->title,
            'short_desc'  => $this->short_desc,
            'description' => $this->description,
            'updated_at'  => $this->updated_at,
            'deleted_at'  => $this->deleted_at,
            'created_by'  => $this->created_by,
            'updated_by'  => $this->updated_by,
        ];
    }

}