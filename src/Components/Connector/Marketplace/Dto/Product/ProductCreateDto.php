<?php

namespace Eternity\Components\Connector\Marketplace\Dto\Product;

use Eternity\Contracts\Arrayable;
use Eternity\Traits\FromArray;

/**
 * DTO
 *
 * Contains fields needed for entity creation case. This dto contains only user input data.
 *
 * Class ProductCreateDto
 * @package App\Domain\Product\Dto
 */
class ProductCreateDto implements Arrayable
{
    use FromArray;

    /**
     * @var float $price Price
     */
    public $price;

    /**
     * @var int $product_category_id Product Category ID
     */
    public $product_category_id;

    /**
     * @var string $title Title
     */
    public $title;

    /**
     * @var string|null $short_desc Short description
     */
    public $short_desc;

    /**
     * @var string|null $description Description
     */
    public $description;

    /**
     * Transforms object into array
     *
     * @return array
     */
    public function toArray(): array
    {
        return [
            'price'               => $this->price,
            'product_category_id' => $this->product_category_id,
            'title'               => $this->title,
            'short_desc'          => $this->short_desc,
            'description'         => $this->description,
        ];
    }
}