<?php

namespace Eternity\Components\Connector\Marketplace;

use Eternity\Components\Connector\AbstractConnector;
use Eternity\Components\Connector\Marketplace\Definition\DefinitionConnector;
use Eternity\Components\Connector\Marketplace\SubConnectors\Auth\AuthConnector;
use Eternity\Components\Connector\Marketplace\SubConnectors\Campaign\CampaignConnector;
use Eternity\Components\Connector\Marketplace\SubConnectors\Product\PhotoConnector;
use Eternity\Components\Connector\Marketplace\SubConnectors\Product\ProductConnector;
use Eternity\Components\Connector\Marketplace\SubConnectors\Product\TranslationConnector;
use Eternity\Components\Connector\Marketplace\SubConnectors\QrCode\QrCodeConnector;
use Eternity\Components\Connector\Marketplace\SubConnectors\User\UserConnector;
use Eternity\Components\Connector\Marketplace\SubConnectors\UserGroup\UserGroupConnector;
use Eternity\Http\Client;

/**
 * Marketplace microservice connector
 *
 * Use this connector to operate with marketplace.
 * DON'T use sub-connectors directly !!!
 *
 * Class MarketplaceConnector
 * @package Eternity\Components\Connector\Marketplace\SubConnectors
 * @property CampaignConnector $campaign
 * @property QrCodeConnector $qrCode
 * @property UserConnector $user
 * @property UserGroupConnector $userGroup
 * @property ProductConnector $product
 * @property PhotoConnector $productPhoto
 * @property AuthConnector $auth
 * @property TranslationConnector $productTranslation
 * @property \Eternity\Components\Connector\Marketplace\Definition\DefinitionConnector $definition
 */
class MarketplaceConnector extends AbstractConnector
{
    /**
     * MarketplaceConnector constructor.
     * @param \Eternity\Http\Client $client
     * @param string $serviceUrl
     * @param string $traceId
     * @param array $headers
     */
    public function __construct(Client $client, string $serviceUrl, string $traceId, array $headers = [])
    {
        parent::__construct($client, $serviceUrl, $traceId, $headers);
        $this->addSubConnector(CampaignConnector::class, 'campaign')
            ->addSubConnector(QrCodeConnector::class, 'qrCode')
            ->addSubConnector(UserConnector::class, 'user')
            ->addSubConnector(UserGroupConnector::class, 'userGroup')
            ->addSubConnector(ProductConnector::class, 'product')
            ->addSubConnector(AuthConnector::class, 'auth')
            ->addSubConnector(PhotoConnector::class, 'productPhoto')
            ->addSubConnector(TranslationConnector::class, 'productTranslation')
            ->addSubConnector(DefinitionConnector::class, 'definition');
    }
}