<?php

namespace Eternity\Components\Connector\Marketplace\Definition;

use Eternity\Components\Connector\AbstractConnector;
use Eternity\Components\Connector\AnimalId\Definitions\HeadersDefinition;
use Eternity\Components\Connector\AnimalId\Responses\Definition\DefinitionResponse;

/**
 * Class DefinitionConnector
 *
 * @package Eternity\Components\Connector\Marketplace\Definition
 */
class DefinitionConnector extends AbstractConnector
{
    /**
     * Returns the list of definitions
     * @param int $userId
     * @param string $language
     * @return \Eternity\Components\Connector\AnimalId\Responses\Definition\DefinitionResponse
     */
    public function getList(int $userId, string $language): DefinitionResponse
    {
        $headers = [
            HeadersDefinition::LANG_CODE => $language,
            HeadersDefinition::USER_ID   => $userId,
        ];
        $response = $this->get('v1/definitions', 200, $headers);

        return new DefinitionResponse($response->getPayloadItems(), $response);
    }
}