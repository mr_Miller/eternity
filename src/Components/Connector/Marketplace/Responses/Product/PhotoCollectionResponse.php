<?php

namespace Eternity\Components\Connector\Marketplace\Responses\Product;

use Eternity\Components\Connector\AbstractResponse;
use Eternity\Components\Connector\Marketplace\Dto\Product\PhotoDto;

/**
 * Class PhotoCollectionResponse
 * @package Eternity\Components\Connector\Marketplace\Responses\Product
 */
class PhotoCollectionResponse extends AbstractResponse
{
    /**
     * Returns product dto collection
     * @return \Generator|\Eternity\Components\Connector\Marketplace\Dto\Product\PhotoDto[]
     */
    public function collection(): \Generator
    {
        foreach ($this->getResponse()->getPayloadItems() as $payloadItem) {
            yield PhotoDto::fromArray($payloadItem);
        }
    }
}