<?php

namespace Eternity\Components\Connector\Marketplace\Responses\Product;

use Eternity\Components\Connector\AbstractResponse;
use Eternity\Components\Connector\Marketplace\Dto\Product\TranslationDto;
use Eternity\Http\Contracts\ExtendedResponse;

/**
 * Class TranslationResponse
 * @package Eternity\Components\Connector\Marketplace\Responses\Product
 */
class TranslationResponse extends AbstractResponse
{
    /**
     * @var \Eternity\Components\Connector\Marketplace\Dto\Product\TranslationDto
     */
    private $translationDto;

    /**
     * TranslationResponse constructor.
     * @param \Eternity\Http\Contracts\ExtendedResponse $response
     * @param \Eternity\Components\Connector\Marketplace\Dto\Product\TranslationDto $translationDto
     */
    public function __construct(ExtendedResponse $response, TranslationDto $translationDto)
    {
        parent::__construct($response);
        $this->translationDto = $translationDto;
    }

    /**
     * @return \Eternity\Components\Connector\Marketplace\Dto\Product\TranslationDto
     */
    public function getTranslationDto(): TranslationDto
    {
        return $this->translationDto;
    }
}