<?php

namespace Eternity\Components\Connector\Marketplace\Responses\Product;

use Eternity\Components\Connector\AbstractResponse;
use Eternity\Components\Connector\Marketplace\Dto\Product\TranslationDto;

/**
 * Class TranslationCollectionResponse
 * @package Eternity\Components\Connector\Marketplace\Responses\Product
 */
class TranslationCollectionResponse extends AbstractResponse
{
    /**
     * Returns product dto collection
     * @return \Generator|TranslationDto[]
     */
    public function collection(): \Generator
    {
        foreach ($this->getResponse()->getPayloadItems() as $payloadItem) {
            yield TranslationDto::fromArray($payloadItem);
        }
    }
}