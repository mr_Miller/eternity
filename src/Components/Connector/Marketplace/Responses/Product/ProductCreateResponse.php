<?php

namespace Eternity\Components\Connector\Marketplace\Responses\Product;

use Eternity\Components\Connector\AbstractResponse;
use Eternity\Http\Contracts\ExtendedResponse;

/**
 * Class ProductCreateResponse
 * @package Eternity\Components\Connector\Marketplace\Responses\Product
 */
class ProductCreateResponse extends AbstractResponse
{
    /**
     * @var int
     */
    private $productId;

    /**
     * ProductCreateResponse constructor.
     * @param int $productId
     * @param \Eternity\Http\Contracts\ExtendedResponse $response
     */
    public function __construct(int $productId, ExtendedResponse $response)
    {
        parent::__construct($response);
        $this->productId = $productId;
    }

    /**
     * @return int
     */
    public function getProductId(): int
    {
        return $this->productId;
    }
}