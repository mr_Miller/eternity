<?php

namespace Eternity\Components\Connector\Marketplace\Responses\Product;

use Eternity\Components\Connector\AbstractResponse;
use Eternity\Http\Contracts\ExtendedResponse;

/**
 * Class PhotoCreateResponse
 * @package Eternity\Components\Connector\Marketplace\Responses\Product
 */
class PhotoCreateResponse extends AbstractResponse
{
    /**
     * @var int
     */
    private $photoId;

    /**
     * PhotoCreateResponse constructor.
     * @param int $photoId
     * @param \Eternity\Http\Contracts\ExtendedResponse $response
     */
    public function __construct(int $photoId, ExtendedResponse $response)
    {
        parent::__construct($response);
        $this->photoId = $photoId;
    }

    /**
     * @return int
     */
    public function getPhotoId(): int
    {
        return $this->photoId;
    }
}