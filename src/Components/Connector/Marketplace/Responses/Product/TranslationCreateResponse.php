<?php

namespace Eternity\Components\Connector\Marketplace\Responses\Product;

use Eternity\Components\Connector\AbstractResponse;
use Eternity\Http\Contracts\ExtendedResponse;

/**
 * Class TranslationCreateResponse
 * @package Eternity\Components\Connector\Marketplace\Responses\Product
 */
class TranslationCreateResponse extends AbstractResponse
{
    /**
     * @var int
     */
    private $translationId;

    /**
     * TranslationCreateResponse constructor.
     * @param \Eternity\Http\Contracts\ExtendedResponse $response
     * @param int $translationId
     */
    public function __construct(ExtendedResponse $response, int $translationId)
    {
        parent::__construct($response);
        $this->translationId = $translationId;
    }

    /**
     * @return int
     */
    public function getTranslationId(): int
    {
        return $this->translationId;
    }
}