<?php

namespace Eternity\Components\Connector\Marketplace\Responses\Product;

use Eternity\Components\Connector\AbstractResponse;
use Eternity\Components\Connector\Marketplace\Dto\Product\ProductDto;

/**
 * Class ProductCollectionResponse
 * @package Eternity\Components\Connector\Marketplace\Responses\Product
 */
class ProductCollectionResponse extends AbstractResponse
{
    /**
     * Returns product dto collection
     * @return \Generator|ProductDto[]
     */
    public function collection(): \Generator
    {
        foreach ($this->getResponse()->getPayloadItems() as $payloadItem) {
            yield ProductDto::fromArray($payloadItem);
        }
    }
}