<?php

namespace Eternity\Components\Connector\Marketplace\Responses\Product;

use Eternity\Components\Connector\AbstractResponse;
use Eternity\Components\Connector\Marketplace\Dto\Product\PhotoDto;
use Eternity\Http\Contracts\ExtendedResponse;

/**
 * Class PhotoResponse
 * @package Eternity\Components\Connector\Marketplace\Responses\Product
 */
class PhotoResponse extends AbstractResponse
{
    /**
     * @var \Eternity\Components\Connector\Marketplace\Dto\Product\PhotoDto
     */
    private $photoDto;

    /**
     * PhotoResponse constructor.
     * @param \Eternity\Components\Connector\Marketplace\Dto\Product\PhotoDto $photoDto
     * @param \Eternity\Http\Contracts\ExtendedResponse $response
     */
    public function __construct(PhotoDto $photoDto, ExtendedResponse $response)
    {
        parent::__construct($response);
        $this->photoDto = $photoDto;
    }

    /**
     * @return \Eternity\Components\Connector\Marketplace\Dto\Product\PhotoDto
     */
    public function getPhotoDto(): PhotoDto
    {
        return $this->photoDto;
    }
}