<?php

namespace Eternity\Components\Connector\Marketplace\Responses\Product;

use Eternity\Components\Connector\AbstractResponse;
use Eternity\Components\Connector\Marketplace\Dto\Product\ProductDto;
use Eternity\Http\Contracts\ExtendedResponse;

/**
 * Class ProductResponse
 * @package Eternity\Components\Connector\Marketplace\Responses\Product
 */
class ProductResponse extends AbstractResponse
{
    /**
     * @var \Eternity\Components\Connector\Marketplace\Dto\Product\ProductDto
     */
    private $productDto;

    /**
     * ProductResponse constructor.
     * @param \Eternity\Components\Connector\Marketplace\Dto\Product\ProductDto $productDto
     * @param \Eternity\Http\Contracts\ExtendedResponse $response
     */
    public function __construct(ProductDto $productDto, ExtendedResponse $response)
    {
        parent::__construct($response);
        $this->productDto = $productDto;
    }

    /**
     * @return \Eternity\Components\Connector\Marketplace\Dto\Product\ProductDto
     */
    public function getProductDto(): ProductDto
    {
        return $this->productDto;
    }
}