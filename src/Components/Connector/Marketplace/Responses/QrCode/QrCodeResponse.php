<?php

namespace Eternity\Components\Connector\Marketplace\Responses\QrCode;

use Eternity\Components\Connector\Marketplace\Dto\QrCode\QrCodeDto;
use Eternity\Http\Contracts\ExtendedResponse;

/**
 * Qr Code Response
 *
 * Class QrCodeResponse
 */
class QrCodeResponse
{
    /**
     * @var \Eternity\Http\Contracts\ExtendedResponse
     */
    private $extendedResponse;

    /**
     * @var \Eternity\Components\Connector\Marketplace\Dto\QrCode\QrCodeDto
     */
    private $qrCode;

    /**
     * AnimalResponse constructor.
     *
     * @param \Eternity\Components\Connector\Marketplace\Dto\QrCode\QrCodeDto $qrCode
     * @param \Eternity\Http\Contracts\ExtendedResponse $extendedResponse
     */
    public function __construct(QrCodeDto $qrCode, ExtendedResponse $extendedResponse)
    {
        $this->qrCode = $qrCode;
        $this->extendedResponse = $extendedResponse;
    }

    /**
     * @return \Eternity\Components\Connector\Marketplace\Dto\QrCode\QrCodeDto
     */
    public function getQrCode(): QrCodeDto
    {
        return $this->qrCode;
    }

    /**
     * @return \Eternity\Http\Contracts\ExtendedResponse
     */
    public function getExtendedResponse(): ExtendedResponse
    {
        return $this->extendedResponse;
    }
}