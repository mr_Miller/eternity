<?php


namespace Eternity\Components\Connector\Marketplace\Responses\QrCode;


use Eternity\Http\Contracts\ExtendedResponse;

class CustomQrCodeResponse
{
    /**
     * @var \Eternity\Http\Contracts\ExtendedResponse
     */
    private $extendedResponse;

    /**
     * @var array
     */
    private $data;

    /**
     * CreatedAnimalResponse constructor.
     *
     * @param array                                     $data
     * @param \Eternity\Http\Contracts\ExtendedResponse $extendedResponse
     */
    public function __construct(array $data, ExtendedResponse $extendedResponse)
    {
        $this->data = $data;
        $this->extendedResponse = $extendedResponse;
    }

    /**
     * @return array
     */
    public function geData(): array
    {
        return $this->data;
    }

    /**
     * @return \Eternity\Http\Contracts\ExtendedResponse
     */
    public function getExtendedResponse(): ExtendedResponse
    {
        return $this->extendedResponse;
    }
}