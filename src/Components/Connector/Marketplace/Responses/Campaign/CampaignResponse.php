<?php

namespace Eternity\Components\Connector\Marketplace\Responses\Campaign;

use Eternity\Components\Connector\Marketplace\Dto\Campaign\CampaignDto;
use Eternity\Http\Contracts\ExtendedResponse;

/**
 * Campaign Response
 *
 * Class CampaignResponse
 */
class CampaignResponse
{
    /**
     * @var \Eternity\Http\Contracts\ExtendedResponse
     */
    private $extendedResponse;

    /**
     * @var \Eternity\Components\Connector\Marketplace\Dto\Campaign\CampaignDto
     */
    private $campaign;

    /**
     * AnimalResponse constructor.
     *
     * @param \Eternity\Components\Connector\Marketplace\Dto\Campaign\CampaignDto $campaign
     * @param \Eternity\Http\Contracts\ExtendedResponse $extendedResponse
     */
    public function __construct(CampaignDto $campaign, ExtendedResponse $extendedResponse)
    {
        $this->campaign = $campaign;
        $this->extendedResponse = $extendedResponse;
    }

    /**
     * @return \Eternity\Components\Connector\Marketplace\Dto\Campaign\CampaignDto
     */
    public function getCampaign(): CampaignDto
    {
        return $this->campaign;
    }

    /**
     * @return \Eternity\Http\Contracts\ExtendedResponse
     */
    public function getExtendedResponse(): ExtendedResponse
    {
        return $this->extendedResponse;
    }
}