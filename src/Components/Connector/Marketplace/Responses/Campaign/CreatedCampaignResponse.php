<?php

namespace Eternity\Components\Connector\Marketplace\Responses\Campaign;

use Eternity\Http\Contracts\ExtendedResponse;

/**
 * Campaign Create Response
 *
 * Class CreatedCampaignResponse
 *
 * @package Eternity\Components\Connector\AnimalId\SubConnectors\Animal\Responses
 */
class CreatedCampaignResponse
{
    /**
     * @var \Eternity\Http\Contracts\ExtendedResponse
     */
    private $extendedResponse;

    /**
     * @var int
     */
    private $campaignId;

    /**
     * CreatedAnimalResponse constructor.
     *
     * @param int                                       $campaignId
     * @param \Eternity\Http\Contracts\ExtendedResponse $extendedResponse
     */
    public function __construct(int $campaignId, ExtendedResponse $extendedResponse)
    {
        $this->campaignId = $campaignId;
        $this->extendedResponse = $extendedResponse;
    }

    /**
     * @return int
     */
    public function getCampaignId(): int
    {
        return $this->campaignId;
    }

    /**
     * @return \Eternity\Http\Contracts\ExtendedResponse
     */
    public function getExtendedResponse(): ExtendedResponse
    {
        return $this->extendedResponse;
    }
}