<?php

namespace Eternity\Components\Connector\Marketplace\Responses\UserGroup;

use Eternity\Components\Connector\Marketplace\Dto\UserGroup\UserGroupDto;
use Eternity\Http\Contracts\ExtendedResponse;

class UserGroupCollectionResponse
{
    /**
     * @var \Eternity\Http\Contracts\ExtendedResponse
     */
    private $response;

    /**
     * AnimalCollectionResponse constructor.
     * @param \Eternity\Http\Contracts\ExtendedResponse $response
     */
    public function __construct(ExtendedResponse $response)
    {
        $this->response = $response;
    }

    /**
     * @return \Generator
     */
    public function getUserGroupCollection(): \Generator
    {
        foreach ($this->response->getPayloadItems() as $payloadItem) {
            yield UserGroupDto::fromArray($payloadItem);
        }
    }

    /**
     * @return \Eternity\Http\Contracts\ExtendedResponse
     */
    public function getExtendedResponse(): ExtendedResponse
    {
        return $this->response;
    }
}