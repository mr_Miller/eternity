<?php

namespace Eternity\Components\Connector\Marketplace\Responses\UserGroup;

use Eternity\Http\Contracts\ExtendedResponse;

/**
 * Class CreatedUserGroupResponse
 *
 * @package Eternity\Components\Connector\AnimalId\SubConnectors\Animal\Responses
 */
class CreatedUserGroupResponse
{
    /**
     * @var \Eternity\Http\Contracts\ExtendedResponse
     */
    private $extendedResponse;

    /**
     * @var int
     */
    private $groupId;

    /**
     * CreatedAnimalResponse constructor.
     *
     * @param int $groupId
     * @param \Eternity\Http\Contracts\ExtendedResponse $extendedResponse
     */
    public function __construct(int $groupId, ExtendedResponse $extendedResponse)
    {
        $this->groupId = $groupId;
        $this->extendedResponse = $extendedResponse;
    }

    /**
     * @return int
     */
    public function getGroupId(): int
    {
        return $this->groupId;
    }

    /**
     * @return \Eternity\Http\Contracts\ExtendedResponse
     */
    public function getExtendedResponse(): ExtendedResponse
    {
        return $this->extendedResponse;
    }
}