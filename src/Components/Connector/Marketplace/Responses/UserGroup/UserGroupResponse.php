<?php


namespace Eternity\Components\Connector\Marketplace\Responses\UserGroup;


use Eternity\Components\Connector\Marketplace\Dto\UserGroup\UserGroupDto;
use Eternity\Http\Contracts\ExtendedResponse;

/**
 * User Group Response
 *
 * Class UserGroupResponse
 *
 * @package Eternity\Components\Connector\Marketplace\UserGroup
 */
class UserGroupResponse
{
    /**
     * @var \Eternity\Http\Contracts\ExtendedResponse
     */
    private $extendedResponse;

    /**
     * @var \Eternity\Components\Connector\Marketplace\Dto\UserGroup\UserGroupDto
     */
    private $group;

    /**
     * AnimalResponse constructor.
     *
     * @param \Eternity\Components\Connector\Marketplace\Dto\UserGroup\UserGroupDto $groupDto
     * @param \Eternity\Http\Contracts\ExtendedResponse $extendedResponse
     */
    public function __construct(UserGroupDto $groupDto, ExtendedResponse $extendedResponse)
    {
        $this->group = $groupDto;
        $this->extendedResponse = $extendedResponse;
    }

    /**
     * @return \Eternity\Components\Connector\Marketplace\Dto\UserGroup\UserGroupDto
     */
    public function getGroup(): UserGroupDto
    {
        return $this->group;
    }

    /**
     * @return \Eternity\Http\Contracts\ExtendedResponse
     */
    public function getExtendedResponse(): ExtendedResponse
    {
        return $this->extendedResponse;
    }
}