<?php

namespace Eternity\Components\Connector\Marketplace\Responses\User;

use Eternity\Components\Connector\Marketplace\Dto\User\UserDto;
use Eternity\Http\Contracts\ExtendedResponse;

/**
 * Class UserCollectionResponse
 * @package Eternity\Components\Connector\AnimalId\Responses\User
 */
class UserCollectionResponse
{
    /**
     * @var \Eternity\Http\Contracts\ExtendedResponse
     */
    private $response;

    /**
     * AnimalCollectionResponse constructor.
     * @param \Eternity\Http\Contracts\ExtendedResponse $response
     */
    public function __construct(ExtendedResponse $response)
    {
        $this->response = $response;
    }

    /**
     * @return \Generator
     */
    public function getUserCollection(): \Generator
    {
        foreach ($this->response->getPayloadItems() as $payloadItem) {
            yield UserDto::fromArray($payloadItem);
        }
    }

    /**
     * @return \Eternity\Http\Contracts\ExtendedResponse
     */
    public function getExtendedResponse(): ExtendedResponse
    {
        return $this->response;
    }
}