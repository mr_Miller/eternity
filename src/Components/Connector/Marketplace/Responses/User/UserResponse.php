<?php


namespace Eternity\Components\Connector\Marketplace\Responses\User;


use Eternity\Components\Connector\Marketplace\Dto\User\UserDto;
use Eternity\Http\Contracts\ExtendedResponse;

class UserResponse
{
    /**
     * @var \Eternity\Http\Contracts\ExtendedResponse
     */
    private $extendedResponse;

    /**
     * @var \Eternity\Components\Connector\Marketplace\Dto\User\UserDto;
     */
    private $dto;

    /**
     * AnimalResponse constructor.
     *
     * @param \Eternity\Components\Connector\Marketplace\Dto\User\UserDto $dto
     * @param \Eternity\Http\Contracts\ExtendedResponse               $extendedResponse
     */
    public function __construct(UserDto $dto, ExtendedResponse $extendedResponse)
    {
        $this->dto = $dto;
        $this->extendedResponse = $extendedResponse;
    }

    /**
     * @return \Eternity\Components\Connector\Marketplace\Dto\User\UserDto
     */
    public function getDto(): UserDto
    {
        return $this->dto;
    }

    /**
     * @return \Eternity\Http\Contracts\ExtendedResponse
     */
    public function getExtendedResponse(): ExtendedResponse
    {
        return $this->extendedResponse;
    }
}