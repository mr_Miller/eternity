<?php

namespace Eternity\Components\Connector\Marketplace\Responses\User;

use Eternity\Components\Connector\AbstractResponse;
use Eternity\Http\Contracts\ExtendedResponse;

/**
 * Class UserCreateResponse
 * @package Eternity\Components\Connector\Marketplace\Responses\User
 */
class UserCreateResponse extends AbstractResponse
{
    /**
     * @var int
     */
    private $userId;

    /**
     * UserCreateResponse constructor.
     * @param \Eternity\Http\Contracts\ExtendedResponse $response
     * @param int $userId
     */
    public function __construct(ExtendedResponse $response, int $userId)
    {
        parent::__construct($response);
        $this->userId = $userId;
    }

    /**
     * @return int
     */
    public function getUserId(): int
    {
        return $this->userId;
    }
}