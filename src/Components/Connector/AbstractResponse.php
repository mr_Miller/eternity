<?php

namespace Eternity\Components\Connector;

use Eternity\Http\Contracts\ExtendedResponse;

/**
 * Abstract response class
 *
 * Extend your response from this class
 *
 * Class AbstractResponse
 * @package Eternity\Components\Connector
 */
abstract class AbstractResponse
{
    /**
     * @var \Eternity\Http\Contracts\ExtendedResponse Response
     */
    private $response;

    /**
     * PhotoResponse constructor.
     * @param \Eternity\Http\Contracts\ExtendedResponse $response
     */
    public function __construct(ExtendedResponse $response)
    {
        $this->response = $response;
    }

    /**
     * Returns response
     * @return \Eternity\Http\Contracts\ExtendedResponse
     */
    public function getResponse(): ExtendedResponse
    {
        return $this->response;
    }
}