<?php

namespace Eternity\Components\Connector;

use Eternity\Components\Connector\Exceptions\SubConnectorNotSetException;
use Eternity\Definitions\HeadersDefinition;
use Eternity\Helpers\Uri;
use Eternity\Http\Client;
use Eternity\Http\Contracts\ExtendedResponse;

/**
 * Abstract Connector for prepare and send request to services
 *
 * Class AbstractConnector
 *
 * @package App\Application\Components\Connectors
 */
abstract class AbstractConnector
{
    /**
     * @var \Eternity\Http\Client Http client
     */
    private $client;

    /**
     * @var string URL of the service to which requests will be sent
     */
    private $serviceUrl;

    /**
     * @var array These headers can be set by connector methods
     */
    private $headers = [];

    /**
     * @var array List of child connectors, these connectors inherit headers from the parent,
     * and are loaded dynamically when called
     */
    private $subConnectors;

    /**
     * @var \Eternity\Trace\TraceId Trace ID cannot be changed, this will be passed as header in request
     */
    private $traceId;

    /**
     * AbstractConnector constructor.
     *
     * @param \Eternity\Http\Client $client
     * @param string $serviceUrl
     * @param string $traceId
     * @param array $headers
     */
    public function __construct(
        Client $client,
        string $serviceUrl,
        string $traceId,
        array $headers = []
    ) {
        $this->client = $client;
        $this->serviceUrl = $serviceUrl;
        $this->traceId = $traceId;
        $this->headers = $headers;
    }

    /**
     * Set language header
     * @param string $language
     * @return $this
     */
    public function setLanguageHeader(string $language): self
    {
        $this->headers[HeadersDefinition::LANG_CODE] = $language;

        return $this;
    }

    /**
     * Set UID header
     * @param int $uid
     * @return $this
     */
    public function setUidHeader(int $uid): self
    {
        $this->headers[HeadersDefinition::USER_ID] = $uid;

        return $this;
    }

    /**
     * Set Authorized For (RBAC) header
     * @param string $authorizedFor
     * @return $this
     */
    public function setAuthorizedForHeader(string $authorizedFor): self
    {
        $this->headers[HeadersDefinition::AUTHORIZED_FOR] = $authorizedFor;

        return $this;
    }

    /**
     * Call anonymous function for first call
     * @param $name
     * @return mixed
     * @throws \Eternity\Components\Connector\Exceptions\SubConnectorNotSetException
     */
    public function __get($name)
    {
        if (isset($this->subConnectors[$name])) {
            if (is_callable($this->subConnectors[$name])) {
                $closure = $this->subConnectors[$name];
                $this->subConnectors[$name] = $closure();
            }

            return $this->subConnectors[$name];
        }

        throw new SubConnectorNotSetException('SubConnector Error', 'SubConnector [' . $name . '] is not set');
    }

    /**
     * Add SubConnector
     *
     * Connector will be initialized on first call
     *
     * @param string $class
     * @param string $name
     * @return \Eternity\Components\Connector\AbstractConnector
     */
    protected function addSubConnector(string $class, string $name): self
    {
        $this->subConnectors[$name] = $this->defferLoad($class);

        return $this;
    }

    /**
     * Eager connector load
     * @param string $class
     * @return callable
     */
    private function defferLoad(string $class): callable
    {
        return function () use ($class) {
            return new $class($this->client, $this->serviceUrl, $this->traceId, $this->headers);
        };
    }

    /**
     * Build url to service
     *
     * @param string|\Eternity\Helpers\Uri $uri
     *
     * @return string
     */
    private function buildUrl($uri): string
    {
        return $this->serviceUrl . '/' . $uri;
    }

    /**
     * Adds headers array
     * @param array $headers
     */
    private function setHeaders(array $headers): void
    {
        foreach (array_filter($headers) as $headerKey => $headerValue) {
            $this->setHeader($headerKey, $headerValue);
        }
    }

    /**
     * Adds single header
     * @param string $headerKey
     * @param string $headerValue
     */
    private function setHeader(string $headerKey, string $headerValue): void
    {
        $this->headers[$headerKey] = $headerValue;
    }

    /**
     * Returns headers
     * @return array
     */
    protected function getHeaders(): array
    {
        return $this->headers;
    }

    /**
     * Resolver to services
     *
     * @param string $method
     * @param string|\Eternity\Helpers\Uri $uri
     * @param int $expectedStatus
     * @param array $headers
     * @param array|null $body Body or Query params depends on request type. For get request body will be transferd as
     * query params
     *
     * @return \Eternity\Http\Contracts\ExtendedResponse
     */
    private function request(
        string $method,
        $uri,
        int $expectedStatus = 200,
        array $headers = [],
        ?array $body = null
    ): ExtendedResponse {
        if (is_array($body)) {
            $body = json_encode($body);
        }
        // Default headers will be overridden by current request headers
        $this->setHeaders($headers);
        // Trace ID header cannot be overridden
        $this->setHeader(HeadersDefinition::TRACE_ID, $this->traceId);
        if ($uri instanceof Uri) {
            $uri = $uri->path();
        }

        return $this->client->{$method}($this->buildUrl($uri), $expectedStatus, $this->getHeaders(), $body);
    }

    /**
     * Method GET to services
     *
     * @param string|\Eternity\Helpers\Uri $uri
     * @param int $expectedStatus
     * @param array $headers
     * @param array|null $body
     *
     * @return \Eternity\Http\Contracts\ExtendedResponse
     */
    protected function get(
        $uri,
        int $expectedStatus = 200,
        array $headers = [],
        ?array $body = null
    ): ExtendedResponse {
        return $this->request('get', $uri, $expectedStatus, $headers, $body);
    }

    /**
     * Method POST to services
     *
     * @param string|\Eternity\Helpers\Uri $uri
     * @param int $expectedStatus
     * @param array $headers
     * @param array|null $body
     *
     * @return \Eternity\Http\Contracts\ExtendedResponse
     */
    protected function post(
        $uri,
        int $expectedStatus = 200,
        array $headers = [],
        ?array $body = null
    ): ExtendedResponse {
        return $this->request('post', $uri, $expectedStatus, $headers, $body);
    }

    /**
     * Method PUT to services
     *
     * @param string|\Eternity\Helpers\Uri $uri
     * @param int $expectedStatus
     * @param array $headers
     * @param array|null $body
     *
     * @return \Eternity\Http\Contracts\ExtendedResponse
     */
    protected function put(
        $uri,
        int $expectedStatus = 200,
        array $headers = [],
        ?array $body = null
    ): ExtendedResponse {
        return $this->request('put', $uri, $expectedStatus, $headers, $body);
    }

    /**
     * Method DELETE to services
     *
     * @param string|\Eternity\Helpers\Uri $uri
     * @param int $expectedStatus
     * @param array $headers
     * @param array|null $body
     *
     * @return \Eternity\Http\Contracts\ExtendedResponse
     */
    protected function delete(
        $uri,
        int $expectedStatus = 200,
        array $headers = [],
        ?array $body = null
    ): ExtendedResponse {
        return $this->request('delete', $uri, $expectedStatus, $headers, $body);
    }

    /**
     * Method PATCH to services
     *
     * @param string|\Eternity\Helpers\Uri $uri
     * @param int $expectedStatus
     * @param array $headers
     * @param array|null $body
     *
     * @return \Eternity\Http\Contracts\ExtendedResponse
     */
    protected function patch(
        $uri,
        int $expectedStatus = 200,
        array $headers = [],
        ?array $body = null
    ): ExtendedResponse {
        return $this->request('patch', $uri, $expectedStatus, $headers, $body);
    }

    /**
     * Method HEAD to services
     *
     * @param string|\Eternity\Helpers\Uri $uri
     * @param int $expectedStatus
     * @param array $headers
     * @param array|null $body
     *
     * @return \Eternity\Http\Contracts\ExtendedResponse
     */
    protected function head(
        $uri,
        int $expectedStatus = 200,
        array $headers = [],
        ?array $body = null
    ): ExtendedResponse {
        return $this->request('head', $uri, $expectedStatus, $headers, $body);
    }

    /**
     * Method OPTIONS to services
     *
     * @param string|\Eternity\Helpers\Uri $uri
     * @param int $expectedStatus
     * @param array $headers
     * @param array|null $body
     *
     * @return \Eternity\Http\Contracts\ExtendedResponse
     */
    protected function options(
        $uri,
        int $expectedStatus = 200,
        array $headers = [],
        ?array $body = null
    ): ExtendedResponse {
        return $this->request('options', $uri, $expectedStatus, $headers, $body);
    }
}
