<?php

namespace Eternity\Components\Connector\AnimalId\Responses\Animal;

use Eternity\Http\Contracts\ExtendedResponse;

class AnimalCollectionResponse
{
    /**
     * @var \Eternity\Components\Connector\AnimalId\Dto\Animal\AnimalDto[]
     */
    private $animalCollection;

    /**
     * @var \Eternity\Http\Contracts\ExtendedResponse
     */
    private $extendedResponse;

    /**
     * AnimalCollectionResponse constructor.
     * @param \Eternity\Components\Connector\AnimalId\Dto\Animal\AnimalDto[] $animalCollection
     * @param \Eternity\Http\Contracts\ExtendedResponse $extendedResponse
     */
    public function __construct(array $animalCollection, ExtendedResponse $extendedResponse)
    {
        $this->animalCollection = $animalCollection;
        $this->extendedResponse = $extendedResponse;
    }

    /**
     * @return \Eternity\Components\Connector\AnimalId\Dto\Animal\AnimalDto[]
     */
    public function getAnimalCollection(): array
    {
        return $this->animalCollection;
    }

    /**
     * @return \Eternity\Http\Contracts\ExtendedResponse
     */
    public function getExtendedResponse(): ExtendedResponse
    {
        return $this->extendedResponse;
    }
}