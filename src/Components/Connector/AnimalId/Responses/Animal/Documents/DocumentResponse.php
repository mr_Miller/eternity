<?php

namespace Eternity\Components\Connector\AnimalId\Responses\Animal\Documents;

/**
 * Class DocumentResponse
 * @package Eternity\Components\Connector\AnimalId\Responses\Animal\Documents
 */
class DocumentResponse
{
    /**
     * @var \Eternity\Components\Connector\AnimalId\Dto\Animal\Documents\DocumentDto
     */
    private $documentDto;

    /**
     * @var \Eternity\Http\Contracts\ExtendedResponse
     */
    private $response;

    /**
     * DocumentResponse constructor.
     * @param \Eternity\Components\Connector\AnimalId\Dto\Animal\Documents\DocumentDto $documentDto
     * @param \Eternity\Http\Contracts\ExtendedResponse $response
     */
    public function __construct(
        \Eternity\Components\Connector\AnimalId\Dto\Animal\Documents\DocumentDto $documentDto,
        \Eternity\Http\Contracts\ExtendedResponse $response
    ) {
        $this->documentDto = $documentDto;
        $this->response = $response;
    }

    /**
     * @return \Eternity\Components\Connector\AnimalId\Dto\Animal\Documents\DocumentDto
     */
    public function getDocumentDto(): \Eternity\Components\Connector\AnimalId\Dto\Animal\Documents\DocumentDto
    {
        return $this->documentDto;
    }

    /**
     * @return \Eternity\Http\Contracts\ExtendedResponse
     */
    public function getResponse(): \Eternity\Http\Contracts\ExtendedResponse
    {
        return $this->response;
    }

}