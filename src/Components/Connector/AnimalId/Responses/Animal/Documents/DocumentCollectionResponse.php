<?php

namespace Eternity\Components\Connector\AnimalId\Responses\Animal\Documents;

use Eternity\Components\Connector\AnimalId\Dto\Animal\Documents\DocumentDto;

/**
 * Class DocumentCollectionResponse
 * @package Eternity\Components\Connector\AnimalId\Responses\Animal\Documents
 */
class DocumentCollectionResponse
{
    /**
     * @var \Eternity\Components\Connector\AnimalId\Dto\Animal\Documents\DocumentDto[]|null
     */
    private $collection;

    /**
     * @var \Eternity\Http\Contracts\ExtendedResponse
     */
    private $response;

    /**
     * DocumentCollectionResponse constructor.
     * @param \Eternity\Http\Contracts\ExtendedResponse $response
     */
    public function __construct(\Eternity\Http\Contracts\ExtendedResponse $response)
    {
        $this->response = $response;
    }

    /**
     * Build collection on first access
     *
     * @return \Generator|DocumentDto[]
     */
    public function collection(): \Generator
    {
        foreach ($this->response->getPayloadItems() as $payloadItem) {
            yield DocumentDto::fromArray($payloadItem);
        }
    }

    /**
     * @return \Eternity\Http\Contracts\ExtendedResponse
     */
    public function getResponse(): \Eternity\Http\Contracts\ExtendedResponse
    {
        return $this->response;
    }

}