<?php

namespace Eternity\Components\Connector\AnimalId\Responses\Animal\Documents;

/**
 * Class DocumentResponse
 * @package Eternity\Components\Connector\AnimalId\Responses\Animal\Documents
 */
class DocumentCreateResponse
{
    /**
     * @var int
     */
    private $documentId;

    /**
     * @var \Eternity\Http\Contracts\ExtendedResponse
     */
    private $response;

    /**
     * DocumentResponse constructor.
     * @param int $documentId
     * @param \Eternity\Http\Contracts\ExtendedResponse $response
     */
    public function __construct(
        int $documentId,
        \Eternity\Http\Contracts\ExtendedResponse $response
    ) {
        $this->documentId = $documentId;
        $this->response = $response;
    }

    /**
     * @return int
     */
    public function getDocumentId(): int
    {
        return $this->documentId;
    }

    /**
     * @return \Eternity\Http\Contracts\ExtendedResponse
     */
    public function getResponse(): \Eternity\Http\Contracts\ExtendedResponse
    {
        return $this->response;
    }

}