<?php

namespace Eternity\Components\Connector\AnimalId\Responses\Animal\Photo;

/**
 * Class PhotoCollectionResponse
 * @package Eternity\Components\Connector\AnimalId\Responses\Animal\Photo
 */
class PhotoCollectionResponse
{
    /**
     * @var \Eternity\Components\Connector\AnimalId\Dto\Animal\Photos\PhotoDto[]
     */
    private $collection;

    /**
     * @var \Eternity\Http\Contracts\ExtendedResponse
     */
    private $response;

    /**
     * PhotoCollectionResponse constructor.
     * @param \Eternity\Components\Connector\AnimalId\Dto\Animal\Photos\PhotoDto[] $collection
     * @param \Eternity\Http\Contracts\ExtendedResponse $response
     */
    public function __construct(array $collection, \Eternity\Http\Contracts\ExtendedResponse $response)
    {
        $this->collection = $collection;
        $this->response = $response;
    }

    /**
     * @return \Eternity\Components\Connector\AnimalId\Dto\Animal\Photos\PhotoDto[]
     */
    public function getCollection(): array
    {
        return $this->collection;
    }

    /**
     * @return \Eternity\Http\Contracts\ExtendedResponse
     */
    public function getResponse(): \Eternity\Http\Contracts\ExtendedResponse
    {
        return $this->response;
    }

}