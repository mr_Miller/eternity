<?php

namespace Eternity\Components\Connector\AnimalId\Responses\Animal\Photo;

/**
 * Class PhotoResponse
 * @package Eternity\Components\Connector\AnimalId\Responses\Animal\Photo
 */
class PhotoResponse
{
    /**
     * @var \Eternity\Components\Connector\AnimalId\Dto\Animal\Photos\PhotoDto
     */
    private $photoDto;

    /**
     * @var \Eternity\Http\Contracts\ExtendedResponse
     */
    private $response;

    /**
     * PhotoResponse constructor.
     * @param \Eternity\Components\Connector\AnimalId\Dto\Animal\Photos\PhotoDto $photoDto
     * @param \Eternity\Http\Contracts\ExtendedResponse $response
     */
    public function __construct(
        \Eternity\Components\Connector\AnimalId\Dto\Animal\Photos\PhotoDto $photoDto,
        \Eternity\Http\Contracts\ExtendedResponse $response
    ) {
        $this->photoDto = $photoDto;
        $this->response = $response;
    }

    /**
     * @return \Eternity\Components\Connector\AnimalId\Dto\Animal\Photos\PhotoDto
     */
    public function getPhotoDto(): \Eternity\Components\Connector\AnimalId\Dto\Animal\Photos\PhotoDto
    {
        return $this->photoDto;
    }

    /**
     * @return \Eternity\Http\Contracts\ExtendedResponse
     */
    public function getResponse(): \Eternity\Http\Contracts\ExtendedResponse
    {
        return $this->response;
    }
}