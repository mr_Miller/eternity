<?php

namespace Eternity\Components\Connector\AnimalId\Responses\Animal\QrPassport;

use Eternity\Components\Connector\AbstractResponse;
use Eternity\Components\Connector\AnimalId\Dto\Animal\QrPassport\QrPassportDto;
use Eternity\Http\Contracts\ExtendedResponse;

/**
 * Class QrPassportResponse
 * @package Eternity\Components\Connector\AnimalId\Responses\Animal\QrPassport
 */
class QrPassportResponse extends AbstractResponse
{
    /**
     * @var \Eternity\Components\Connector\AnimalId\Dto\Animal\QrPassport\QrPassportDto
     */
    private $qrPassportDto;

    /**
     * CategoryResponse constructor.
     * @param \Eternity\Http\Contracts\ExtendedResponse $response
     * @param \Eternity\Components\Connector\AnimalId\Dto\Animal\QrPassport\QrPassportDto $qrPassportDto
     */
    public function __construct(ExtendedResponse $response, QrPassportDto $qrPassportDto)
    {
        parent::__construct($response);
        $this->qrPassportDto = $qrPassportDto;
    }
}