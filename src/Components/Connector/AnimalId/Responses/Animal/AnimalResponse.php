<?php

namespace Eternity\Components\Connector\AnimalId\Responses\Animal;

use Eternity\Components\Connector\AbstractResponse;
use Eternity\Components\Connector\AnimalId\Dto\Animal\AnimalDto;
use Eternity\Http\Contracts\ExtendedResponse;

/**
 * Class AnimalResponse
 * @package Eternity\Components\Connector\AnimalId\Responses\Animal
 */
class AnimalResponse extends AbstractResponse
{
    /**
     * @var \Eternity\Components\Connector\AnimalId\Dto\Animal\AnimalDto
     */
    private $animal;

    /**
     * @var \Eternity\Http\Contracts\ExtendedResponse
     */
    private $extendedResponse;

    /**
     * AnimalResponse constructor.
     * @param \Eternity\Http\Contracts\ExtendedResponse $response
     * @param \Eternity\Components\Connector\AnimalId\Dto\Animal\AnimalDto $animal
     */
    public function __construct(ExtendedResponse $response, AnimalDto $animal)
    {
        parent::__construct($response);
        $this->animal = $animal;
    }

    /**
     * @return \Eternity\Components\Connector\AnimalId\Dto\Animal\AnimalDto
     */
    public function getAnimal(): AnimalDto
    {
        return $this->animal;
    }

    /**
     * @return \Eternity\Http\Contracts\ExtendedResponse
     */
    public function getExtendedResponse(): ExtendedResponse
    {
        return $this->extendedResponse;
    }
}