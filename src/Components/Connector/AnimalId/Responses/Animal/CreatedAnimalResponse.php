<?php

namespace Eternity\Components\Connector\AnimalId\Responses\Animal;

use Eternity\Http\Contracts\ExtendedResponse;

/**
 * Class CreatedAnimalResponse
 * @package Eternity\Components\Connector\AnimalId\Responses\Animal
 */
class CreatedAnimalResponse
{
    /**
     * @var int
     */
    private $animalId;

    /**
     * @var \Eternity\Http\Contracts\ExtendedResponse
     */
    private $extendedResponse;

    /**
     * CreatedAnimalResponse constructor.
     * @param int $animalId
     * @param \Eternity\Http\Contracts\ExtendedResponse $extendedResponse
     */
    public function __construct(int $animalId, ExtendedResponse $extendedResponse)
    {
        $this->animalId = $animalId;
        $this->extendedResponse = $extendedResponse;
    }

    /**
     * @return int
     */
    public function getAnimalId(): int
    {
        return $this->animalId;
    }

    /**
     * @return \Eternity\Http\Contracts\ExtendedResponse
     */
    public function getExtendedResponse(): ExtendedResponse
    {
        return $this->extendedResponse;
    }
}