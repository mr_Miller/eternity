<?php

namespace Eternity\Components\Connector\AnimalId\Responses\Animal;

use Eternity\Http\Contracts\ExtendedResponse;

/**
 * Class GeoLocationsResponse
 * @package Eternity\Components\Connector\AnimalId\Responses\Animal
 */
class GeoLocationsResponse
{
    /**
     * @var \Eternity\Components\Connector\AnimalId\Dto\Animal\GeoLocationDto[]
     */
    private $scanLocations;

    /**
     * @var \Eternity\Http\Contracts\ExtendedResponse
     */
    private $extendedResponse;

    /**
     * AnimalScanLocationResponse constructor.
     * @param \Eternity\Components\Connector\AnimalId\Dto\Animal\GeoLocationDto[] $scanLocations
     * @param \Eternity\Http\Contracts\ExtendedResponse $extendedResponse
     */
    public function __construct(array $scanLocations, ExtendedResponse $extendedResponse)
    {
        $this->scanLocations = $scanLocations;
        $this->extendedResponse = $extendedResponse;
    }

    /**
     * @return array
     */
    public function getScanLocations(): array
    {
        return $this->scanLocations;
    }

    /**
     * @return \Eternity\Http\Contracts\ExtendedResponse
     */
    public function getExtendedResponse(): ExtendedResponse
    {
        return $this->extendedResponse;
    }
}