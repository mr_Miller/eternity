<?php

// todo : delete duplicated responses directory
namespace Eternity\Components\Connector\AnimalId\Responses\User\Responses;

/**
 * User profile
 *
 * Class ProfileResponse
 * @package Eternity\Components\Connector\AnimalId\Responses\User\Responses
 */
class ProfileResponse
{

    /**
     * @var \Eternity\Components\Connector\AnimalId\Dto\User\UserProfileDto User profile
     */
    private $profile;

    /**
     * @var \Eternity\Http\Contracts\ExtendedResponse
     */
    private $response;

    /**
     * ProfileResponse constructor.
     * @param \Eternity\Components\Connector\AnimalId\Dto\User\UserProfileDto $profile
     * @param \Eternity\Http\Contracts\ExtendedResponse $response
     */
    public function __construct(
        \Eternity\Components\Connector\AnimalId\Dto\User\UserProfileDto $profile,
        \Eternity\Http\Contracts\ExtendedResponse $response
    ) {
        $this->profile = $profile;
        $this->response = $response;
    }

    /**
     * @return \Eternity\Components\Connector\AnimalId\Dto\User\UserProfileDto
     */
    public function getProfile(): \Eternity\Components\Connector\AnimalId\Dto\User\UserProfileDto
    {
        return $this->profile;
    }

    /**
     * @return \Eternity\Http\Contracts\ExtendedResponse
     */
    public function getResponse(): \Eternity\Http\Contracts\ExtendedResponse
    {
        return $this->response;
    }
}