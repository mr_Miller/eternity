<?php

// todo : delete duplicated responses directory
namespace Eternity\Components\Connector\AnimalId\Responses\User\Responses;

/**
 * Class AvatarResponse
 * @package Eternity\Components\Connector\AnimalId\Responses\User\Responses
 */
class AvatarResponse
{
    /**
     * @var \Eternity\Components\Connector\AnimalId\Dto\User\AvatarDto
     */
    private $avatar;

    /**
     * @var \Eternity\Http\Contracts\ExtendedResponse
     */
    private $response;

    /**
     * AvatarResponse constructor.
     * @param \Eternity\Components\Connector\AnimalId\Dto\User\AvatarDto $avatar
     * @param \Eternity\Http\Contracts\ExtendedResponse $response
     */
    public function __construct(
        \Eternity\Components\Connector\AnimalId\Dto\User\AvatarDto $avatar,
        \Eternity\Http\Contracts\ExtendedResponse $response
    ) {
        $this->avatar = $avatar;
        $this->response = $response;
    }

    /**
     * @return \Eternity\Components\Connector\AnimalId\Dto\User\AvatarDto
     */
    public function getAvatar(): \Eternity\Components\Connector\AnimalId\Dto\User\AvatarDto
    {
        return $this->avatar;
    }

    /**
     * @return \Eternity\Http\Contracts\ExtendedResponse
     */
    public function getResponse(): \Eternity\Http\Contracts\ExtendedResponse
    {
        return $this->response;
    }
}