<?php

namespace Eternity\Components\Connector\AnimalId\Responses\Definition;

/**
 * Class DefinitionResponse
 * @package Eternity\Components\Connector\AnimalId\Responses\Definition
 */
class DefinitionResponse
{
    /**
     * @var array
     */
    private $definitions;

    /**
     * @var \Eternity\Http\Contracts\ExtendedResponse
     */
    private $response;

    /**
     * DefinitionResponse constructor.
     * @param array $definitions
     * @param \Eternity\Http\Contracts\ExtendedResponse $response
     */
    public function __construct(array $definitions, \Eternity\Http\Contracts\ExtendedResponse $response)
    {
        $this->definitions = $definitions;
        $this->response = $response;
    }

    /**
     * @return array
     */
    public function getDefinitions(): array
    {
        return $this->definitions;
    }

    /**
     * @return \Eternity\Http\Contracts\ExtendedResponse
     */
    public function getResponse(): \Eternity\Http\Contracts\ExtendedResponse
    {
        return $this->response;
    }
}