<?php

namespace Eternity\Components\Connector\AnimalId\Definitions;

use Eternity\Components\Definition\AbstractDefinition;

/**
 * Language codes definition
 *
 * Class LanguageCodeDefinition
 * @package Eternity\Components\Connector\AnimalId\Definitions
 */
class LanguageCodeDefinition extends AbstractDefinition
{
    /**
     * English language code
     */
    public const EN = 'en';

    /**
     * Russian language code
     */
    public const RU = 'ru';

    /**
     * German language code
     */
    public const DE = 'de';

    /**
     * Ukrainian language code
     */
    public const UA = 'ua';

    /**
     * Method builds the whole definition structure
     *
     * Every array item must have next structure:
     *  - id
     *  - title
     *  - translation
     *
     * For example:
     *  [
     *      5 => [
     *          'id' => 5,
     *          'title' => 'String description',
     *          'translation_key' => 'Key for user readable translation',
     *      ],
     *      ...
     *  ]
     *
     * @return mixed
     */
    protected static function getList(): array
    {
        return [
            static::EN => [
                'id' => static::EN,
                'title' => 'English code',
            ],
            static::RU => [
                'id' => static::RU,
                'title' => 'Russian code',
            ],
            static::DE => [
                'id' => static::DE,
                'title' => 'German code',
            ],
            static::UA => [
                'id' => static::UA,
                'title' => 'Ukrainian code'
            ]
        ];
    }
}