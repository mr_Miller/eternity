<?php

namespace Eternity\Components\Connector\AnimalId\Definitions;

/**
 * @deprecated Use Eternity\Definitions\HeadersDefinition in stead of this class
 * Headers used in cross microservice requests
 *
 * Class HeadersDefinition
 * @package Eternity\Components\Connector\AnimalId\Definitions
 */
class HeadersDefinition
{
    /**
     * User ID
     */
    public const USER_ID = 'X-Eternity-User-Id';

    /**
     * Language Code
     */
    public const LANG_CODE = 'X-Eternity-Lang-Code';

    /**
     * Expand entities key
     */
    public const EXPAND = 'X-Eternity-Expand';

    /**
     * Number of items on single page
     */
    public const PAGE_SIZE = 'X-Pagination-Page-Size';

    /**
     * Current pagination page
     */
    public const PAGE_CURRENT = 'X-Pagination-Page-Current';

    /**
     * Order by
     */
    public const ORDER_BY = 'X-Order-By';

    /**
     * Order direction
     */
    public const ORDER_DIRECTION = 'X-Order-Direction';

    /**
     * X Sort
     */
    public const SORT = 'X-Sort';

    /**
     * Filter Object
     */
    public const FILTER_OBJECT = 'X-Filter-Object';

    /**
     * Filter Suite
     */
    public const FILTER_SUITE = 'X-Filter-Suite';

    /**
     * Filter For
     */
    public const FILTER_FOR = 'X-Filter-For';
}