<?php

namespace Eternity\Components\Connector\AnimalId\Dto;

use Eternity\Contracts\Arrayable;
use Eternity\Traits\FromArray;

/**
 * @deprecated For new connectors use pagination DTO in gateway
 * Pagination DTO
 *
 * Pagination options
 *
 * Class MetadataDto
 * @package Eternity\Components\Connector
 */
class PaginationDto implements Arrayable
{
    use FromArray;

    /**
     * @var string|null Sorting field name
     */
    public $orderBy;

    /**
     * @var string|null Sorting direction. Example: "ASC", "DESC"
     */
    public $orderDirection;

    /**
     * @var int|null Current page number
     */
    public $pageCurrent;

    /**
     * @var int|null Number of items on single page
     */
    public $pageSize;

    /**
     * Transforms object into array
     *
     * @return array
     */
    public function toArray(): array
    {
        return [
            'orderBy'        => $this->orderBy,
            'orderDirection' => $this->orderDirection,
            'pageCurrent'    => $this->pageCurrent,
            'pageSize'       => $this->pageSize,
        ];
    }

}