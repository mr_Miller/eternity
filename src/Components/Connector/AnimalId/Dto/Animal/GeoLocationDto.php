<?php

namespace Eternity\Components\Connector\AnimalId\Dto\Animal;

use Eternity\Contracts\Arrayable;
use Eternity\Traits\FromArray;

/**
 * Class GeoLocationDto
 * @package Eternity\Components\Connector\AnimalId\Dto\Animal
 */
class GeoLocationDto implements Arrayable
{
    use FromArray;

    /**
     * @var int
     */
    public $id;

    /**
     * @var int
     */
    public $animal_id;

    /**
     * @var int
     */
    public $created_by;

    /**
     * @var float
     */
    public $latitude;

    /**
     * @var float
     */
    public $longitude;

    /**
     * @var string
     */
    public $user_agent;

    /**
     * @var string
     */
    public $ip_address;

    /**
     * @var string
     */
    public $track_key;

    /**
     * @var string
     */
    public $created_at;

    /**
     * Transforms object into array
     *
     * @return array
     */
    public function toArray(): array
    {
        return [
            'id'         => $this->id,
            'animal_id'  => $this->animal_id,
            'created_by' => $this->created_by,
            'latitude'   => $this->latitude,
            'longitude'  => $this->longitude,
            'user_agent' => $this->user_agent,
            'ip_address' => $this->ip_address,
            'track_key'  => $this->track_key,
            'created_at' => $this->created_at
        ];
    }
}