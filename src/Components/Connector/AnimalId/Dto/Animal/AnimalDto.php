<?php

namespace Eternity\Components\Connector\AnimalId\Dto\Animal;

use Eternity\Contracts\Arrayable;
use Eternity\Traits\FromArray;

/**
 * Class PetDto
 * @package Eternity\Components\Connector\AnimalId\Dto\Animal
 */
class AnimalDto implements Arrayable
{
    use FromArray;

    /**
     * @var int
     */
    public $id;

    /**
     * @var string format ISO 8601
     */
    public $birthday;

    /**
     * @var string
     */
    public $breed;

    /**
     * @var string
     */
    public $color;

    /**
     * @var string
     */
    public $nickname;

    /**
     * @var string format ISO 8601
     */
    public $register_date;

    /**
     * @var int
     */
    public $size;

    /**
     * @var bool
     */
    public $sterilization;

    /**
     * @var string
     */
    public $transponder;

    /**
     * @var string
     */
    public $qr_passport_number;

    /**
     * @var bool
     */
    public $is_wanted;

    /**
     * @var bool
     */
    public $in_europetnet;

    /**
     * @var int
     */
    public $cites;

    /**
     * @var int
     */
    public $status;

    /**
     * @var string
     */
    public $breeder_email;

    /**
     * @var string
     */
    public $father_transponder;

    /**
     * @var string
     */
    public $mother_transponder;

    /**
     * @var string
     */
    public $pedigree_number;

    /**
     * @var string
     */
    public $short_description;

    /**
     * @var int
     */
    public $species_id;

    /**
     * @var bool
     */
    public $deleted;

    /**
     * @var int
     */
    public $gender;

    /**
     * @var int
     */
    public $created_by;

    /**
     * @var string format ISO 8601
     */
    public $created_at;

    /**
     * @var string format ISO 8601
     */
    public $updated_at;

    /**
     * @var int
     */
    public $country_id;

    /**
     * @var string
     */
    public $postcode;

    /**
     * @var string
     */
    public $address;

    /**
     * @var int|null
     */
    public $checked_by;

    /**
     * @var int|null
     */
    public $verified_by;

    /**
     * @var float
     */
    public $_lat;

    /**
     * @var float
     */
    public $_long;

    /**
     * @var bool
     */
    public $_baby;

    /**
     * @var int
     */
    public $_weight;

    /**
     * @var int
     */
    public $_preg_lact;

    /**
     * @var bool
     */
    public $_skin_problem;

    /**
     * @var bool
     */
    public $_lame;

    /**
     * @var bool
     */
    public $ap_basic_housetraining;

    /**
     * @var bool
     */
    public $ap_needs_some_training;

    /**
     * @var bool
     */
    public $ap_loves_toys_games;

    /**
     * @var bool
     */
    public $ap_crossbreed;

    /**
     * @var bool
     */
    public $ap_lazy;

    /**
     * @var bool
     */
    public $ap_active;

    /**
     * @var bool
     */
    public $ap_dog_friendly;

    /**
     * @var bool
     */
    public $ap_child_friendly;

    /**
     * @var bool
     */
    public $ap_stranger_friendly;

    /**
     * @var bool
     */
    public $ap_very_clever;

    /**
     * @var bool
     */
    public $ap_young_at_heart;

    /**
     * @var bool
     */
    public $ap_medical_needs;

    /**
     * @var bool
     */
    public $ap_gentle_giant;

    /**
     * @var bool
     */
    public $ap_watchful;

    /**
     * @var bool
     */
    public $ap_pure_breed;

    /**
     * @var string
     */
    public $ap_likes;

    /**
     * @var string
     */
    public $ap_type_of_home_needed;

    /**
     * @var string
     */
    public $ap_more_about_me;

    /**
     * Transforms object into array
     *
     * @return array
     */
    public function toArray(): array
    {
        return [
            'id'                     => $this->id,
            'birthday'               => $this->birthday,
            'breed'                  => $this->breed,
            'color'                  => $this->color,
            'nickname'               => $this->nickname,
            'register_date'          => $this->register_date,
            'size'                   => $this->size,
            'sterilization'          => $this->sterilization,
            'transponder'            => $this->transponder,
            'qr_passport_number'     => $this->qr_passport_number,
            'is_wanted'              => $this->is_wanted,
            'in_europetnet'          => $this->in_europetnet,
            'cites'                  => $this->cites,
            'status'                 => $this->status,
            'breeder_email'          => $this->breeder_email,
            'father_transponder'     => $this->father_transponder,
            'mother_transponder'     => $this->mother_transponder,
            'pedigree_number'        => $this->pedigree_number,
            'short_description'      => $this->short_description,
            'species_id'             => $this->species_id,
            'deleted'                => $this->deleted,
            'gender'                 => $this->gender,
            'created_by'             => $this->created_by,
            'created_at'             => $this->created_at,
            'updated_at'             => $this->updated_at,
            'country_id'             => $this->country_id,
            'postcode'               => $this->postcode,
            'address'                => $this->address,
            'checked_by'             => $this->checked_by,
            'verified_by'            => $this->verified_by,
            '_lat'                   => $this->_lat,
            '_long'                  => $this->_long,
            '_baby'                  => $this->_baby,
            '_weight'                => $this->_weight,
            '_preg_lact'             => $this->_preg_lact,
            '_skin_problem'          => $this->_skin_problem,
            '_lame'                  => $this->_lame,
            'ap_basic_housetraining' => $this->ap_basic_housetraining,
            'ap_needs_some_training' => $this->ap_needs_some_training,
            'ap_loves_toys_games'    => $this->ap_loves_toys_games,
            'ap_crossbreed'          => $this->ap_crossbreed,
            'ap_lazy'                => $this->ap_lazy,
            'ap_active'              => $this->ap_active,
            'ap_dog_friendly'        => $this->ap_dog_friendly,
            'ap_child_friendly'      => $this->ap_child_friendly,
            'ap_stranger_friendly'   => $this->ap_stranger_friendly,
            'ap_very_clever'         => $this->ap_very_clever,
            'ap_young_at_heart'      => $this->ap_young_at_heart,
            'ap_medical_needs'       => $this->ap_medical_needs,
            'ap_gentle_giant'        => $this->ap_gentle_giant,
            'ap_watchful'            => $this->ap_watchful,
            'ap_likes'               => $this->ap_likes,
            'ap_type_of_home_needed' => $this->ap_type_of_home_needed,
            'ap_more_about_me'       => $this->ap_more_about_me,
            'ap_pure_breed'          => $this->ap_pure_breed,
        ];
    }
}