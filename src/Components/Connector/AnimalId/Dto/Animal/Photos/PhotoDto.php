<?php

namespace Eternity\Components\Connector\AnimalId\Dto\Animal\Photos;

use Eternity\Contracts\Arrayable;
use Eternity\Traits\FromArray;

/**
 * Class PhotoDto
 * @package Eternity\Components\Connector\AnimalId\Dto\Animal\Photos
 */
class PhotoDto implements Arrayable
{
    use FromArray;

    /**
     * @var int ID
     */
    public $id;

    /**
     * @var bool Is main photo
     */
    public $is_main;

    /**
     * @var string Filename
     */
    public $filename;

    /**
     * @var string Creation date
     */
    public $created_at;

    /**
     * @var string Url to photo original file
     */
    public $origin;

    /**
     * @var array List of available thumbnails of photo
     */
    public $thumbnails;

    /**
     * Transforms object into array
     *
     * @return array
     */
    public function toArray(): array
    {
        return [
            'id'         => $this->id,
            'is_main'    => $this->is_main,
            'filename'   => $this->filename,
            'created_at' => $this->created_at,
            'origin'     => $this->origin,
            'thumbnails' => $this->thumbnails,
        ];
    }

}