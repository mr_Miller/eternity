<?php

namespace Eternity\Components\Connector\AnimalId\Dto\Animal;

use Eternity\Contracts\Arrayable;
use Eternity\Traits\FromArray;

/**
 * Class UpdateAnimalDto
 * @package Eternity\Components\Connector\AnimalId\Dto\Animal
 */
class UpdateAnimalDto implements Arrayable
{
    use FromArray;
    /**
     * @var string format ISO 8601
     */
    public $birthday;

    /**
     * @var string
     */
    public $breed;

    /**
     * @var string
     */
    public $color;

    /**
     * @var string
     */
    public $nickname;

    /**
     * @var int
     */
    public $size;

    /**
     * @var bool
     */
    public $sterilization;

    /**
     * @var int
     */
    public $cites;

    /**
     * @var string
     */
    public $breeder_email;

    /**
     * @var string
     */
    public $father_transponder;

    /**
     * @var string
     */
    public $mother_transponder;

    /**
     * @var string
     */
    public $pedigree_number;

    /**
     * @var string
     */
    public $short_description;

    /**
     * @var int
     */
    public $species_id;

    /**
     * @var int
     */
    public $gender;

    /**
     * Transforms object into array
     *
     * @return array
     */
    public function toArray(): array
    {
        return [
            'birthday'           => $this->birthday,
            'breed'              => $this->breed,
            'color'              => $this->color,
            'nickname'           => $this->nickname,
            'size'               => $this->size,
            'sterilization'      => $this->sterilization,
            'cites'              => $this->cites,
            'breeder_email'      => $this->breeder_email,
            'father_transponder' => $this->father_transponder,
            'mother_transponder' => $this->mother_transponder,
            'pedigree_number'    => $this->pedigree_number,
            'short_description'  => $this->short_description,
            'species_id'         => $this->species_id,
            'gender'             => $this->gender,
        ];
    }
}