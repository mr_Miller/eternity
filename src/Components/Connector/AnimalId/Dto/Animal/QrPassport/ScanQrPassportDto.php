<?php

namespace Eternity\Components\Connector\AnimalId\Dto\Animal\QrPassport;

use Eternity\Contracts\Arrayable;
use Eternity\Traits\FromArray;

/**
 * Class ScanQrPassportDto
 * @package Eternity\Components\Connector\AnimalId\Dto\QrPassport
 */
class ScanQrPassportDto implements Arrayable
{
    use FromArray;

    /**
     * @var float
     */
    public $latitude;

    /**
     * @var float
     */
    public $longitude;

    /**
     * Transforms object into array
     *
     * @return array
     */
    public function toArray(): array
    {
        return [
            'latitude'   => $this->latitude,
            'longitude'  => $this->longitude,
        ];
    }
}