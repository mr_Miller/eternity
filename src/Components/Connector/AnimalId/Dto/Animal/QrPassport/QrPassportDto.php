<?php

namespace Eternity\Components\Connector\AnimalId\Dto\Animal\QrPassport;

use Eternity\Contracts\Arrayable;
use Eternity\Traits\FromArray;

/**
 * Class QrPassportDto
 * @package Eternity\Components\Connector\AnimalId\Dto\QrPassport
 */
class QrPassportDto implements Arrayable
{
    use FromArray;

    /**
     * @var
     */
    public $id;

    /**
     * @var
     */
    public $ais_product_card_id;

    /**
     * @var
     */
    public $lot_number;

    /**
     * @var
     */
    public $batch_number;

    /**
     * @var
     */
    public $value;

    /**
     * @var
     */
    public $current_user_id;

    /**
     * @var
     */
    public $created_at;

    /**
     * @var
     */
    public $updated_at;

    /**
     * @var
     */
    public $status;

    /**
     * @var
     */
    public $activated_code;

    /**
     * Transforms object into array
     *
     * @return array
     */
    public function toArray(): array
    {
        return [
            'id'              => $this->id,
            'product_card_id' => $this->ais_product_card_id,
            'lot'             => $this->lot_number,
            'batch'           => $this->batch_number,
            'serial_code'     => $this->value,
            'owner_user_id'   => $this->current_user_id,
            'created_at'      => $this->created_at,
            'updated_at'      => $this->updated_at,
            'status'          => $this->status,
            'activation_code' => $this->activated_code,
        ];
    }
}