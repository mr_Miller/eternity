<?php

namespace Eternity\Components\Connector\AnimalId\Dto\Animal\Documents;

use Eternity\Contracts\Arrayable;
use Eternity\Traits\FromArray;

/**
 * Class DocumentDto
 * @package Eternity\Components\Connector\AnimalId\Dto\Animal\Documents
 */
class DocumentDto implements Arrayable
{
    use FromArray;

    /**
     * @var int
     */
    public $id;

    /**
     * @var int
     */
    public $type;

    /**
     * @var string
     */
    public $filename;

    /**
     * @var string
     */
    public $caption;

    /**
     * @var bool
     */
    public $is_public;

    /**
     * @var string
     */
    public $created_at;

    /**
     * @var string
     */
    public $url;

    /**
     * Transforms object into array
     *
     * @return array
     */
    public function toArray(): array
    {
        return [
            'id'         => $this->id,
            'type'       => $this->type,
            'filename'   => $this->filename,
            'caption'    => $this->caption,
            'is_public'  => $this->is_public,
            'created_at' => $this->created_at,
            'url'        => $this->url,
        ];
    }
}