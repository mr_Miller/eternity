<?php

namespace Eternity\Components\Connector\AnimalId\Dto\Animal\Documents;

use Eternity\Contracts\Arrayable;
use Eternity\Traits\FromArray;

class DocumentCreateDto implements Arrayable
{
    use FromArray;

    /**
     * @var int
     */
    public $type;

    /**
     * @var string
     */
    public $filename;

    /**
     * @var string
     */
    public $caption;

    /**
     * @var bool
     */
    public $is_public;

    /**
     * Transforms object into array
     *
     * @return array
     */
    public function toArray(): array
    {
        return [
            'type'      => $this->type,
            'filename'  => $this->filename,
            'caption'   => $this->caption,
            'is_public' => $this->is_public,
        ];
    }
}