<?php

namespace Eternity\Components\Connector\AnimalId\Dto\User;

use Eternity\Contracts\Arrayable;
use Eternity\Traits\FromArray;

/**
 * Class AvatarDto
 * @package Eternity\Components\Connector\AnimalId\Dto\User
 */
class AvatarDto implements Arrayable
{
    use FromArray;

    /**
     * @var string
     */
    public $filename;

    /**
     * @var string
     */
    public $origin;

    /**
     * @var array
     */
    public $thumbnails;

    /**
     * Transforms object into array
     *
     * @return array
     */
    public function toArray(): array
    {
        return [
            'filename'   => $this->filename,
            'origin'     => $this->origin,
            'thumbnails' => $this->thumbnails,
        ];
    }

}