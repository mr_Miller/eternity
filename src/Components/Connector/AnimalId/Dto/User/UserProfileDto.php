<?php

namespace Eternity\Components\Connector\AnimalId\Dto\User;

use Eternity\Contracts\Arrayable;
use Eternity\Traits\FromArray;

/**
 * Class UserProfileDto
 * @package Eternity\Components\Connector\AnimalId\Dto\User
 */
class UserProfileDto implements Arrayable
{
    use FromArray;

    /**
     * @var int
     */
    public $id;

    /**
     * @var string
     */
    public $email;

    /**
     * @var string
     */
    public $phone;

    /**
     * @var string
     */
    public $first_name;

    /**
     * @var string|null
     */
    public $last_name;

    /**
     * @var int|null
     */
    public $country_id;

    /**
     * @var string|null
     */
    public $postcode;

    /**
     * @var string|null
     */
    public $address;

    /**
     * @var int|null
     */
    public $gender;

    /**
     * @var array|null
     */
    public $avatar;

    /**
     * Transforms object into array
     *
     * @return array
     */
    public function toArray(): array
    {
        return [
            'id'         => $this->id,
            'email'      => $this->email,
            'phone'      => $this->phone,
            'first_name' => $this->first_name,
            'last_name'  => $this->last_name,
            'country_id' => $this->country_id,
            'postcode'   => $this->postcode,
            'address'    => $this->address,
            'gender'     => $this->gender,
            'avatar'     => $this->avatar,
        ];
    }

}