<?php

namespace Eternity\Components\Connector\AnimalId\SubConnectors\Animal;

use Eternity\Components\Connector\AbstractConnector;
use Eternity\Components\Connector\AnimalId\Dto\Animal\AnimalDto;
use Eternity\Components\Connector\AnimalId\Dto\Animal\CreateAnimalDto;
use Eternity\Components\Connector\AnimalId\Dto\Animal\GeoLocationDto;
use Eternity\Components\Connector\AnimalId\Dto\Animal\UpdateAnimalDto;
use Eternity\Components\Connector\AnimalId\Dto\PaginationDto;
use Eternity\Components\Connector\AnimalId\Responses\Animal\AnimalCollectionResponse;
use Eternity\Components\Connector\AnimalId\Responses\Animal\AnimalResponse;
use Eternity\Components\Connector\AnimalId\Responses\Animal\CreatedAnimalResponse;
use Eternity\Components\Connector\AnimalId\Responses\Animal\GeoLocationsResponse;
use Eternity\Definitions\HeadersDefinition;

/**
 * Class PetConnector
 *
 * Describes methods for pets management in Animal ID
 *
 * Dont use this connector in your microservice!
 * Use AnimalIdConnector instead!
 *
 * @package Eternity\Components\Connector\AnimalId\Animal
 */
class AnimalConnector extends AbstractConnector
{
    /**
     * Returns pets by owner
     * @param int $userId Authorized user ID
     * @param string $languageCode
     * @param \Eternity\Components\Connector\AnimalId\Dto\PaginationDto $paginationDto
     * @param string $expand
     * @return \Eternity\Components\Connector\AnimalId\Responses\Animal\AnimalCollectionResponse
     */
    public function getPetsByOwner(
        int $userId,
        PaginationDto $paginationDto,
        string $languageCode = null,
        ?string $expand = null
    ): AnimalCollectionResponse {
        $headers = [
            HeadersDefinition::LANG_CODE       => $languageCode,
            HeadersDefinition::USER_ID         => $userId,
            HeadersDefinition::ORDER_DIRECTION => $paginationDto->orderDirection,
            HeadersDefinition::ORDER_BY        => $paginationDto->orderBy,
            HeadersDefinition::PAGE_CURRENT    => $paginationDto->pageCurrent,
            HeadersDefinition::PAGE_SIZE       => $paginationDto->pageSize,
            HeadersDefinition::EXPAND          => $expand,
        ];
        $response = $this->get('v1/pets', 200, array_filter($headers));
        $result = [];
        foreach ($response->getPayloadItems() as $payloadItem) {
            $result[] = AnimalDto::fromArray($payloadItem);
        }

        return new AnimalCollectionResponse($result, $response);
    }

    /**
     * Returns pet by ID
     * @param int $id Unique animal identifier
     * @param int $userId Authorized user ID
     * @param string $languageCode
     * @param string|null $expand
     * @return \Eternity\Components\Connector\AnimalId\Responses\Animal\AnimalResponse
     */
    public function getPetById(int $id, int $userId, string $languageCode, ?string $expand = null): AnimalResponse
    {
        $headers = [
            HeadersDefinition::LANG_CODE => $languageCode,
            HeadersDefinition::USER_ID   => $userId,
            HeadersDefinition::EXPAND    => $expand,
        ];
        $response = $this->get('v1/pets/' . $id, 200, array_filter($headers));

        return new AnimalResponse($response, AnimalDto::fromArray($response->getPayloadItem()));
    }

    /**
     * Returns pet by code
     * @param string $code
     * @param int $userId Authorized user ID
     * @param string $languageCode
     * @param string|null $expand
     * @return \Eternity\Components\Connector\AnimalId\Responses\Animal\AnimalResponse
     */
    public function getPetByCode(string $code, int $userId, string $languageCode, ?string $expand = null): AnimalResponse
    {
        $headers = [
            HeadersDefinition::LANG_CODE => $languageCode,
            HeadersDefinition::USER_ID   => $userId,
            HeadersDefinition::EXPAND    => $expand,
        ];
        $response = $this->get('v1/pets/by/code/' . $code, 200, array_filter($headers));

        return new AnimalResponse($response, AnimalDto::fromArray($response->getPayloadItem()));
    }

    /**
     * Create pet
     * @param \Eternity\Components\Connector\AnimalId\Dto\Animal\CreateAnimalDto $animalDto
     * @param int $userId Authorized user ID
     * @param string $languageCode
     * @return \Eternity\Components\Connector\AnimalId\Responses\Animal\CreatedAnimalResponse
     */
    public function createPet(CreateAnimalDto $animalDto, int $userId, string $languageCode): CreatedAnimalResponse
    {
        $headers = [
            HeadersDefinition::LANG_CODE => $languageCode,
            HeadersDefinition::USER_ID   => $userId
        ];
        $response = $this->post('v1/pets', 201, $headers, $animalDto->toArray());

        return new CreatedAnimalResponse($response->getPayloadItem()['id'], $response);
    }

    /**
     * Update pet
     * @param int $id Unique animal identifier
     * @param \Eternity\Components\Connector\AnimalId\Dto\Animal\UpdateAnimalDto $animalDto
     * @param int $userId Authorized user ID
     * @param string $languageCode
     */
    public function updatePet(int $id, UpdateAnimalDto $animalDto, int $userId, string $languageCode): void
    {
        $headers = [
            HeadersDefinition::LANG_CODE => $languageCode,
            HeadersDefinition::USER_ID   => $userId
        ];
        $this->put('v1/pets/' . $id, 204, $headers, $animalDto->toArray());
    }

    /**
     * Get scans collection
     * @param \Eternity\Components\Connector\AnimalId\Dto\PaginationDto $paginationDto
     * @param int $id Unique animal identifier
     * @param int $userId Authorized user ID
     * @param string $languageCode
     * @return \Eternity\Components\Connector\AnimalId\Responses\Animal\GeoLocationsResponse
     */
    public function getAnimalScanLocations(
        int $userId,
        PaginationDto $paginationDto,
        int $id,
        string $languageCode
    ): GeoLocationsResponse {
        $headers = [
            HeadersDefinition::LANG_CODE       => $languageCode,
            HeadersDefinition::USER_ID         => $userId,
            HeadersDefinition::ORDER_DIRECTION => $paginationDto->orderDirection,
            HeadersDefinition::ORDER_BY        => $paginationDto->orderBy,
            HeadersDefinition::PAGE_CURRENT    => $paginationDto->pageCurrent,
            HeadersDefinition::PAGE_SIZE       => $paginationDto->pageSize,
        ];
        $response = $this->get('v1/pets/' . $id . '/scans', 200, array_filter($headers));
        $result = [];
        foreach ($response->getPayloadItems() as $payloadItem) {
            $result[] = GeoLocationDto::fromArray($payloadItem);
        }

        return new GeoLocationsResponse($result, $response);
    }
}