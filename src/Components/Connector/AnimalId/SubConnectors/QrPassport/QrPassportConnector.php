<?php

namespace Eternity\Components\Connector\AnimalId\SubConnectors\QrPassport;

use Eternity\Components\Connector\AbstractConnector;
use Eternity\Components\Connector\AnimalId\Dto\Animal\QrPassport\QrPassportDto;
use Eternity\Components\Connector\AnimalId\Dto\Animal\QrPassport\ScanQrPassportDto;
use Eternity\Components\Connector\AnimalId\Responses\Animal\QrPassport\QrPassportResponse;
use Eternity\Definitions\HeadersDefinition;

/**
 * Class QrPassportConnector
 * @package Eternity\Components\Connector\AnimalId\SubConnectors\QrPassport
 */
class QrPassportConnector extends AbstractConnector
{

    /**
     * Returns pet by code
     * @param string $code
     * @param string $activation
     * @param int $userId Authorized user ID
     * @param string $languageCode
     * @param string|null $expand
     * @return \Eternity\Components\Connector\AnimalId\Responses\Animal\QrPassport\QrPassportResponse
     */
    public function getPassportByCodeAndActivation(string $code, string $activation, int $userId, string $languageCode, ?string $expand = null): QrPassportResponse
    {
        $headers = [
            HeadersDefinition::LANG_CODE => $languageCode,
            HeadersDefinition::USER_ID   => $userId,
            HeadersDefinition::EXPAND    => $expand,
        ];
        $response = $this->get('v1/passports/by/code/' . $code . '/' . $activation, 200, array_filter($headers));

        return new QrPassportResponse($response, QrPassportDto::fromArray($response->getPayloadItem()));
    }



    /**
     * @param string $serialCode
     * @param string $activationCode
     * @param int $userId
     * @param string $languageCode
     */
    public function validateQrPassport(
        string $serialCode,
        string $activationCode,
        int $userId,
        string $languageCode
    ): void {
        $headers = [
            HeadersDefinition::LANG_CODE => $languageCode,
            HeadersDefinition::USER_ID   => $userId
        ];
        $body = [
            'serial_code'     => $serialCode,
            'activation_code' => $activationCode
        ];
        $this->post('v1/passports/validate', 204, $headers, $body);
    }

    /**
     * @param string $code
     * @param \Eternity\Components\Connector\AnimalId\Dto\Animal\QrPassport\ScanQrPassportDto $scanQrPassportDto
     * @param int $userId
     * @param string $languageCode
     */
    public function scanQrPassport(
        string $code,
        ScanQrPassportDto $scanQrPassportDto,
        int $userId,
        string $languageCode
    ): void {
        $headers = [
            HeadersDefinition::LANG_CODE => $languageCode,
            HeadersDefinition::USER_ID   => $userId
        ];
        $this->post('v1/passports/' . $code. '/scans', 204, $headers, $scanQrPassportDto->toArray());
    }



    /**
     * @param string $serialCode
     * @param string $activationCode
     * @param int $animalId
     * @param int $userId
     * @param string $languageCode
     */
    public function activateQrPassport(
        string $serialCode,
        string $activationCode,
        int $animalId,
        int $userId,
        string $languageCode
    ): void {
        $headers = [
            HeadersDefinition::LANG_CODE => $languageCode,
            HeadersDefinition::USER_ID   => $userId
        ];
        $body = [
            'serial_code'     => $serialCode,
            'activation_code' => $activationCode,
            'animal_id'       => $animalId
        ];
        $this->post('v1/passports/activate', 204, $headers, $body);
    }
}