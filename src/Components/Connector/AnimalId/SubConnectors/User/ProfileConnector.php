<?php

namespace Eternity\Components\Connector\AnimalId\SubConnectors\User;

use Eternity\Components\Connector\AbstractConnector;
use Eternity\Components\Connector\AnimalId\Definitions\HeadersDefinition;
use Eternity\Components\Connector\AnimalId\Dto\User\AvatarDto;
use Eternity\Components\Connector\AnimalId\Dto\User\UpdateProfileDto;
use Eternity\Components\Connector\AnimalId\Dto\User\UserProfileDto;
use Eternity\Components\Connector\AnimalId\Responses\User\Responses\AvatarResponse;
use Eternity\Components\Connector\AnimalId\Responses\User\Responses\ProfileResponse;

/**
 * Class ProfileConnector
 * @package Eternity\Components\Connector\AnimalId\Responses\User
 */
class ProfileConnector extends AbstractConnector
{
    /**
     * Get user profile
     * @param int $userId Current authorized user ID
     * @param string|null $expand Format json
     * @return \Eternity\Components\Connector\AnimalId\Responses\User\Responses\ProfileResponse
     */
    public function getProfile(int $userId, ?string $expand = null): ProfileResponse
    {
        $headers = [
            HeadersDefinition::EXPAND  => $expand,
            HeadersDefinition::USER_ID => $userId,
        ];
        $response = $this->get('v1/profiles', 200, array_filter($headers));

        return new ProfileResponse(UserProfileDto::fromArray($response->getPayloadItem()), $response);
    }

    /**
     * Updates user profile fields
     *
     * Each field can be updated separetly
     *
     * @param \Eternity\Components\Connector\AnimalId\Dto\User\UpdateProfileDto $updateProfileDto
     * @param int $userId
     * @param string $languageCode
     */
    public function updateProfile(UpdateProfileDto $updateProfileDto, int $userId, string $languageCode = null): void
    {
        $headers = [
            HeadersDefinition::USER_ID   => $userId,
            HeadersDefinition::LANG_CODE => $languageCode,
        ];
        $this->patch('v1/profiles', 204, array_filter($headers), $updateProfileDto->toArray());
    }

    /**
     * Update or create avatar
     *
     * @param string $filename
     * @param int $userId
     * @param string|null $languageCode
     */
    public function updateAvatar(string $filename, int $userId, string $languageCode = null): void
    {
        $headers = [
            HeadersDefinition::USER_ID   => $userId,
            HeadersDefinition::LANG_CODE => $languageCode,
        ];
        $this->put('v1/profiles/avatar', 204, $headers, [
            'filename' => $filename,
        ]);
    }

    /**
     * Returns current user avatar
     *
     * @param int $userId
     * @return \Eternity\Components\Connector\AnimalId\Responses\User\Responses\AvatarResponse
     */
    public function getAvatar(int $userId): AvatarResponse
    {
        $headers = [
            HeadersDefinition::USER_ID => $userId,
        ];
        $response = $this->get('v1/profiles/avatar', 200, $headers);

        return new AvatarResponse(AvatarDto::fromArray($response->getPayloadItem()), $response);
    }
}