<?php

namespace Eternity\Components\Connector\AnimalId\SubConnectors\Definitions;

use Eternity\Components\Connector\AbstractConnector;
use Eternity\Components\Connector\AnimalId\Responses\Definition\DefinitionResponse;

/**
 * Class DefinitionConnector
 * @package Eternity\Components\Connector\AnimalId\SubConnectors\Definitions
 */
class DefinitionConnector extends AbstractConnector
{
    /**
     * Returns the list of definitions
     */
    public function getList(): DefinitionResponse
    {
        $response = $this->get('v1/definitions');
        return new DefinitionResponse($response->getPayloadItems(), $response);
    }
}