<?php

namespace Eternity\Components\Connector\AnimalId;

use Eternity\Components\Connector\AbstractConnector;
use Eternity\Components\Connector\AnimalId\SubConnectors\Animal\AnimalConnector;
use Eternity\Components\Connector\AnimalId\SubConnectors\Definitions\DefinitionConnector;
use Eternity\Components\Connector\AnimalId\SubConnectors\QrPassport\QrPassportConnector;
use Eternity\Components\Connector\AnimalId\SubConnectors\User\ProfileConnector;
use Eternity\Http\Client;

/**
 * Animal ID connector
 *
 * Use this connector in your microservice!
 *
 * Class AnimalIdConnector
 * @package Eternity\Components\Connector\AnimalId
 * @property \Eternity\Components\Connector\AnimalId\SubConnectors\Definitions\DefinitionConnector $definition
 * @property \Eternity\Components\Connector\AnimalId\SubConnectors\User\ProfileConnector $userProfile
 * @property \Eternity\Components\Connector\AnimalId\SubConnectors\Animal\AnimalConnector $animal
 * @property \Eternity\Components\Connector\AnimalId\SubConnectors\QrPassport\QrPassportConnector $qrPassport
 *
 */
class AnimalIdConnector extends AbstractConnector
{
    /**
     * AnimalIdConnector constructor.
     * @param \Eternity\Http\Client $client
     * @param string $serviceUrl
     * @param string $traceId
     * @param array $headers
     */
    public function __construct(Client $client, string $serviceUrl, string $traceId, array $headers = [])
    {
        parent::__construct($client, $serviceUrl, $traceId, $headers);
        $this->addSubConnector(DefinitionConnector::class, 'definition')
            ->addSubConnector(ProfileConnector::class, 'userProfile')
            ->addSubConnector(AnimalConnector::class, 'animal')
            ->addSubConnector(QrPassportConnector::class, 'qrPassport');
    }
}