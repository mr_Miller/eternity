## Abstract Connector
#### Class reference
`Eternity\Components\Connector\AbstractConnector`

#### Usage
Extend this class and define methods to interact via HTTP with external microservice

#### Usage example of extended connector

Assume, we have created `Marketplace` which extends `AbstractConnector`. 
We can use this connector in `Gateway` microservice controller.

Request flow will be next:
1. Client request product with ID 1 from Gateway;
2. Gateway through the connector asks product with ID 1;
3. If product is found Gateway would transfer the response to client.
4. In case if error has occurred on external Marketplace microservice, the `ResponseException` will be thrown.
This exception must be caught up and transmitted to client in original form.

```php
// use ResponseDto;
use Eternity\Components\Connector\AbstractConnector;

class Marketplace extends AbstractConnector
{
    public function getProduct(int $id): ResponseDto
    {
        /** @var \Eternity\Http\Contracts\ExtendedResponse $response */
        $response = $this->get('http://animal-id/product/' . $id);
        $productDto = ProductDto::fromArray($response->getPayloadItem());
        return new ResponseDto($response, $productDto);        
    }
}

class ProductController
{
    /**
     * @var \Eternity\Http\Client
     */
    private $client;
    
    public function __construct(\Eternity\Http\Client $client) 
    {
        $this->client = $client;
    }

    public function get(int $id): array
    {
        $connector = new Marketplace('http://marketplace.loc', $this->client);
        // Connector returns ResponseDto of requested product which contains 
        // Product Dto and Response raw array
        $responseDto = $connector->getProduct($id);
        // To get ProductDto and do some actions with:
        $productDto = $responseDto->getDto();
        // For example notify user that we can obtain his secret information about products
        $this->emailService->notify($productDto->email, 'We know where you live!');
        // From connector we can obtain response, and get it's body
        /** @var \Eternity\Http\Contracts\ExtendedResponse $response */
        $response = $responseDto->getResponse();
        // Just transmit the response original body to client
        return $response->getBody();
    }
}
```