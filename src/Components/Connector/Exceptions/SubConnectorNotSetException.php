<?php

namespace Eternity\Components\Connector\Exceptions;

use Eternity\Exceptions\ServerException;
use Throwable;

/**
 * Class SubConnectorNotSetException
 * @package Eternity\Components\Connector\AnimalId
 */
class SubConnectorNotSetException extends ServerException
{

    public function __construct(string $title = null, string $detail = null, Throwable $previous = null)
    {
        parent::__construct($title, $detail, $previous);
        $this->type = 'SubConnectorNotSetException';
    }
}