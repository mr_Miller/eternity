<?php

namespace Eternity\Components\Connector\Notification;

use Eternity\Components\Connector\AbstractConnector;
use Eternity\Components\Connector\Notification\SubConnectors\Device\DeviceConnector;
use Eternity\Components\Connector\Notification\SubConnectors\Notify\NotifyConnector;
use Eternity\Http\Client;

/**
 * Class NotificationConnector
 * @package Eternity\Components\Connector\Notification
 * @property \Eternity\Components\Connector\Notification\SubConnectors\Device\DeviceConnector $device
 * @property \Eternity\Components\Connector\Notification\SubConnectors\Notify\NotifyConnector $notify
 */
class NotificationConnector extends AbstractConnector
{
    /**
     * NotificationConnector constructor.
     * @param \Eternity\Http\Client $client
     * @param string $serviceUrl
     * @param string $traceId
     * @param array $headers
     */
    public function __construct(Client $client, string $serviceUrl, string $traceId, array $headers = [])
    {
        parent::__construct($client, $serviceUrl, $traceId, $headers);
        $this->addSubConnector(DeviceConnector::class, 'device')
            ->addSubConnector(NotifyConnector::class, 'notify');
    }
}