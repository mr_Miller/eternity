<?php

namespace Eternity\Components\Connector\Notification\Firebase\Map;

use Eternity\Definitions\Notification\Firebase\Apple\PriorityDefinition as ApplePriorityDefinition;
use Eternity\Definitions\Notification\Firebase\PriorityDefinition;

/**
 * Class ApplePriority
 * @package Eternity\Components\Connector\Notification\Firebase\Map
 */
class ApplePriority
{
    private const MAP = [
        PriorityDefinition::NORMAL => ApplePriorityDefinition::NORMAL,
        PriorityDefinition::HIGH => ApplePriorityDefinition::HIGH,
    ];

    /**
     * Get IOS priority
     * @param string $priority
     * @return string
     */
    public static function map(string $priority): string
    {
        if (!isset(self::MAP[$priority])) {
            throw new \RuntimeException("Priority '$priority' is not defined for IOS priorities");
        }

        return self::MAP[$priority];
    }
}