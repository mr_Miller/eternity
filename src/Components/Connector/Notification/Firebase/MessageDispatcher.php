<?php

namespace Eternity\Components\Connector\Notification\Firebase;

use Eternity\Components\Connector\Notification\NotificationConnector;

/**
 * @deprecated Because Push messages must be sent via Event-Bus
 * todo Resolve this class as its deprecated
 * todo When Event-Bus will be the only way to send push, delete Message dispatcher
 *
 * Class PushDispatcher
 * @package Eternity\Components\Connector\Notification
 */
class MessageDispatcher
{
    /**
     * @var array List of deferred push events
     */
    protected $messages;

    /**
     * @var \Eternity\Components\Connector\Notification\NotificationConnector
     */
    private $notification;

    /**
     * PushDispatcher constructor.
     * @param \Eternity\Components\Connector\Notification\NotificationConnector $notification
     */
    public function __construct(NotificationConnector $notification)
    {
        $this->notification = $notification;
    }

    /**
     * Fires push event immediately
     *
     * This method makes external HTTP request and increase the request response time
     *
     * @param \Eternity\Components\Connector\Notification\Firebase\Message $message
     */
    public function fire(Message $message): void
    {
        $this->notification->notify->push([$message->toArray()]);
    }

    /**
     * Defers push event
     *
     * Deferred events can be sent by executing [[dispatch()]] method
     *
     * @param \Eternity\Components\Connector\Notification\Firebase\Message $message
     */
    public function defer(Message $message): void
    {
        $this->messages[] = $message->toArray();
    }

    /**
     * Defers multiple events
     * @param \Eternity\Components\Connector\Notification\Firebase\Message[] $messages
     */
    public function deferMultiple(array $messages): void
    {
        foreach ($messages as $message) {
            if (!$message instanceof Message) {
                throw new \InvalidArgumentException(
                    'Each element of $messages array must be instance of "' . Message::class . '" class'
                );
            }
            $this->defer($message);
        }
    }

    /**
     * Returns array of deferred messages
     * @return array
     */
    public function messages(): array
    {
        return $this->messages;
    }

    /**
     * Dispatches stored events
     *
     * Pushes will be sent to notification service, and further delivered to user
     */
    public function dispatch(): void
    {
        if (!empty($this->messages)) {
            $this->notification->notify->push($this->messages);
        }
    }
}