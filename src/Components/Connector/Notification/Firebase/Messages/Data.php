<?php

namespace Eternity\Components\Connector\Notification\Firebase\Messages;

use Eternity\Contracts\Arrayable;

/**
 * Class DataMessage
 * @package App\Infrastructure\Adapters\Firebase\Messages
 */
class Data implements Arrayable
{
    /**
     * @var array
     */
    private $data;

    /**
     * DataMessage constructor.
     * @param array $data
     */
    public function __construct(array $data)
    {
        $this->data = $data;
    }

    /**
     * @return array
     */
    public function data(): array
    {
        return $this->data;
    }

    /**
     * @return array
     */
    public function toArray(): array
    {
        return $this->data;
    }
}
