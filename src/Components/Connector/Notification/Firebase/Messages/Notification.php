<?php

namespace Eternity\Components\Connector\Notification\Firebase\Messages;

use Eternity\Contracts\Arrayable;

/**
 * Class NotificationMessage
 * @package App\Infrastructure\Adapters\Firebase\Messages
 */
class Notification implements Arrayable
{
    /**
     * @var string
     */
    private $title;

    /**
     * @var string
     */
    private $body;

    /**
     * NotificationMessage constructor.
     * @param string $title
     * @param string $body
     */
    public function __construct(string $title, string $body)
    {
        $this->title = $title;
        $this->body = $body;
    }

    /**
     * @return string
     */
    public function title(): string
    {
        return $this->title;
    }

    /**
     * @return string
     */
    public function body(): string
    {
        return $this->body;
    }

    /**
     * @return array
     */
    public function toArray(): array
    {
        return [
            'title' => $this->title,
            'body'  => $this->body,
        ];
    }
}
