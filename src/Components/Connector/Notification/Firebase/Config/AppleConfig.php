<?php

namespace Eternity\Components\Connector\Notification\Firebase\Config;

use Eternity\Contracts\Arrayable;

/**
 * Apple config (Ios)
 *
 * @link https://firebase.google.com/docs/reference/fcm/rest/v1/projects.messages#apnsconfig
 *
 * Class AppleConfig
 * @package Eternity\Components\Connector\Notification\Firebase\Config
 */
class AppleConfig extends AbstractConfig implements Arrayable
{
    /**
     * @var array
     */
    protected $headers;

    /**
     * @param array $options
     * @return \Eternity\Components\Connector\Notification\Firebase\Config\AppleConfig
     */
    public static function fromArray(array $options): self
    {
        $config = new self;

        // Parse headers section
        if ($headers = $options['headers'] ?? null) {
            if ($priority = $headers['apns-priority'] ?? null) {
                $config->addPriority($priority);
            }
        }

        return $config;
    }

    /**
     * @param string $priority
     * @return \Eternity\Components\Connector\Notification\Firebase\Config\AppleConfig
     */
    public function addPriority(string $priority): self
    {
        $this->headers['apns-priority'] = $priority;

        return $this;
    }

    /**
     * @return array
     */
    public function toArray(): array
    {
        $result = [];
        // Build headers
        if (!empty($this->headers)) {
            $result['headers'] = $this->headers;
        }
        return $result;
    }
}