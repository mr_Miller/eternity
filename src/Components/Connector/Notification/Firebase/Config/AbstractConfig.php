<?php

namespace Eternity\Components\Connector\Notification\Firebase\Config;

/**
 * Class AbstractConfig
 * @package Eternity\Components\Connector\Notification\Firebase\Config
 */
class AbstractConfig
{
    /**
     * @return \Eternity\Components\Connector\Notification\Firebase\Config\AbstractConfig
     */
    public static function create(): self
    {
        return new static();
    }
}