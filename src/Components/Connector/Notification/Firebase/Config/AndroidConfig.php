<?php

namespace Eternity\Components\Connector\Notification\Firebase\Config;

use Eternity\Contracts\Arrayable;

/**
 * Android config
 *
 * @link https://firebase.google.com/docs/reference/fcm/rest/v1/projects.messages#androidconfig
 *
 * Class AndroidConfig
 * @package App\Infrastructure\Adapters\Firebase\Config
 */
class AndroidConfig extends AbstractConfig implements Arrayable
{
    /**
     * @var string
     */
    protected $priority;

    /**
     * @param array $options
     * @return \Eternity\Components\Connector\Notification\Firebase\Config\AndroidConfig
     */
    public static function fromArray(array $options): self
    {
        $config = new self;
        if ($priority = $options['priority'] ?? null) {
            $config->addPriority($priority);
        }

        return $config;
    }

    /**
     * @param string $priority
     * @return \Eternity\Components\Connector\Notification\Firebase\Config\AndroidConfig
     */
    public function addPriority(string $priority): self
    {
        $this->priority = $priority;

        return $this;
    }

    /**
     * @return array
     */
    public function toArray(): array
    {
        $result = [];
        if ($this->priority) {
            $result['priority'] = $this->priority;
        }

        return $result;
    }
}