<?php

namespace Eternity\Components\Connector\Notification\Firebase;

use Eternity\Components\Connector\Notification\Firebase\Config\AndroidConfig;
use Eternity\Components\Connector\Notification\Firebase\Config\AppleConfig;
use Eternity\Components\Connector\Notification\Firebase\Map\ApplePriority;
use Eternity\Components\Connector\Notification\Firebase\Messages\Data;
use Eternity\Components\Connector\Notification\Firebase\Messages\Notification;
use Eternity\Contracts\Arrayable;
use Eternity\Exceptions\EternityException;

/**
 * Class Notification
 * @package App\Infrastructure\Adapters\Firebase
 */
class Message implements Arrayable
{
    /**
     * @var array Push notification
     */
    private $notification;

    /**
     * @var array Not structured custom data that can be sent to client
     */
    private $data;

    /**
     * @var string Targeted device specific ID (Device token)
     * IF set notification will be delivered to specific device with this token
     */
    private $token;

    /**
     * @var integer
     */
    private $uid;

    /**
     * Apply (IOS) device specific config
     * @var \Eternity\Components\Connector\Notification\Firebase\Config\AppleConfig
     */
    private $appleConfig;

    /**
     * Android device specific config
     * @var \Eternity\Components\Connector\Notification\Firebase\Config\AndroidConfig
     */
    private $androidConfig;

    /**
     * Notification constructor.
     * @param array $messageData
     * @throws \Eternity\Exceptions\EternityException
     */
    private function __construct(array $messageData = [])
    {
        if ($messageData) {
            $this->build($messageData);
        } else {
            $this->appleConfig = AppleConfig::create();
            $this->androidConfig = AndroidConfig::create();
        }
    }

    /**
     * @return int
     */
    public function uid(): int
    {
        return $this->uid;
    }

    /**
     * Builds object
     * @param array $messageData
     * @throws \Eternity\Exceptions\EternityException
     */
    private function build(array $messageData): void
    {
        // Building message base properties
        if (!empty($messageData['notification']['title']) && !empty($messageData['notification']['body'])) {
            $this->addNotification(new Notification(
                $messageData['notification']['title'],
                $messageData['notification']['body']
            ));
        } else {
            throw new EternityException(
                'Structure error',
                'Message (push) data has incorrect structure. Title or body is missing'
            );
        }

        // Set UID
        if (!empty($messageData['uid'])) {
            $this->uid = $messageData['uid'];
        } else {
            throw new EternityException(
                'Structure error',
                'Message UID is not provided when try to build Message'
            );
        }

        // set token
        $this->token = $messageData['token'] ?? null;

        // Add data
        if (!empty($messageData['data'])) {
            $this->addData(new Data($messageData['data']));
        }

        // Android config
        $this->androidConfig = AndroidConfig::fromArray($messageData['android'] ?? []);
        // Apple config
        $this->appleConfig = AppleConfig::fromArray($messageData['apns'] ?? []);
    }

    /**
     * @param array $messageData
     * @return \Eternity\Components\Connector\Notification\Firebase\Message
     * @throws \Eternity\Exceptions\EternityException
     */
    public static function fromArray(array $messageData): self
    {
        return new static($messageData);
    }

    /**
     * @param int $uid
     * @param string $title
     * @param string $body
     * @param array $data
     * @return \Eternity\Components\Connector\Notification\Firebase\Message
     * @throws \Eternity\Exceptions\EternityException
     */
    public static function create(int $uid, string $title, string $body, array $data = []): self
    {
        return new static([
            'uid'          => $uid,
            'notification' => [
                'title' => $title,
                'body'  => $body
            ],
            'data'         => $data
        ]);
    }

    /**
     * Add Data message to notification
     * @param \Eternity\Components\Connector\Notification\Firebase\Messages\Data $data
     * @return \Eternity\Components\Connector\Notification\Firebase\Message
     */
    public function addData(Data $data): self
    {
        $this->data = $data->toArray();

        return $this;
    }

    /**
     * Add Firebase message to notification
     * @param \Eternity\Components\Connector\Notification\Firebase\Messages\Notification $notification
     * @return \Eternity\Components\Connector\Notification\Firebase\Message
     */
    public function addNotification(Notification $notification): self
    {
        $this->notification = $notification->toArray();

        return $this;
    }

    /**
     * @return \Eternity\Components\Connector\Notification\Firebase\Config\AppleConfig
     */
    public function appleConfig(): AppleConfig
    {
        return $this->appleConfig;
    }

    /**
     * @return \Eternity\Components\Connector\Notification\Firebase\Config\AndroidConfig
     */
    public function androidConfig(): AndroidConfig
    {
        return $this->androidConfig;
    }

    /**
     * @param string $token
     * @return \Eternity\Components\Connector\Notification\Firebase\Message
     */
    public function addToken(string $token): self
    {
        $this->token = $token;

        return $this;
    }

    /**
     * @param string $priority
     * @return \Eternity\Components\Connector\Notification\Firebase\Message
     */
    public function addPriority(string $priority): self
    {
        $this->appleConfig->addPriority(ApplePriority::map($priority));
        $this->androidConfig->addPriority($priority);

        return $this;
    }

    /**
     * @return array
     */
    public function toArray(): array
    {
        return [
            'uid'          => $this->uid,
            'token'        => $this->token,
            'data'         => $this->data ?: null,
            'notification' => $this->notification,
            'android'      => $this->androidConfig->toArray(),
            'apns'         => $this->appleConfig->toArray(),
        ];
    }

    /**
     * Firebase specific toArray method
     *
     * Removes:
     *  - uid property
     *
     * @return array
     */
    public function toFirebase(): array
    {
        $array = $this->toArray();
        unset($array['uid']);

        return $array;
    }
}