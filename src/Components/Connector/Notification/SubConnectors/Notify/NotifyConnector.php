<?php

namespace Eternity\Components\Connector\Notification\SubConnectors\Notify;

use Eternity\Components\Connector\AbstractConnector;

/**
 * Class NotifyConnector
 * @package Eternity\Components\Connector\Notification\SubConnectors\Notify
 */
class NotifyConnector extends AbstractConnector
{
    /**
     * Send multiple push notifications
     * @param array
     */
    public function push(array $messages): void
    {
        $this->post('v1/notifications/push', 204, [], $messages);
    }
}