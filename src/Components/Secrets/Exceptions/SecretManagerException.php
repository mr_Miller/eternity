<?php

namespace Eternity\Components\Secrets\Exceptions;

use Eternity\Exceptions\EternityException;
use Throwable;

/**
 * Class SecretManagerException
 * @package Eternity\Components\SecretsManager\Exceptions
 */
class SecretManagerException extends EternityException
{
    /**
     * @param string $title
     * @param string $detail
     * @param \Throwable|null $previous
     */
    public function __construct(string $title, string $detail, Throwable $previous = null)
    {
        parent::__construct($title, $detail, $previous);
        $this->type = 'SecretManagerException';
    }
}
