<?php

namespace Eternity\Components\Secrets\Contracts;

/**
 * SecretInterface
 * @package Eternity\Components\SecretsManager\Contracts
 */
interface Secret
{
    /**
     * Creates secret
     * @param string $key
     * @param array $data
     * @param string|null $description
     * @return \Eternity\Components\Secrets\Contracts\Response
     * @throws \Eternity\Components\Secrets\Exceptions\SecretManagerException
     */
    public function create(string $key, array $data, ?string $description = null): Response;

    /**
     * Returns secret
     * @param string $key
     * @param string|null $version
     * @return \Eternity\Components\Secrets\Contracts\Response
     * @throws \Eternity\Components\Secrets\Exceptions\SecretManagerException
     */
    public function get(string $key, ?string $version = null): Response;

    /**
     * Updates secret
     * @param string $key
     * @param array $data
     * @return \Eternity\Components\Secrets\Contracts\Response
     * @throws \Eternity\Components\Secrets\Exceptions\SecretManagerException
     */
    public function update(string $key, array $data): Response;

    /**
     * Deletes secret
     * @param string $key
     * @param bool $forceDelete
     * @return \Eternity\Components\Secrets\Contracts\Response
     * @throws \Eternity\Components\Secrets\Exceptions\SecretManagerException
     */
    public function delete(string $key, bool $forceDelete = true): Response;
}
