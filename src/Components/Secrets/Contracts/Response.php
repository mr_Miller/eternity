<?php

namespace Eternity\Components\Secrets\Contracts;

use Eternity\Contracts\Arrayable;

/**
 * ResponseInterface
 * @package Eternity\Components\SecretsManager\Contracts
 */
interface Response extends Arrayable
{
    /**
     * @param string $key
     * @return mixed
     */
    public function key(string $key);

    /**
     * @return string
     */
    public function version(): ?string;

    /**
     * @return array
     */
    public function secret(): array;
}
