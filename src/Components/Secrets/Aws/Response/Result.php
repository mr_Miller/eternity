<?php

namespace Eternity\Components\Secrets\Aws\Response;

use Aws\Result as AwsResult;
use Eternity\Components\Secrets\Contracts\Response;
use Eternity\Components\Secrets\Exceptions\SecretManagerException;

/**
 * Class Result
 * @package Eternity\Components\SecretsManager\Aws\Response
 */
class Result implements Response
{
    public const VERSION = 'VersionId';
    public const SECRET = 'SecretString';

    /**
     * @var \Aws\Result
     */
    private $result;

    /**
     * @param \Aws\Result $result
     */
    public function __construct(AwsResult $result)
    {
        $this->result = $result;
    }

    /**
     * Returns from data by key
     * @param string $key
     * @return mixed|null
     */
    public function key(string $key)
    {
        return $this->result->get($key) ?: null;
    }

    /**
     * Returns version if exists
     * @return string
     */
    public function version(): ?string
    {
        return $this->key(self::VERSION);
    }

    /**
     * Returns secrete
     * @return array
     * @throws \Eternity\Components\Secrets\Exceptions\SecretManagerException
     */
    public function secret(): array
    {
        $value = $this->key(self::SECRET);
        if (is_array($value)) {
            return $value;
        }

        // verifies it is json
        if (is_string($value) && strpos($value, '{') === 0) {
            try {
                return json_decode($value, true, 64, JSON_THROW_ON_ERROR);
            } catch (\JsonException $e) {
                throw new SecretManagerException('Error', $e->getMessage());
            }
        }

        return array_filter(['value' => $value]);
    }

    /**
     * Returns response from AWS to array
     * @return array
     */
    public function toArray(): array
    {
        return $this->result->toArray();
    }
}
