<?php

namespace Eternity\Components\Secrets\Aws;

use Aws\Exception\AwsException as SdkException;
use Aws\SecretsManager\SecretsManagerClient;
use Eternity\Components\Secrets\Aws\Response\Result;
use Eternity\Components\Secrets\Contracts\Secret;
use Eternity\Components\Secrets\Contracts\Response;

/**
 * Class AwsAdapter
 * @package Eternity\Components\SecretsManager\Aws
 */
class AwsAdapter implements Secret
{
    /**
     * @var \Aws\SecretsManager\SecretsManagerClient
     */
    private $client;

    /**
     * @param \Aws\SecretsManager\SecretsManagerClient $client
     */
    public function __construct(SecretsManagerClient $client)
    {
        $this->client = $client;
    }

    /**
     * Creates secret
     * @param string $key
     * @param array $data
     * @param string|null $description
     * @return \Eternity\Components\Secrets\Contracts\Response
     * @throws AwsException
     */
    public function create(string $key, array $data, ?string $description = null): Response
    {
        try {
            return new Result($this->client->createSecret([
                'Name'         => $key,
                'SecretString' => json_encode($data),
                'Description'  => $description,
            ]));
        } catch (SdkException $e) {
            throw new AwsException('Error', $e->getAwsErrorMessage(), $e);
        }
    }

    /**
     * Returns secret
     * @param string $key
     * @param string|null $version
     * @return \Eternity\Components\Secrets\Contracts\Response
     * @throws AwsException
     */
    public function get(string $key, ?string $version = null): Response
    {
        try {
            return new Result($this->client->getSecretValue(array_filter([
                'SecretId'  => $key,
                'VersionId' => $version,
            ])));
        } catch (SdkException $e) {
            throw new AwsException('Error', $e->getAwsErrorMessage(), $e);
        }
    }

    /**
     * Updates secret
     * @param string $key
     * @param array $data
     * @return \Eternity\Components\Secrets\Contracts\Response
     * @throws AwsException
     */
    public function update(string $key, array $data): Response
    {
        try {
            return new Result($this->client->updateSecret([
                'SecretId'     => $key,
                'SecretString' => json_encode($data),
            ]));
        } catch (SdkException $e) {
            throw new AwsException('Error', $e->getAwsErrorMessage(), $e);
        }
    }

    /**
     * Deletes secret
     * @param string $key
     * @param bool $forceDelete
     * @return \Eternity\Components\Secrets\Contracts\Response
     * @throws AwsException
     */
    public function delete(string $key, bool $forceDelete = true): Response
    {
        try {
            return new Result($this->client->deleteSecret([
                'SecretId'                   => $key,
                'ForceDeleteWithoutRecovery' => $forceDelete,
            ]));
        } catch (SdkException $e) {
            throw new AwsException('Error', $e->getAwsErrorMessage(), $e);
        }
    }
}
