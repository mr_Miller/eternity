<?php

namespace Eternity\Components\Secrets\Aws;

use Aws\Exception\AwsException as SdkException;
use Eternity\Components\Secrets\Exceptions\SecretManagerException;

class AwsException extends SecretManagerException
{
    /**
     * @param string $title
     * @param string $detail
     * @param SdkException|null $previous
     */
    public function __construct(string $title, string $detail, SdkException $previous = null)
    {
        parent::__construct($title, $detail, $previous);
        $this->type = 'AwsException';

        if ($previous) {
            $this->transformAwsErrorCode($previous);
        }
    }

    /**
     * Required for aws error type recognition
     * @return string|null
     */
    public function getAwsErrorCode(): ?string
    {
        if ($e = $this->getPrevious()) {
            return $e->getAwsErrorCode();
        }

        return null;
    }

    /***
     * @param SdkException $e
     */
    private function transformAwsErrorCode(SdkException $e): void
    {
        switch ($e->getAwsErrorCode()) {
            case 'ResourceNotFoundException':
                $this->code = 404;
                break;
            case 'ResourceExistsException':
                $this->code = 409;
                break;
            case 'InternalServiceError':
                $this->code = 500;
                break;
            case 'LimitExceededException':
                $this->code = 429;
                break;
            case 'InvalidParameterException':
                $this->code = 406;
                break;
            case 'PreconditionNotMetException':
                $this->code = 412;
                break;
            case 'EncryptionFailure':
            case 'DecryptionFailure':
            case 'MalformedPolicyDocumentException':
                $this->code = 422;
                break;
            default:
                $this->code = 400;
        }
    }
}
