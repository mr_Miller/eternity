<?php

namespace Eternity\Components\Secrets;

use Eternity\Components\Secrets\Contracts\Secret;
use Eternity\Components\Secrets\Contracts\Response;

/**
 * Class SecretsManager
 * @package Eternity\Components\SecretsManager
 */
class Manager
{
    /**
     * @var \Eternity\Components\Secrets\Contracts\Secret
     */
    private $client;

    /**
     * @param \Eternity\Components\Secrets\Contracts\Secret $secret
     */
    public function __construct(Secret $secret)
    {
        $this->client = $secret;
    }

    /**
     * @param \Eternity\Components\Secrets\Contracts\Secret $secret
     * @return static
     */
    public static function fromAdapter(Secret $secret): self
    {
        return new static($secret);
    }

    /**
     * @param string $key
     * @param array $data
     * @param string|null $description
     * @return \Eternity\Components\Secrets\Contracts\Response
     * @throws \Eternity\Components\Secrets\Exceptions\SecretManagerException
     */
    public function create(string $key, array $data, ?string $description = null): Response
    {
        return $this->client->create($key, $data, $description);
    }

    /**
     * @param string $key
     * @param string|null $version
     * @return \Eternity\Components\Secrets\Contracts\Response
     * @throws \Eternity\Components\Secrets\Exceptions\SecretManagerException
     */
    public function get(string $key, ?string $version = null): Response
    {
        return $this->client->get($key, $version);
    }

    /**
     * @param string $key
     * @param array $data
     * @return \Eternity\Components\Secrets\Contracts\Response
     * @throws \Eternity\Components\Secrets\Exceptions\SecretManagerException
     */
    public function update(string $key, array $data): Response
    {
        return $this->client->update($key, $data);
    }

    /**
     * @param string $key
     * @param bool $forceDelete
     * @return \Eternity\Components\Secrets\Contracts\Response
     * @throws \Eternity\Components\Secrets\Exceptions\SecretManagerException
     */
    public function delete(string $key, bool $forceDelete = true): Response
    {
        return $this->client->delete($key, $forceDelete);
    }
}