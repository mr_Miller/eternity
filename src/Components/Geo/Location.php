<?php

namespace Eternity\Components\Geo;

/**
 * Location manager
 *
 * Basic lat,lng is in degrees, but is much easier to make operations with radians
 *
 * Class Location
 * @package App\Infrastructure\Services\Wanted\Geo
 */
class Location
{
    /**
     * @var int Earth radius in meters
     */
    public const EARTH_RADIUS_METERS = 6371140;

    /**
     * @var float Latitude in degrees
     */
    private $lat;

    /**
     * @var float Longitude in degrees
     */
    private $lng;

    /**
     * @var float Latitude in radians
     */
    private $lngRadians;

    /**
     * @var float Longitude in radians
     */
    private $latRadians;

    /**
     * Location constructor.
     * @param float $latDegrees Latitude in degrees
     * @param float $lngDegrees Longitude in degrees
     */
    public function __construct(float $latDegrees, float $lngDegrees)
    {
        $this->lat = $latDegrees;
        $this->lng = $lngDegrees;
        $this->latRadians = static::degreesToRadians($latDegrees);
        $this->lngRadians = static::degreesToRadians($lngDegrees);
    }

    /**
     * Calculate bounding coordinates object with maximum provided distance
     * @param int $distance Distance in meters
     * @return \Eternity\Components\Geo\BoundingCoordinates
     */
    public function boundingCoordinates(int $distance): BoundingCoordinates
    {
        return new BoundingCoordinates($this->latRadians(), $this->lngRadians(), $distance);
    }

    /**
     * Calculates distance to the point with coordinates
     *
     * Formula: dist(m) = arccos(sin(lat1) * sin(lat2) + cos(lat1) * cos(lat2) * cos(lon1 - lon2)) * R
     *
     * @param float $lat Latitude in radians of point to calc distance with
     * @param float $lng Longitude in radians of point to calc distance with
     * @return float
     */
    public function distance(float $lat, float $lng): float
    {
        return acos(sin($this->lat()) * sin($lat) + cos($this->lat()) * cos($lat) * cos($this->lng() - $lng))
            * self::EARTH_RADIUS_METERS;
    }

    /**
     * Returns latitude in degrees
     * @return float
     */
    public function lat(): float
    {
        return $this->lat;
    }

    /**
     * Returns longitude in degrees
     * @return float
     */
    public function lng(): float
    {
        return $this->lng;
    }

    /**
     * Returns latitude in radians
     * @return float
     */
    public function latRadians(): float
    {
        return $this->latRadians;
    }

    /**
     * Returns longitude in radians
     * @return float
     */
    public function lngRadians(): float
    {
        return $this->lngRadians;
    }

    /**
     * @param float $value
     * @return float
     */
    public static function degreesToRadians(float $value): float
    {
        return deg2rad($value);
    }
}
