<?php

namespace Eternity\Components\Geo;

/**
 * Bounding coordinates
 *
 * todo Add support for Poles and the 180th Meridian
 *
 * Class BoundingCoordinates
 * @package App\Infrastructure\Services\Wanted\Geo
 */
class BoundingCoordinates
{
    /**
     * @var float
     */
    private $lat;

    /**
     * @var float
     */
    private $lng;

    /**
     * @var float
     */
    private $latMin;

    /**
     * @var float
     */
    private $latMax;

    /**
     * @var float
     */
    private $lngMin;

    /**
     * @var float
     */
    private $lngMax;

    /**
     * @var int Distance in meters
     */
    private $distance;

    /**
     * @var float Angular distance in radians
     */
    private $angularDistance;

    /**
     * BoundingCoordinates constructor.
     * @param float $lat Center point latitude (rad)
     * @param float $lng Center point longitude (rad)
     * @param int $distance Distance in meters
     */
    public function __construct(float $lat, float $lng, int $distance)
    {
        $this->lat = $lat;
        $this->lng = $lng;
        $this->distance = $distance;

        // Angular radius
        $this->angularDistance = $this->calcAngularDistance($this->distance);

        // Maximum and minimum latitude
        $this->latMin = $this->calcLatMin($this->lat(), $this->angularDistance);
        $this->latMax = $this->calcLatMax($this->lat(), $this->angularDistance);

        // Maximum and minimum longitude
        $deltaLng =  $this->calcDeltaLng($this->angularDistance, $this->lat());
        $this->lngMin = $this->calcLngMin($this->lng(), $deltaLng);
        $this->lngMax = $this->calcLngMax($this->lng(), $deltaLng);
    }

    /**
     * Calculate minimal Longitude
     *
     * Formula: lngMin(rad) = lng - deltaLng
     *
     * @param float $lng
     * @param float $deltaLng
     * @return float
     */
    public function calcLngMin(float $lng, float $deltaLng): float
    {
        return $lng - $deltaLng;
    }

    /**
     * Calculate maximal Longitude
     *
     * Formula: lngMin(rad) = lng + deltaLng
     *
     * @param float $lng
     * @param float $deltaLng
     * @return float
     */
    public function calcLngMax(float $lng, float $deltaLng): float
    {
        return $lng + $deltaLng;
    }

    /**
     * Calculate delta Longitude
     *
     * Formula: deltaLon(rad) = arcsin(sin(r)/cos(lat))
     *
     * @param float $angularDistance
     * @param float $lat
     * @return float
     */
    public function calcDeltaLng(float $angularDistance, float $lat): float
    {
        return asin(sin($angularDistance) / cos($lat));
    }

    /**
     * Calculate minimum latitude
     *
     * Formula: latMin(rad) = lat - r
     *
     * @param float $lat
     * @param float $angularDistance
     * @return float
     */
    public function calcLatMin(float $lat, float $angularDistance): float
    {
        return $lat - $angularDistance;
    }

    /**
     * Calculate maximum latitude
     *
     * Formula: latMax(rad) = lat + r
     *
     * @param float $lat
     * @param float $angularDistance
     * @return float
     */
    public function calcLatMax(float $lat, float $angularDistance): float
    {
        return $lat + $angularDistance;
    }

    /**
     * Returns angular distance
     *
     * Formula: r(rad) = d/R
     *
     * @param int $distance Distance (m)
     * @return float
     */
    public function calcAngularDistance(int $distance): float
    {
        return $distance / Location::EARTH_RADIUS_METERS;
    }

    /**
     * Return angular distance in radians
     * @return float
     */
    public function angularDistance(): float
    {
        return $this->angularDistance;
    }

    /**
     * Returns distance in meters
     * @return int
     */
    public function distance(): int
    {
        return $this->distance;
    }

    /**
     * @return float
     */
    public function lat(): float
    {
        return $this->lat;
    }

    /**
     * @return float
     */
    public function lng(): float
    {
        return $this->lng;
    }

    /**
     * @return float
     */
    public function latMin(): float
    {
        return $this->latMin;
    }

    /**
     * @return float
     */
    public function latMax(): float
    {
        return $this->latMax;
    }

    /**
     * @return float
     */
    public function lngMin(): float
    {
        return $this->lngMin;
    }

    /**
     * @return float
     */
    public function lngMax(): float
    {
        return $this->lngMax;
    }
}
