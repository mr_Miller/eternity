<?php

namespace Eternity\Components\Http\External;

use GuzzleHttp\Client as GuzzleHttp;

/**
 * Class GuzzleFactory
 * @package Eternity\Components\Http\External
 */
class GuzzleFactory
{
    /**
     * The default connect timeout.
     * @var int
     */
    const CONNECT_TIMEOUT = 10;

    /**
     * The default transport timeout.
     * @var int
     */
    const TIMEOUT = 15;

    /**
     * Create a new guzzle client.
     *
     * @param array $options
     * @return Client
     */
    public static function make(array $options = []): Client
    {
        $options['http_errors'] = false; // Guzzle will not throw exceptions when status 4xx or 5xx is received
        $config = array_merge(['connect_timeout' => self::CONNECT_TIMEOUT, 'timeout' => self::TIMEOUT], $options);

        return new Client(new GuzzleHttp($config));
    }
}
