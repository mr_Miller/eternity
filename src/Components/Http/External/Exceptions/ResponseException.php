<?php

namespace Eternity\Components\Http\External\Exceptions;

use Eternity\Components\Http\External\Response\Response;
use Eternity\Exceptions\HttpException;

/**
 * Class ResponseException
 * @package App\Application\Components\Apple\Exceptions
 */
class ResponseException extends HttpException
{
    /**
     * @var \Eternity\Components\Http\External\Response\Response
     */
    private $response;

    /**
     * ResponseException constructor.
     * @param \Eternity\Components\Http\External\Response\Response $response
     * @param string|null $title
     * @param string|null $detail
     * @param \Throwable|null $previous
     */
    public function __construct(Response $response, string $title = null, string $detail = null, \Throwable $previous = null)
    {
        parent::__construct($title, $detail, $previous);
        $this->response = $response;
        $this->type = 'ResponseException';
    }

    /**
     * @return \Eternity\Components\Http\External\Response\Response
     */
    public function getResponse(): Response
    {
        return $this->response;
    }
}
