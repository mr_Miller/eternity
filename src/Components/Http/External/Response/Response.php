<?php

namespace Eternity\Components\Http\External\Response;

use Psr\Http\Message\ResponseInterface;

/**
 * Http response
 *
 * Basic class for http connections with external services
 *
 * Class Response
 * @package App\Application\Components\Http\
 */
class Response
{
    public const APPLICATION_JSON = 'application/json';

    /**
     * @var ResponseInterface
     */
    private $originalResponse;

    /**
     * @var array Decoded json body of response
     */
    private $body;

    /**
     * @var array
     */
    private $headers = [];

    /**
     * @var string|null
     */
    private $contentType;

    /**
     * Response constructor.
     * @param \Psr\Http\Message\ResponseInterface $sourceResponse
     */
    public function __construct(ResponseInterface $sourceResponse)
    {
        $this->originalResponse = $sourceResponse;
        $this->initHeaders();
        $this->initBody();
    }

    /**
     * Initialize response body
     */
    private function initBody(): void
    {
        if ($this->isApplicationJson()) {
            $this->body = json_decode($this->originalResponse->getBody()->getContents(), true);
        }
    }

    /**
     * Initialize headers
     */
    private function initHeaders(): void
    {
        $this->headers = $this->originalResponse->getHeaders();
        $this->contentType = $this->originalResponse->getHeaderLine('Content-Type');
    }

    /**
     * Get response body property
     * @param string $propertyName
     * @param $default
     * @return mixed|null
     */
    public function getBodyProperty(string $propertyName, $default = null)
    {
        if ($this->body === null || !isset($this->body[$propertyName])) {
            // if body is null OR property not set
            return $default;
        }

        return $this->body[$propertyName];
    }

    /**
     * Check response type is application/json
     * @return bool
     */
    public function isApplicationJson(): bool
    {
        return strpos($this->contentType(), self::APPLICATION_JSON) !== false;
    }

    /**
     * Init response body
     * @return array|null
     */
    public function body(): ?array
    {
        return $this->body;
    }

    /**
     * Return the original response class
     * @return mixed
     */
    public function originalResponse(): ResponseInterface
    {
        return $this->originalResponse;
    }

    /**
     * Return headers
     * @return array
     */
    public function headers(): array
    {
        return $this->headers;
    }

    /**
     * Return http status code
     * @return int
     */
    public function httpStatus(): int
    {
        return $this->originalResponse->getStatusCode();
    }

    /**
     * Returns content type
     * @return string|null
     */
    public function contentType(): ?string
    {
        return $this->contentType;
    }
}
