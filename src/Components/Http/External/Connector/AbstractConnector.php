<?php

namespace Eternity\Components\Http\External\Connector;

use Eternity\Components\Http\External\Client;
use Eternity\Components\Http\External\Response\Response;

/**
 * Abstract HTTP connector to external resources
 *
 * Class AbstractConnector
 */
abstract class AbstractConnector
{
    /**
     * @var \Eternity\Components\Http\External\Client Http client
     */
    private $client;

    /**
     * @var string URL of the service to which requests will be sent
     */
    private $serviceUrl;

    /**
     * @var array These headers can be set by connector methods
     */
    private $headers;

    /**
     * AbstractConnector constructor.
     *
     * @param \Eternity\Components\Http\External\Client $client
     * @param string $serviceUrl
     * @param array $headers
     */
    public function __construct(
        Client $client,
        string $serviceUrl,
        array $headers = []
    ) {
        $this->client = $client;
        $this->serviceUrl = $serviceUrl;
        $this->headers = $headers;
    }

    /**
     * Build url to service
     *
     * @param string|\Eternity\Helpers\Uri $uri
     *
     * @return string
     */
    private function buildUrl($uri): string
    {
        return $this->serviceUrl . '/' . $uri;
    }

    /**
     * Adds headers array
     * @param array $headers
     */
    private function setHeaders(array $headers): void
    {
        foreach (array_filter($headers) as $headerKey => $headerValue) {
            $this->setHeader($headerKey, $headerValue);
        }
    }

    /**
     * Adds single header
     * @param string $headerKey
     * @param string $headerValue
     */
    private function setHeader(string $headerKey, string $headerValue): void
    {
        $this->headers[$headerKey] = $headerValue;
    }

    /**
     * Returns headers
     * @return array
     */
    protected function getHeaders(): array
    {
        return $this->headers;
    }

    /**
     * Resolver to services
     *
     * @param string $method
     * @param string|\Eternity\Helpers\Uri $uri
     * @param int $expectedStatus
     * @param array $headers
     * @param array|null $body Body or Query params depends on request type. For get request body will be transferred as
     * query params
     *
     * @return \Eternity\Components\Http\External\Response\Response
     */
    private function request(
        string $method,
        $uri,
        int $expectedStatus = 200,
        array $headers = [],
        ?array $body = null
    ): Response {
        if (is_array($body)) {
            $body = json_encode($body);
        }
        // Default headers will be overridden by current request headers
        $this->setHeaders($headers);
        $this->authorize();

        return $this->client->{$method}($this->buildUrl($uri), $expectedStatus, $this->getHeaders(), $body);
    }

    /**
     * Authorize request hook
     *
     * Executes before send HTTP request
     */
    private function authorize(): void
    {
        // implement if needed
    }

    /**
     * Method GET to services
     *
     * @param string|\Eternity\Helpers\Uri $uri
     * @param int $expectedStatus
     * @param array $headers
     * @param array|null $query
     *
     * @return \Eternity\Components\Http\External\Response\Response
     */
    protected function get(
        $uri,
        int $expectedStatus = 200,
        array $headers = [],
        ?array $query = null
    ): Response {
        return $this->request('get', $uri, $expectedStatus, $headers, $query);
    }

    /**
     * Method POST to services
     *
     * @param string|\Eternity\Helpers\Uri $uri
     * @param int $expectedStatus
     * @param array $headers
     * @param array|null $body
     *
     * @return \Eternity\Components\Http\External\Response\Response
     */
    protected function post(
        $uri,
        int $expectedStatus = 200,
        array $headers = [],
        ?array $body = null
    ): Response {
        return $this->request('post', $uri, $expectedStatus, $headers, $body);
    }

    /**
     * Method PUT to services
     *
     * @param string|\Eternity\Helpers\Uri $uri
     * @param int $expectedStatus
     * @param array $headers
     * @param array|null $body
     *
     * @return \Eternity\Components\Http\External\Response\Response
     */
    protected function put(
        $uri,
        int $expectedStatus = 200,
        array $headers = [],
        ?array $body = null
    ): Response {
        return $this->request('put', $uri, $expectedStatus, $headers, $body);
    }

    /**
     * Method DELETE to services
     *
     * @param string|\Eternity\Helpers\Uri $uri
     * @param int $expectedStatus
     * @param array $headers
     * @param array|null $body
     *
     * @return \Eternity\Components\Http\External\Response\Response
     */
    protected function delete(
        $uri,
        int $expectedStatus = 200,
        array $headers = [],
        ?array $body = null
    ): Response {
        return $this->request('delete', $uri, $expectedStatus, $headers, $body);
    }

    /**
     * Method PATCH to services
     *
     * @param string|\Eternity\Helpers\Uri $uri
     * @param int $expectedStatus
     * @param array $headers
     * @param array|null $body
     *
     * @return \Eternity\Components\Http\External\Response\Response
     */
    protected function patch(
        $uri,
        int $expectedStatus = 200,
        array $headers = [],
        ?array $body = null
    ): Response {
        return $this->request('patch', $uri, $expectedStatus, $headers, $body);
    }

    /**
     * Method HEAD to services
     *
     * @param string|\Eternity\Helpers\Uri $uri
     * @param int $expectedStatus
     * @param array $headers
     * @param array|null $body
     *
     * @return \Eternity\Components\Http\External\Response\Response
     */
    protected function head(
        $uri,
        int $expectedStatus = 200,
        array $headers = [],
        ?array $body = null
    ): Response {
        return $this->request('head', $uri, $expectedStatus, $headers, $body);
    }

    /**
     * Method OPTIONS to services
     *
     * @param string|\Eternity\Helpers\Uri $uri
     * @param int $expectedStatus
     * @param array $headers
     * @param array|null $body
     *
     * @return \Eternity\Components\Http\External\Response\Response
     */
    protected function options(
        $uri,
        int $expectedStatus = 200,
        array $headers = [],
        ?array $body = null
    ): Response {
        return $this->request('options', $uri, $expectedStatus, $headers, $body);
    }
}
