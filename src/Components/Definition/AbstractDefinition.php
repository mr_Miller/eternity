<?php

namespace Eternity\Components\Definition;

use Eternity\Components\Definition\Exceptions\DefinitionException;

/**
 * @deprecated use new AbstractBaseDefinition instead this class
 * @deprecated Reason: Output format changed
 *
 * Abstract base definition class
 *
 * Usage:
 * Extend this class and realize getList method
 *
 * Class AbstractDefinition
 * @package App\Infrastructure\Definitions
 */
abstract class AbstractDefinition
{
    /**
     * @deprecated use new AbstractBaseDefinition instead this class
     * @deprecated Reason: Output format changed
     *
     * Method builds the whole definition structure
     *
     * Every array item must have next structure:
     *  - id
     *  - title
     *  - translation
     *
     * For example:
     *  [
     *      5 => [
     *          'id' => 5,
     *          'title' => 'String description',
     *          'translation_key' => 'Key for user readable translation',
     *      ],
     *      ...
     *  ]
     *
     * @return mixed
     */
    abstract protected static function getList(): array;

    /**
     * @deprecated use new AbstractBaseDefinition instead this class
     * @deprecated Reason: Output format changed
     *
     * Return list of definitions (ID's)
     */
    public static function getConstList(): array
    {
        return array_keys(static::getList());
    }

    /**
     * @deprecated use new AbstractBaseDefinition instead this class
     * @deprecated Reason: Output format changed
     *
     * Returns definition title
     *
     * @param int $definition
     * @return mixed
     * @throws \Eternity\Exceptions\ServerException
     */
    public static function getTitle(int $definition): string
    {
        $definitions = static::getList();
        if (!isset($definitions[$definition]['title'])) {
            throw new DefinitionException(
                'Definition error',
                'Title is not defined for definition id: ' . $definition
            );
        }

        return $definitions[$definition]['title'];
    }

    /**
     * @deprecated use new AbstractBaseDefinition instead this class
     * @deprecated Reason: Output format changed
     *
     * Returns titles for related definitions (ID's)
     *
     * ID as key, and title as value in array
     */
    public static function getTitleList(): array
    {
        return array_column(static::getList(), 'title', 'id');
    }

    /**
     * @deprecated use new AbstractBaseDefinition instead this class
     * @deprecated Reason: Output format changed
     *
     * Returns definitions (ID's) with user readable translations
     *
     * @return array
     */
    public static function getTranslationWithIdList(): array
    {
        return array_column(static::getList(), 'translation_key', 'id');
    }

    /**
     * @deprecated use new AbstractBaseDefinition instead this class
     * @deprecated Reason: Output format changed
     *
     * Returns list of translation keys
     *
     * @return array
     */
    public static function getTranslationsList(): array
    {
        return array_column(static::getList(), 'translation_key');
    }

    /**
     * @deprecated use new AbstractBaseDefinition instead this class
     * @deprecated Reason: Output format changed
     *
     * Returns user readable translation key for definition (ID)
     * @param int $definition
     * @return string
     * @throws \Eternity\Exceptions\ServerException
     */
    public static function getTranslationKey(int $definition): string
    {
        $definitionList = static::getList();
        if (!isset($definitionList[$definition]['translation_key'])) {
            throw new DefinitionException(
                'Definition error',
                'Translation key is not defined for definition: ' . $definition
            );
        }
        return $definitionList[$definition]['translation_key'];
    }

}
