<?php

namespace Eternity\Components\Definition;

use Eternity\Components\Definition\Exceptions\DefinitionException;

/**
 * Abstract base definition class
 *
 * New definition version
 *
 * Usage:
 * Extend this class and realize [getList] method
 *
 * Class AbstractBaseDefinition
 * @package Eternity\Components\Definition
 */
abstract class AbstractBaseDefinition
{
    /**
     * Method builds the whole definition structure
     *
     * Every array item must have next structure:
     *  - id
     *  - title
     *  - translation
     *
     * For example:
     *  [
     *      [
     *          'id' => 5,
     *          'title' => 'String description',
     *          'translation_key' => 'Key for user readable translation',
     *      ],
     *      ...
     *  ]
     *
     * @return array
     */
    abstract public static function getDefinitionsList(): array;

    /**
     * Returns list of translation keys
     *
     * Example:
     * [
     *    'Key for user readable translation'
     * ]
     * @return array
     */
    public static function getTranslationKeys(): array
    {
        return array_column(static::getDefinitionsList(), 'translation_key');
    }

    /**
     * Returns title by definition id
     * @param string $definitionId
     * @return string
     * @throws \Eternity\Components\Definition\Exceptions\DefinitionException
     */
    public static function getTitle(string $definitionId): string
    {
        $array = array_column(static::getDefinitionsList(), 'title', 'id');
        if (!isset($array[$definitionId])) {
            throw new DefinitionException(
                'Definition error',
                'Title is not defined for definition key: ' . $definitionId
            );
        }

        return $array[$definitionId];
    }

    /**
     * Returns translation key by definition id
     * @param string $definitionId
     * @return string
     * @throws \Eternity\Components\Definition\Exceptions\DefinitionException
     */
    public static function getTranslationKey(string $definitionId): string
    {
        $array = array_column(static::getDefinitionsList(), 'translation_key', 'id');
        if (!isset($array[$definitionId])) {
            throw new DefinitionException(
                'Definition error',
                'Translation key is not defined for definition key: ' . $definitionId
            );
        }

        return $array[$definitionId];
    }

    /**
     * Returns translated list of values
     * [
     *    [
     *        'id' => 5,
     *        'title' => 'Translated title',
     *    ]
     * ]
     *
     * @param array $translations
     * [
     *     "translation-key": "Translation value",
     * ]
     * @return array
     * @throws \Eternity\Components\Definition\Exceptions\DefinitionException
     */
    public static function getTranslatedList(array $translations): array
    {
        $result = [];
        foreach (static::getDefinitionsList() as $definitionItem) {
            if (!isset($definitionItem['translation_key'])) {
                throw new DefinitionException(
                    'Definition error',
                    'Translation KEY is not set for key: [' . $definitionItem['id'] . ']'
                );
            }
            if (!isset($translations[$definitionItem['translation_key']])) {
                throw new DefinitionException(
                    'Definition error',
                    'Translation is not found for translation key: [' . $definitionItem['translation_key'] . ']'
                );
            }
            $result[] = [
                'id'    => $definitionItem['id'],
                'title' => $translations[$definitionItem['translation_key']],
            ];
        }

        return $result;
    }

    /**
     * Returns list of constants
     * @return array
     */
    public static function getConstList(): array
    {
        return array_column(static::getDefinitionsList(), 'id');
    }

    /**
     * Returns list of titles
     * Example:
     * [
     *    'id' => 5,
     *    'title' => 'USA',
     * ]
     * @return array
     */
    public static function getTitleList(): array
    {
        $result = [];
        foreach (static::getDefinitionsList() as $item) {
            $result[] = [
                'id'    => $item['id'],
                'title' => $item['title']
            ];
        }

        return $result;
    }
}