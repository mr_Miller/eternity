<?php

namespace Eternity\Components\Definition\Exceptions;

use Eternity\Exceptions\ServerException;
use Throwable;

/**
 * Class DefinitionException
 * @package App\Infrastructure\Definitions\Exceptions
 */
class DefinitionException extends ServerException
{
    public function __construct(string $title = null, string $detail = null, Throwable $previous = null)
    {
        parent::__construct($title, $detail, $previous);
        $this->type = 'DefinitionException';
    }
}
