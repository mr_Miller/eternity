<?php

namespace Eternity\Messages;

use Eternity\Exceptions\ErrorCodes;

/**
 * Class Messages
 * @package Eternity\Messages
 */
abstract class Messages
{
    /**
     * Mapping between message code and corresponding messages
     *
     * @var array
     */
    protected static $messages = [
        ErrorCodes::REQUEST                      => "Request: {method} {url}",
        ErrorCodes::REQUEST_HEADERS              => "Request Headers: {headers}",
        ErrorCodes::REQUEST_BODY                 => "Request Body: {body}",
        ErrorCodes::LANGUAGE_NOT_EXISTS_ERROR    => 'Language {language} is not exists',
        ErrorCodes::REGION_NOT_SET_ERROR         => 'Region is not set in config',
        ErrorCodes::REGION_NOT_EXISTS_ERROR      => 'Region is not exists',
        ErrorCodes::TRACE_ID_GENERATION_OVERRIDE => 'It is abandoned to regenerate the existed Trace ID',
        ErrorCodes::TRACE_ID_INIT_OVERRIDE       => 'It is abandoned to reinitialize the existed Trace ID',
        ErrorCodes::TRACE_ID_HEADER_MISSING      => 'Trace ID is not provided',
        ErrorCodes::TRACE_ID_INIT_ERROR          => 'Trace ID must be set before accessed to it',
        ErrorCodes::LOGGER_CONFIG_HANDLERS_EMPTY => "Missing ['handlers'] section in logger configuration",
        ErrorCodes::DB_QUERY_ERROR               => 'Db query error',
    ];

    /**
     * Returns code relative message
     * @param int $code
     * @param array $params
     * @return string
     */
    final public static function message(int $code, array $params = []): string
    {
        return strtr(self::messages()[$code] ?? '', $params);
    }

    /**
     * Recursively composes messages from all parent classes
     * @return array|string[]
     */
    final protected static function messages(): array
    {
        static $internalMessages = [];

        if (!isset($internalMessages[static::class])) {
            $internalMessages[static::class] = get_parent_class(static::class)
                ? call_user_func([get_parent_class(static::class), 'messages']) + static::$messages
                : static::$messages;
        }

        return $internalMessages[static::class];
    }
}