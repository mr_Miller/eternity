<?php

namespace Eternity\Definitions\Country;

use Eternity\Components\Definition\AbstractBaseDefinition;

/**
 * Country list definition
 *
 * todo add translate keys to every country and translate them
 * 
 * Class CountryDefinition
 * @package Eternity\Definitions\Country
 */
class CountryDefinition extends AbstractBaseDefinition
{
    public const UKRAINE = '804';
    public const UNITED_STATES_OF_AMERICA = '840';
    public const FRANCE = '250';
    public const POLAND = '616';
    public const GERMANY = '276';

    /**
     * Method builds the whole definition structure
     *
     * Every array item must have next structure:
     *  - id
     *  - title
     *  - translation
     *
     * For example:
     *  [
     *      [
     *          'id' => 5,
     *          'title' => 'String description',
     *          'translation_key' => 'country-definitions.Key for user readable translation',
     *      ],
     *      ...
     *  ]
     *
     * @return array
     */
    public static function getDefinitionsList(): array
    {
        return [
            [
                'title' => 'Afghanistan',
                'alpha2' => 'AF',
                'alpha3' => 'AFG',
                'translation_key' => 'country-definitions.AFG',
                'id' => '004',
                'currency' => [
                    'AFN',
                ],
            ],
            [
                'title' => 'Åland Islands',
                'alpha2' => 'AX',
                'alpha3' => 'ALA',
                'translation_key' => 'country-definitions.ALA',
                'id' => '248',
                'currency' => [
                    'EUR',
                ],
            ],
            [
                'title' => 'Albania',
                'alpha2' => 'AL',
                'alpha3' => 'ALB',
                'translation_key' => 'country-definitions.ALB',
                'id' => '008',
                'currency' => [
                    'ALL',
                ],
            ],
            [
                'title' => 'Algeria',
                'alpha2' => 'DZ',
                'alpha3' => 'DZA',
                'translation_key' => 'country-definitions.DZA',
                'id' => '012',
                'currency' => [
                    'DZD',
                ],
            ],
            [
                'title' => 'American Samoa',
                'alpha2' => 'AS',
                'alpha3' => 'ASM',
                'translation_key' => 'country-definitions.ASM',
                'id' => '016',
                'currency' => [
                    'USD',
                ],
            ],
            [
                'title' => 'Andorra',
                'alpha2' => 'AD',
                'alpha3' => 'AND',
                'translation_key' => 'country-definitions.AND',
                'id' => '020',
                'currency' => [
                    'EUR',
                ],
            ],
            [
                'title' => 'Angola',
                'alpha2' => 'AO',
                'alpha3' => 'AGO',
                'translation_key' => 'country-definitions.AGO',
                'id' => '024',
                'currency' => [
                    'AOA',
                ],
            ],
            [
                'title' => 'Anguilla',
                'alpha2' => 'AI',
                'alpha3' => 'AIA',
                'translation_key' => 'country-definitions.AIA',
                'id' => '660',
                'currency' => [
                    'XCD',
                ],
            ],
            [
                'title' => 'Antarctica',
                'alpha2' => 'AQ',
                'alpha3' => 'ATA',
                'translation_key' => 'country-definitions.ATA',
                'id' => '010',
                'currency' => [
                    'ARS',
                    'AUD',
                    'BGN',
                    'BRL',
                    'BYR',
                    'CLP',
                    'CNY',
                    'CZK',
                    'EUR',
                    'GBP',
                    'INR',
                    'JPY',
                    'KRW',
                    'NOK',
                    'NZD',
                    'PEN',
                    'PKR',
                    'PLN',
                    'RON',
                    'RUB',
                    'SEK',
                    'UAH',
                    'USD',
                    'UYU',
                    'ZAR',
                ],
            ],
            [
                'title' => 'Antigua and Barbuda',
                'alpha2' => 'AG',
                'alpha3' => 'ATG',
                'translation_key' => 'country-definitions.ATG',
                'id' => '028',
                'currency' => [
                    'XCD',
                ],
            ],
            [
                'title' => 'Argentina',
                'alpha2' => 'AR',
                'alpha3' => 'ARG',
                'translation_key' => 'country-definitions.ARG',
                'id' => '032',
                'currency' => [
                    'ARS',
                ],
            ],
            [
                'title' => 'Armenia',
                'alpha2' => 'AM',
                'alpha3' => 'ARM',
                'translation_key' => 'country-definitions.ARM',
                'id' => '051',
                'currency' => [
                    'AMD',
                ],
            ],
            [
                'title' => 'Aruba',
                'alpha2' => 'AW',
                'alpha3' => 'ABW',
                'translation_key' => 'country-definitions.ABW',
                'id' => '533',
                'currency' => [
                    'AWG',
                ],
            ],
            [
                'title' => 'Australia',
                'alpha2' => 'AU',
                'alpha3' => 'AUS',
                'translation_key' => 'country-definitions.AUS',
                'id' => '036',
                'currency' => [
                    'AUD',
                ],
            ],
            [
                'title' => 'Austria',
                'alpha2' => 'AT',
                'alpha3' => 'AUT',
                'translation_key' => 'country-definitions.AUT',
                'id' => '040',
                'currency' => [
                    'EUR',
                ],
            ],
            [
                'title' => 'Azerbaijan',
                'alpha2' => 'AZ',
                'alpha3' => 'AZE',
                'translation_key' => 'country-definitions.AZE',
                'id' => '031',
                'currency' => [
                    'AZN',
                ],
            ],
            [
                'title' => 'Bahamas',
                'alpha2' => 'BS',
                'alpha3' => 'BHS',
                'translation_key' => 'country-definitions.BHS',
                'id' => '044',
                'currency' => [
                    'BSD',
                ],
            ],
            [
                'title' => 'Bahrain',
                'alpha2' => 'BH',
                'alpha3' => 'BHR',
                'translation_key' => 'country-definitions.BHR',
                'id' => '048',
                'currency' => [
                    'BHD',
                ],
            ],
            [
                'title' => 'Bangladesh',
                'alpha2' => 'BD',
                'alpha3' => 'BGD',
                'translation_key' => 'country-definitions.BGD',
                'id' => '050',
                'currency' => [
                    'BDT',
                ],
            ],
            [
                'title' => 'Barbados',
                'alpha2' => 'BB',
                'alpha3' => 'BRB',
                'translation_key' => 'country-definitions.BRB',
                'id' => '052',
                'currency' => [
                    'BBD',
                ],
            ],
            [
                'title' => 'Belarus',
                'alpha2' => 'BY',
                'alpha3' => 'BLR',
                'translation_key' => 'country-definitions.BLR',
                'id' => '112',
                'currency' => [
                    'BYN',
                ],
            ],
            [
                'title' => 'Belgium',
                'alpha2' => 'BE',
                'alpha3' => 'BEL',
                'translation_key' => 'country-definitions.BEL',
                'id' => '056',
                'currency' => [
                    'EUR',
                ],
            ],
            [
                'title' => 'Belize',
                'alpha2' => 'BZ',
                'alpha3' => 'BLZ',
                'translation_key' => 'country-definitions.BLZ',
                'id' => '084',
                'currency' => [
                    'BZD',
                ],
            ],
            [
                'title' => 'Benin',
                'alpha2' => 'BJ',
                'alpha3' => 'BEN',
                'translation_key' => 'country-definitions.BEN',
                'id' => '204',
                'currency' => [
                    'XOF',
                ],
            ],
            [
                'title' => 'Bermuda',
                'alpha2' => 'BM',
                'alpha3' => 'BMU',
                'translation_key' => 'country-definitions.BMU',
                'id' => '060',
                'currency' => [
                    'BMD',
                ],
            ],
            [
                'title' => 'Bhutan',
                'alpha2' => 'BT',
                'alpha3' => 'BTN',
                'translation_key' => 'country-definitions.BTN',
                'id' => '064',
                'currency' => [
                    'BTN',
                ],
            ],
            [
                'title' => 'Bolivia',
                'alpha2' => 'BO',
                'alpha3' => 'BOL',
                'translation_key' => 'country-definitions.BOL',
                'id' => '068',
                'currency' => [
                    'BOB',
                ],
            ],
            [
                'title' => 'Bonaire, Sint Eustatius and Saba',
                'alpha2' => 'BQ',
                'alpha3' => 'BES',
                'translation_key' => 'country-definitions.BES',
                'id' => '535',
                'currency' => [
                    'USD',
                ],
            ],
            [
                'title' => 'Bosnia and Herzegovina',
                'alpha2' => 'BA',
                'alpha3' => 'BIH',
                'translation_key' => 'country-definitions.BIH',
                'id' => '070',
                'currency' => [
                    'BAM',
                ],
            ],
            [
                'title' => 'Botswana',
                'alpha2' => 'BW',
                'alpha3' => 'BWA',
                'translation_key' => 'country-definitions.BWA',
                'id' => '072',
                'currency' => [
                    'BWP',
                ],
            ],
            [
                'title' => 'Bouvet Island',
                'alpha2' => 'BV',
                'alpha3' => 'BVT',
                'translation_key' => 'country-definitions.BVT',
                'id' => '074',
                'currency' => [
                    'NOK',
                ],
            ],
            [
                'title' => 'Brazil',
                'alpha2' => 'BR',
                'alpha3' => 'BRA',
                'translation_key' => 'country-definitions.BRA',
                'id' => '076',
                'currency' => [
                    'BRL',
                ],
            ],
            [
                'title' => 'British Indian Ocean Territory',
                'alpha2' => 'IO',
                'alpha3' => 'IOT',
                'translation_key' => 'country-definitions.IOT',
                'id' => '086',
                'currency' => [
                    'GBP',
                ],
            ],
            [
                'title' => 'Brunei Darussalam',
                'alpha2' => 'BN',
                'alpha3' => 'BRN',
                'translation_key' => 'country-definitions.BRN',
                'id' => '096',
                'currency' => [
                    'BND',
                    'SGD',
                ],
            ],
            [
                'title' => 'Bulgaria',
                'alpha2' => 'BG',
                'alpha3' => 'BGR',
                'translation_key' => 'country-definitions.BGR',
                'id' => '100',
                'currency' => [
                    'BGN',
                ],
            ],
            [
                'title' => 'Burkina Faso',
                'alpha2' => 'BF',
                'alpha3' => 'BFA',
                'translation_key' => 'country-definitions.BFA',
                'id' => '854',
                'currency' => [
                    'XOF',
                ],
            ],
            [
                'title' => 'Burundi',
                'alpha2' => 'BI',
                'alpha3' => 'BDI',
                'translation_key' => 'country-definitions.BDI',
                'id' => '108',
                'currency' => [
                    'BIF',
                ],
            ],
            [
                'title' => 'Cabo Verde',
                'alpha2' => 'CV',
                'alpha3' => 'CPV',
                'translation_key' => 'country-definitions.CPV',
                'id' => '132',
                'currency' => [
                    'CVE',
                ],
            ],
            [
                'title' => 'Cambodia',
                'alpha2' => 'KH',
                'alpha3' => 'KHM',
                'translation_key' => 'country-definitions.KHM',
                'id' => '116',
                'currency' => [
                    'KHR',
                ],
            ],
            [
                'title' => 'Cameroon',
                'alpha2' => 'CM',
                'alpha3' => 'CMR',
                'translation_key' => 'country-definitions.CMR',
                'id' => '120',
                'currency' => [
                    'XAF',
                ],
            ],
            [
                'title' => 'Canada',
                'alpha2' => 'CA',
                'alpha3' => 'CAN',
                'translation_key' => 'country-definitions.CAN',
                'id' => '124',
                'currency' => [
                    'CAD',
                ],
            ],
            [
                'title' => 'Cayman Islands',
                'alpha2' => 'KY',
                'alpha3' => 'CYM',
                'translation_key' => 'country-definitions.CYM',
                'id' => '136',
                'currency' => [
                    'KYD',
                ],
            ],
            [
                'title' => 'Central African Republic',
                'alpha2' => 'CF',
                'alpha3' => 'CAF',
                'translation_key' => 'country-definitions.CAF',
                'id' => '140',
                'currency' => [
                    'XAF',
                ],
            ],
            [
                'title' => 'Chad',
                'alpha2' => 'TD',
                'alpha3' => 'TCD',
                'translation_key' => 'country-definitions.TCD',
                'id' => '148',
                'currency' => [
                    'XAF',
                ],
            ],
            [
                'title' => 'Chile',
                'alpha2' => 'CL',
                'alpha3' => 'CHL',
                'translation_key' => 'country-definitions.CHL',
                'id' => '152',
                'currency' => [
                    'CLP',
                ],
            ],
            [
                'title' => 'China',
                'alpha2' => 'CN',
                'alpha3' => 'CHN',
                'translation_key' => 'country-definitions.CHN',
                'id' => '156',
                'currency' => [
                    'CNY',
                ],
            ],
            [
                'title' => 'Christmas Island',
                'alpha2' => 'CX',
                'alpha3' => 'CXR',
                'translation_key' => 'country-definitions.CXR',
                'id' => '162',
                'currency' => [
                    'AUD',
                ],
            ],
            [
                'title' => 'Cocos (Keeling) Islands',
                'alpha2' => 'CC',
                'alpha3' => 'CCK',
                'translation_key' => 'country-definitions.CCK',
                'id' => '166',
                'currency' => [
                    'AUD',
                ],
            ],
            [
                'title' => 'Colombia',
                'alpha2' => 'CO',
                'alpha3' => 'COL',
                'translation_key' => 'country-definitions.COL',
                'id' => '170',
                'currency' => [
                    'COP',
                ],
            ],
            [
                'title' => 'Comoros',
                'alpha2' => 'KM',
                'alpha3' => 'COM',
                'translation_key' => 'country-definitions.COM',
                'id' => '174',
                'currency' => [
                    'KMF',
                ],
            ],
            [
                'title' => 'Congo',
                'alpha2' => 'CG',
                'alpha3' => 'COG',
                'translation_key' => 'country-definitions.COG',
                'id' => '178',
                'currency' => [
                    'XAF',
                ],
            ],
            [
                'title' => 'Democratic Republic of the Congo',
                'alpha2' => 'CD',
                'alpha3' => 'COD',
                'translation_key' => 'country-definitions.COD',
                'id' => '180',
                'currency' => [
                    'CDF',
                ],
            ],
            [
                'title' => 'Cook Islands',
                'alpha2' => 'CK',
                'alpha3' => 'COK',
                'translation_key' => 'country-definitions.COK',
                'id' => '184',
                'currency' => [
                    'NZD',
                ],
            ],
            [
                'title' => 'Costa Rica',
                'alpha2' => 'CR',
                'alpha3' => 'CRI',
                'translation_key' => 'country-definitions.CRI',
                'id' => '188',
                'currency' => [
                    'CRC',
                ],
            ],
            [
                'title' => 'Côte d\'Ivoire',
                'alpha2' => 'CI',
                'alpha3' => 'CIV',
                'translation_key' => 'country-definitions.CIV',
                'id' => '384',
                'currency' => [
                    'XOF',
                ],
            ],
            [
                'title' => 'Croatia',
                'alpha2' => 'HR',
                'alpha3' => 'HRV',
                'translation_key' => 'country-definitions.HRV',
                'id' => '191',
                'currency' => [
                    'HRK',
                ],
            ],
            [
                'title' => 'Cuba',
                'alpha2' => 'CU',
                'alpha3' => 'CUB',
                'translation_key' => 'country-definitions.CUB',
                'id' => '192',
                'currency' => [
                    'CUC',
                    'CUP',
                ],
            ],
            [
                'title' => 'Curaçao',
                'alpha2' => 'CW',
                'alpha3' => 'CUW',
                'translation_key' => 'country-definitions.CUW',
                'id' => '531',
                'currency' => [
                    'ANG',
                ],
            ],
            [
                'title' => 'Cyprus',
                'alpha2' => 'CY',
                'alpha3' => 'CYP',
                'translation_key' => 'country-definitions.CYP',
                'id' => '196',
                'currency' => [
                    'EUR',
                ],
            ],
            [
                'title' => 'Czechia',
                'alpha2' => 'CZ',
                'alpha3' => 'CZE',
                'translation_key' => 'country-definitions.CZE',
                'id' => '203',
                'currency' => [
                    'CZK',
                ],
            ],
            [
                'title' => 'Denmark',
                'alpha2' => 'DK',
                'alpha3' => 'DNK',
                'translation_key' => 'country-definitions.DNK',
                'id' => '208',
                'currency' => [
                    'DKK',
                ],
            ],
            [
                'title' => 'Djibouti',
                'alpha2' => 'DJ',
                'alpha3' => 'DJI',
                'translation_key' => 'country-definitions.DJI',
                'id' => '262',
                'currency' => [
                    'DJF',
                ],
            ],
            [
                'title' => 'Dominica',
                'alpha2' => 'DM',
                'alpha3' => 'DMA',
                'translation_key' => 'country-definitions.DMA',
                'id' => '212',
                'currency' => [
                    'XCD',
                ],
            ],
            [
                'title' => 'Dominican Republic',
                'alpha2' => 'DO',
                'alpha3' => 'DOM',
                'translation_key' => 'country-definitions.DOM',
                'id' => '214',
                'currency' => [
                    'DOP',
                ],
            ],
            [
                'title' => 'Ecuador',
                'alpha2' => 'EC',
                'alpha3' => 'ECU',
                'translation_key' => 'country-definitions.ECU',
                'id' => '218',
                'currency' => [
                    'USD',
                ],
            ],
            [
                'title' => 'Egypt',
                'alpha2' => 'EG',
                'alpha3' => 'EGY',
                'translation_key' => 'country-definitions.EGY',
                'id' => '818',
                'currency' => [
                    'EGP',
                ],
            ],
            [
                'title' => 'El Salvador',
                'alpha2' => 'SV',
                'alpha3' => 'SLV',
                'translation_key' => 'country-definitions.SLV',
                'id' => '222',
                'currency' => [
                    'USD',
                ],
            ],
            [
                'title' => 'Equatorial Guinea',
                'alpha2' => 'GQ',
                'alpha3' => 'GNQ',
                'translation_key' => 'country-definitions.GNQ',
                'id' => '226',
                'currency' => [
                    'XAF',
                ],
            ],
            [
                'title' => 'Eritrea',
                'alpha2' => 'ER',
                'alpha3' => 'ERI',
                'translation_key' => 'country-definitions.ERI',
                'id' => '232',
                'currency' => [
                    'ERN',
                ],
            ],
            [
                'title' => 'Estonia',
                'alpha2' => 'EE',
                'alpha3' => 'EST',
                'translation_key' => 'country-definitions.EST',
                'id' => '233',
                'currency' => [
                    'EUR',
                ],
            ],
            [
                'title' => 'Ethiopia',
                'alpha2' => 'ET',
                'alpha3' => 'ETH',
                'translation_key' => 'country-definitions.ETH',
                'id' => '231',
                'currency' => [
                    'ETB',
                ],
            ],
            [
                'title' => 'Eswatini',
                'alpha2' => 'SZ',
                'alpha3' => 'SWZ',
                'translation_key' => 'country-definitions.SWZ',
                'id' => '748',
                'currency' => [
                    'SZL',
                    'ZAR',
                ],
            ],
            [
                'title' => 'Falkland Islands (Islas Malvinas)',
                'alpha2' => 'FK',
                'alpha3' => 'FLK',
                'translation_key' => 'country-definitions.FLK',
                'id' => '238',
                'currency' => [
                    'FKP',
                ],
            ],
            [
                'title' => 'Faroe Islands',
                'alpha2' => 'FO',
                'alpha3' => 'FRO',
                'translation_key' => 'country-definitions.FRO',
                'id' => '234',
                'currency' => [
                    'DKK',
                ],
            ],
            [
                'title' => 'Fiji',
                'alpha2' => 'FJ',
                'alpha3' => 'FJI',
                'translation_key' => 'country-definitions.FJI',
                'id' => '242',
                'currency' => [
                    'FJD',
                ],
            ],
            [
                'title' => 'Finland',
                'alpha2' => 'FI',
                'alpha3' => 'FIN',
                'translation_key' => 'country-definitions.FIN',
                'id' => '246',
                'currency' => [
                    'EUR',
                ],
            ],
            [
                'title' => 'France',
                'alpha2' => 'FR',
                'alpha3' => 'FRA',
                'translation_key' => 'country-definitions.FRA',
                'id' => self::FRANCE,
                'currency' => [
                    'EUR',
                ],
            ],
            [
                'title' => 'French Guiana',
                'alpha2' => 'GF',
                'alpha3' => 'GUF',
                'translation_key' => 'country-definitions.GUF',
                'id' => '254',
                'currency' => [
                    'EUR',
                ],
            ],
            [
                'title' => 'French Polynesia',
                'alpha2' => 'PF',
                'alpha3' => 'PYF',
                'translation_key' => 'country-definitions.PYF',
                'id' => '258',
                'currency' => [
                    'XPF',
                ],
            ],
            [
                'title' => 'French Southern Territories',
                'alpha2' => 'TF',
                'alpha3' => 'ATF',
                'translation_key' => 'country-definitions.ATF',
                'id' => '260',
                'currency' => [
                    'EUR',
                ],
            ],
            [
                'title' => 'Gabon',
                'alpha2' => 'GA',
                'alpha3' => 'GAB',
                'translation_key' => 'country-definitions.GAB',
                'id' => '266',
                'currency' => [
                    'XAF',
                ],
            ],
            [
                'title' => 'Gambia',
                'alpha2' => 'GM',
                'alpha3' => 'GMB',
                'translation_key' => 'country-definitions.GMB',
                'id' => '270',
                'currency' => [
                    'GMD',
                ],
            ],
            [
                'title' => 'Georgia',
                'alpha2' => 'GE',
                'alpha3' => 'GEO',
                'translation_key' => 'country-definitions.GEO',
                'id' => '268',
                'currency' => [
                    'GEL',
                ],
            ],
            [
                'title' => 'Germany',
                'alpha2' => 'DE',
                'alpha3' => 'DEU',
                'translation_key' => 'country-definitions.DEU',
                'id' => self::GERMANY,
                'currency' => [
                    'EUR',
                ],
            ],
            [
                'title' => 'Ghana',
                'alpha2' => 'GH',
                'alpha3' => 'GHA',
                'translation_key' => 'country-definitions.GHA',
                'id' => '288',
                'currency' => [
                    'GHS',
                ],
            ],
            [
                'title' => 'Gibraltar',
                'alpha2' => 'GI',
                'alpha3' => 'GIB',
                'translation_key' => 'country-definitions.GIB',
                'id' => '292',
                'currency' => [
                    'GIP',
                ],
            ],
            [
                'title' => 'Greece',
                'alpha2' => 'GR',
                'alpha3' => 'GRC',
                'translation_key' => 'country-definitions.GRC',
                'id' => '300',
                'currency' => [
                    'EUR',
                ],
            ],
            [
                'title' => 'Greenland',
                'alpha2' => 'GL',
                'alpha3' => 'GRL',
                'translation_key' => 'country-definitions.GRL',
                'id' => '304',
                'currency' => [
                    'DKK',
                ],
            ],
            [
                'title' => 'Grenada',
                'alpha2' => 'GD',
                'alpha3' => 'GRD',
                'translation_key' => 'country-definitions.GRD',
                'id' => '308',
                'currency' => [
                    'XCD',
                ],
            ],
            [
                'title' => 'Guadeloupe',
                'alpha2' => 'GP',
                'alpha3' => 'GLP',
                'translation_key' => 'country-definitions.GLP',
                'id' => '312',
                'currency' => [
                    'EUR',
                ],
            ],
            [
                'title' => 'Guam',
                'alpha2' => 'GU',
                'alpha3' => 'GUM',
                'translation_key' => 'country-definitions.GUM',
                'id' => '316',
                'currency' => [
                    'USD',
                ],
            ],
            [
                'title' => 'Guatemala',
                'alpha2' => 'GT',
                'alpha3' => 'GTM',
                'translation_key' => 'country-definitions.GTM',
                'id' => '320',
                'currency' => [
                    'GTQ',
                ],
            ],
            [
                'title' => 'Guernsey',
                'alpha2' => 'GG',
                'alpha3' => 'GGY',
                'translation_key' => 'country-definitions.GGY',
                'id' => '831',
                'currency' => [
                    'GBP',
                ],
            ],
            [
                'title' => 'Guinea',
                'alpha2' => 'GN',
                'alpha3' => 'GIN',
                'translation_key' => 'country-definitions.GIN',
                'id' => '324',
                'currency' => [
                    'GNF',
                ],
            ],
            [
                'title' => 'Guinea-Bissau',
                'alpha2' => 'GW',
                'alpha3' => 'GNB',
                'translation_key' => 'country-definitions.GNB',
                'id' => '624',
                'currency' => [
                    'XOF',
                ],
            ],
            [
                'title' => 'Guyana',
                'alpha2' => 'GY',
                'alpha3' => 'GUY',
                'translation_key' => 'country-definitions.GUY',
                'id' => '328',
                'currency' => [
                    'GYD',
                ],
            ],
            [
                'title' => 'Haiti',
                'alpha2' => 'HT',
                'alpha3' => 'HTI',
                'translation_key' => 'country-definitions.HTI',
                'id' => '332',
                'currency' => [
                    'HTG',
                ],
            ],
            [
                'title' => 'Heard Island and McDonald Islands',
                'alpha2' => 'HM',
                'alpha3' => 'HMD',
                'translation_key' => 'country-definitions.HMD',
                'id' => '334',
                'currency' => [
                    'AUD',
                ],
            ],
            [
                'title' => 'Holy See',
                'alpha2' => 'VA',
                'alpha3' => 'VAT',
                'translation_key' => 'country-definitions.VAT',
                'id' => '336',
                'currency' => [
                    'EUR',
                ],
            ],
            [
                'title' => 'Honduras',
                'alpha2' => 'HN',
                'alpha3' => 'HND',
                'translation_key' => 'country-definitions.HND',
                'id' => '340',
                'currency' => [
                    'HNL',
                ],
            ],
            [
                'title' => 'Hong Kong',
                'alpha2' => 'HK',
                'alpha3' => 'HKG',
                'translation_key' => 'country-definitions.HKG',
                'id' => '344',
                'currency' => [
                    'HKD',
                ],
            ],
            [
                'title' => 'Hungary',
                'alpha2' => 'HU',
                'alpha3' => 'HUN',
                'translation_key' => 'country-definitions.HUN',
                'id' => '348',
                'currency' => [
                    'HUF',
                ],
            ],
            [
                'title' => 'Iceland',
                'alpha2' => 'IS',
                'alpha3' => 'ISL',
                'translation_key' => 'country-definitions.ISL',
                'id' => '352',
                'currency' => [
                    'ISK',
                ],
            ],
            [
                'title' => 'India',
                'alpha2' => 'IN',
                'alpha3' => 'IND',
                'translation_key' => 'country-definitions.IND',
                'id' => '356',
                'currency' => [
                    'INR',
                ],
            ],
            [
                'title' => 'Indonesia',
                'alpha2' => 'ID',
                'alpha3' => 'IDN',
                'translation_key' => 'country-definitions.IDN',
                'id' => '360',
                'currency' => [
                    'IDR',
                ],
            ],
            [
                'title' => 'Iran',
                'alpha2' => 'IR',
                'alpha3' => 'IRN',
                'translation_key' => 'country-definitions.IRN',
                'id' => '364',
                'currency' => [
                    'IRR',
                ],
            ],
            [
                'title' => 'Iraq',
                'alpha2' => 'IQ',
                'alpha3' => 'IRQ',
                'translation_key' => 'country-definitions.IRQ',
                'id' => '368',
                'currency' => [
                    'IQD',
                ],
            ],
            [
                'title' => 'Ireland',
                'alpha2' => 'IE',
                'alpha3' => 'IRL',
                'translation_key' => 'country-definitions.IRL',
                'id' => '372',
                'currency' => [
                    'EUR',
                ],
            ],
            [
                'title' => 'Isle of Man',
                'alpha2' => 'IM',
                'alpha3' => 'IMN',
                'translation_key' => 'country-definitions.IMN',
                'id' => '833',
                'currency' => [
                    'GBP',
                ],
            ],
            [
                'title' => 'Israel',
                'alpha2' => 'IL',
                'alpha3' => 'ISR',
                'translation_key' => 'country-definitions.ISR',
                'id' => '376',
                'currency' => [
                    'ILS',
                ],
            ],
            [
                'title' => 'Italy',
                'alpha2' => 'IT',
                'alpha3' => 'ITA',
                'translation_key' => 'country-definitions.ITA',
                'id' => '380',
                'currency' => [
                    'EUR',
                ],
            ],
            [
                'title' => 'Jamaica',
                'alpha2' => 'JM',
                'alpha3' => 'JAM',
                'translation_key' => 'country-definitions.JAM',
                'id' => '388',
                'currency' => [
                    'JMD',
                ],
            ],
            [
                'title' => 'Japan',
                'alpha2' => 'JP',
                'alpha3' => 'JPN',
                'translation_key' => 'country-definitions.JPN',
                'id' => '392',
                'currency' => [
                    'JPY',
                ],
            ],
            [
                'title' => 'Jersey',
                'alpha2' => 'JE',
                'alpha3' => 'JEY',
                'translation_key' => 'country-definitions.JEY',
                'id' => '832',
                'currency' => [
                    'GBP',
                ],
            ],
            [
                'title' => 'Jordan',
                'alpha2' => 'JO',
                'alpha3' => 'JOR',
                'translation_key' => 'country-definitions.JOR',
                'id' => '400',
                'currency' => [
                    'JOD',
                ],
            ],
            [
                'title' => 'Kazakhstan',
                'alpha2' => 'KZ',
                'alpha3' => 'KAZ',
                'translation_key' => 'country-definitions.KAZ',
                'id' => '398',
                'currency' => [
                    'KZT',
                ],
            ],
            [
                'title' => 'Kenya',
                'alpha2' => 'KE',
                'alpha3' => 'KEN',
                'translation_key' => 'country-definitions.KEN',
                'id' => '404',
                'currency' => [
                    'KES',
                ],
            ],
            [
                'title' => 'Kiribati',
                'alpha2' => 'KI',
                'alpha3' => 'KIR',
                'translation_key' => 'country-definitions.KIR',
                'id' => '296',
                'currency' => [
                    'AUD',
                ],
            ],
            [
                'title' => 'North Korea',
                'alpha2' => 'KP',
                'alpha3' => 'PRK',
                'translation_key' => 'country-definitions.PRK',
                'id' => '408',
                'currency' => [
                    'KPW',
                ],
            ],
            [
                'title' => 'South Korea',
                'alpha2' => 'KR',
                'alpha3' => 'KOR',
                'translation_key' => 'country-definitions.KOR',
                'id' => '410',
                'currency' => [
                    'KRW',
                ],
            ],
            [
                'title' => 'Kuwait',
                'alpha2' => 'KW',
                'alpha3' => 'KWT',
                'translation_key' => 'country-definitions.KWT',
                'id' => '414',
                'currency' => [
                    'KWD',
                ],
            ],
            [
                'title' => 'Kyrgyzstan',
                'alpha2' => 'KG',
                'alpha3' => 'KGZ',
                'translation_key' => 'country-definitions.KGZ',
                'id' => '417',
                'currency' => [
                    'KGS',
                ],
            ],
            [
                'title' => 'Lao People\'s Democratic Republic',
                'alpha2' => 'LA',
                'alpha3' => 'LAO',
                'translation_key' => 'country-definitions.LAO',
                'id' => '418',
                'currency' => [
                    'LAK',
                ],
            ],
            [
                'title' => 'Latvia',
                'alpha2' => 'LV',
                'alpha3' => 'LVA',
                'translation_key' => 'country-definitions.LVA',
                'id' => '428',
                'currency' => [
                    'EUR',
                ],
            ],
            [
                'title' => 'Lebanon',
                'alpha2' => 'LB',
                'alpha3' => 'LBN',
                'translation_key' => 'country-definitions.LBN',
                'id' => '422',
                'currency' => [
                    'LBP',
                ],
            ],
            [
                'title' => 'Lesotho',
                'alpha2' => 'LS',
                'alpha3' => 'LSO',
                'translation_key' => 'country-definitions.LSO',
                'id' => '426',
                'currency' => [
                    'LSL',
                    'ZAR',
                ],
            ],
            [
                'title' => 'Liberia',
                'alpha2' => 'LR',
                'alpha3' => 'LBR',
                'translation_key' => 'country-definitions.LBR',
                'id' => '430',
                'currency' => [
                    'LRD',
                ],
            ],
            [
                'title' => 'Libya',
                'alpha2' => 'LY',
                'alpha3' => 'LBY',
                'translation_key' => 'country-definitions.LBY',
                'id' => '434',
                'currency' => [
                    'LYD',
                ],
            ],
            [
                'title' => 'Liechtenstein',
                'alpha2' => 'LI',
                'alpha3' => 'LIE',
                'translation_key' => 'country-definitions.LIE',
                'id' => '438',
                'currency' => [
                    'CHF',
                ],
            ],
            [
                'title' => 'Lithuania',
                'alpha2' => 'LT',
                'alpha3' => 'LTU',
                'translation_key' => 'country-definitions.LTU',
                'id' => '440',
                'currency' => [
                    'EUR',
                ],
            ],
            [
                'title' => 'Luxembourg',
                'alpha2' => 'LU',
                'alpha3' => 'LUX',
                'translation_key' => 'country-definitions.LUX',
                'id' => '442',
                'currency' => [
                    'EUR',
                ],
            ],
            [
                'title' => 'Macao',
                'alpha2' => 'MO',
                'alpha3' => 'MAC',
                'translation_key' => 'country-definitions.MAC',
                'id' => '446',
                'currency' => [
                    'MOP',
                ],
            ],
            [
                'title' => 'North Macedonia',
                'alpha2' => 'MK',
                'alpha3' => 'MKD',
                'translation_key' => 'country-definitions.MKD',
                'id' => '807',
                'currency' => [
                    'MKD',
                ],
            ],
            [
                'title' => 'Madagascar',
                'alpha2' => 'MG',
                'alpha3' => 'MDG',
                'translation_key' => 'country-definitions.MDG',
                'id' => '450',
                'currency' => [
                    'MGA',
                ],
            ],
            [
                'title' => 'Malawi',
                'alpha2' => 'MW',
                'alpha3' => 'MWI',
                'translation_key' => 'country-definitions.MWI',
                'id' => '454',
                'currency' => [
                    'MWK',
                ],
            ],
            [
                'title' => 'Malaysia',
                'alpha2' => 'MY',
                'alpha3' => 'MYS',
                'translation_key' => 'country-definitions.MYS',
                'id' => '458',
                'currency' => [
                    'MYR',
                ],
            ],
            [
                'title' => 'Maldives',
                'alpha2' => 'MV',
                'alpha3' => 'MDV',
                'translation_key' => 'country-definitions.MDV',
                'id' => '462',
                'currency' => [
                    'MVR',
                ],
            ],
            [
                'title' => 'Mali',
                'alpha2' => 'ML',
                'alpha3' => 'MLI',
                'translation_key' => 'country-definitions.MLI',
                'id' => '466',
                'currency' => [
                    'XOF',
                ],
            ],
            [
                'title' => 'Malta',
                'alpha2' => 'MT',
                'alpha3' => 'MLT',
                'translation_key' => 'country-definitions.MLT',
                'id' => '470',
                'currency' => [
                    'EUR',
                ],
            ],
            [
                'title' => 'Marshall Islands',
                'alpha2' => 'MH',
                'alpha3' => 'MHL',
                'translation_key' => 'country-definitions.MHL',
                'id' => '584',
                'currency' => [
                    'USD',
                ],
            ],
            [
                'title' => 'Martinique',
                'alpha2' => 'MQ',
                'alpha3' => 'MTQ',
                'translation_key' => 'country-definitions.MTQ',
                'id' => '474',
                'currency' => [
                    'EUR',
                ],
            ],
            [
                'title' => 'Mauritania',
                'alpha2' => 'MR',
                'alpha3' => 'MRT',
                'translation_key' => 'country-definitions.MRT',
                'id' => '478',
                'currency' => [
                    'MRO',
                ],
            ],
            [
                'title' => 'Mauritius',
                'alpha2' => 'MU',
                'alpha3' => 'MUS',
                'translation_key' => 'country-definitions.MUS',
                'id' => '480',
                'currency' => [
                    'MUR',
                ],
            ],
            [
                'title' => 'Mayotte',
                'alpha2' => 'YT',
                'alpha3' => 'MYT',
                'translation_key' => 'country-definitions.MYT',
                'id' => '175',
                'currency' => [
                    'EUR',
                ],
            ],
            [
                'title' => 'United Mexican States',
                'alpha2' => 'MX',
                'alpha3' => 'MEX',
                'translation_key' => 'country-definitions.MEX',
                'id' => '484',
                'currency' => [
                    'MXN',
                ],
            ],
            [
                'title' => 'Micronesia',
                'alpha2' => 'FM',
                'alpha3' => 'FSM',
                'translation_key' => 'country-definitions.FSM',
                'id' => '583',
                'currency' => [
                    'USD',
                ],
            ],
            [
                'title' => 'Moldova',
                'alpha2' => 'MD',
                'alpha3' => 'MDA',
                'translation_key' => 'country-definitions.MDA',
                'id' => '498',
                'currency' => [
                    'MDL',
                ],
            ],
            [
                'title' => 'Monaco',
                'alpha2' => 'MC',
                'alpha3' => 'MCO',
                'translation_key' => 'country-definitions.MCO',
                'id' => '492',
                'currency' => [
                    'EUR',
                ],
            ],
            [
                'title' => 'Mongolia',
                'alpha2' => 'MN',
                'alpha3' => 'MNG',
                'translation_key' => 'country-definitions.MNG',
                'id' => '496',
                'currency' => [
                    'MNT',
                ],
            ],
            [
                'title' => 'Montenegro',
                'alpha2' => 'ME',
                'alpha3' => 'MNE',
                'translation_key' => 'country-definitions.MNE',
                'id' => '499',
                'currency' => [
                    'EUR',
                ],
            ],
            [
                'title' => 'Montserrat',
                'alpha2' => 'MS',
                'alpha3' => 'MSR',
                'translation_key' => 'country-definitions.MSR',
                'id' => '500',
                'currency' => [
                    'XCD',
                ],
            ],
            [
                'title' => 'Morocco',
                'alpha2' => 'MA',
                'alpha3' => 'MAR',
                'translation_key' => 'country-definitions.MAR',
                'id' => '504',
                'currency' => [
                    'MAD',
                ],
            ],
            [
                'title' => 'Mozambique',
                'alpha2' => 'MZ',
                'alpha3' => 'MOZ',
                'translation_key' => 'country-definitions.MOZ',
                'id' => '508',
                'currency' => [
                    'MZN',
                ],
            ],
            [
                'title' => 'Myanmar',
                'alpha2' => 'MM',
                'alpha3' => 'MMR',
                'translation_key' => 'country-definitions.MMR',
                'id' => '104',
                'currency' => [
                    'MMK',
                ],
            ],
            [
                'title' => 'Namibia',
                'alpha2' => 'NA',
                'alpha3' => 'NAM',
                'translation_key' => 'country-definitions.NAM',
                'id' => '516',
                'currency' => [
                    'NAD',
                    'ZAR',
                ],
            ],
            [
                'title' => 'Nauru',
                'alpha2' => 'NR',
                'alpha3' => 'NRU',
                'translation_key' => 'country-definitions.NRU',
                'id' => '520',
                'currency' => [
                    'AUD',
                ],
            ],
            [
                'title' => 'Nepal',
                'alpha2' => 'NP',
                'alpha3' => 'NPL',
                'translation_key' => 'country-definitions.NPL',
                'id' => '524',
                'currency' => [
                    'NPR',
                ],
            ],
            [
                'title' => 'Netherlands',
                'alpha2' => 'NL',
                'alpha3' => 'NLD',
                'translation_key' => 'country-definitions.NLD',
                'id' => '528',
                'currency' => [
                    'EUR',
                ],
            ],
            [
                'title' => 'New Caledonia',
                'alpha2' => 'NC',
                'alpha3' => 'NCL',
                'translation_key' => 'country-definitions.NCL',
                'id' => '540',
                'currency' => [
                    'XPF',
                ],
            ],
            [
                'title' => 'New Zealand',
                'alpha2' => 'NZ',
                'alpha3' => 'NZL',
                'translation_key' => 'country-definitions.NZL',
                'id' => '554',
                'currency' => [
                    'NZD',
                ],
            ],
            [
                'title' => 'Nicaragua',
                'alpha2' => 'NI',
                'alpha3' => 'NIC',
                'translation_key' => 'country-definitions.NIC',
                'id' => '558',
                'currency' => [
                    'NIO',
                ],
            ],
            [
                'title' => 'Niger',
                'alpha2' => 'NE',
                'alpha3' => 'NER',
                'translation_key' => 'country-definitions.NER',
                'id' => '562',
                'currency' => [
                    'XOF',
                ],
            ],
            [
                'title' => 'Nigeria',
                'alpha2' => 'NG',
                'alpha3' => 'NGA',
                'translation_key' => 'country-definitions.NGA',
                'id' => '566',
                'currency' => [
                    'NGN',
                ],
            ],
            [
                'title' => 'Niue',
                'alpha2' => 'NU',
                'alpha3' => 'NIU',
                'translation_key' => 'country-definitions.NIU',
                'id' => '570',
                'currency' => [
                    'NZD',
                ],
            ],
            [
                'title' => 'Norfolk Island',
                'alpha2' => 'NF',
                'alpha3' => 'NFK',
                'translation_key' => 'country-definitions.NFK',
                'id' => '574',
                'currency' => [
                    'AUD',
                ],
            ],
            [
                'title' => 'Northern Mariana Islands',
                'alpha2' => 'MP',
                'alpha3' => 'MNP',
                'translation_key' => 'country-definitions.MNP',
                'id' => '580',
                'currency' => [
                    'USD',
                ],
            ],
            [
                'title' => 'Norway',
                'alpha2' => 'NO',
                'alpha3' => 'NOR',
                'translation_key' => 'country-definitions.NOR',
                'id' => '578',
                'currency' => [
                    'NOK',
                ],
            ],
            [
                'title' => 'Oman',
                'alpha2' => 'OM',
                'alpha3' => 'OMN',
                'translation_key' => 'country-definitions.OMN',
                'id' => '512',
                'currency' => [
                    'OMR',
                ],
            ],
            [
                'title' => 'Pakistan',
                'alpha2' => 'PK',
                'alpha3' => 'PAK',
                'translation_key' => 'country-definitions.PAK',
                'id' => '586',
                'currency' => [
                    'PKR',
                ],
            ],
            [
                'title' => 'Palau',
                'alpha2' => 'PW',
                'alpha3' => 'PLW',
                'translation_key' => 'country-definitions.PLW',
                'id' => '585',
                'currency' => [
                    'USD',
                ],
            ],
            [
                'title' => 'Palestine',
                'alpha2' => 'PS',
                'alpha3' => 'PSE',
                'translation_key' => 'country-definitions.PSE',
                'id' => '275',
                'currency' => [
                    'ILS',
                ],
            ],
            [
                'title' => 'Panama',
                'alpha2' => 'PA',
                'alpha3' => 'PAN',
                'translation_key' => 'country-definitions.PAN',
                'id' => '591',
                'currency' => [
                    'PAB',
                ],
            ],
            [
                'title' => 'Papua New Guinea',
                'alpha2' => 'PG',
                'alpha3' => 'PNG',
                'translation_key' => 'country-definitions.PNG',
                'id' => '598',
                'currency' => [
                    'PGK',
                ],
            ],
            [
                'title' => 'Paraguay',
                'alpha2' => 'PY',
                'alpha3' => 'PRY',
                'translation_key' => 'country-definitions.PRY',
                'id' => '600',
                'currency' => [
                    'PYG',
                ],
            ],
            [
                'title' => 'Peru',
                'alpha2' => 'PE',
                'alpha3' => 'PER',
                'translation_key' => 'country-definitions.PER',
                'id' => '604',
                'currency' => [
                    'PEN',
                ],
            ],
            [
                'title' => 'Philippines',
                'alpha2' => 'PH',
                'alpha3' => 'PHL',
                'translation_key' => 'country-definitions.PHL',
                'id' => '608',
                'currency' => [
                    'PHP',
                ],
            ],
            [
                'title' => 'Pitcairn',
                'alpha2' => 'PN',
                'alpha3' => 'PCN',
                'translation_key' => 'country-definitions.PCN',
                'id' => '612',
                'currency' => [
                    'NZD',
                ],
            ],
            [
                'title' => 'Poland',
                'alpha2' => 'PL',
                'alpha3' => 'POL',
                'translation_key' => 'country-definitions.POL',
                'id' => self::POLAND,
                'currency' => [
                    'PLN',
                ],
            ],
            [
                'title' => 'Portugal',
                'alpha2' => 'PT',
                'alpha3' => 'PRT',
                'translation_key' => 'country-definitions.PRT',
                'id' => '620',
                'currency' => [
                    'EUR',
                ],
            ],
            [
                'title' => 'Puerto Rico',
                'alpha2' => 'PR',
                'alpha3' => 'PRI',
                'translation_key' => 'country-definitions.PRI',
                'id' => '630',
                'currency' => [
                    'USD',
                ],
            ],
            [
                'title' => 'Qatar',
                'alpha2' => 'QA',
                'alpha3' => 'QAT',
                'translation_key' => 'country-definitions.QAT',
                'id' => '634',
                'currency' => [
                    'QAR',
                ],
            ],
            [
                'title' => 'Réunion',
                'alpha2' => 'RE',
                'alpha3' => 'REU',
                'translation_key' => 'country-definitions.REU',
                'id' => '638',
                'currency' => [
                    'EUR',
                ],
            ],
            [
                'title' => 'Romania',
                'alpha2' => 'RO',
                'alpha3' => 'ROU',
                'translation_key' => 'country-definitions.ROU',
                'id' => '642',
                'currency' => [
                    'RON',
                ],
            ],
            [
                'title' => 'Russian Federation',
                'alpha2' => 'RU',
                'alpha3' => 'RUS',
                'translation_key' => 'country-definitions.RUS',
                'id' => '643',
                'currency' => [
                    'RUB',
                ],
            ],
            [
                'title' => 'Rwanda',
                'alpha2' => 'RW',
                'alpha3' => 'RWA',
                'translation_key' => 'country-definitions.RWA',
                'id' => '646',
                'currency' => [
                    'RWF',
                ],
            ],
            [
                'title' => 'Saint Barthélemy',
                'alpha2' => 'BL',
                'alpha3' => 'BLM',
                'translation_key' => 'country-definitions.BLM',
                'id' => '652',
                'currency' => [
                    'EUR',
                ],
            ],
            [
                'title' => 'Saint Helena, Ascension and Tristan da Cunha',
                'alpha2' => 'SH',
                'alpha3' => 'SHN',
                'translation_key' => 'country-definitions.SHN',
                'id' => '654',
                'currency' => [
                    'SHP',
                ],
            ],
            [
                'title' => 'Saint Kitts and Nevis',
                'alpha2' => 'KN',
                'alpha3' => 'KNA',
                'translation_key' => 'country-definitions.KNA',
                'id' => '659',
                'currency' => [
                    'XCD',
                ],
            ],
            [
                'title' => 'Saint Lucia',
                'alpha2' => 'LC',
                'alpha3' => 'LCA',
                'translation_key' => 'country-definitions.LCA',
                'id' => '662',
                'currency' => [
                    'XCD',
                ],
            ],
            [
                'title' => 'Saint Martin',
                'alpha2' => 'MF',
                'alpha3' => 'MAF',
                'translation_key' => 'country-definitions.MAF',
                'id' => '663',
                'currency' => [
                    'EUR',
                    'USD',
                ],
            ],
            [
                'title' => 'Saint Pierre and Miquelon',
                'alpha2' => 'PM',
                'alpha3' => 'SPM',
                'translation_key' => 'country-definitions.SPM',
                'id' => '666',
                'currency' => [
                    'EUR',
                ],
            ],
            [
                'title' => 'Saint Vincent and the Grenadines',
                'alpha2' => 'VC',
                'alpha3' => 'VCT',
                'translation_key' => 'country-definitions.VCT',
                'id' => '670',
                'currency' => [
                    'XCD',
                ],
            ],
            [
                'title' => 'Samoa',
                'alpha2' => 'WS',
                'alpha3' => 'WSM',
                'translation_key' => 'country-definitions.WSM',
                'id' => '882',
                'currency' => [
                    'WST',
                ],
            ],
            [
                'title' => 'San Marino',
                'alpha2' => 'SM',
                'alpha3' => 'SMR',
                'translation_key' => 'country-definitions.SMR',
                'id' => '674',
                'currency' => [
                    'EUR',
                ],
            ],
            [
                'title' => 'Sao Tome and Principe',
                'alpha2' => 'ST',
                'alpha3' => 'STP',
                'translation_key' => 'country-definitions.STP',
                'id' => '678',
                'currency' => [
                    'STD',
                ],
            ],
            [
                'title' => 'Saudi Arabia',
                'alpha2' => 'SA',
                'alpha3' => 'SAU',
                'translation_key' => 'country-definitions.SAU',
                'id' => '682',
                'currency' => [
                    'SAR',
                ],
            ],
            [
                'title' => 'Senegal',
                'alpha2' => 'SN',
                'alpha3' => 'SEN',
                'translation_key' => 'country-definitions.SEN',
                'id' => '686',
                'currency' => [
                    'XOF',
                ],
            ],
            [
                'title' => 'Serbia',
                'alpha2' => 'RS',
                'alpha3' => 'SRB',
                'translation_key' => 'country-definitions.SRB',
                'id' => '688',
                'currency' => [
                    'RSD',
                ],
            ],
            [
                'title' => 'Seychelles',
                'alpha2' => 'SC',
                'alpha3' => 'SYC',
                'translation_key' => 'country-definitions.SYC',
                'id' => '690',
                'currency' => [
                    'SCR',
                ],
            ],
            [
                'title' => 'Sierra Leone',
                'alpha2' => 'SL',
                'alpha3' => 'SLE',
                'translation_key' => 'country-definitions.SLE',
                'id' => '694',
                'currency' => [
                    'SLL',
                ],
            ],
            [
                'title' => 'Singapore',
                'alpha2' => 'SG',
                'alpha3' => 'SGP',
                'translation_key' => 'country-definitions.SGP',
                'id' => '702',
                'currency' => [
                    'SGD',
                ],
            ],
            [
                'title' => 'Sint Maarten',
                'alpha2' => 'SX',
                'alpha3' => 'SXM',
                'translation_key' => 'country-definitions.SXM',
                'id' => '534',
                'currency' => [
                    'ANG',
                ],
            ],
            [
                'title' => 'Slovakia',
                'alpha2' => 'SK',
                'alpha3' => 'SVK',
                'translation_key' => 'country-definitions.SVK',
                'id' => '703',
                'currency' => [
                    'EUR',
                ],
            ],
            [
                'title' => 'Slovenia',
                'alpha2' => 'SI',
                'alpha3' => 'SVN',
                'translation_key' => 'country-definitions.SVN',
                'id' => '705',
                'currency' => [
                    'EUR',
                ],
            ],
            [
                'title' => 'Solomon Islands',
                'alpha2' => 'SB',
                'alpha3' => 'SLB',
                'translation_key' => 'country-definitions.SLB',
                'id' => '090',
                'currency' => [
                    'SBD',
                ],
            ],
            [
                'title' => 'Somalia',
                'alpha2' => 'SO',
                'alpha3' => 'SOM',
                'translation_key' => 'country-definitions.SOM',
                'id' => '706',
                'currency' => [
                    'SOS',
                ],
            ],
            [
                'title' => 'South Africa',
                'alpha2' => 'ZA',
                'alpha3' => 'ZAF',
                'translation_key' => 'country-definitions.ZAF',
                'id' => '710',
                'currency' => [
                    'ZAR',
                ],
            ],
            [
                'title' => 'South Georgia and the South Sandwich Islands',
                'alpha2' => 'GS',
                'alpha3' => 'SGS',
                'translation_key' => 'country-definitions.SGS',
                'id' => '239',
                'currency' => [
                    'GBP',
                ],
            ],
            [
                'title' => 'South Sudan',
                'alpha2' => 'SS',
                'alpha3' => 'SSD',
                'translation_key' => 'country-definitions.SSD',
                'id' => '728',
                'currency' => [
                    'SSP',
                ],
            ],
            [
                'title' => 'Spain',
                'alpha2' => 'ES',
                'alpha3' => 'ESP',
                'translation_key' => 'country-definitions.ESP',
                'id' => '724',
                'currency' => [
                    'EUR',
                ],
            ],
            [
                'title' => 'Sri Lanka',
                'alpha2' => 'LK',
                'alpha3' => 'LKA',
                'translation_key' => 'country-definitions.LKA',
                'id' => '144',
                'currency' => [
                    'LKR',
                ],
            ],
            [
                'title' => 'Sudan',
                'alpha2' => 'SD',
                'alpha3' => 'SDN',
                'translation_key' => 'country-definitions.SDN',
                'id' => '729',
                'currency' => [
                    'SDG',
                ],
            ],
            [
                'title' => 'Suriname',
                'alpha2' => 'SR',
                'alpha3' => 'SUR',
                'translation_key' => 'country-definitions.SUR',
                'id' => '740',
                'currency' => [
                    'SRD',
                ],
            ],
            [
                'title' => 'Svalbard and Jan Mayen',
                'alpha2' => 'SJ',
                'alpha3' => 'SJM',
                'translation_key' => 'country-definitions.SJM',
                'id' => '744',
                'currency' => [
                    'NOK',
                ],
            ],
            [
                'title' => 'Sweden',
                'alpha2' => 'SE',
                'alpha3' => 'SWE',
                'translation_key' => 'country-definitions.SWE',
                'id' => '752',
                'currency' => [
                    'SEK',
                ],
            ],
            [
                'title' => 'Switzerland',
                'alpha2' => 'CH',
                'alpha3' => 'CHE',
                'translation_key' => 'country-definitions.CHE',
                'id' => '756',
                'currency' => [
                    'CHF',
                ],
            ],
            [
                'title' => 'Syrian Arab Republic',
                'alpha2' => 'SY',
                'alpha3' => 'SYR',
                'translation_key' => 'country-definitions.SYR',
                'id' => '760',
                'currency' => [
                    'SYP',
                ],
            ],
            [
                'title' => 'Taiwan',
                'alpha2' => 'TW',
                'alpha3' => 'TWN',
                'translation_key' => 'country-definitions.TWN',
                'id' => '158',
                'currency' => [
                    'TWD',
                ],
            ],
            [
                'title' => 'Tajikistan',
                'alpha2' => 'TJ',
                'alpha3' => 'TJK',
                'translation_key' => 'country-definitions.TJK',
                'id' => '762',
                'currency' => [
                    'TJS',
                ],
            ],
            [
                'title' => 'Tanzania',
                'alpha2' => 'TZ',
                'alpha3' => 'TZA',
                'translation_key' => 'country-definitions.TZA',
                'id' => '834',
                'currency' => [
                    'TZS',
                ],
            ],
            [
                'title' => 'Thailand',
                'alpha2' => 'TH',
                'alpha3' => 'THA',
                'translation_key' => 'country-definitions.THA',
                'id' => '764',
                'currency' => [
                    'THB',
                ],
            ],
            [
                'title' => 'Timor-Leste',
                'alpha2' => 'TL',
                'alpha3' => 'TLS',
                'translation_key' => 'country-definitions.TLS',
                'id' => '626',
                'currency' => [
                    'USD',
                ],
            ],
            [
                'title' => 'Togo',
                'alpha2' => 'TG',
                'alpha3' => 'TGO',
                'translation_key' => 'country-definitions.TGO',
                'id' => '768',
                'currency' => [
                    'XOF',
                ],
            ],
            [
                'title' => 'Tokelau',
                'alpha2' => 'TK',
                'alpha3' => 'TKL',
                'translation_key' => 'country-definitions.country-definitions.TKL',
                'id' => '772',
                'currency' => [
                    'NZD',
                ],
            ],
            [
                'title' => 'Tonga',
                'alpha2' => 'TO',
                'alpha3' => 'TON',
                'translation_key' => 'country-definitions.TON',
                'id' => '776',
                'currency' => [
                    'TOP',
                ],
            ],
            [
                'title' => 'Trinidad and Tobago',
                'alpha2' => 'TT',
                'alpha3' => 'TTO',
                'translation_key' => 'country-definitions.TTO',
                'id' => '780',
                'currency' => [
                    'TTD',
                ],
            ],
            [
                'title' => 'Tunisia',
                'alpha2' => 'TN',
                'alpha3' => 'TUN',
                'translation_key' => 'country-definitions.TUN',
                'id' => '788',
                'currency' => [
                    'TND',
                ],
            ],
            [
                'title' => 'Turkey',
                'alpha2' => 'TR',
                'alpha3' => 'TUR',
                'translation_key' => 'country-definitions.TUR',
                'id' => '792',
                'currency' => [
                    'TRY',
                ],
            ],
            [
                'title' => 'Turkmenistan',
                'alpha2' => 'TM',
                'alpha3' => 'TKM',
                'translation_key' => 'country-definitions.TKM',
                'id' => '795',
                'currency' => [
                    'TMT',
                ],
            ],
            [
                'title' => 'Turks and Caicos Islands',
                'alpha2' => 'TC',
                'alpha3' => 'TCA',
                'translation_key' => 'country-definitions.TCA',
                'id' => '796',
                'currency' => [
                    'USD',
                ],
            ],
            [
                'title' => 'Tuvalu',
                'alpha2' => 'TV',
                'alpha3' => 'TUV',
                'translation_key' => 'country-definitions.TUV',
                'id' => '798',
                'currency' => [
                    'AUD',
                ],
            ],
            [
                'title' => 'Uganda',
                'alpha2' => 'UG',
                'alpha3' => 'UGA',
                'translation_key' => 'country-definitions.UGA',
                'id' => '800',
                'currency' => [
                    'UGX',
                ],
            ],
            [
                'title' => 'Ukraine',
                'alpha2' => 'UA',
                'alpha3' => 'UKR',
                'translation_key' => 'country-definitions.UKR',
                'id' => self::UKRAINE,
                'currency' => [
                    'UAH',
                ],
            ],
            [
                'title' => 'United Arab Emirates',
                'alpha2' => 'AE',
                'alpha3' => 'ARE',
                'translation_key' => 'country-definitions.ARE',
                'id' => '784',
                'currency' => [
                    'AED',
                ],
            ],
            [
                'title' => 'United Kingdom',
                'alpha2' => 'GB',
                'alpha3' => 'GBR',
                'translation_key' => 'country-definitions.GBR',
                'id' => '826',
                'currency' => [
                    'GBP',
                ],
            ],
            [
                'title' => 'United States of America',
                'alpha2' => 'US',
                'alpha3' => 'USA',
                'translation_key' => 'country-definitions.USA',
                'id' => self::UNITED_STATES_OF_AMERICA,
                'currency' => [
                    'USD',
                ],
            ],
            [
                'title' => 'United States Minor Outlying Islands',
                'alpha2' => 'UM',
                'alpha3' => 'UMI',
                'translation_key' => 'country-definitions.UMI',
                'id' => '581',
                'currency' => [
                    'USD',
                ],
            ],
            [
                'title' => 'Uruguay',
                'alpha2' => 'UY',
                'alpha3' => 'URY',
                'translation_key' => 'country-definitions.URY',
                'id' => '858',
                'currency' => [
                    'UYU',
                ],
            ],
            [
                'title' => 'Uzbekistan',
                'alpha2' => 'UZ',
                'alpha3' => 'UZB',
                'translation_key' => 'country-definitions.UZB',
                'id' => '860',
                'currency' => [
                    'UZS',
                ],
            ],
            [
                'title' => 'Vanuatu',
                'alpha2' => 'VU',
                'alpha3' => 'VUT',
                'translation_key' => 'country-definitions.VUT',
                'id' => '548',
                'currency' => [
                    'VUV',
                ],
            ],
            [
                'title' => 'Venezuela',
                'alpha2' => 'VE',
                'alpha3' => 'VEN',
                'translation_key' => 'country-definitions.VEN',
                'id' => '862',
                'currency' => [
                    'VEF',
                ],
            ],
            [
                'title' => 'Viet Nam',
                'alpha2' => 'VN',
                'alpha3' => 'VNM',
                'translation_key' => 'country-definitions.VNM',
                'id' => '704',
                'currency' => [
                    'VND',
                ],
            ],
            [
                'title' => 'British Virgin Islands',
                'alpha2' => 'VG',
                'alpha3' => 'VGB',
                'translation_key' => 'country-definitions.VGB',
                'id' => '092',
                'currency' => [
                    'USD',
                ],
            ],
            [
                'title' => 'U.S. Virgin Islands',
                'alpha2' => 'VI',
                'alpha3' => 'VIR',
                'translation_key' => 'country-definitions.VIR',
                'id' => '850',
                'currency' => [
                    'USD',
                ],
            ],
            [
                'title' => 'Wallis and Futuna',
                'alpha2' => 'WF',
                'alpha3' => 'WLF',
                'translation_key' => 'country-definitions.WLF',
                'id' => '876',
                'currency' => [
                    'XPF',
                ],
            ],
            [
                'title' => 'Western Sahara',
                'alpha2' => 'EH',
                'alpha3' => 'ESH',
                'translation_key' => 'country-definitions.ESH',
                'id' => '732',
                'currency' => [
                    'MAD',
                ],
            ],
            [
                'title' => 'Yemen',
                'alpha2' => 'YE',
                'alpha3' => 'YEM',
                'translation_key' => 'country-definitions.YEM',
                'id' => '887',
                'currency' => [
                    'YER',
                ],
            ],
            [
                'title' => 'Zambia',
                'alpha2' => 'ZM',
                'alpha3' => 'ZMB',
                'translation_key' => 'country-definitions.ZMB',
                'id' => '894',
                'currency' => [
                    'ZMW',
                ],
            ],
            [
                'title' => 'Zimbabwe',
                'alpha2' => 'ZW',
                'alpha3' => 'ZWE',
                'translation_key' => 'country-definitions.ZWE',
                'id' => '716',
                'currency' => [
                    'BWP',
                    'EUR',
                    'GBP',
                    'USD',
                    'ZAR',
                ],
            ],
        ];
    }

}