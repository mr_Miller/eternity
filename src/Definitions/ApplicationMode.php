<?php

namespace Eternity\Definitions;

/**
 * Class ApplicationMode
 * @package Eternity\Definitions
 */
class ApplicationMode
{
    public const MODE_WORKER = 'worker';
    public const MODE_API = 'api';
    public const MODE_CRON = 'cron';
    public const MODE_COMMAND = 'command';
}