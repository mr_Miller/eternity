<?php

namespace Eternity\Definitions\Files\Calendar;

use Eternity\Components\Definition\AbstractBaseDefinition;

/**
 * Class FilePathDefinition
 * @package Eternity\Definitions\Files\Calendar
 */
class FilePathDefinition extends AbstractBaseDefinition
{
    public const NOTE_DOCUMENT = 'note-docs';

    /**
     * Method builds the whole definition structure
     *
     * Every array item must have next structure:
     *  - id
     *  - title
     *  - translation
     *
     * For example:
     *  [
     *      [
     *          'id' => 5,
     *          'title' => 'String description',
     *          'translation_key' => 'Key for user readable translation',
     *      ],
     *      ...
     *  ]
     *
     * @return array
     */
    public static function getDefinitionsList(): array
    {
        return [
            [
                'id' => self::NOTE_DOCUMENT,
                'title' => 'Relative path to notes document'
            ]
        ];
    }
}