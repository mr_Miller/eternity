<?php

namespace Eternity\Definitions\Files;

use Eternity\Components\Definition\AbstractBaseDefinition;

/**
 * Class FileTypeDefinition
 * @package Eternity\Definitions\Files
 */
class FileTypeDefinition extends AbstractBaseDefinition
{
    public const IMAGE = 1;
    public const DOCUMENT = 10;

    /**
     * Method builds the whole definition structure
     *
     * Every array item must have next structure:
     *  - id
     *  - title
     *  - translation
     *
     * For example:
     *  [
     *      [
     *          'id' => 5,
     *          'title' => 'String description',
     *          'translation_key' => 'Key for user readable translation',
     *      ],
     *      ...
     *  ]
     *
     * @return array
     */
    public static function getDefinitionsList(): array
    {
        return [
            [
                'id'    => self::IMAGE,
                'title' => 'Image',
            ],
            [
                'id'    => self::DOCUMENT,
                'title' => 'Document',
            ]
        ];
    }
}