<?php

namespace Eternity\Definitions\Files\Marketplace;

use Eternity\Components\Definition\AbstractBaseDefinition;

/**
 * Definition with marketplace file relative paths in storage
 *
 * Class FilePathDefinition
 * @package Eternity\Definitions\Files\Marketplace
 */
class FilePathDefinition extends AbstractBaseDefinition
{
    public const PRODUCT_CATEGORY_PHOTOS = 'product-category-photos';
    public const PRODUCT_PHOTOS = 'product-photos';

    /**
     * @return array
     */
    public static function getDefinitionsList(): array
    {
        return [
            [
                'id'    => self::PRODUCT_PHOTOS,
                'title' => 'Product photos',
            ],
            [
                'id'    => self::PRODUCT_CATEGORY_PHOTOS,
                'title' => 'Product Category photos',
            ]
        ];
    }

}