<?php

namespace Eternity\Definitions\Files\Animal;

use Eternity\Components\Definition\AbstractBaseDefinition;

/**
 * Class FilePathDefinitions
 * @package Eternity\Definitions\Files\Animal
 */
class FilePathDefinitions extends AbstractBaseDefinition
{
    public const PET_DOCUMENT = 'animal-docs';

    /**
     * Method builds the whole definition structure
     *
     * Every array item must have next structure:
     *  - id
     *  - title
     *  - translation
     *
     * For example:
     *  [
     *      [
     *          'id' => 5,
     *          'title' => 'String description',
     *          'translation_key' => 'Key for user readable translation',
     *      ],
     *      ...
     *  ]
     *
     * @return array
     */
    public static function getDefinitionsList(): array
    {
        return [
            [
                'id' => self::PET_DOCUMENT,
                'title' => 'Relative path to pet document'
            ]
        ];
    }

}