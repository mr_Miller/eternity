<?php

namespace Eternity\Definitions\Files\Animal;

use Eternity\Components\Definition\AbstractBaseDefinition;

/**
 * Class ImagePathDefinition
 * @package Eternity\Definitions\Files\Animal
 */
class ImagePathDefinition extends AbstractBaseDefinition
{
    /**
     * Animal photos path
     */
    public const ANIMAL_PHOTO = 'animal-photos';

    /**
     * User avatar photos path
     */
    public const USER_AVATAR_PHOTO = 'user-photos/avatar';

    /**
     * B2B user logotype photos path
     */
    public const B2B_LOGOTYPE_PHOTO = 'b2b-logotypes';

    /**
     * User profile default covers path
     */
    public const USER_PROFILE_COVER_DEFAULT = 'user-profile-covers/default';

    /**
     * User profile custom covers path
     */
    public const USER_PROFILE_COVER_CUSTOM = 'user-profile-covers/custom';

    /**
     * Method builds the whole definition structure
     *
     * Every array item must have next structure:
     *  - id
     *  - title
     *  - translation
     *
     * For example:
     *  [
     *      [
     *          'id' => 5,
     *          'title' => 'String description',
     *          'translation_key' => 'Key for user readable translation',
     *      ],
     *      ...
     *  ]
     *
     * @return array
     */
    public static function getDefinitionsList(): array
    {
        return [
            [
                'id'    => self::ANIMAL_PHOTO,
                'title' => 'Animal photo path',
            ],
            [
                'id'    => self::USER_AVATAR_PHOTO,
                'title' => 'User avatar photo',
            ],
            [
                'id'    => self::B2B_LOGOTYPE_PHOTO,
                'title' => 'B2B User logotype photo',
            ],
            [
                'id'    => self::USER_PROFILE_COVER_DEFAULT,
                'title' => 'User profile default cover',
            ],
            [
                'id'    => self::USER_PROFILE_COVER_CUSTOM,
                'title' => 'User profile custom cover',
            ],
        ];
    }
}