<?php

namespace Eternity\Definitions\Platform;

use Eternity\Components\Definition\AbstractBaseDefinition;

/**
 * Class PlatformDefinition
 * @package Eternity\Definitions\Platform
 */
class PlatformDefinition extends AbstractBaseDefinition
{
    public const IOS = 10;
    public const ANDROID = 20;

    /**
     * Method builds the whole definition structure
     *
     * Every array item must have next structure:
     *  - id
     *  - title
     *  - translation
     *
     * For example:
     *  [
     *      [
     *          'id' => 5,
     *          'title' => 'String description',
     *          'translation_key' => 'Key for user readable translation',
     *      ],
     *      ...
     *  ]
     *
     * @return array
     */
    public static function getDefinitionsList(): array
    {
        return [
            [
                'id'    => self::IOS,
                'title' => 'iOS',
            ],
            [
                'id'    => self::ANDROID,
                'title' => 'Android',
            ],
        ];
    }

}