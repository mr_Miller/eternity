<?php

namespace Eternity\Definitions;

/**
 * Class HeadersDefinition
 * @package Eternity\Definitions
 */
class HeadersDefinition
{
    /**
     * User ID
     */
    public const USER_ID = 'X-Eternity-User-Id';

    /**
     * Language Code
     */
    public const LANG_CODE = 'X-Eternity-Lang-Code';

    /**
     * Expand entities key
     */
    public const EXPAND = 'X-Eternity-Expand';

    /**
     * Number of items on single page
     */
    public const PAGE_SIZE = 'X-Pagination-Page-Size';

    /**
     * Current pagination page
     */
    public const PAGE_CURRENT = 'X-Pagination-Page-Current';

    /**
     * Order by
     */
    public const ORDER_BY = 'X-Order-By';

    /**
     * Order direction
     */
    public const ORDER_DIRECTION = 'X-Order-Direction';

    /**
     * Filter
     */
    public const FILTER = 'X-Filter';

    /**
     * Trace ID
     */
    public const TRACE_ID = 'X-Eternity-Trace-Id';

    /**
     * Authorization
     */
    public const AUTHORIZATION = 'Authorization';

    /**
     * RBAC
     */
    public const AUTHORIZED_FOR = 'X-Authorized-For';

    /**
     * User entity
     */
    public const USER = 'X-Eternity-User';

    /**
     * Client IP address
     */
    public const CLIENT_IP = 'X-Eternity-Client-Ip';

    /**
     * Application ID
     */
    public const APP_ID = 'X-Eternity-App-Id';

    /**
     * Application version
     */
    public const APP_VERSION = 'X-Eternity-App-Version';

    /**
     * Application authorization hash (encrypted)
     */
    public const APP_AUTH = 'X-Eternity-App-Auth';

    /**
     * Type operating system
     */
    public const OS_TYPE = 'X-Eternity-Os-Type';

    /**
     * Version operating system
     */
    public const OS_VERSION = 'X-Eternity-Os-Version';

    /**
     * Device name
     */
    public const DEVICE_NAME = 'X-Eternity-Device-name';

    /**
     * AWS Request ID
     */
    public const AWS_REQUEST_ID = 'X-Aws-Request-Id';
}