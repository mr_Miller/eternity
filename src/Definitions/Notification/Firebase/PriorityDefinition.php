<?php

namespace Eternity\Definitions\Notification\Firebase;

use Eternity\Components\Definition\AbstractBaseDefinition;

/**
 * Class PriorityDefinition
 * @package Eternity\Definitions\Notification\Firebase
 */
class PriorityDefinition extends AbstractBaseDefinition
{
    public const NORMAL = 'normal';
    public const HIGH = 'high';

    /**
     * Method builds the whole definition structure
     *
     * Every array item must have next structure:
     *  - id
     *  - title
     *  - translation
     *
     * For example:
     *  [
     *      [
     *          'id' => 5,
     *          'title' => 'String description',
     *          'translation_key' => 'Key for user readable translation',
     *      ],
     *      ...
     *  ]
     *
     * @return array
     */
    public static function getDefinitionsList(): array
    {
        return [
            [
                'id'    => self::NORMAL,
                'title' => 'Normal'
            ],
            [
                'id'    => self::HIGH,
                'title' => 'High'
            ],
        ];
    }
}