<?php

namespace Eternity\Definitions\Notification\Firebase\Apple;

/**
 * Class PriorityDefinition
 * @package Eternity\Definitions\Notification\Firebase\Apple
 */
class PriorityDefinition
{
    public const NORMAL = '5';
    public const HIGH = '10';
}