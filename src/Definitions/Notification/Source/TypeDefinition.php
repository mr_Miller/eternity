<?php

namespace Eternity\Definitions\Notification\Source;

/**
 * Class TypeDefinition
 * @package Eternity\Definitions\Notification\Source
 */
class TypeDefinition
{
    public const REFERRAL_PROMO_CODE = 'referral_promo_code';
    public const PET_FOUND = 'pet_found';
    public const MP_CASHBACK = 'marketplace_cashback';
}