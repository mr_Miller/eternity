<?php

namespace Eternity\Definitions\Language;

use Eternity\Components\Definition\AbstractBaseDefinition;

/**
 * Global language definition
 *
 * Should be used across the all microservices (except old Animal ID)
 *
 * Class LanguageDefinition
 * @package Eternity\Definitions\Language
 */
class LanguageDefinition extends AbstractBaseDefinition
{
    public const ENG = 'en';
    public const UKR = 'uk';
    public const RUS = 'ru';

    /**
     * Method builds the whole definition structure
     *
     * Every array item must have next structure:
     *  - id
     *  - title
     *  - translation
     *
     * For example:
     *  [
     *      [
     *          'id' => 5,
     *          'title' => 'String description',
     *          'translation_key' => 'Key for user readable translation',
     *      ],
     *      ...
     *  ]
     *
     * @return array
     */
    public static function getDefinitionsList(): array
    {
        return [
            [
                'id'    => self::ENG,
                'title' => 'English',
            ],
            [
                'id'    => self::UKR,
                'title' => 'Українська',
            ],
            [
                'id'    => self::RUS,
                'title' => 'Русский',
            ]
        ];
    }

}