<?php

namespace Eternity\Definitions\Rbac;

use Eternity\Components\Definition\AbstractBaseDefinition;

/**
 * Class RoleDefinition
 * @package Eternity\Definitions\Rbac
 */
class RoleDefinition extends AbstractBaseDefinition
{
    public const SUPER_ADMIN = 'super_admin';
    public const VENDOR = 'vendor';
    public const PET_OWNER = 'pet_owner';
    public const VETERINARIAN = 'veterinarian';
    public const CYNOLOGIST = 'cynologist';
    public const SHELTER_WORKER = 'shelter_worker';
    public const BREEDER = 'breeder';

    /**
     * Method builds the whole definition structure
     *
     * Every array item must have next structure:
     *  - id
     *  - title
     *  - translation
     *
     * For example:
     *  [
     *      [
     *          'id' => 5,
     *          'title' => 'String description',
     *          'translation_key' => 'Key for user readable translation',
     *      ],
     *      ...
     *  ]
     *
     * @return array
     */
    public static function getDefinitionsList(): array
    {
        return [
            [
                'id'    => self::SUPER_ADMIN,
                'title' => 'Super Admin',
            ],
            [
                'id'    => self::VENDOR,
                'title' => 'Vendor',
            ],
            [
                'id'    => self::PET_OWNER,
                'title' => 'Pet owner',
            ],
            [
                'id'    => self::VETERINARIAN,
                'title' => 'Veterinarian',
            ],
            [
                'id'    => self::CYNOLOGIST,
                'title' => 'Cynologist',
            ],
            [
                'id'    => self::SHELTER_WORKER,
                'title' => 'Shelter worker',
            ],
            [
                'id'    => self::BREEDER,
                'title' => 'Breeder',
            ],
        ];
    }
}