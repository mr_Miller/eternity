<?php

namespace Eternity\Definitions\Rbac\Abilities;

use Eternity\Definitions\Rbac\Abilities;

/**
 * Pet microservice abilities
 *
 * @package Eternity\Definitions\Rbac\Abilities
 */
abstract class Pet extends Abilities
{
    /*
     * Put prefix "PE" for each ability
     */

    /*
     * USER section
     * --------------------------------------------------
     */

    /* Weight */
    public const MANAGE_WEIGHTS = 'pe-manage-weights';

    /* Wanted */
    public const MANAGE_WANTED = 'pe-manage-wanted';

    /* Scan history */
    public const MANAGE_SCAN_HISTORY = 'pe-manage-scan-history';

    /**
     * ADMIN section
     * ----------------------------------------------------
     */
    // will be
}