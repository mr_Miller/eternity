<?php

namespace Eternity\Definitions\Rbac\Abilities;

use Eternity\Definitions\Rbac\Abilities;

/**
 * Subscription MS permissions
 *
 * Class Subscription
 * @package Eternity\Definitions\Rbac\Abilities
 */
abstract class Subscription extends Abilities
{
    /*
     * Put prefix "SC" for each ability
     */

    /*
     * USER section
     * --------------------------------------------------
     */

    /* Apple - Subscriptions */
    public const VERIFY_RECEIPT = 'sc-verify-receipt';
    public const VIEW_PRODUCTS = 'sc-view-apple-products';

    /* Subscriptions */
    public const VIEW_SUBSCRIPTIONS = 'sc-view-subscriptions';

    /* Definitions */
    public const VIEW_DEFINITIONS = 'sc-view-definitions';

}