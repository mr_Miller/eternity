<?php

namespace Eternity\Definitions\Rbac\Abilities;

use Eternity\Definitions\Rbac\Abilities;

/**
 * Marketplace microservice abilities
 *
 * Class Marketplace
 * @package Eternity\Definitions\Rbac\Abilities
 */
abstract class Marketplace extends Abilities
{
    /*
     * Put prefix "MP" for each ability
     */

    /*
     * USER section
     * --------------------------------------------------
     */

    /* Product */
    public const MANAGE_PRODUCTS = 'mp-manage-products';
    public const VIEW_SINGLE_PRODUCT = 'mp-view-single-product';
    public const VIEW_PRODUCTS = 'mp-view-products';

    /* Category */
    public const MANAGE_CATEGORIES = 'mp-manage-categories';
    public const VIEW_SINGLE_CATEGORY = 'mp-view-single-category';
    public const VIEW_CATEGORIES = 'mp-view-categories';

    /* Definitions */
    public const VIEW_DEFINITIONS = 'mp-view-definitions';

    /* Account */
    public const VIEW_USER_ACCOUNT = 'mp-view-user-account';
    public const VIEW_GROUP_ACCOUNT = 'mp-view-group-account';

    /* Transactions */
    public const VIEW_USER_TRANSACTIONS = 'mp-view-user-transactions';
    public const VIEW_GROUP_TRANSACTIONS = 'mp-view-group-transactions';
    public const VIEW_USER_SINGLE_TRANSACTION = 'mp-view-user-single-transaction';

    /* Qr-codes */
    public const VIEW_QR_CODES = 'mp-view-qr-codes';
    public const MANAGE_QR_CODES = 'mp-view-qr-codes';
    public const SCAN_QR_CODES = 'mp-scan-qr-codes';

    /* Campaigns */
    public const MANAGE_CAMPAIGNS = 'mp-manage-campaigns';

    /* Variations */
    public const MANAGE_VARIATIONS = 'mp-manage-variations';

    /* Shelters */
    public const VIEW_SHELTERS = 'mp-view-shelters';

    /* Withdrawal */
    public const MANAGE_WITHDRAWAL = 'mp-manage-withdrawal';

    /* Group */
    public const MANAGE_GROUP = 'mp-manage-group';
    public const VIEW_GROUP = 'mp-view-group';

    /* Analytics */
    public const VIEW_QR_CODE_ANALYTICS = 'mp-view-qr-code-analytics';

    /**
     * ADMIN section
     * ----------------------------------------------------
     */
    /* Account */
    public const MANAGE_ACCOUNT = 'mp-manage-accounts';
    public const MANAGE_DEPOSIT = 'mp-deposit-group';

    /* Transaction */
    public const MANAGE_TRANSACTIONS = 'mp-manage-transactions';

    /* Transaction request */
    public const MANAGE_REQUESTS = 'mp-manage-requests';
    public const CANCEL_REQUESTS = 'mp-cancel-requests';

    /* Transaction history */
    public const VIEW_REQUEST_HISTORY = 'mp-view-request-history';
}