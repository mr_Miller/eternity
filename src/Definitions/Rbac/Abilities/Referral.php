<?php

namespace Eternity\Definitions\Rbac\Abilities;

use Eternity\Definitions\Rbac\Abilities;

/**
 * Referral microservice abilities
 *
 * Class Referral
 * @package Eternity\Definitions\Rbac\Abilities
 */
abstract class Referral extends Abilities
{
    /*
     * Put prefix "RF" for each ability
     */

    /*
     * USER section
     * --------------------------------------------------
     */

    /* Referent */
    public const VIEW_REFERENT = 'rf-view-referent';
}