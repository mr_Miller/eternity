<?php

namespace Eternity\Definitions\Rbac\Abilities;

use Eternity\Definitions\Rbac\Abilities;

/**
 * Gateway microservice abilities
 *
 * Class Gateway
 * @package Eternity\Definitions\Rbac\Abilities
 */
abstract class Gateway extends Abilities
{
    /*
     * Put prefix "GW" for each ability
     */

    /*
     * USER section
     * --------------------------------------------------
     */
    public const VIEW_DEFINITIONS = 'gw-view-definitions';
    public const MANAGE_USER = 'gw-manage-user';

    /**
     * ADMIN section
     * ----------------------------------------------------
     */
    // will be
}