<?php

namespace Eternity\Definitions\Rbac\Abilities;

use Eternity\Definitions\Rbac\Abilities;

/**
 * Calendar microservice abilities
 * Class Calendar
 * @package Eternity\Definitions\Rbac\Abilities
 */
abstract class Calendar extends Abilities
{
    /*
     * Put prefix "CA" for each ability
     */

    /*
     * USER section
     * --------------------------------------------------
     */
    public const MANAGE_EVENTS = 'ca-manage-events';

    /**
     * ADMIN section
     * ----------------------------------------------------
     */
    // will be
}