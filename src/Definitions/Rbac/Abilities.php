<?php

namespace Eternity\Definitions\Rbac;

/**
 * Base Class that describes abilities
 *
 * Class BaseAbilities
 * @package Eternity\Definitions\Rbac\Abilities
 */
abstract class Abilities
{
    /*
     * Base abilities and methods are here
     */
}