<?php

namespace Eternity\User;

use Eternity\Contracts\Arrayable;

/**
 * User object
 *
 * Class User
 * @package App\Infrastructure\Services\User\Entities
 */
class User implements \Serializable, Arrayable
{
    /**
     * @var int
     */
    protected $id;

    /**
     * @var array Abilities
     */
    protected $abilities;

    /**
     * @var array Roles
     */
    protected $roles;

    /**
     * @var \Eternity\User\Subscription
     */
    protected $subscription;

    /**
     * @var int|null Group ID
     */
    protected $group_id;

    /**
     * String representation of object
     * @link https://php.net/manual/en/serializable.serialize.php
     * @return string the string representation of the object or null
     */
    public function serialize(): string
    {
        return json_encode($this->toArray());
    }

    /**
     * Constructs the object
     * @link https://php.net/manual/en/serializable.unserialize.php
     * @param string $serialized <p>
     * The string representation of the object.
     * </p>
     * @return void
     */
    public function unserialize($serialized): void
    {
        $this->fromArray(json_decode($serialized, true));
    }

    /**
     * Creates event from array
     * @param array $data
     */
    public function fromArray(array $data): void
    {
        $this->abilities = $data['abilities'];
        $this->roles = $data['roles'];
        $this->id = $data['id'];
        // Build subscription
        $this->subscription = new Subscription();
        $this->subscription->fromArray($data['subscription']);
        // Build group
        $this->group_id = $data['group_id'] ?? null;
    }

    /**
     * Transforms object into array
     *
     * @return array
     */
    public function toArray(): array
    {
        return [
            'id'           => $this->id,
            'abilities'    => $this->abilities,
            'roles'        => $this->roles,
            'subscription' => $this->subscription->toArray(),
            'group_id'     => $this->group_id,
        ];
    }

    /**
     * @return int
     */
    public function id(): int
    {
        return $this->id;
    }

    /**
     * @return array
     */
    public function abilities(): array
    {
        return $this->abilities;
    }

    /**
     * @return array
     */
    public function roles(): array
    {
        return $this->roles;
    }

    /**
     * @return \Eternity\User\Subscription
     */
    public function subscription(): Subscription
    {
        return $this->subscription;
    }

    /**
     * @return int|null
     */
    public function groupId(): ?int
    {
        return $this->group_id;
    }
}
