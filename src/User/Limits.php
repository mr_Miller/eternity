<?php

namespace Eternity\User;

use Eternity\Contracts\Arrayable;

/**
 * Class Limits
 * @package App\Infrastructure\Services\User\Entities
 */
class Limits implements Arrayable
{
    /**
     * @var array
     */
    protected $values;

    /**
     * Limits constructor.
     * @param array $values
     */
    public function __construct(array $values)
    {
        $this->values = $values;
    }

    /**
     * Returns value by key
     * @param string $key
     * @return int
     */
    public function get(string $key): int
    {
        if (!isset($this->values[$key])) {
            throw new \InvalidArgumentException("Limit '$key' is not set");
        }

        return $this->values[$key];
    }

    /**
     * Returns Tag scan sms notification per month count
     * @return mixed
     */
    public function getTagScanSmsNotificationPerMonthCount()
    {
        return $this->get('tag_scan_sms_notification_count_month');
    }

    /**
     * Returns Tag guarantee service per year count
     * @return mixed
     */
    public function getTagGuaranteeServicePerYearCount()
    {
        return $this->get('tag_guarantee_service_count_year');
    }

    /**
     * Returns Pet document max count
     * @return mixed
     */
    public function getPetDocumentMaxCount()
    {
        return $this->get('pet_document_max_count');
    }

    /**
     * Returns Additional contact max count
     * @return mixed
     */
    public function getAdditionalContactMaxCount()
    {
        return $this->get('additional_contact_max_count');
    }

    /**
     * Returns max weight list period in month
     * @return mixed
     * @deprecated todo remove
     */
    public function getMaxWeightListPeriodInMonth()
    {
        return $this->get('max_weight_list_period_in_month');
    }

    /**
     * Returns the maximum number to create records
     * @return mixed
     */
    public function getMaxCreatedWeightRecords()
    {
        return $this->get('max_created_weight_records');
    }

    /**
     * Is limit exceeded
     * @param string $key
     * @param $value
     * @return bool
     */
    public function isExceeded(string $key, $value): bool
    {
        return $value >= $this->get($key);
    }

    /**
     * Is limit not exceeded
     *
     * @param string $key
     * @param $value
     * @return bool
     */
    public function isNotExceeded(string $key, $value): bool
    {
        return !$this->isExceeded($key, $value);
    }

    /**
     * @return array
     */
    public function toArray(): array
    {
        return $this->values;
    }
}
