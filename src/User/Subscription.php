<?php

namespace Eternity\User;

use Eternity\Contracts\Arrayable;

/**
 * Class Subscription
 * @package App\Infrastructure\Services\User\Entities
 */
class Subscription implements Arrayable
{
    /**
     * @var int|null
     */
    protected $id;

    /**
     * @var int
     */
    protected $type;

    /**
     * @var int|null
     */
    protected $provider_type;

    /**
     * @var int
     */
    protected $status;

    /**
     * @var string|null
     */
    protected $entitlement_code;

    /**
     * @var bool
     */
    protected $is_trial_period_was_used;

    /**
     * @var string|null
     */
    protected $expires_at;

    /**
     * @var string|null
     */
    protected $created_at;

    /**
     * @var string|null
     */
    protected $updated_at;

    /**
     * @var \Eternity\User\Limits
     */
    protected $limits;

    /**
     * @var string|null
     */
    protected $product_id;

    /**
     * @param array $data
     * @return void
     */
    public function fromArray(array $data): void
    {
        // Required parameters
        $this->type = $data['type'];
        $this->status = $data['status'];
        $this->limits = new Limits($data['limits']);
        // Optional parameters
        $this->id = $data['id'] ?? null;
        $this->provider_type = $data['provider_type'] ?? null;
        $this->expires_at = $data['expires_at'] ?? null;
        $this->created_at = $data['created_at'] ?? null;
        $this->updated_at = $data['updated_at'] ?? null;
        $this->product_id = $data['product_id'] ?? null;
        $this->entitlement_code = $data['entitlement_code'] ?? null;
        $this->is_trial_period_was_used = $data['is_trial_period_was_used'] ?? false;
    }

    public function toArray(): array
    {
        return [
            'id'                       => $this->id,
            'type'                     => $this->type,
            'provider_type'            => $this->provider_type,
            'status'                   => $this->status,
            'entitlement_code'         => $this->entitlement_code,
            'is_trial_period_was_used' => $this->is_trial_period_was_used,
            'product_id'               => $this->product_id,
            'expires_at'               => $this->expires_at,
            'created_at'               => $this->created_at,
            'updated_at'               => $this->updated_at,
            'limits'                   => $this->limits->toArray(),
        ];
    }

    /**
     * @return int|null
     */
    public function id(): ?int
    {
        return $this->id;
    }

    /**
     * @return int
     */
    public function type(): int
    {
        return $this->type;
    }

    /**
     * @return int|null
     */
    public function providerType(): ?int
    {
        return $this->provider_type;
    }

    /**
     * @return int
     */
    public function status(): int
    {
        return $this->status;
    }

    /**
     * @return string|null
     */
    public function entitlementCode(): ?string
    {
        return $this->entitlement_code;
    }

    /**
     * @return bool
     */
    public function isTrialPeriodWasUsed(): bool
    {
        return $this->is_trial_period_was_used;
    }

    /**
     * @return string|null
     */
    public function expiresAt(): ?string
    {
        return $this->expires_at;
    }

    /**
     * @return string|null
     */
    public function createdAt(): ?string
    {
        return $this->created_at;
    }

    /**
     * @return string|null
     */
    public function updatedAt(): ?string
    {
        return $this->updated_at;
    }

    /**
     * @return \Eternity\User\Limits
     */
    public function limits(): Limits
    {
        return $this->limits;
    }
}
