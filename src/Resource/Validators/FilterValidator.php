<?php

namespace Eternity\Resource\Validators;

use Eternity\Resource\Exceptions\FilterException;
use Eternity\Resource\Objects\Filter\FilterCollection;

/**
 * Filter validator
 *
 * Validates:
 *  - allowed filters;
 *  - abandons duplications;
 *
 * Class FilterValidator
 * @package App\Infrastructure\Services\Subscription
 */
abstract class FilterValidator
{
    /**
     * Default configuration
     */
    public const DEFAULT_CONFIG = [];

    /**
     * @var \Eternity\Resource\Objects\Filter\FilterCollection
     */
    private $filterCollection;

    /**
     * @var array
     */
    private $config;

    /**
     * FilterValidator constructor.
     * @param \Eternity\Resource\Objects\Filter\FilterCollection $filterCollection
     * @param array $config
     */
    public function __construct(FilterCollection $filterCollection, $config = [])
    {
        $this->filterCollection = $filterCollection;

        // Config init
        $this->config = self::DEFAULT_CONFIG;
        // Configuration can be overridden
        $this->config = array_merge_recursive($this->config, $config);
    }

    /**
     * Returns list of allowed filters
     * Format:
     * field name => list of allowed filter types
     *
     * Example:
     * [
     *   'type' => ['='],
     *   'created_at' => ['>', '<']
     * ]
     *
     * @return array
     */
    abstract protected function allowedFilters(): array;

    /**
     * Returns config property
     * @param string $property
     * @param string|null $default
     * @return mixed
     */
    public function configProperty(string $property, string $default = null)
    {
        if (isset($this->config[$property])) {
            return $this->config[$property];
        }

        return $default;
    }

    /**
     * Validate filter
     */
    public function validate(): void
    {
        $this->validateAllowed();
    }

    /**
     * Validates for allowed filters
     * @throws \Eternity\Resource\Exceptions\FilterException
     */
    public function validateAllowed(): void
    {
        foreach ($this->filterCollection as $filter) {
            // Check if filter key is in list and filter type is allowed for this field
            /** @var \Eternity\Resource\Objects\Filter\Filter $filter */
            if (!isset($this->allowedFilters()[$filter->key()])
                || !in_array($filter->type(), $this->allowedFilters()[$filter->key()])
            ) {
                throw new FilterException(
                    'Filter validation',
                    "Filter type: '{$filter->type()}' for field: '{$filter->key()}' is not allowed"
                );
            }
        }
    }
}
