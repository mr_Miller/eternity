<?php

namespace Eternity\Resource\Metadata;

use Eternity\Contracts\Arrayable;
use Eternity\Resource\Objects\Filter\FilterCollection;
use Eternity\Resource\Objects\Order;
use Eternity\Resource\Objects\Page;
use Eternity\Resource\Objects\Pagination;

/**
 * Pagination metadata old
 *
 * todo Delete this class when this will be not used
 *
 * Class Metadata
 * @deprecated This class is deprecated because of new Metadata
 * @link \Eternity\Metadata\Metadata
 * @package Eternity\Components\Resource\Metadata
 */
class Metadata implements Arrayable
{
    /**
     * @var \Eternity\Resource\Objects\Filter\FilterCollection
     */
    private $filter;

    /**
     * @var \Eternity\Resource\Objects\Order
     */
    private $order;

    /**
     * @var \Eternity\Resource\Objects\Page
     */
    private $page;

    /**
     * @var \Eternity\Resource\Objects\Pagination
     */
    private $pagination;

    /**
     * Metadata constructor.
     * @param \Eternity\Resource\Objects\Filter\FilterCollection $filter
     * @param \Eternity\Resource\Objects\Order $order
     * @param \Eternity\Resource\Objects\Page $page
     */
    public function __construct(FilterCollection $filter, Order $order, Page $page)
    {
        $this->filter = $filter;
        $this->order = $order;
        $this->page = $page;
    }

    /**
     * @return \Eternity\Resource\Objects\Filter\FilterCollection
     */
    public function getFilter(): FilterCollection
    {
        return $this->filter;
    }

    /**
     * @return \Eternity\Resource\Objects\Order
     */
    public function getOrder(): Order
    {
        return $this->order;
    }

    /**
     * @return \Eternity\Resource\Objects\Page
     */
    public function getPage(): Page
    {
        return $this->page;
    }

    /**
     * @param \Eternity\Resource\Objects\Pagination $pagination
     */
    public function addPagination(Pagination $pagination): void
    {
        $this->pagination = $pagination;
    }

    /**
     * @return \Eternity\Resource\Objects\Pagination
     */
    public function getPagination(): ?Pagination
    {
        return $this->pagination;
    }

    /**
     * @return bool
     */
    public function hasPagination(): bool
    {
        return $this->pagination !== null;
    }

    /**
     * Transforms object into array
     *
     * @return array
     */
    public function toArray(): array
    {
        return [
            'order'      => $this->order->toArray(),
            'filter'     => $this->filter->toArray(),
            'page'       => $this->page->toArray(),
            'pagination' => $this->hasPagination() ? $this->pagination->toArray() : null,
        ];
    }
}