<?php

namespace Eternity\Resource\Exceptions;

use Eternity\Exceptions\EternityException;

/**
 * Class MetadataException
 * @package Eternity\Resource\Exceptions
 */
class MetadataException extends EternityException
{
    /**
     * MetadataException constructor.
     * @param string|null $title
     * @param string|null $detail
     * @param \Throwable|null $previous
     */
    public function __construct(string $title = null, string $detail = null, \Throwable $previous = null)
    {
        parent::__construct($title, $detail, $previous);
        $this->type = 'MetadataException';
    }
}