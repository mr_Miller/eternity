<?php

namespace Eternity\Resource\Exceptions;

use Eternity\Exceptions\UserException;

/**
 * Class FilterException
 * @package App\Infrastructure\Services\Subscription\Exceptions
 */
class FilterException extends UserException
{
    /**
     * FilterException constructor.
     * @param string|null $title
     * @param string|null $detail
     * @param \Throwable|null $previous
     */
    public function __construct(string $title = null, string $detail = null, \Throwable $previous = null)
    {
        parent::__construct($title, $detail, $previous);
        $this->type = 'FilterException';
    }
}
