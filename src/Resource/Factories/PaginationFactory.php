<?php

namespace Eternity\Resource\Factories;

use Eternity\Resource\Objects\Page;
use Eternity\Resource\Objects\Pagination;

/**
 * Class PaginationFactory
 * @package Eternity\Resource\Factories
 */
class PaginationFactory
{
    /**
     * @var int
     */
    private $totalItems;

    /**
     * @var int
     */
    private $totalPages;

    /**
     * @var \Eternity\Resource\Objects\Page
     */
    private $page;

    /**
     * PaginationFactory constructor.
     * @param \Eternity\Resource\Objects\Page $page
     * @param int $totalItems
     * @param int $totalPages
     */
    public function __construct(Page $page, int $totalItems, int $totalPages)
    {
        $this->totalItems = $totalItems;
        $this->totalPages = $totalPages;
        $this->page = $page;
    }

    /**
     * Creates Pagination
     * @return \Eternity\Resource\Objects\Pagination
     */
    public function create(): Pagination
    {
        return new Pagination(
            $this->totalPages,
            $this->totalItems,
            PagesFactory::createStatic($this->totalPages, $this->page)
        );
    }

    /**
     * Creates Pagination statically
     * @param \Eternity\Resource\Objects\Page $page
     * @param int $totalItems
     * @param int $totalPages
     * @return \Eternity\Resource\Objects\Pagination
     */
    public static function createStatic(Page $page, int $totalItems, int $totalPages): Pagination
    {
        return (new static($page, $totalItems, $totalPages))->create();
    }
}