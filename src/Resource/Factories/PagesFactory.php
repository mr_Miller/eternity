<?php

namespace Eternity\Resource\Factories;

use Eternity\Resource\Objects\Page;
use Eternity\Resource\Objects\Pages;

/**
 * Pages factory
 *
 * Calculates first, last, prev, next pages
 *
 * Class PaginationLinksFactory
 * @package Eternity\Resource\Factories
 */
class PagesFactory
{
    /**
     * @var \Eternity\Resource\Objects\Page
     */
    private $page;

    /**
     * @var int
     */
    private $totalPages;

    /**
     * PagesFactory constructor.
     * @param int $totalPages
     * @param \Eternity\Resource\Objects\Page $page
     */
    public function __construct(int $totalPages, Page $page)
    {
        $this->page = $page;
        $this->totalPages = $totalPages;
    }

    /**
     * Create
     * @return \Eternity\Resource\Objects\Pages
     */
    public function create(): Pages
    {
        return new Pages(
            $this->calcFirst(),
            $this->calcLast(),
            $this->calcPrev(),
            $this->calcNext()
        );
    }

    /**
     * Static create method
     * @param int $totalPages
     * @param \Eternity\Resource\Objects\Page $page
     * @return \Eternity\Resource\Objects\Pages
     */
    public static function createStatic(int $totalPages, Page $page): Pages
    {
        return (new static($totalPages, $page))->create();
    }

    /**
     * Return first-based current page number
     *
     * @return int
     */
    protected function getPage(): int
    {
        return $this->page->getCurrent();
    }

    /**
     * Calculate first page number
     * @return int
     */
    protected function calcFirst(): int
    {
        return 1;
    }

    /**
     * Calculate last page number
     * @return int
     */
    protected function calcLast(): int
    {
        if ($this->totalPages > 0) {
            return $this->totalPages;
        }

        return 1;
    }

    /**
     * Calculate next page number
     * @return int|null
     */
    protected function calcNext(): ?int
    {
        if ($this->getPage() < $this->totalPages) {
            return $this->getPage() + 1;
        }

        return null;
    }

    /**
     * Calculate prev page number
     * @return int|null
     */
    protected function calcPrev(): ?int
    {
        if ($this->getPage() > 1) {
            return $this->getPage() - 1;
        }

        return null;
    }
}