<?php

namespace Eternity\Resource\Objects;

use Eternity\Contracts\Arrayable;

/**
 * Class PaginationLinks
 *
 * Pagination links metadata
 *
 * @package Eternity\Resource\Objects
 */
class Pages implements Arrayable
{
    /**
     * @var string First page number
     */
    private $first;

    /**
     * @var string Last page number
     */
    private $last;

    /**
     * @var string|null Prev page number
     */
    private $prev;

    /**
     * @var string|null Next page number
     */
    private $next;

    /**
     * PaginationLink constructor.
     * @param string $first
     * @param string $last
     * @param string|null $prev
     * @param string|null $next
     */
    public function __construct(string $first, string $last, ?string $prev = null, ?string $next = null)
    {
        $this->first = $first;
        $this->last = $last;
        $this->prev = $prev;
        $this->next = $next;
    }

    /**
     * @return string
     */
    public function getFirst(): string
    {
        return $this->first;
    }

    /**
     * @return string
     */
    public function getLast(): string
    {
        return $this->last;
    }

    /**
     * @return string|null
     */
    public function getPrev(): ?string
    {
        return $this->prev;
    }

    /**
     * @return string|null
     */
    public function getNext(): ?string
    {
        return $this->next;
    }

    /**
     * @inheritDoc
     */
    public function toArray(): array
    {
        return [
            'first' => $this->first,
            'last'  => $this->last,
            'prev'  => $this->prev,
            'next'  => $this->next,
        ];
    }
}