<?php

namespace Eternity\Resource\Objects;

use Eternity\Contracts\Arrayable;

/**
 * Class Order
 * @package Eternity\Resource\Objects
 */
class Order implements Arrayable
{
    /**
     * @var string Sorting direction
     */
    private $direction;

    /**
     * @var string Sorting by field name
     */
    private $by;

    /**
     * Order constructor.
     * @param string $direction
     * @param string $by
     */
    public function __construct(string $direction, string $by)
    {
        $this->direction = $direction;
        $this->by = $by;
    }

    /**
     * @return string
     */
    public function getDirection(): string
    {
        return $this->direction;
    }

    /**
     * @return string
     */
    public function getBy(): string
    {
        return $this->by;
    }

    /**
     * @inheritDoc
     */
    public function toArray(): array
    {
        return [
            'direction' => $this->direction,
            'by'        => $this->by,
        ];
    }
}