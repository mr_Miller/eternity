<?php

namespace Eternity\Resource\Objects;

use Eternity\Contracts\Arrayable;

/**
 * Class Pagination
 * @package Eternity\Resource\Objects
 */
class Pagination implements Arrayable
{
    /**
     * @var int Total pages
     */
    private $totalPages;

    /**
     * @var int Total items
     */
    private $totalItems;

    /**
     * @var \Eternity\Resource\Objects\Pages
     */
    private $pages;

    /**
     * Pagination constructor.
     * @param int $totalPages
     * @param int $totalItems
     * @param \Eternity\Resource\Objects\Pages $pages
     */
    public function __construct(int $totalPages, int $totalItems, Pages $pages)
    {
        $this->totalPages = $totalPages;
        $this->totalItems = $totalItems;
        $this->pages = $pages;
    }

    /**
     * @return int
     */
    public function getTotalPages(): int
    {
        return $this->totalPages;
    }

    /**
     * @return int
     */
    public function getTotalItems(): int
    {
        return $this->totalItems;
    }

    /**
     * @return \Eternity\Resource\Objects\Pages
     */
    public function getPages(): Pages
    {
        return $this->pages;
    }

    /**
     * Transforms object into array
     *
     * @return array
     */
    public function toArray(): array
    {
        return [
            'pages_total' => $this->totalPages,
            'items_total' => $this->totalItems,
            'pages'       => $this->pages->toArray()
        ];
    }
}
