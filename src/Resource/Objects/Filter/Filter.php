<?php

namespace Eternity\Resource\Objects\Filter;

use Eternity\Contracts\Arrayable;

/**
 * Class Filter
 * @package Eternity\Resource\Objects\Filter
 */
class Filter implements Arrayable
{
    /**
     * @var string Type.
     * Allowed values described here:
     * @link \Eternity\Resource\Objects\Filter\Type
     */
    private $type;

    /**
     * @var string Field name for filtering
     */
    private $key;

    /**
     * @var string|array Field value for comparison
     */
    private $value;

    /**
     * Filter constructor.
     * @param string $type
     * @param string $key
     * @param string|array $value
     */
    public function __construct(string $type, string $key, $value)
    {
        $this->type = $type;
        $this->key = $key;
        $this->value = $value;
    }

    /**
     * @return string
     */
    public function type(): string
    {
        return $this->type;
    }

    /**
     * @return string
     */
    public function key(): string
    {
        return $this->key;
    }

    /**
     * @return string|array
     */
    public function value()
    {
        return $this->value;
    }

    /**
     * Transforms object into array
     *
     * @return array
     */
    public function toArray(): array
    {
        return [
            'type'  => $this->type,
            'key'   => $this->key,
            'value' => $this->value,
        ];
    }

}