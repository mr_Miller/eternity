<?php

namespace Eternity\Resource\Objects\Filter;

/**
 * Class Type
 * @package Eternity\Resource\Objects\Filter
 */
class Type
{
    public const EQUALS = '=';
    public const GREATER = '>';
    public const LESS = '<';
    public const IN = 'IN';
    public const LIKE = 'LIKE';

    /**
     * List of available filter types
     */
    public const LIST = [
        self::EQUALS,
        self::GREATER,
        self::LESS,
        self::IN,
        self::LIKE,
    ];

    /**
     * Returns list of filter types
     * @return array
     */
    public static function list(): array
    {
        return self::LIST;
    }
}