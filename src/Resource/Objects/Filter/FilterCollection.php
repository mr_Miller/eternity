<?php

namespace Eternity\Resource\Objects\Filter;

use Eternity\Contracts\Arrayable;
use Eternity\Resource\Exceptions\MetadataException;
use Exception;
use IteratorAggregate;
use Traversable;

/**
 * Class FiltersCollection
 * @package Eternity\Resource\Objects\Filter
 */
class FilterCollection implements Arrayable, IteratorAggregate
{
    /**
     * @var Filter[]
     */
    protected $filters = [];

    /**
     * FilterCollection constructor.
     * @param array $filters
     * @throws \Eternity\Resource\Exceptions\MetadataException
     */
    public function __construct(array $filters = [])
    {
        $this->addFilters($filters);
    }

    /**
     * @return string
     */
    public function __toString(): string
    {
        return $this->toJson();
    }

    /**
     * Converts to json string
     * @return string
     */
    public function toJson(): string
    {
        return json_encode($this->toArray());
    }

    /**
     * Add filter
     * @param \Eternity\Resource\Objects\Filter\Filter $filter
     * @return \Eternity\Resource\Objects\Filter\FilterCollection
     */
    public function addFilter(Filter $filter): self
    {
        $this->filters[] = $filter;

        return $this;
    }

    /**
     * @param array $filters
     * @throws \Eternity\Resource\Exceptions\MetadataException
     */
    public function addFilters(array $filters): void
    {
        foreach ($filters as $filter) {
            if (!isset($filter['type'], $filter['key'], $filter['value'])) {
                throw new MetadataException('Metadata error', 'Filter incorrect structure');
            }
            $this->addFilter(new Filter($filter['type'], $filter['key'], $filter['value']));
        }
    }

    /**
     * Retrieve an external iterator
     * @link https://php.net/manual/en/iteratoraggregate.getiterator.php
     * @return Traversable|\Generator|\Eternity\Resource\Objects\Filter\Filter An instance of an object implementing <b>Iterator</b> or
     * <b>Traversable</b>
     * @throws Exception on failure.
     */
    public function getIterator()
    {
        foreach ($this->filters as $key => $filter) {
            yield $key => $filter;
        }
    }

    /**
     * Gets filter by filter key
     * @param string $key
     * @return \Eternity\Resource\Objects\Filter\Filter
     * @throws \Eternity\Resource\Exceptions\MetadataException
     */
    public function get(string $key): Filter
    {
        foreach ($this->filters as $filter) {
            if ($filter->key() === $key) {
                return $filter;
            }
        }

        throw new MetadataException('Filter error', "Filter by key: $key is not found");
    }

    /**
     * @return bool
     */
    public function isEmpty(): bool
    {
        return empty($this->filters);
    }

    /**
     * Transforms object into array
     *
     * @return array
     */
    public function toArray(): array
    {
        $result = [];
        foreach ($this->filters as $filter) {
            $result[] = $filter->toArray();
        }

        return $result;
    }
}