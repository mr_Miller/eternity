<?php

namespace Eternity\Resource\Objects;

use Eternity\Contracts\Arrayable;

/**
 * Class Page
 * @package Eternity\Resource\Objects
 */
class Page implements Arrayable
{
    /**
     * @var int Number of items per page
     */
    private $size;

    /**
     * @var int Current page number
     */
    private $current;

    /**
     * Page constructor.
     * @param int $size
     * @param int $current
     */
    public function __construct(int $size, int $current)
    {
        $this->size = $size;
        $this->current = $current;
    }

    /**
     * @return int
     */
    public function getSize(): int
    {
        return $this->size;
    }

    /**
     * @return int
     */
    public function getCurrent(): int
    {
        return $this->current;
    }

    /**
     * Transforms object into array
     *
     * @return array
     */
    public function toArray(): array
    {
        return [
            'size'    => $this->size,
            'current' => $this->current,
        ];
    }
}