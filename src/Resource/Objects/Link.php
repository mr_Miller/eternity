<?php

namespace Eternity\Resource\Objects;

use Eternity\Contracts\Arrayable;

/**
 * Class Link
 * @package Eternity\Resource\Objects
 */
class Link implements Arrayable
{
    /**
     * @var string
     */
    protected $rel;

    /**
     * @var string
     */
    protected $method;

    /**
     * @var string
     */
    protected $href;

    /**
     * Link constructor.
     *
     * @param $rel
     * @param $method
     * @param $href
     */
    public function __construct(string $rel, string $method, string $href)
    {
        $this->rel = $rel;
        $this->method = $method;
        $this->href = $href;
    }

    /**
     * Prepare Link object to array
     *
     * @return array
     */
    public function toArray(): array
    {
        return [
            'rel'    => $this->rel,
            'method' => $this->method,
            'href'   => $this->href,
        ];
    }
}
