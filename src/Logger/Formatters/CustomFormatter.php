<?php

namespace Eternity\Logger\Formatters;

use Monolog\Formatter\LineFormatter;

/**
 * Class CustomFormatter
 */
class CustomFormatter extends LineFormatter
{
    /**
     * @param array $record
     * @return array|mixed|string|string[]|null
     */
    public function format(array $record): string
    {
        return parent::format($record) . PHP_EOL;
    }
}