<?php
/**
 * TMS: Maintenance Manager
 */

namespace Eternity\Logger\Formatters;

use Monolog\Formatter\LineFormatter;

/**
 * Class JsonFormatter
 */
class JsonFormatter extends LineFormatter
{
    /**
     * @param array $record
     * @return array|mixed|string|string[]|null
     * @throws \Exception
     */
    public function format(array $record): string
    {
        $format = $this->format;

        $this->format = json_decode($this->format, true);

        if (json_last_error()) {
            throw new \Exception(
                'Cannot format log message. Not a valid json format specified in configuration'
            );
        }

        foreach ($record as $key => $value) {
            if (empty($value)) {
                unset($this->format[$key]);
            } elseif (is_string($value) && array_key_exists($key, $this->format)) {
                $record[$key] = addcslashes($value, '"\\');
            }
        }

        if (empty($record['stack-trace'])) {
            unset($this->format['stack-trace']);
        }

        $this->format = json_encode($this->format);

        $formatted = parent::format($record) . PHP_EOL;

        $this->format = $format;

        return $formatted;
    }
}