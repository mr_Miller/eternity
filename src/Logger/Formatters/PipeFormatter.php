<?php

namespace Eternity\Logger\Formatters;

use Monolog\Formatter\LineFormatter as BaseLineFormatter;

/**
 * Class LineFormatter
 */
class PipeFormatter extends BaseLineFormatter
{
    /**
     * @var bool
     */
    private $debug;

    /**
     * PipeFormatter constructor.
     * @param string|null $format
     * @param string|null $dateFormat
     * @param bool $debug
     * @param bool $allowInlineLineBreaks
     * @param bool $ignoreEmptyContextAndExtra
     */
    public function __construct(
        ?string $format = null,
        ?string $dateFormat = null,
        bool $debug = false,
        bool $allowInlineLineBreaks = false,
        bool $ignoreEmptyContextAndExtra = false
    ) {
        parent::__construct($format, $dateFormat, $allowInlineLineBreaks, $ignoreEmptyContextAndExtra);
        $this->debug = $debug;
    }

    /**
     * @param array $record
     * @return array|mixed|string|string[]|null
     */
    public function format(array $record): string
    {
        $format = $this->format;

        if (empty($record['stack-trace'])) {
            $this->format = str_replace('%stack-trace%', '', $this->format);
            $this->format = str_replace('%raw-stack-trace%', '', $this->format);
        }

        $formatted = parent::format($record) . PHP_EOL;

        if ($this->debug) {
            foreach (array_filter($record, 'is_scalar') as $var => $val) {
                $formatted = str_replace('%raw-' . $var . '%', $val, $formatted);
            }
        }

        $this->format = $format;

        return $formatted;
    }
}