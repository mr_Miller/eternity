<?php

namespace Eternity\Logger\Processors;

use Eternity\Logger\Logger;
use Monolog\Processor\ProcessorInterface;

/**
 * Class CommonProcessor
 */
class CommonProcessor implements ProcessorInterface
{
    /**
     * @var string
     */
    private $component;

    /**
     * CommonProcessor constructor.
     * @param string $component
     */
    public function __construct(string $component)
    {
        $this->component = $component;
    }

    /**
     * @param array $record
     * @return array
     */
    public function __invoke(array $record)
    {
        $record['timestamp'] = $record['datetime']->format(\DateTimeInterface::ISO8601);
        $record['component'] = $this->component;
        $record['severity'] = Logger::getLevelName($record['level']);

        return $record;
    }
}