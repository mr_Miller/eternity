<?php

namespace Eternity\Logger\Processors;

use Eternity\Trace\TraceId;
use Monolog\Processor\ProcessorInterface;

/**
 * Class TraceIdProcessor
 */
class TraceIdProcessor implements ProcessorInterface
{
    /**
     * @var TraceId
     */
    private $traceId;

    /**
     * TraceIdProcessor constructor.
     * @param TraceId $traceId
     */
    public function __construct(TraceId $traceId)
    {
        $this->traceId = $traceId;
    }

    /**
     * @param array $record
     * @return array
     */
    public function __invoke(array $record)
    {
        $record['trace-id'] = (string)$this->traceId;

        return $record;
    }
}