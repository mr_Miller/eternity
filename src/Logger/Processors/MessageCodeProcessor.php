<?php

namespace Eternity\Logger\Processors;

use Eternity\Logger\Messages\LogMessage;
use Monolog\Processor\ProcessorInterface;

/**
 * Class MessageProcessor
 */
class MessageCodeProcessor implements ProcessorInterface
{
    /**
     * @param array $record
     * @return array
     */
    public function __invoke(array $record)
    {
        $record['msg-code'] = "";

        if (preg_match(LogMessage::PATTERN, $record['message'], $matches)) {
            if (ctype_digit($matches[2])) {
                $record['msg-code'] = $matches[2];
            }
        }

        return $record;
    }
}