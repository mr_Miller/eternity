<?php

namespace Eternity\Logger\Processors;

/**
 * Class StackTraceProcessor
 */
class StackTraceProcessor
{
    /**
     * @param array $record
     * @return array
     */
    public function __invoke(array $record)
    {
        if (!empty($record['context']['exception'])) {
            $exception = $record['context']['exception'];

            if ($exception instanceof \Throwable && is_null($exception->getPrevious())) {
                $record['stack-trace'] = $exception->getTraceAsString();
            }
        }

        return $record;
    }
}