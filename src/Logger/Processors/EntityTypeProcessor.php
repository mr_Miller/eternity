<?php

namespace Eternity\Logger\Processors;

use Eternity\Contracts\PublicEntity;
use Eternity\Helpers\NameHelper;
use Monolog\Processor\ProcessorInterface;

/**
 * Class EntityTypeProcessor
 */
class EntityTypeProcessor implements ProcessorInterface
{
    /**
     * @param array $record
     * @return array
     */
    public function __invoke(array $record)
    {
        switch (true) {
            case isset($record['context']['exception']):
                $type = $this->typeByException($record['context']['exception']);
                break;
            case isset($record['context']['entity']):
                $type = $this->typeOf($record['context']['entity']);
                break;
            case isset($record['context']['entity-type']):
                $type = $record['context']['entity-type'];
                break;
            default:
                $type = '';
        }

        $record['entity-type'] = $type;

        return $record;
    }

    /**
     * @param object $entity
     * @return string
     */
    private function typeOf($entity): string
    {
        if (!is_object($entity)) {
            throw new \LogicException("Entity passed to log must be an object");
        }

        switch (true) {
            case $entity instanceof PublicEntity:
                return $entity->publicType();
            default:
                return lcfirst(NameHelper::shortClassName(get_class($entity)));
        }
    }

    /**
     * @param \Throwable $exception
     * @return string
     */
    private function typeByException(\Throwable $exception): string
    {
        if (method_exists($exception, 'getEntity')) {
            $entity = $exception->getEntity();

            if ($entity instanceof PublicEntity) {
                return $entity->publicType();
            }
        }

        return '';
    }
}