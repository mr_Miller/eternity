<?php

namespace Eternity\Logger\Processors;

use Eternity\Logger\Messages\LogMessage;
use Monolog\Processor\ProcessorInterface;

/**
 * Class MessageProcessor
 */
class MessageProcessor implements ProcessorInterface
{
    /**
     * @param array $record
     * @return array
     */
    public function __invoke(array $record)
    {
        $record['msg'] = $record['message'];

        if (preg_match(LogMessage::PATTERN, $record['message'], $matches)) {
            $record['msg'] = $matches[3];
        }

        return $record;
    }
}