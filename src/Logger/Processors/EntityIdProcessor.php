<?php

namespace Eternity\Logger\Processors;

use Eternity\Contracts\PublicEntity;
use Monolog\Processor\ProcessorInterface;

/**
 * Class EntityIdProcessor
 */
class EntityIdProcessor implements ProcessorInterface
{
    /**
     * @param array $record
     * @return array
     * @throws \Exception
     */
    public function __invoke(array $record)
    {
        switch (true) {
            case isset($record['context']['exception']):
                $entityId = $this->idByException($record['context']['exception']);
                break;
            case isset($record['context']['entity']):
                $entityId = $this->idOf($record['context']['entity']);
                break;
            case isset($record['context']['entity-id']):
                $entityId = $record['context']['entity-id'];
                break;
            default:
                $entityId = '';
        }

        $record['entity-id'] = $entityId;

        return $record;
    }

    /**
     * @param object $entity
     * @return string
     * @throws \LogicException
     */
    private function idOf($entity): string
    {
        if (!is_object($entity)) {
            throw new \LogicException("Entity passed to log must be an object");
        }

        switch (true) {
            case $entity instanceof PublicEntity:
                return (string)$entity->publicId();
            case method_exists($entity, '__toString'):
                return (string)$entity;
            case method_exists($entity, 'getLabel'):
                return (string)$entity->getLabel();
            case method_exists($entity, 'getId'):
                return (string)$entity->getId();
            default:
                return '';
        }
    }

    /**
     * @param \Throwable $exception
     * @return string
     */
    private function idByException(\Throwable $exception): string
    {
        if (method_exists($exception, 'getEntity')) {
            $entity = $exception->getEntity();

            if ($entity instanceof PublicEntity) {
                return $entity->publicId() ?? '';
            }
        }

        return '';
    }
}