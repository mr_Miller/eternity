<?php

namespace Eternity\Logger\Messages;

use Eternity\Messages\Messages;

/**
 * Class LogMessage
 */
class LogMessage extends AbstractMessage
{
    /**
     * @var int
     */
    private $code;

    /**
     * LogMessage constructor.
     * @param int $code
     */
    public function __construct(int $code)
    {
        $this->code = $code;
    }

    /**
     * @return string
     */
    public function code(): string
    {
        return $this->code;
    }

    /**
     * @return string
     */
    public function message(): string
    {
        return Messages::message($this->code);
    }
}