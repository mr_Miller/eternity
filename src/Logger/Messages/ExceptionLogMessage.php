<?php

namespace Eternity\Logger\Messages;

use Eternity\Exceptions\EternityException;

/**
 * Class ExceptionLogMessage
 * @package Eternity\Logger\Messages
 */
class ExceptionLogMessage extends AbstractMessage
{
    /**
     * @var \Throwable|\Eternity\Exceptions\EternityException
     */
    private $e;

    /**
     * ExceptionLogMessage constructor.
     * @param \Throwable $exception
     */
    public function __construct(\Throwable $exception)
    {
        $this->e = $exception;
    }

    /**
     * @return \Throwable
     */
    public function exception(): \Throwable
    {
        return $this->e;
    }

    /**
     * @return string
     */
    public function code(): string
    {
        // For case if error has specific predefined error CODE
        if (!is_null($this->e->getCode()) && $this->e->getCode() != 0) {
            return $this->e->getCode();
        }

        if ($this->e instanceof EternityException) {
            // Return Http status code for all other cases
            return $this->e->getStatus();
        }

        return $this->e->getCode();
    }

    /**
     * @return string
     */
    public function message(): string
    {
        if ($this->e instanceof EternityException) {
            // Build message for eternity exceptions
            $message = '';
            if (!empty($this->e->getTitle())) {
                $message .= $this->e->getTitle() . (!empty($this->e->getDetail()) ? '. ' : '.');
            }
            if (!empty($this->e->getDetail())) {
                $message .= $this->e->getDetail() . '.';
            }

            $message .= " Type: {$this->e->getType()}.";

            return $message;
        }

        return $this->e->getMessage();
    }
}