<?php

namespace Eternity\Logger\Messages;

/**
 * Class CustomLogMessage
 */
class CustomLogMessage extends AbstractMessage
{
    /**
     * @var int
     */
    private $code;

    /**
     * @var string
     */
    private $message;

    /**
     * CustomMessage constructor.
     * @param string $message
     * @param string $code
     */
    public function __construct(string $message, string $code = '0')
    {
        $this->message = $message;
        $this->code = $code;
    }

    /**
     * @return int
     */
    public function code(): string
    {
        return $this->code;
    }

    /**
     * @return string
     */
    public function message(): string
    {
        return $this->message;
    }
}