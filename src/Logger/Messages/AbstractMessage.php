<?php

namespace Eternity\Logger\Messages;

/**
 * Class AbstractMessage
 */
abstract class AbstractMessage
{
    const PATTERN = '#^(\[\[(\d*)\]\])?\s*(.*)$#s';

    /**
     * @return int
     */
    abstract public function code(): string;

    /**
     * @return string
     */
    abstract public function message(): string;

    /**
     * @return string
     */
    public final function __toString()
    {
        if ($this->code() > 0)
        {
            return "[[{$this->code()}]] {$this->message()}";
        }

        return $this->message();
    }
}