<?php

namespace Eternity\Logger\Messages;

use Eternity\Http\Dto\ErrorDto;
use Eternity\Http\Exceptions\ResponseException;

/**
 * Class ExternalLogMessage
 * @package Eternity\Logger\Messages
 */
class ExternalLogMessage extends AbstractMessage
{
    /**
     * @var \Eternity\Http\Exceptions\ResponseException
     */
    private $e;

    /**
     * ExternalLogMessage constructor
     * @param \Eternity\Http\Exceptions\ResponseException $exception
     */
    public function __construct(ResponseException $exception)
    {
        $this->e = $exception;
    }

    /**
     * @return \Eternity\Http\Exceptions\ResponseException
     */
    public function exception(): ResponseException
    {
        return $this->e;
    }

    /**
     * @return string
     */
    public function code(): string
    {
        // Logging exception from external microservice
        $error = $this->e->getResponse()->getError();

        // For cases if response has unknown structure
        if ($error === null) {
            return '';
        }

        // For case if error has specific predefined error CODE
        if (!is_null($error->code) && $error->code != 0) {
            return $error->code;
        }

        // Return Http status code for all other cases
        return $error->status;
    }

    /**
     * @return string
     */
    public function message(): string
    {
        // Try retrieve first error
        $error = $this->e->getResponse()->getError();

        $message = $this->buildStartMessage($this->e->getTitle(), $this->e->getDetail());

        if ($error instanceof ErrorDto) {
            $message .= " External \"$error->title\". Details: \"$error->detail\".";

            // prepare validation errors
            if (!empty($error->validation_errors)) {
                $message .= ' Validation errors: ' . json_encode($error->validation_errors) . '.';
            }

            $message .= " Type: $error->type.";
        } else {
            $message .= " Error has unexpected structure, probably microservice is down. Type: {$this->e->getType()}.";
        }

        return $message;
    }

    /**
     * @param string|null $title
     * @param string|null $detail
     * @return string
     */
    private function buildStartMessage(?string $title = null, ?string $detail = null): string
    {
        $message = '';
        if (!empty($title)) {
            $message .= $title . (!empty($detail) ? '. ' : '.');
        }
        if (!empty($detail)) {
            $message .= $detail . '.';
        }

        return $message;
    }
}