<?php

namespace Eternity\Logger\Messages;

use Eternity\Exceptions\ValidationException;

/**
 * Class CustomMessage
 */
class ValidationLogMessage extends AbstractMessage
{
    /**
     * @var \Eternity\Exceptions\ValidationException
     */
    private $e;

    /**
     * ExceptionLogMessage constructor.
     * @param \Eternity\Exceptions\ValidationException $exception
     */
    public function __construct(ValidationException $exception)
    {
        $this->e = $exception;
    }

    /**
     * @return \Eternity\Exceptions\ValidationException
     */
    public function exception(): ValidationException
    {
        return $this->e;
    }

    /**
     * @return string
     */
    public function code(): string
    {
        return $this->e->getCode() > 0 ? $this->e->getCode() : $this->e->getStatus();
    }

    /**
     * @return string
     */
    public function message(): string
    {
        $message = '';
        if (!empty($this->e->getTitle())) {
            $message .= $this->e->getTitle() . (!empty($this->e->getDetail()) ? '. ' : '.');
        }
        if (!empty($this->e->getDetail())) {
            $message .= $this->e->getDetail() . '.';
        }

        $message .= " Type: {$this->e->getType()}.";

        if (!empty($this->e->getValidationErrors())) {
            $message .= " Validation errors: " . json_encode($this->e->getValidationErrors()) . '.';
        }

        return $message;
    }
}