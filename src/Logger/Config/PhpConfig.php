<?php

namespace Eternity\Logger\Config;

use Eternity\Exceptions\ErrorCodes;
use Eternity\Logger\Exceptions\LoggerException;
use Eternity\Logger\Formatters\CustomFormatter;
use Eternity\Logger\Formatters\JsonFormatter;
use Eternity\Logger\Formatters\PipeFormatter;
use Eternity\Logger\Interfaces\LogConfig;
use Eternity\Logger\Logger;
use Eternity\Messages\Messages;
use Monolog\Formatter\FormatterInterface;
use Monolog\Handler\HandlerInterface;
use Monolog\Handler\RotatingFileHandler;
use Monolog\Handler\StreamHandler;
use Monolog\Handler\SyslogHandler;
use Monolog\Handler\TelegramBotHandler;
use Monolog\Logger as Monolog;

/**
 * Class PhpConfig
 * @package Eternity\Logger\Config
 */
class PhpConfig implements LogConfig
{
    const DEFAULT_FACILITY = 'user';
    const DEFAULT_FORMATTER = 'pipe';
    const DEFAULT_IDENTITY = 'dev';
    const DEFAULT_FORMAT = "%timestamp%|%component%|%severity%|%msg-code%|%msg%|%trace-id%|%entity-type%|%entity-id%|%stack-trace%";

    /**
     * @var array
     */
    private $config;

    /**
     * @var
     */
    private $debug = false;

    /**
     * @var FormatterInterface[]
     */
    private $formatters;

    /**
     * @var HandlerInterface[][]
     */
    private $handlers = [];

    /**
     * @var string
     */
    private $type;

    /**
     * PhpConfig constructor.
     * @param string $type
     * @param array $config
     * @throws \Eternity\Logger\Exceptions\LoggerException
     */
    public function __construct(string $type, array $config)
    {
        $this->initConfig($config);
        $this->type = $type;
    }

    /**
     * @return string
     * @throws \Exception
     */
    public function channel(): string
    {
        return $this->name() . '-' . $this->type;
    }

    /**
     * @return string
     * @throws \Exception
     */
    private function name(): string
    {
        if (empty($this->config['component']['name'])) {
            throw new \Exception('Component name [component.name] is required in config');
        }

        return $this->config['component']['name'];
    }

    /**
     * @param string $type
     * @return array
     * @throws \Exception
     */
    public function handlers(string $type): array
    {
        if (!isset($this->handlers[$type])) {
            $this->handlers[$type] = [];

            foreach ($this->handlerConfig()[$type] as $handlerName => $handler) {
                $this->handlers[$type][$handlerName] = $this->handler($handlerName, $handler);
            }
        }

        return $this->handlers[$type];
    }

    /**
     * @return bool
     */
    public function debug(): bool
    {
        return $this->debug;
    }

    /**
     * @param string $handlerName
     * @param array $config
     * @return HandlerInterface
     * @throws \Exception
     */
    private function handler(string $handlerName, array $config)
    {
        if (empty($config['type'])) {
            throw new \Exception("Required parameter 'type' is not set for the '$handlerName' handler in logger configuration");
        }

        $level = isset($config['level']) ? Logger::getLevelByName($config['level']) : Monolog::DEBUG;

        switch ($config['type']) {
            case 'file':
                if (empty($config['file'])) {
                    throw new \Exception("Required parameter 'file' is not set for the '$handlerName' {$config['type']} handler in logger configuration");
                }

                $handler = new StreamHandler($config['file'], $level);
                break;
            case 'rotating-file':
                $handler = new RotatingFileHandler(
                    $config['filename'] ?? 'log',
                    $config['max_files'] ?? 0,
                    $level,
                    $config['bubble'] ?? true,
                    $config['file_permission'] ?? 0664,
                    $config['use_locking'] ?? false
                );

                if (!empty($config['filename_format'])) {
                    $handler->setFilenameFormat($config['filename_format'], $config['date_format'] ?? 'Y-m-d');
                }

                break;
            case 'telegram':
                if (empty($config['api_key'])) {
                    throw new \Exception("Required parameter 'api_key' is not set for the '$handlerName' handler in logger configuration");
                }
                if (empty($config['channel'])) {
                    throw new \Exception("Required parameter 'channel' (Channel or Group ID) is not set for the '$handlerName' handler in logger configuration");
                }

                $handler = new TelegramBotHandler(
                    $config['api_key'],
                    $config['channel'],
                    $level,
                    $config['bubble'] ?? true
                );
                break;
            case 'syslog':
                $handler = new SyslogHandler(
                    empty($config['ident']) ? $this->channel() : $config['ident'],
                    empty($config['facility']) ? static::DEFAULT_FACILITY : $config['facility'],
                    $level
                );
                break;
            default:
                throw new \Exception("Undefined handler type '{$config['type']}' has been found in logger configuration");
        }

        $handler->setFormatter($this->formatterByHandler($handlerName, $config));

        return $handler;
    }

    /**
     * @param string $handlerName
     * @param array $handler
     * @return FormatterInterface
     * @throws \Exception
     */
    private function formatterByHandler(string $handlerName, array $handler): FormatterInterface
    {
        $formatter = $handler['formatter'] ?? static::DEFAULT_FORMATTER;

        if (!array_key_exists($formatter, $this->formatters())) {
            throw new \Exception("'$handlerName' handler formatters contain disallowed value '$formatter'");
        }

        return $this->formatters()[$formatter];
    }

    /**
     * @return FormatterInterface[]
     * @throws \Exception
     */
    private function formatters(): array
    {
        if ($this->formatters === null) {
            $formatters = $this->config()['formatters'] ?? [];

            if (empty($formatters)) {
                $formatters[static::DEFAULT_FORMATTER] = static::DEFAULT_FORMAT;
            }

            $this->formatters = [];

            foreach ($formatters as $type => $format) {
                if (!is_string($format)) {
                    throw new \Exception("Formatter is set incorrectly. Check keys in 'formatters' section");
                }

                switch ($type) {
                    case 'pipe':
                        $this->formatters[$type] = new PipeFormatter($format ?? null, \DateTimeInterface::ISO8601, $this->debug());
                        break;
                    case 'json':
                        $this->formatters[$type] = new JsonFormatter($format ?? null, \DateTimeInterface::ISO8601);
                        break;
                    default:
                        $this->formatters[$type] = new CustomFormatter($format ?? null, \DateTimeInterface::ISO8601);
                }
            }
        }

        return $this->formatters;
    }

    /**
     * Returns config
     * @return array
     * @throws \Exception
     */
    private function config(): array
    {
        return $this->config;
    }

    /**
     * Returns handler config section
     * @return array
     */
    private function handlerConfig(): array
    {
        return $this->config['handlers'];
    }

    /**
     * Returns handler default config
     * @return array
     */
    private function defaultConfig(): array
    {
        return [
            'handler1' => [
                'type'      => 'syslog',
                'level'     => Logger::getLevelName(Logger::INFO),
                'formatter' => self::DEFAULT_FORMATTER,
                'ident'     => self::DEFAULT_IDENTITY,
                'facility'  => self::DEFAULT_FACILITY,
            ],
        ];
    }

    /**
     * Config initialization
     * @param array $config
     * @throws \Eternity\Logger\Exceptions\LoggerException
     */
    private function initConfig(array $config): void
    {
        // Init debug
        $this->debug = $config['debug'] ?? false;

        // Check if handlers option in not empty and is array
        if (empty($config['handlers']) || !is_array($config['handlers'])) {
            throw new LoggerException(
                ErrorCodes::LOGGER_CONFIG_HANDLERS_EMPTY,
                'Logger error',
                Messages::message(ErrorCodes::LOGGER_CONFIG_HANDLERS_EMPTY)
            );
        }
        // Check each section for handlers existence
        // If not handlers found the default handler will be set
        foreach ($config['handlers'] as $sectionName => $handlersArray) {
            if (empty($handlersArray)) {
                $config['handlers'][$sectionName] = $this->defaultConfig();
            }
        }
        $this->config = $config;
    }
}