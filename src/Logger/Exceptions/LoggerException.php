<?php

namespace Eternity\Logger\Exceptions;

use Eternity\Exceptions\EternityException;

/**
 * Class LoggerException
 * @package Eternity\Logger\Exceptions
 */
class LoggerException extends EternityException
{
    /**
     * LoggerException constructor.
     * @param int $code
     * @param string $title
     * @param string|null $detail
     * @param \Throwable|null $previous
     */
    public function __construct(int $code, string $title = 'Logger error', string $detail = null, \Throwable $previous = null)
    {
        parent::__construct($title, $detail, $previous);
        $this->code = $code;
    }
}