<?php

namespace Eternity\Logger;

use Eternity\Exceptions\ValidationException;
use Eternity\Http\Exceptions\ResponseException;
use Eternity\Logger\Interfaces\LogConfig;
use Eternity\Logger\Messages\ExceptionLogMessage;
use Eternity\Logger\Messages\ExternalLogMessage;
use Eternity\Logger\Messages\ValidationLogMessage;
use Eternity\Logger\Processors\CommonProcessor;
use Eternity\Logger\Processors\EntityIdProcessor;
use Eternity\Logger\Processors\EntityTypeProcessor;
use Eternity\Logger\Processors\MessageCodeProcessor;
use Eternity\Logger\Processors\MessageProcessor;
use Eternity\Logger\Processors\StackTraceProcessor;
use Eternity\Logger\Processors\TraceIdProcessor;
use Eternity\Trace\TraceId;
use InvalidArgumentException;
use Monolog\Handler\HandlerInterface;
use Monolog\Handler\NullHandler;
use Monolog\Logger as Monolog;
use Monolog\Processor\ProcessorInterface;
use Monolog\Processor\PsrLogMessageProcessor;

/**
 * Class Logger
 * @package Eternity\Logger
 */
class Logger extends Monolog
{
    /**
     * @var LogConfig
     */
    private $config;

    /**
     * Logging levels from syslog protocol defined in RFC 5424
     *
     * @var array $levels Logging levels
     */
    protected static $levels = [
        self::DEBUG     => 'DEBUG',
        self::INFO      => 'INFO',
        self::NOTICE    => 'NOTICE',
        self::WARNING   => 'WARN',
        self::ERROR     => 'ERROR',
        self::CRITICAL  => 'CRITICAL',
        self::ALERT     => 'ALERT',
        self::EMERGENCY => 'EMERGENCY',
    ];

    /**
     * @var \Eternity\Trace\TraceId
     */
    private $traceId;

    /**
     * @var array
     */
    private $defaultContext = [];

    /**
     * Logger constructor.
     * @param string $type
     * @param \Eternity\Trace\TraceId $traceId
     * @param LogConfig $config
     * @throws \Exception
     */
    public function __construct(string $type, TraceId $traceId, LogConfig $config)
    {
        $this->config = $config;
        $this->traceId = $traceId;
        parent::__construct($this->config()->channel(), $this->handlers($type), $this->processors());
    }

    /**
     * @inheritDoc
     */
    public function addRecord(int $level, string $message, array $context = []): bool
    {
        /**
         * Merge with default context
         */
        $context = array_merge($this->defaultContext, $context);

        if (isset($context['exception'])) {
            $exception = $context['exception'];

            /**
             * Log previous exception(s)
             */
            if ($exception instanceof \Throwable && $exception->getPrevious() instanceof \Throwable) {
                $previous = $exception->getPrevious();
                if ($previous instanceof ResponseException) {
                    $this->addRecord($level, new ExternalLogMessage($previous), ['exception' => $previous]);
                } else if ($previous instanceof ValidationException) {
                    $this->addRecord($level, new ValidationLogMessage($previous), ['exception' => $previous]);
                } else {
                    $this->addRecord($level, new ExceptionLogMessage($previous), ['exception' => $previous]);
                }
            }
        }

        return parent::addRecord($level, $message, $context);
    }

    /**
     * @param string $type
     * @return HandlerInterface[]
     */
    protected function handlers(string $type): array
    {
        $handlers = [];

        foreach ($this->config()->handlers($type) as $name => $handler) {
            $handlers[$name] = $handler;
        }

        if (empty($handlers)) {
            $handlers[] = new NullHandler();
        }

        return $handlers;
    }

    /**
     * @return ProcessorInterface[]
     * @throws \Exception
     */
    protected function processors(): array
    {
        if ($this->processors === null) {
            // Do not change processors order
            $this->processors = [
                new StackTraceProcessor(),
                new EntityIdProcessor(),
                new EntityTypeProcessor(),
                new MessageCodeProcessor(),
                new PsrLogMessageProcessor(),
                new MessageProcessor(),
                new TraceIdProcessor($this->traceId),
                new CommonProcessor($this->config()->channel()),
            ];
        }

        return $this->processors;
    }

    /**
     * Gets the name of the logging level.
     *
     * @param int $level
     * @return string
     */
    public static function getLevelName($level): string
    {
        if (!isset(static::$levels[$level])) {
            throw new InvalidArgumentException(
                'Log level is not defined, use one of: ' . implode(', ', static::$levels)
            );
        }

        return static::$levels[$level];
    }

    /**
     * @param string $name
     * @return int
     */
    public static function getLevelByName(string $name): int
    {
        $levels = array_flip(static::$levels);
        $upperCaseName = strtoupper($name);

        if (!isset($levels[$upperCaseName])) {
            throw new InvalidArgumentException(
                "No log level represented as '$name', use one of: " . implode(', ', static::$levels)
            );
        }

        return $levels[$upperCaseName];
    }

    /**
     * @return LogConfig
     */
    public function config(): LogConfig
    {
        return $this->config;
    }
}