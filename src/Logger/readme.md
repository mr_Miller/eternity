## Logger
Application base logger, extended Monolog logger.

### Log structure
Documentation about log structure [here (Confluence)](https://animal-id.atlassian.net/wiki/spaces/A/pages/304250881/Logging);

### Levels
Each logger handler can be configured to log only certain log levels.

| Level | Priority | Description |
| --- | --- | --- |
| debug | 100 | This level is used for developing. |
| info | 200 | When some event firing must be logged, use this level of logging. |
| warn | 300 | For example when some parameter is NOT provided and code repair itself with setting the default value. |
| error | 400 | For all errors and exceptions. |

### Formatters
Allowed formatters: `pipe`, `json`. 
> Json is not tested.

### Usage
Create an instance of config class which extends the interface (`Eternity\Logger\Interfaces\LogConfig`), component class from interface
 (`Eternity\Logger\Interfaces\Component`) and provide it to logger. Also provide logger type, __type__ must be the same as handler's __section name__
  in logger config, to apply that config to current instance.
Example with usage of PhpConfig:
```php
use Eternity\Logger\Config\PhpConfig;
use Eternity\Logger\Component\ApiComponent;
use Eternity\Logger\Logger;
use Eternity\Trace\TraceId;

$config = [
   'section'   => env('LOGGER_SECTION'),
   'debug'     => env('APP_DEBUG'),
   'handlers'  => [
       'general'  => [
           'handler1' => [
               'type'      => 'syslog',  // Type is required
               'ident'     => 'gw', // String to identify the source of the log message. Default value is 'gw'
               'level'     => 'info',
               'formatter' => 'pipe',
               'facility'  => 'user',
               'pipe'      => '%timestamp%|%component%|%severity%|%msg-code%|%msg%|%trace-id%|%entity-type%|%entity-id%|%stack-trace%',
           ]
       ],
   ],
   'component' => [
       'name' => 'gw',
   ]
];

// Init config
$config = new PhpConfig(new ApiComponent(), $config);
// Init and generate trace ID, needed for log analisys
$traceId = new TraceId();
$traceId->generate();
$logger = Logger('general', $traceId, $config);

// Log
$logger->info(new \Eternity\Logger\Messages\LogMessage(1), []);
```