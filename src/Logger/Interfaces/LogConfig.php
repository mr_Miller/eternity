<?php
/**
 * TMS: Maintenance Manager
 */

namespace Eternity\Logger\Interfaces;

use Monolog\Handler\HandlerInterface;

/**
 * Interface LogConfig
 */
interface LogConfig
{
    /**
     * @return string
     */
    public function channel(): string;

    /**
     * @return bool
     */
    public function debug(): bool;

    /**
     * @param string $type Handlers type
     * @return HandlerInterface[]
     */
    public function handlers(string $type): array;
}