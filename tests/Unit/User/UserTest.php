<?php

namespace Eternity\Tests\Unit\User;

use Eternity\User\User;
use PHPUnit\Framework\TestCase;

/**
 * Class UserTest
 * @package Eternity\Tests\Unit\User
 */
class UserTest extends TestCase
{
    /**
     * @var array
     */
    protected $data;

    /**
     *
     */
    protected function setUp(): void
    {
        parent::setUp();

        $this->data = [
            "id"           => 2,
            "abilities"    => [
                "mp-view-group",
                "mp-view-single-product",
                "mp-view-single-category",
                "mp-view-qr-codes",
                "mp-view-qr-codes",
                "mp-scan-qr-codes",
                "mp-view-user-account",
                "mp-view-user-transactions",
                "mp-view-user-single-transaction",
                "mp-view-shelters",
                "mp-manage-withdrawal",
                "mp-view-definitions",
                "gw-manage-user",
                "gw-view-definitions",
                "rf-view-referent",
                "sc-view-definitions",
                "sc-view-subscriptions",
                "sc-verify-receipt",
            ],
            "roles"        => [
                "pet_owner",
            ],
            "subscription" => [
                "id"                       => null,
                "type"                     => 10,
                "provider_type"            => null,
                "status"                   => 10,
                'entitlement_code'         => null,
                'is_trial_period_was_used' => false,
                "product_id"               => null,
                "expires_at"               => null,
                "created_at"               => null,
                "updated_at"               => null,
                "limits"                   => [
                    "tag_scan_sms_notification_count_month" => 0,
                    "tag_guarantee_service_count_year"      => 0,
                    "pet_document_max_count"                => 2,
                    "additional_contact_max_count"          => 0,
                ],
            ],
            "group_id"     => 1,
        ];
    }

    public function testFromArrayWithCorrectDataSuccess(): void
    {
        $user = new User();
        $user->fromArray($this->data);
        $this->assertEquals($user->toArray(), $this->data);
    }
}