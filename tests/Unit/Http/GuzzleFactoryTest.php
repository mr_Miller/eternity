<?php

namespace Eternity\Tests\Unit\Http;

use Eternity\Http\Client;
use Eternity\Http\GuzzleFactory;
use GuzzleHttp\HandlerStack;

class GuzzleFactoryTest extends AbstractTestCase
{
    public function testMake(): void
    {
        $this->assertInstanceOf(Client::class, GuzzleFactory::make());
    }

    public function testHandler(): void
    {
        $this->assertInstanceOf(HandlerStack::class, GuzzleFactory::handler());
    }
}
