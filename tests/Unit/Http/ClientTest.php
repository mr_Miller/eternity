<?php

namespace Eternity\Tests\Unit\Http;

use Eternity\Http\Contracts\ExtendedResponse;
use Eternity\Http\Exceptions\RequestException as EternityRequestException;
use Eternity\Http\Exceptions\ResponseException;
use Eternity\Http\GuzzleFactory;
use GuzzleHttp\Exception\RequestException;
use GuzzleHttp\Psr7\Request;
use Psr\Http\Message\ResponseInterface;

class ClientTest extends AbstractTestCase
{
    const BASE_URI = 'http://httpbin.org';

    /**
     * @var \Eternity\http\Client
     */
    private $httpClient;

    public function setUp(): void
    {
        parent::setUp();

        $this->httpClient = GuzzleFactory::make([
            'base_uri' => self::BASE_URI,
            'headers'  => [
                'Accept'       => 'application/json',
                'Content-Type' => 'application/json',
            ],
        ]);
    }

    /**
     * @throws \Eternity\Http\Exceptions\RequestBodyFormatException
     * @throws \Eternity\Http\Exceptions\RequestException
     * @throws \Eternity\Http\Exceptions\ResponseException
     */
    public function testExpectedMethodTrue()
    {
        $response = $this->httpClient->get('/status/202', 202);
        $this->assertTrue($this->httpClient->expected(202, $response));
    }

    /**
     * @throws \Eternity\Http\Exceptions\RequestBodyFormatException
     * @throws \Eternity\Http\Exceptions\RequestException
     * @throws \Eternity\Http\Exceptions\ResponseException
     */
    public function testExpectedMethodFalse()
    {
        $this->assertFalse($this->httpClient->expected(
            202,
            $this->httpClient->get('/status/200', 200)
        ));
    }

    /**
     * @throws \Eternity\Http\Exceptions\RequestBodyFormatException
     * @throws \Eternity\Http\Exceptions\RequestException
     */
    public function testResponseExceptionHasResponseOnError()
    {
        try {
            $this->httpClient->get('/status/200', 204);
        } catch (ResponseException $e) {
            $this->assertInstanceOf(ExtendedResponse::class, $e->getResponse());
        }
    }

    public function testMagicCallFromClient()
    {
        $this->assertInstanceOf(ResponseInterface::class, $this->httpClient->request('GET', '/get'));
    }

    public function testMagicCallFromClientCallNotExistsMethod()
    {
        $this->expectException(\InvalidArgumentException::class);
        $this->expectExceptionMessage('Magic request methods require a URI and optional options array');
        $this->httpClient->notExistsMethod();
    }

    /**
     * @throws \ReflectionException
     */
    public function testSendMethodResponse()
    {
        $request = new Request('GET', '/uuid');
        $response = $this->invokeMethod($this->httpClient, 'send', [$request, 200]);
        $this->assertInstanceOf(ExtendedResponse::class, $response);
    }

    /**
     * @throws \ReflectionException
     */
    public function testSendMethodSuccess()
    {
        $body = ['EternityHttpClientTest'];
        $request = $this->invokeMethod($this->httpClient, 'buildRequest', ['POST', '/anything', [], $body]);
        /** @var ExtendedResponse $response */
        $response = $this->invokeMethod($this->httpClient, 'send', [$request, 200]);
        $this->assertEquals($body, $response->getBody()['json']);
    }

    /**
     * @throws \ReflectionException
     */
    public function testSendMethodThrowsResponseExceptionOnNotFound()
    {
        $this->expectException(ResponseException::class);
        $request = new Request('GET', '/uuid/incorrect');
        $this->invokeMethod($this->httpClient, 'send', [$request, 200]);
    }

    public function sendMagicCallWithIncorrectArgument()
    {
        $this->expectException(RequestException::class);
        $request = new Request('GET1', '/uuid/incorrect');
        $this->invokeMethod($this->httpClient, 'send', [$request, 200]);
    }

    /**
     * @throws \ReflectionException
     */
    public function testSendMethodThrowsResponseExceptionOnWrongReturnStatus()
    {
        $this->expectException(ResponseException::class);
        $request = new Request('GET', '/status/202');
        $this->invokeMethod($this->httpClient, 'send', [$request, 201]);
    }
}
