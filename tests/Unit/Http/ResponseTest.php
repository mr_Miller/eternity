<?php

namespace Unit\Http;

use Eternity\Http\Dto\ErrorDto;
use Eternity\Http\GuzzleFactory;
use Eternity\Http\Response\Response;
use GuzzleHttp\Psr7\Response as GuzzleResponse;
use PHPUnit\Framework\TestCase;

/**
 * Class ResponseTest
 * @package Unit\Http
 */
class ResponseTest extends TestCase
{
    const BASE_URI = 'http://httpbin.org';

    const BODY_WITH_PAYLOAD = [
        'payload' => [
            [
                'firstItem' => 'data',
            ],
            [
                'secondItem' => 'data',
            ]
        ]
    ];

    const BODY_WITH_ERRORS = [
        'errors' => [
            [
                'id' => 1,
            ]
        ],
        'trace' => [
            'some-trace'
        ]
    ];

    /**
     * @var \Eternity\http\Client
     */
    private $httpClient;

    public function setUp(): void
    {
        parent::setUp();

        $this->httpClient = GuzzleFactory::make([
            'base_uri' => self::BASE_URI,
            'headers'  => [
                'Accept'       => 'application/json',
                'Content-Type' => 'application/json',
            ],
        ]);
    }

    /**
     * @throws \Eternity\Http\Exceptions\RequestBodyFormatException
     * @throws \Eternity\Http\Exceptions\RequestException
     * @throws \Eternity\Http\Exceptions\ResponseException
     */
    public function testApplicationJsonResponseType()
    {
        $response = $this->httpClient->get('/json');
        $this->assertTrue($response->isApplicationJson());
    }

    public function testGetBodyMethodSuccess()
    {
        $guzzleResponse = new GuzzleResponse(200, ['Content-Type' => 'application/json'], json_encode(self::BODY_WITH_PAYLOAD));
        $response = new Response($guzzleResponse);
        $this->assertEquals(self::BODY_WITH_PAYLOAD, $response->getBody());
    }

    public function testGetPayloadMethodSuccess()
    {
        $guzzleResponse = new GuzzleResponse(200, ['Content-Type' => 'application/json'], json_encode(self::BODY_WITH_PAYLOAD));
        $response = new Response($guzzleResponse);
        $this->assertEquals(self::BODY_WITH_PAYLOAD['payload'], $response->getPayloadItems());
    }

    public function testGetPayloadSingleMethodSuccess()
    {
        $guzzleResponse = new GuzzleResponse(200, ['Content-Type' => 'application/json'], json_encode(self::BODY_WITH_PAYLOAD));
        $response = new Response($guzzleResponse);
        $this->assertEquals(self::BODY_WITH_PAYLOAD['payload'][0], $response->getPayloadItem());
    }

    public function testGetErrorsMethodSuccess()
    {
        $guzzleResponse = new GuzzleResponse(200, ['Content-Type' => 'application/json'], json_encode(self::BODY_WITH_ERRORS));
        $response = new Response($guzzleResponse);
        $this->assertEquals(self::BODY_WITH_ERRORS['errors'], $response->getErrors());
    }

    public function testGetErrorMethodSuccess()
    {
        $guzzleResponse = new GuzzleResponse(200, ['Content-Type' => 'application/json'], json_encode(self::BODY_WITH_ERRORS));
        $response = new Response($guzzleResponse);
        $this->assertInstanceOf(ErrorDto::class, $response->getError());
    }

    public function testGetHttpStatusSuccess()
    {
        $guzzleResponse = new GuzzleResponse(200, ['Content-Type' => 'application/json'], json_encode(self::BODY_WITH_ERRORS));
        $response = new Response($guzzleResponse);
        $this->assertEquals(200, $response->getHttpStatus());
    }
}