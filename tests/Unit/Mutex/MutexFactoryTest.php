<?php

namespace Eternity\Tests\Unit\Mutex;

use Eternity\Mutex\Contracts\MutexInterface;
use Eternity\Mutex\Contracts\Storage;
use Eternity\Mutex\MutexFactory;
use PHPUnit\Framework\TestCase;

/**
 * Class MutexFactoryTest
 * @package Eternity\Tests\Unit\Mutex
 */
class MutexFactoryTest extends TestCase
{
    public function testMake()
    {
        $store   = $this->getMockBuilder(Storage::class)->getMock();

        static::assertInstanceOf(MutexInterface::class, (new MutexFactory($store))->make('phpunit:mutex'));
    }
}
