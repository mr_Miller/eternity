<?php

namespace Eternity\Tests\Unit\Mutex;

use RuntimeException;
use UnexpectedValueException;
use Eternity\Mutex\Exceptions\TimeoutException;
use Eternity\Mutex\Loop\SmartLoop;
use PHPUnit\Framework\TestCase;

class SmartLoopTest extends TestCase
{
    public function testInvalidTimeout()
    {
        $this->expectException(UnexpectedValueException::class);

        new SmartLoop(0);
    }

    public function testExecutionWithinTimeout()
    {
        $this->expectNotToPerformAssertions();

        $loop = new SmartLoop(1);
        $loop->execute(function () use ($loop) {
            usleep(100000);
            $loop->end();
        });
    }

    public function testExceedTimeout()
    {
        $this->expectException(TimeoutException::class);

        $loop = new SmartLoop(.1);
        $loop->execute(function () use ($loop) {
            usleep(100000);
            $loop->end();
        });
    }

    public function testExceedTimeoutWithoutExplicitEnd()
    {
        $this->expectException(TimeoutException::class);

        $loop = new SmartLoop(.1);
        $loop->execute(function () {
            usleep(100000);
        });
    }

    public function testExceptionStopsIteration()
    {
        $this->expectException(RuntimeException::class);

        $loop = new SmartLoop();
        $loop->execute(function () {
            throw new RuntimeException('loop failed');
        });
    }

    public function testEnd()
    {
        $i = 0;
        $loop = new SmartLoop();
        $loop->execute(function () use ($loop, &$i) {
            $i++;
            $loop->end();
        });

        self::assertEquals(1, $i);
    }

    public function testIteration()
    {
        $i = 0;
        $loop = new SmartLoop();
        $loop->execute(function () use ($loop, &$i): void {
            $i++;
            if ($i > 1) {
                $loop->end();
            }
        });

        self::assertSame(2, $i);
    }
}
