<?php

namespace Eternity\Tests\Unit\Mutex;

use Eternity\Mutex\Checks\ChainedCondition;
use Eternity\Mutex\Contracts\Storage;
use Eternity\Mutex\Mutex;
use Eternity\Mutex\Token;
use PHPUnit\Framework\TestCase;

/**
 * Class MutexFactoryTest
 * @package Eternity\Tests\Unit\Mutex
 */
class ChainedConditionTest extends TestCase
{
    public function testConditionalFails()
    {
        $token = new Token(uniqid(__METHOD__, true));
        $store = $this->getMockBuilder(Storage::class)->getMock();
        $mutex = new Mutex($store, $token);

        $check = $mutex->condition(function (): bool {
            return false;
        });

        $result = $check->then(function (): void {
            self::fail();
        });

        // Failed check should return false.
        self::assertFalse($result);
    }

    public function testConditionalCodeExecution()
    {
        $token = new Token(uniqid(__METHOD__, true));
        $store = $this->getMockBuilder(Storage::class)->getMock();
        $mutex = $this->getMockBuilder(Mutex::class)
        ->disableOriginalConstructor()
        ->setConstructorArgs([$store, $token])
        ->getMock();

        $lock = 0;
        $check = 0;

        $mutex->expects(self::once())
        ->method('isolate')
        ->willReturnCallback(function (callable $block) use (&$lock) {
            $lock++;
            $result = $block();
            $lock++;

            return $result;
        });

        $chain = new ChainedCondition($mutex);
        $chain->try(function () use (&$lock, &$check): bool {
            if ($check === 1) {
                self::assertSame(1, $lock);
            }
            $check++;

            return true;
        });

        $result = $chain->then(function () use (&$lock) {
            self::assertSame(1, $lock);

            return 'protected';
        });

        self::assertSame(2, $check);

        // Synchronized code should return a test string.
        self::assertSame('protected', $result);
    }

    public function conditionalCallbacks()
    {
        $checkCounter = 0;

        return [
            [function (): bool {
                return false;
            }],

            [function () use (&$checkCounter): bool {
                $result = $checkCounter == 0;
                $checkCounter++;

                return $result;
            }],
        ];
    }

    /**
     * @param  callable  $check_function
     *
     * @dataProvider conditionalCallbacks
     * @throws \Exception
     */
    public function testConditionalCodeNotExecuted(callable $check_function)
    {
        $token = new Token(uniqid(__METHOD__, true));
        $store = $this->getMockBuilder(Storage::class)->getMock();
        $mutex = $this->getMockBuilder(Mutex::class)
        ->disableOriginalConstructor()
        ->setConstructorArgs([$store, $token])
        ->getMock();

        $mutex->expects(self::any())
        ->method('isolate')
        ->willReturnCallback(function (callable $block) {
            return $block();
        });

        $chain = new ChainedCondition($mutex);
        $chain->try($check_function);
        $result = $chain->then(function (): void {
            self::fail();
        });

        // Each failed check should return false.
        self::assertFalse($result);
    }

    public function testConditionalCodeExecuted()
    {
        $token = new Token(uniqid(__METHOD__, true));
        $store = $this->getMockBuilder(Storage::class)->getMock();
        $mutex = $this->getMockBuilder(Mutex::class)
        ->disableOriginalConstructor()
        ->setConstructorArgs([$store, $token])
        ->getMock();

        $mutex->expects(self::once())
        ->method('isolate')
        ->willReturnCallback(function (callable $block) {
            return $block();
        });

        $chain = new ChainedCondition($mutex);
        $chain->try(function (): bool {
            return true;
        });

        $executed = false;
        $result = $chain->then(function () use (&$executed) {
            $executed = true;

            return 'protected';
        });

        self::assertTrue($executed);
        self::assertEquals('protected', $result);
    }
}
