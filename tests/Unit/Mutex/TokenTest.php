<?php

namespace Eternity\Tests\Unit\Mutex;

use Eternity\Mutex\Token;
use PHPUnit\Framework\TestCase;

class TokenTest extends TestCase
{
    public function testReset()
    {
        $token = new Token(uniqid(__METHOD__, true));
        $token->reset();

        self::assertNull($token->timeout());
    }

    /**
     * @dataProvider timeouts
     */
    public function testTtl($timeout, $sleep, $expected)
    {
        $token = new Token(uniqid(__METHOD__, true), $timeout);

        usleep($sleep * 1000);

        self::assertLessThan($expected, $token->ttl());
    }

    public function timeouts()
    {
        yield [3, 200, 2.80];
        yield [2, 150, 1.85];
        yield [1, 100, 0.9];
        yield [0.5, 50, 0.45];
        yield [0.1, 25, 0.075];
    }

    /**
     * @dataProvider expirations
     */
    public function testIsExpired($timeout, $sleep, $expected)
    {
        $token = new Token(uniqid(__METHOD__, true));

        if ($timeout === null)
        {
            $token->reset();;
        } else {
            $token->refresh($timeout);
            usleep($sleep * 1000);
        }

        self::assertSame($expected, $token->expired());
    }

    public function expirations()
    {
        yield [0, 0, true];
        yield [0.05, 50, true];
        yield [0.15, 150, true];
        yield [0.1, 100, true];

        yield [null, 0, false];
        yield [0.1, 50, false];
        yield [0.2, 100, false];
    }
}
