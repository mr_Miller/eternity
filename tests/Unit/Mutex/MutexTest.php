<?php

namespace Eternity\Tests\Unit\Mutex;

use RuntimeException;
use PHPUnit\Framework\TestCase;

use Eternity\Mutex\Contracts\Storage;
use Eternity\Mutex\Exceptions\LockAcquiringException;
use Eternity\Mutex\Exceptions\LockReleasingException;
use Eternity\Mutex\Exceptions\LockStorageException;
use Eternity\Mutex\Mutex;
use Eternity\Mutex\Token;

class MutexTest extends TestCase
{
    public function testAcquireReturnsTrue()
    {
        $token = new Token(uniqid(__METHOD__, true));
        $store = $this->getMockBuilder(Storage::class)->getMock();
        $mutex = new Mutex($store, $token);

        $store
            ->expects(self::once())
            ->method('store');
        $store
            ->expects(self::once())
            ->method('exists')
            ->willReturn(true);

        static::assertTrue($mutex->acquire());
    }

    public function testAcquireReturnsFalse()
    {
        $token = new Token(uniqid(__METHOD__, true));
        $store = $this->getMockBuilder(Storage::class)->getMock();
        $mutex = new Mutex($store, $token);

        $store
            ->expects(self::once())
            ->method('store')
            ->willThrowException(new LockStorageException());
        $store
            ->expects(self::once())
            ->method('exists')
            ->willReturn(true);

        self::assertFalse($mutex->acquire());
    }

    public function testAcquireThrowsException()
    {
        $this->expectException(LockAcquiringException::class);

        $token = new Token(uniqid(__METHOD__, true));
        $store = $this->getMockBuilder(Storage::class)->getMock();
        $mutex = new Mutex($store, $token);

        $store
            ->expects(self::once())
            ->method('store')
            ->willThrowException(new RuntimeException());
        $store
            ->expects(self::once())
            ->method('exists')
            ->willReturn(true);

        $mutex->acquire();
    }

    public function testAcquireCustom()
    {
        $token = new Token(uniqid(__METHOD__, true));
        $store = $this->getMockBuilder(Storage::class)->getMock();
        $mutex = new Mutex($store, $token);

        $store
            ->expects(self::atLeast(5))
            ->method('store')
            ->willThrowException(new LockStorageException());

        $mutex->acquire(1);
    }

    public function testRefresh()
    {
        $token = new Token(uniqid(__METHOD__, true));
        $store = $this->getMockBuilder(Storage::class)->getMock();
        $mutex = new Mutex($store, $token);

        $store
            ->expects(self::once())
            ->method('store')
            ->with($token);
        $store
            ->expects(self::once())
            ->method('update')
            ->with($token);
        $store
            ->expects(self::once())
            ->method('exists')
            ->willReturn(true);

        $timeout = 30;
        $mutex->acquire();
        $mutex->refresh($timeout);

        self::assertEquals($timeout, $token->timeout());
    }

    public function testIsAquired()
    {
        $token = new Token(uniqid(__METHOD__, true));
        $store = $this->getMockBuilder(Storage::class)->getMock();
        $mutex = new Mutex($store, $token);

        $store
            ->expects(self::exactly(2))
            ->method('exists')
            ->with($token)
            ->willReturn(true);

        self::assertTrue($mutex->acquired());
    }

    public function testRelease()
    {
        $token = new Token(uniqid(__METHOD__, true));
        $store = $this->getMockBuilder(Storage::class)->getMock();
        $mutex = new Mutex($store, $token);

        $mutex->acquire();

        $store
            ->expects(self::once())
            ->method('delete')
            ->with($token);
        $store
            ->expects(self::once())
            ->method('exists')
            ->with($token)
            ->willReturn(false);

        $mutex->release();
    }

    public function testReleaseOnDestruction()
    {
        $token = new Token(uniqid(__METHOD__, true));
        $store = $this->getMockBuilder(Storage::class)->getMock();
        $mutex = new Mutex($store, $token);

        $store
            ->expects(self::once())
            ->method('exists')
            ->willReturn(true);
        $store
            ->expects(self::once())
            ->method('delete');

        $mutex->acquire();
        unset($mutex);
    }

    public function testReleaseThrowsExceptionIfNotWellDeleted()
    {
        $this->expectException(LockReleasingException::class);

        $token = new Token(uniqid(__METHOD__, true));
        $store = $this->getMockBuilder(Storage::class)->getMock();
        $mutex = new Mutex($store, $token);

        $mutex->acquire();

        $store
            ->expects(self::once())
            ->method('delete')
            ->with($token)
            ->willThrowException(new RuntimeException('StorageError'));
        $store
            ->expects(self::once())
            ->method('exists')
            ->with($token)
            ->willReturn(false);

        $mutex->release();
    }

    public function testReleaseThrowsExceptionIfNotAquired()
    {
        $this->expectException(LockReleasingException::class);

        $token = new Token(uniqid(__METHOD__, true));
        $store = $this->getMockBuilder(Storage::class)->getMock();
        $mutex = new Mutex($store, $token);

        $store
            ->expects(self::never())
            ->method('delete')
            ->with($token);
        $store
            ->expects(self::once())
            ->method('exists')
            ->with($token)
            ->willReturn(false);

        $mutex->release();
    }

    public function testIsolateExecutesCallback()
    {
        $token = new Token(uniqid(__METHOD__, true));
        $store = $this->getMockBuilder(Storage::class)->getMock();
        $mutex = new Mutex($store, $token);

        $store
            ->expects(self::once())
            ->method('exists')
            ->with($token)
            ->willReturn(true);

        $expected = $mutex->isolate(function () {
            return 'isolated';
        });

        self::assertSame('isolated', $expected);
    }

    public function testIsolateExecutingCallbackThrowsException()
    {
        $this->expectException(RuntimeException::class);

        $token = new Token(uniqid(__METHOD__, true));
        $store = $this->getMockBuilder(Storage::class)->getMock();
        $mutex = new Mutex($store, $token);

        $store
            ->expects(self::once())
            ->method('exists')
            ->with($token)
            ->willReturn(true);

        $mutex->isolate(function () {
            throw new RuntimeException('Isolated callback error');
        });
    }

    public function testConditionExecutesCallback()
    {
        $token = new Token(uniqid(__METHOD__, true));
        $store = $this->getMockBuilder(Storage::class)->getMock();
        $mutex = new Mutex($store, $token);

        $store
            ->expects(self::once())
            ->method('exists')
            ->with($token)
            ->willReturn(true);

        $expected = $mutex->condition(function () {
            return true;
        })->then(function () {
            return 'conditional';
        });

        self::assertSame('conditional', $expected);
    }
}
