<?php

namespace Eternity\Tests\Unit\Components\Connector\Notification\Firebase;

use Eternity\Components\Connector\Notification\Firebase\Message;
use PHPUnit\Framework\TestCase;

/**
 * Class MessageTest
 * @package Eternity\Tests\Unit\Components\Connector\Notification\Firebase
 */
class MessageTest extends TestCase
{
    /**
     * @throws \Eternity\Exceptions\EternityException
     */
    public function testBuilderSuccess(): void
    {
        $inputData = [
            'token'        => null,
            'data'         => null,
            'uid'          => 5,
            'notification' => [
                'title' => 'title',
                'body'  => 'body',
            ],
            'android'      => [
                'priority' => 'high',
            ],
            'apns'         => [
                'headers' => [
                    'apns-priority' => '10'
                ],
            ],
        ];
        $notification = Message::fromArray($inputData);
        $this->assertEquals($inputData, $notification->toArray());
    }
}