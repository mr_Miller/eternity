<?php

namespace Eternity\Tests\Unit\Components\File;

use Eternity\Components\File\Thumbnails\ThumbnailGenerator;
use PHPUnit\Framework\TestCase;

/**
 * Class ThumbnailGeneratorTest
 * @package Eternity\Tests\Unit\Components\File
 */
class ThumbnailGeneratorTest extends TestCase
{
    /**
     * @var \Eternity\Components\File\Thumbnails\ThumbnailGenerator
     */
    private $generator;

    /**
     * @var string
     */
    private $photoPath;

    /**
     * @var array
     */
    private $sizes;

    /**
     * Set up
     */
    protected function setUp(): void
    {
        $host = 'http://cdn-test.net';
        $this->photoPath = 'photo-path';
        $this->sizes = [
            '200x200',
            '300x300',
            '400x400',
        ];
        $thumbnailPrefix = 'resized';
        $thumbnailSizeList = [
            $this->photoPath => $this->sizes
        ];
        $this->generator = new ThumbnailGenerator($host, $thumbnailSizeList, 'resized');
    }

    public function testGetThumbnailsSuccess()
    {
        $entityId = 1;
        $filename = 'sdifsidof.jpg';
        // Testing
        $thumbnails = $this->generator->getThumbnails($entityId, $filename, $this->photoPath);
        $this->assertCount(count($this->sizes), $thumbnails);
        $this->assertEquals(
            'http://cdn-test.net/resized/200x200/photo-path/1/sdifsidof.jpg',
            $thumbnails[ThumbnailGenerator::SIZE_PREFIX . '200x200']
        );
        $this->assertEquals(
            'http://cdn-test.net/resized/300x300/photo-path/1/sdifsidof.jpg',
            $thumbnails[ThumbnailGenerator::SIZE_PREFIX . '300x300']
        );
        $this->assertEquals(
            'http://cdn-test.net/resized/400x400/photo-path/1/sdifsidof.jpg',
            $thumbnails[ThumbnailGenerator::SIZE_PREFIX . '400x400']
        );
    }
}