<?php

namespace Eternity\Tests\Unit\Components\Definition;

use Eternity\Components\Definition\Exceptions\DefinitionException;
use PHPUnit\Framework\TestCase;

/**
 * Class AbstractDefinitionTest
 * @package Tests\Unit
 */
class AbstractDefinitionTest extends TestCase
{
    /**
     * @var \Eternity\Components\Definition\AbstractDefinition|\Eternity\Tests\Unit\Components\Definition\DefinitionMock
     */
    protected $definitionMock;

    /**
     *
     */
    protected function setUp(): void
    {
        parent::setUp();
        // Init DefinitionMock which mocks AbstractDefinition class
        $this->definitionMock = DefinitionMock::class;
    }

    /**
     *
     */
    public function testGetConstList(): void
    {
        $this->assertEquals([1, 2], $this->definitionMock::getConstList());
    }

    /**
     * @throws \Eternity\Exceptions\ServerException
     */
    public function testGetTitle(): void
    {
        $this->assertEquals('Deleted status', $this->definitionMock::getTitle(2));
    }

    /**
     * @throws \Eternity\Exceptions\ServerException
     */
    public function testGetNotDefinedTitleThrowsError(): void
    {
        $this->expectException(DefinitionException::class);
        $this->definitionMock::getTitle(3);
    }

    public function testGetTitleList(): void
    {
        $expected = [
            1 => 'Active status',
            2 => 'Deleted status'
        ];
        $this->assertEquals($expected, $this->definitionMock::getTitleList());
    }

    public function testGetTranslationKeyList(): void
    {
        $expected = [
            1 => 'service.definition.testCase.active',
            2 => 'service.definition.testCase.deleted',
        ];
        $this->assertEquals($expected, $this->definitionMock::getTranslationWithIdList());
    }

    public function testGetsTranslationList(): void
    {
        $expected = [
            0 => 'service.definition.testCase.active',
            1 => 'service.definition.testCase.deleted',
        ];
        $this->assertEquals($expected, $this->definitionMock::getTranslationsList());
    }

    /**
     * @throws \Eternity\Exceptions\ServerException
     */
    public function testGetTranslationKey(): void
    {
        $expected = 'service.definition.testCase.deleted';
        $this->assertEquals($expected, $this->definitionMock::getTranslationKey(2));
    }

}
