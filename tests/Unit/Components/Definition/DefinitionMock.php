<?php

namespace Eternity\Tests\Unit\Components\Definition;

use Eternity\Components\Definition\AbstractDefinition;

/**
 * Class DefinitionExample
 * @package Tests\Unit\Definition
 */
class DefinitionMock extends AbstractDefinition
{
    public static $mockData = [
        1 => [
            'id' => 1,
            'title' => 'Active status',
            'translation_key' => 'service.definition.testCase.active',
        ],
        2 => [
            'id' => 2,
            'title' => 'Deleted status',
            'translation_key' => 'service.definition.testCase.deleted',
        ],
    ];

    /**
     * @inheritDoc
     */
    protected static function getList(): array
    {
        // this method returns mocked data
        return static::$mockData;
    }
}
