<?php

namespace Eternity\Tests\Unit\Resource\Objects\FilterCollection;

use Eternity\Resource\Objects\Filter\FilterCollection;
use PHPUnit\Framework\TestCase;

/**
 * Class FilterCollectionTest
 * @package Eternity\Tests\Unit\Resource\Objects\FilterCollection
 */
class FilterCollectionTest extends TestCase
{
    public function testToJsonMethodSuccess(): void
    {
        $filterArray = ['type' => 'equals', 'key' => 'id', 'value' => 1];
        $filterCollection = new FilterCollection([$filterArray]);
        $this->assertEquals($filterCollection->toJson(), json_encode($filterCollection->toArray()));
    }

    public function testToStringMethodSuccess(): void
    {
        $filterArray = ['type' => 'equals', 'key' => 'id', 'value' => 1];
        $filterCollection = new FilterCollection([$filterArray]);
        $this->assertIsString((string)$filterCollection);
    }
}