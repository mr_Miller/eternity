<?php

namespace Eternity\Tests\Unit\Trace;

use Eternity\Exceptions\ErrorCodes;
use Eternity\Trace\Exceptions\TraceIdException;
use Eternity\Trace\TraceId;
use PHPUnit\Framework\TestCase;

/**
 * Class TraceIdTest
 * @package Eternity\Tests\Unit\Trace
 */
class TraceIdTest extends TestCase
{
    /**
     * @var \Eternity\Trace\TraceId
     */
    private $traceId;

    protected function setUp(): void
    {
        $this->traceId = new TraceId();
        parent::setUp();
    }

    public function testValueThrowsException(): void
    {
        $this->expectException(TraceIdException::class);
        $this->expectExceptionCode(ErrorCodes::TRACE_ID_INIT_ERROR);
        $this->traceId->value();
    }

    /**
     * Set value in private property "value"
     * @param string $value
     * @throws \ReflectionException
     */
    private function setValue(string $value): void
    {
        $reflection = new \ReflectionClass($this->traceId);
        $reflectionProperty = $reflection->getProperty('value');
        $reflectionProperty->setAccessible(true);
        $reflectionProperty->setValue($this->traceId, $value);
    }

    /**
     * @throws \Eternity\Trace\Exceptions\TraceIdException
     * @throws \ReflectionException
     */
    public function testGenerateThrowsException(): void
    {
        $this->setValue('some value');
        $this->expectException(TraceIdException::class);
        $this->expectExceptionCode(ErrorCodes::TRACE_ID_GENERATION_OVERRIDE);
        $this->traceId->generate('its an error');
    }

    /**
     * @throws \Eternity\Trace\Exceptions\TraceIdException
     * @throws \ReflectionException
     */
    public function testInit(): void
    {
        $this->traceId->init('traceId');
        $reflection = new \ReflectionClass($this->traceId);
        $reflectionProperty = $reflection->getProperty('value');
        $reflectionProperty->setAccessible(true);
        $this->assertEquals('traceId', $reflectionProperty->getValue($this->traceId));
    }

    /**
     * @throws \Eternity\Trace\Exceptions\TraceIdException
     * @throws \ReflectionException
     */
    public function testValue(): void
    {
        $this->setValue('traceId');
        $this->assertEquals('traceId', $this->traceId->value());
    }

    /**
     * @throws \Eternity\Trace\Exceptions\TraceIdException
     * @throws \ReflectionException
     */
    public function testGenerate(): void
    {
        $this->traceId->generate('prefix');
        $reflection = new \ReflectionClass($this->traceId);
        $reflectionProperty = $reflection->getProperty('value');
        $reflectionProperty->setAccessible(true);
        $this->assertStringContainsString('prefix', $reflectionProperty->getValue($this->traceId));
    }

}