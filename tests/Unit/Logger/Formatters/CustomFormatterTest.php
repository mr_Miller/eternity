<?php

namespace Eternity\Tests\Unit\Logger\Formatters;

use Eternity\Logger\Formatters\CustomFormatter;
use PHPUnit\Framework\TestCase;

/**
 * Class CustomFormatterTest
 * @package Eternity\Tests\Unit\Logger\Formatters
 */
class CustomFormatterTest extends TestCase
{
    /**
     * @var CustomFormatter
     */
    private $formatter;

    public function setUp(): void
    {
        $this->formatter = new CustomFormatter('%msg-code%:%msg%');
    }

    public function testFormat()
    {
        $record = [
            'message' => '[100001] Hello!',
            'context' => [
                'entity-type' => 'instance',
                'entity-id' => 234
            ],
            'level' => 300,
            'level_name' => 'WARN',
            'channel' => 'some-service',
            'datetime' => new \DateTime('2019-05-29 07:40:18'),
            'extra' => [],
            'timestamp' => '2019-05-29T07:40:18',
            'component' => 'some-service',
            'severity' => 'WARN',
            'trace-id' => 'gw_13b80a3351c2be6d2f5221a13768c0cf',
            'msg' => 'Hello!',
            'msg-code' => '100001',
            'entity-type' => 'instance',
            'entity-id' => ''
        ];

        $this->assertEquals(
            "100001:Hello!" . PHP_EOL,
            $this->formatter->format($record)
        );
    }
}