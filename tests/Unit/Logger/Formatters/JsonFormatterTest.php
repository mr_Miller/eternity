<?php
/**
 * TMS: Maintenance Manager
 */

namespace Eternity\Tests\Unit\Logger\Formatters;

use Eternity\Logger\Formatters\JsonFormatter;
use PHPUnit\Framework\TestCase;

/**
 * Class JsonFormatterTest
 */
class JsonFormatterTest extends TestCase
{
    /**
     * @var JsonFormatter
     */
    private $formatter;

    public function setUp(): void
    {
        $this->formatter = new JsonFormatter(
            "{\"timestamp\":\"%timestamp%\",\"component\":\"%component%\",\"severity\":\"%severity%\",\"msg-code\":\"%msg-code%\",\"msg\":\"%msg%\",\"trace-id\":\"%trace-id%\",\"entity-type\":\"%entity-type%\",\"entity-id\":\"%entity-id%\",\"stack-trace\":\"%stack-trace%\"}"
        );
    }

    /**
     */
    public function testFormatInvalidFormatThrowsException()
    {
        $this->expectExceptionMessage("Cannot format log message. Not a valid json format specified in configuration");
        $this->expectException(\Exception::class);
        (new JsonFormatter("{bad-json:"))->format(['message' => '[100001] Hello!']);
    }

    public function testFormat()
    {
        $record = [
            'message' => '[100001] Hello!',
            'context' => [
                'entity-type' => 'instance',
                'entity-id' => 234
            ],
            'level' => 300,
            'level_name' => 'WARN',
            'channel' => 'some-service',
            'datetime' => new \DateTime('2019-05-29 07:40:18'),
            'extra' => [],
            'timestamp' => '2019-05-29T07:40:18',
            'component' => 'some-service',
            'severity' => 'WARN',
            'trace-id' => 'tms_13b80a3351c2be6d2f5221a13768c0cf',
            'msg' => 'Hello!',
            'msg-code' => 'TMS-100001',
            'entity-type' => 'instance',
            'entity-id' => ''
        ];

        $this->assertEquals(
            "{\"timestamp\":\"2019-05-29T07:40:18\",\"component\":\"some-service\",\"severity\":\"WARN\",\"msg-code\":\"TMS-100001\",\"msg\":\"Hello!\",\"trace-id\":\"tms_13b80a3351c2be6d2f5221a13768c0cf\",\"entity-type\":\"instance\"}" . PHP_EOL,
            $this->formatter->format($record)
        );
    }

    public function testFormatStackTrace()
    {
        $stackTrace = "#0 /var/www/tms/mm/vendor/TMS/Framework/lib/Api/HttpController.php(47): " .
            "TMS\MM\Modules\V1\Controllers\ApiVersionController->getAction() " .
            "#1 /var/www/tms/mm/vendor/TMS/Framework/lib/Api/Application.php(201): TMS\Framework\Api\HttpController->__call(\'getAction\', Array) " .
            "#2 /var/www/tms/mm/wwwroot/index.php(22): TMS\Framework\Api\Application->run() #3 {main})";

        $expectedStackTrace = "#0 /var/www/tms/mm/vendor/TMS/Framework/lib/Api/HttpController.php(47): " .
            "TMS\\\MM\\\Modules\\\V1\\\Controllers\\\ApiVersionController->getAction() " .
            "#1 /var/www/tms/mm/vendor/TMS/Framework/lib/Api/Application.php(201): TMS\\\Framework\\\Api\\\HttpController->__call(\\\'getAction\\\', Array) " .
            "#2 /var/www/tms/mm/wwwroot/index.php(22): TMS\\\Framework\\\Api\\\Application->run() #3 {main})";

        $record = [
            'message' => '[100001] Hello!',
            'context' => [
                'entity-type' => 'instance',
                'entity-id' => 234,
            ],
            'level' => 300,
            'level_name' => 'WARN',
            'channel' => 'some-service',
            'datetime' => new \DateTime('2019-05-29 07:40:18'),
            'extra' => [],
            'timestamp' => '2019-05-29T07:40:18',
            'component' => 'some-service',
            'severity' => 'WARN',
            'trace-id' => 'tms_13b80a3351c2be6d2f5221a13768c0cf',
            'msg' => 'Hello!',
            'msg-code' => 'TMS-100001',
            'entity-type' => 'instance',
            'entity-id' => 234,
            'stack-trace' => $stackTrace
        ];

        $this->assertEquals(
            '{"timestamp":"2019-05-29T07:40:18","component":"some-service","severity":"WARN","msg-code":"TMS-100001","msg":"Hello!","trace-id":"tms_13b80a3351c2be6d2f5221a13768c0cf","entity-type":"instance","entity-id":"234","stack-trace":"' . $expectedStackTrace . '"}' . PHP_EOL,
            $this->formatter->format($record)
        );
    }
}