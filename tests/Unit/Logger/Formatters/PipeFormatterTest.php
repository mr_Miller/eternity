<?php
/**
 * TMS: Maintenance Manager
 */

namespace Eternity\Tests\Unit\Logger\Formatters;

use DateTime;
use Eternity\Logger\Formatters\PipeFormatter;
use PHPUnit\Framework\TestCase;

/**
 * Class LineFormatterTest
 */
class PipeFormatterTest extends TestCase
{
    /**
     * @return array
     */
    public function dataProvider()
    {
        return [
            [
                "%timestamp%|%component%|%severity%|%msg-code%|%msg%|%trace-id%|%entity-type%|%entity-id%|%stack-trace%",
                "2019-05-29T07:40:18|some-service|WARN|TMS-100001|Hello!|tms_13b80a3351c2be6d2f5221a13768c0cf|instance|234|Raw stack trace" . PHP_EOL
            ],
            [
                "%trace-id%|%timestamp%|%msg%",
                "tms_13b80a3351c2be6d2f5221a13768c0cf|2019-05-29T07:40:18|Hello!" . PHP_EOL
            ],
            [
                '{"timestamp":"%timestamp%","component":"%component%","severity":"%severity%","msg-code":"%msg-code%","msg":"%msg%","trace-id":"%trace-id%","entity-type":"%entity-type%","entity-id":"%entity-id%"}',
                '{"timestamp":"2019-05-29T07:40:18","component":"some-service","severity":"WARN","msg-code":"TMS-100001","msg":"Hello!","trace-id":"tms_13b80a3351c2be6d2f5221a13768c0cf","entity-type":"instance","entity-id":"234"}' . PHP_EOL
            ],
            [
                // Assuming that we are in debug mode (APP_DEBUG = true)
                "%trace-id%|%timestamp%|%msg%|%raw-stack-trace%",
                "tms_13b80a3351c2be6d2f5221a13768c0cf|2019-05-29T07:40:18|Hello!|Raw stack trace" . PHP_EOL
            ],
        ];
    }

    /**
     * @dataProvider dataProvider
     *
     * @param string $format
     * @param string $expected
     * @throws \Exception
     */
    public function testFormat(string $format, string $expected)
    {
        $record = [
            'message' => '[100001] Hello!',
            'context' => [
                'entity-type' => 'instance',
                'entity-id' => 234
            ],
            'level' => 300,
            'level_name' => 'WARN',
            'channel' => 'some-service',
            'datetime' => new DateTime('2019-05-29 07:40:18'),
            'extra' => [],
            'timestamp' => '2019-05-29T07:40:18',
            'component' => 'some-service',
            'severity' => 'WARN',
            'trace-id' => 'tms_13b80a3351c2be6d2f5221a13768c0cf',
            'msg' => 'Hello!',
            'msg-code' => 'TMS-100001',
            'entity-type' => 'instance',
            'entity-id' => 234,
            'stack-trace' => 'Raw stack trace'
        ];

        $this->assertEquals(
            $expected,
            (new PipeFormatter($format, null, true))->format($record)
        );
    }
}