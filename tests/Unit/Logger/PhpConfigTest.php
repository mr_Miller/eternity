<?php
/**
 * TMS: Maintenance Manager
 */

namespace Eternity\Tests\Unit\Logger;

use Eternity\Definitions\ApplicationMode;
use Eternity\Exceptions\ErrorCodes;
use Eternity\Logger\Config\PhpConfig;
use Eternity\Logger\Exceptions\LoggerException;
use Eternity\Messages\Messages;
use Monolog\Formatter\FormatterInterface;
use Monolog\Handler\HandlerInterface;
use Monolog\Handler\SyslogHandler;
use PHPUnit\Framework\TestCase;

/**
 * Class ConfigTest
 */
class PhpConfigTest extends TestCase
{
    const ANY_HANDLER_PATTERN = '#^handler.*#';

    const DB_HANDLER_PATTERN = '#^database_handler.*#';

    const HTTP_HANDLER_PATTERN = '#^http_handler.*#';

    /**
     * @var \Eternity\Logger\Interfaces\LogConfig
     */
    private $config;

    /**
     * @throws \Exception
     */
    public function setUp(): void
    {
        if (!defined('COMPONENT_NAME')) {
            define('COMPONENT_NAME', 'some-service');
        }

        $this->config = new PhpConfig(ApplicationMode::MODE_API, $this->config('config'));
    }

    /**
     * Returns config
     * @param string $configFile
     * @return array
     */
    private function config(string $configFile): array
    {
        return include __DIR__ . '/config/' . $configFile . '.php';
    }

    public function testDebugReturnsTrue(): void
    {
        $this->assertTrue($this->config->debug());
    }

    /**
     * @throws \Exception
     */
    public function testDefaultHandlersWhenHandlersAreEmpty()
    {
        $config = new PhpConfig(ApplicationMode::MODE_API, $this->config('config-without-handlers'));
        $handlers = $config->handlers('general');
        $this->assertContainsOnlyInstancesOf(SyslogHandler::class, $handlers);
        $this->assertCount(1, $handlers);
    }

    /**
     * @throws \Eternity\Logger\Exceptions\LoggerException
     */
    public function testEmptyHandlerThrowError()
    {
        $this->expectException(LoggerException::class);
        $this->expectExceptionCode(ErrorCodes::LOGGER_CONFIG_HANDLERS_EMPTY);
        $this->expectExceptionMessage('Logger error. ' . Messages::message(ErrorCodes::LOGGER_CONFIG_HANDLERS_EMPTY));
        new PhpConfig(ApplicationMode::MODE_API, $this->config('config-handlers-empty'));
    }

    /**
     * @throws \Exception
     */
    public function testChannel()
    {
        $this->assertEquals(
            'gw-api',
            $this->config->channel()
        );
    }

    /**
     * @return array
     */
    public function badHandlersDataProvider()
    {
        return [
            [
                'config-handler-without-type',
                "Required parameter 'type' is not set for the 'handler1' handler in logger configuration"
            ],
            [
                'config-handler-without-file',
                "Required parameter 'file' is not set for the 'handler1' file handler in logger configuration"
            ],
            [
                'config-handler-unsupported-type',
                "Undefined handler type 'unsupported' has been found in logger configuration"
            ],
            [
                'config-handler-unsupported-formatter',
                "'handler1' handler formatters contain disallowed value 'unsupported'"
            ],
            [
                'config-handler-incorrect-formatter',
                "Formatter is set incorrectly. Check keys in 'formatters' section"
            ],
            [
                'config-handler-telegram-without-api-key',
                "Required parameter 'api_key' is not set for the 'telegram-bot' handler in logger configuration"
            ]
        ];
    }

    /**
     * @dataProvider badHandlersDataProvider
     *
     * @param string $configName
     * @param string $exceptionMessage
     * @throws \Eternity\Logger\Exceptions\LoggerException
     */
    public function testBadHandlersConfiguration(string $configName, string $exceptionMessage)
    {
        $this->expectException(\Exception::class);
        $this->expectExceptionMessage($exceptionMessage);

        (new PhpConfig(ApplicationMode::MODE_API, $this->config($configName)))->handlers('general');
    }

    /**
     * @throws \Exception
     */
    public function testHandlers()
    {
        $handlers = $this->config->handlers('general');

        $this->assertContainsOnlyInstancesOf(HandlerInterface::class, $handlers);
        $this->assertCount(2, $handlers);
    }

    /**
     * @return array
     */
    public function configDataProvider(): array
    {
        return [
            ['config'],
            ['config-without-formatters'],
            ['config-handler-without-formatter'],
            ['config-with-custom-formatters'],
        ];
    }

    /**
     * @dataProvider configDataProvider
     *
     * @param string $configName
     * @throws \Eternity\Logger\Exceptions\LoggerException
     */
    public function testHandlerFormatter(string $configName)
    {
        /** @var HandlerInterface $handler */
        $config = (new PhpConfig(ApplicationMode::MODE_API, $this->config($configName)));
        $handler = $config->handlers('general')['handler1'];

        $this->assertInstanceOf(FormatterInterface::class, $handler->getFormatter());
    }
}