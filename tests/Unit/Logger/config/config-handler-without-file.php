<?php
return [
    'debug'      => true,
    'handlers'   => [
        'general'   => [
            'handler1' => [
                // String to identify the source of the log message. Default value is 'gw'
                'type'      => 'file',
                'ident'     => 'gw',
                'level'     => 'info',
                // Allowed formatters: pipe, json
                'formatter' => 'pipe',
                'facility'  => 'user',
                'pipe'      => '%timestamp%|%component%|%severity%|%msg-code%|%msg%|%trace-id%|%entity-type%|%entity-id%|%stack-trace%',
            ],
        ],
    ],
    'formatters' => [],
    'component'  => [
        // String to identify the source of the log message. Default value is 'gw'
        // (Microservice name)
        'name'   => 'gw',
    ]
];