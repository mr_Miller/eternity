<?php
return [
    'debug'      => true,
    'handlers'   => [
        'general' => [
            'telegram-bot' => [
                'type'    => 'telegram',
                'level'   => 'critical',
                'channel' => '234234234',
                // No trace if send error via telegram
                'pipe'    => '%timestamp%|%component%|%severity%|%msg-code%|%msg%|%trace-id%|%entity-type%|%entity-id%',
                // String to identify the source of the log message. Default value is 'gw'
                'ident'   => 'ua',
            ],
        ],
    ],
    'formatters' => [],
    'component'  => [
        // String to identify the source of the log message. Default value is 'gw'
        // (Microservice name)
        'name' => 'gw',
    ],
];