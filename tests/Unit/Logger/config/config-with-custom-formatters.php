<?php
return [
    'debug'      => true,
    'handlers'   => [
        'general'   => [
            'handler1' => [
                // String to identify the source of the log message. Default value is 'gw'
                'type'      => 'file',
                'file'      => '/var/www/eternity/test/mono.log',
                'ident'     => 'gw',
                'level'     => 'info',
                // Allowed formatters: pipe, json
                'formatter' => 'custom',
                'facility'  => 'user',
            ],
        ],
    ],
    'formatters' => [
        'custom' => '%timestamp%|%component%|%severity%|%msg-code%|%msg%'
    ],
    'component'  => [
        // String to identify the source of the log message. Default value is 'gw'
        // (Microservice name)
        'name'   => 'gw',
    ]
];