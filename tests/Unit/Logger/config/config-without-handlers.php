<?php
return [
    'debug'      => true,
    'handlers'   => [
        'general'   => [],
    ],
    'formatters' => [],
    'component'  => [
        // String to identify the source of the log message. Default value is 'gw'
        // (Microservice name)
        'name'   => 'gw',
    ]
];