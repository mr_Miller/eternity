<?php

namespace Eternity\Tests\Unit\Logger\Stubs;

use Eternity\Logger\Interfaces\LogConfig;

/**
 * Class StubConfigWithoutHandlers
 * @package Eternity\Tests\Unit\Logger\Stubs
 */
class StubConfigWithoutHandlers implements LogConfig
{
    /**
     * @return string
     */
    public function channel(): string
    {
        return 'stub-component';
    }

    /**
     * @return bool
     */
    public function debug(): bool
    {
        return true;
    }

    /**
     * @param string $type
     * @return array
     */
    public function handlers(string $type): array
    {
        return [];
    }
}