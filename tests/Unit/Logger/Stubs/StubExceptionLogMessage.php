<?php

namespace Eternity\Tests\Unit\Logger\Stubs;

use Eternity\Logger\Messages\ExceptionLogMessage;

/**
 * Class StubExceptionLogMessage
 */
class StubExceptionLogMessage extends ExceptionLogMessage
{
    /**
     * StubExceptionLogMessage constructor.
     * @param \Throwable|null $exception
     */
    public function __construct(\Throwable $exception = null)
    {
        parent::__construct(
            $exception
            ?? new \Exception(
                "Stub exception log message",
                8888888888888,
                new \Exception(
                    "Previous exception log message",
                    7777777777777
                )
            )
        );
    }
}