<?php

namespace Eternity\Tests\Unit\Logger\Stubs;

/**
 * Class StubEntityConvertableToString
 * @package Eternity\Tests\Unit\Logger\Stubs
 */
class StubEntityConvertableToString
{
    /**
     * @return string
     */
    public function __toString()
    {
        return 'Stub entity convertable to string';
    }
}