<?php
/**
 * TMS: Maintenance Manager
 */

namespace Eternity\Tests\Unit\Logger\Stubs;

/**
 * Class StubEntityWithPrimaryKey
 */
class StubEntityWithLabel
{
    /**
     * @return string
     */
    public function getLabel()
    {
        return 'abcdef';
    }
}