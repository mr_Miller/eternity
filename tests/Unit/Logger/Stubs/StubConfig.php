<?php
/**
 * TMS: Maintenance Manager
 */

namespace Eternity\Tests\Unit\Logger\Stubs;

use DateTime;
use Eternity\Logger\DbLogger;
use Eternity\Logger\Formatters\PipeFormatter;
use Eternity\Logger\HttpLogger;
use Eternity\Logger\Interfaces\LogConfig;
use Monolog\Handler\StreamHandler;

/**
 * Class StubConfig
 */
class StubConfig implements LogConfig
{
    const PATH_TO_LOG = __DIR__ . '/stub.log';

    /**
     * @return string
     */
    public function channel(): string
    {
        return 'stub-component';
    }

    /**
     * @return bool
     */
    public function debug(): bool
    {
        return true;
    }

    /**
     * @param string $type
     * @return array
     */
    public function handlers(string $type): array
    {
        return [
            'handler1' => (new StreamHandler(self::PATH_TO_LOG, HttpLogger::DEBUG))
                ->setFormatter(
                    new PipeFormatter(
                        "%timestamp%|%component%|%severity%|%msg-code%|%msg%|%trace-id%|%entity-type%|%entity-id%|%stack-trace%",
                        DateTime::ISO8601
                    )
                ),
            'http_handler1' => (new StreamHandler(self::PATH_TO_LOG, HttpLogger::DEBUG))
                ->setFormatter(
                    new PipeFormatter(
                        "%timestamp%|%component%|%severity%|%msg-code%|%msg%|%trace-id%|%entity-type%|%entity-id%|%stack-trace%",
                        DateTime::ISO8601
                    )
                ),
            'database_handler1' => (new StreamHandler(self::PATH_TO_LOG, DbLogger::DEBUG))
                ->setFormatter(
                    new PipeFormatter(
                        "%timestamp%|%component%|%severity%|%msg-code%|%msg%|%trace-id%|%entity-type%|%entity-id%|%stack-trace%",
                        DateTime::ISO8601
                    )
                )
        ];
    }
}