<?php
/**
 * TMS: Maintenance Manager
 */

namespace Eternity\Tests\Unit\Logger\Stubs;

use Eternity\Contracts\PublicEntity;

/**
 * Class PublicEntity
 */
class StubPublicEntity implements PublicEntity
{
    /**
     * @return string
     */
    public function publicType(): string
    {
        return "publicEntityType";
    }

    /**
     * @return mixed
     */
    public function publicId(): string
    {
        return "abcdef";
    }
}