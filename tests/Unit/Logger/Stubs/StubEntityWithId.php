<?php
/**
 * TMS: Maintenance Manager
 */

namespace Eternity\Tests\Unit\Logger\Stubs;

/**
 * Class StubEntityWithId
 */
class StubEntityWithId
{
    /**
     * @var string
     */
    private $name;

    /**
     * StubEntityWithId constructor.
     */
    public function __construct()
    {
        $this->name = 'stub-id';
    }

    /**
     * @return string
     */
    public function getId()
    {
        return $this->name;
    }
}