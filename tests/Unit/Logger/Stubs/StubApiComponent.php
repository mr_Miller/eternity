<?php

namespace Eternity\Tests\Unit\Logger\Stubs;

use Eternity\Logger\Interfaces\Component;

/**
 * Class StubApiComponent
 * @package Eternity\Tests\Unit\Logger\Stubs
 */
class StubApiComponent implements Component
{
    /**
     * Returns component type
     * @return string
     */
    public function type(): string
    {
        return 'api';
    }
}