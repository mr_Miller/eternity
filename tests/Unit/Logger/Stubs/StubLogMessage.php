<?php

namespace Eternity\Tests\Unit\Logger\Stubs;

use Eternity\Logger\Messages\AbstractMessage;

/**
 * Class StubLogMessage
 */
class StubLogMessage extends AbstractMessage
{
    /**
     * @return string
     */
    public function code(): string
    {
        return 99999999999;
    }

    /**
     * @return string
     */
    public function message(): string
    {
        return "Stub message";
    }
}