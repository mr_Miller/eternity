<?php
/**
 * TMS: Maintenance Manager
 */

namespace Eternity\Tests\Unit\Logger\Processors;

use Eternity\Logger\Processors\MessageProcessor;
use Eternity\Tests\Unit\Logger\Stubs\StubLogMessage;
use PHPUnit\Framework\TestCase;

/**
 * Class MessageProcessorTest
 */
class MessageProcessorTest extends TestCase
{
    /**
     * @return array
     */
    /**
     * @return array
     */
    public function dataProvider()
    {
        return [
            [
                ['message' => ''], ''
            ],
            [
                ['message' => 'Message without code'], 'Message without code'
            ],
            [
                ['message' => "[[100500]] Test message"], 'Test message'
            ],
            [
                ['message' => new StubLogMessage()], "Stub message"
            ]
        ];
    }

    /**
     * @dataProvider dataProvider
     *
     * @param array $record
     * @param string $message
     * @throws \Exception
     */
    public function testProcessor(array $record, string $message)
    {
        $processor = new MessageProcessor();

        $this->assertEquals(
            $record + ['msg' => $message],
            $processor($record)
        );
    }
}