<?php
/**
 * TMS: Maintenance Manager
 */

namespace Eternity\Tests\Unit\Logger\Processors;

use Eternity\Logger\Processors\MessageCodeProcessor;
use Eternity\Tests\Unit\Logger\Stubs\StubLogMessage;
use PHPUnit\Framework\TestCase;

/**
 * Class MessageCodeProcessorTest
 */
class MessageCodeProcessorTest extends TestCase
{
    /**
     * @return array
     */
    /**
     * @return array
     */
    public function dataProvider()
    {
        return [
            [
                ['message' => ''], ''
            ],
            [
                ['message' => 'Message without code'], ''
            ],
            [
                ['message' => "[[100500]] Test message"], '100500'
            ],
            [
                ['message' => new StubLogMessage()], '99999999999'
            ]
        ];
    }

    /**
     * @dataProvider dataProvider
     *
     * @param array $record
     * @param string $messageCode
     * @throws \Exception
     */
    public function testProcessor(array $record, string $messageCode)
    {
        $processor = new MessageCodeProcessor();

        $this->assertEquals(
            $record + ['msg-code' => $messageCode],
            $processor($record)
        );
    }
}