<?php
/**
 * TMS: Maintenance Manager
 */

namespace Eternity\Tests\Unit\Logger\Processors;

use DateTime;
use Eternity\Logger\Logger;
use Eternity\Logger\Processors\CommonProcessor;
use PHPUnit\Framework\TestCase;

/**
 * Class CommonProcessorTest
 */
class CommonProcessorTest extends TestCase
{
    /**
     * @return array
     */
    public function dataProvider()
    {
        return [
            [Logger::DEBUG, 'DEBUG'],
            [Logger::INFO, 'INFO'],
            [Logger::WARNING, 'WARN'],
            [Logger::ERROR, 'ERROR'],
        ];
    }

    /**
     * @dataProvider dataProvider
     *
     * @param int $level
     * @param string $severity
     * @throws \Exception
     */
    public function testProcessor(int $level, string $severity)
    {
        $record = ['datetime' => new DateTime('2019-05-29 07:40:18'), 'level' => $level];

        $processor = new CommonProcessor('component-name');

        $this->assertEquals(
            $record + ['timestamp' => '2019-05-29T07:40:18+0000', 'component' => 'component-name', 'severity' => $severity],
            $processor($record)
        );
    }

    /**
     */
    public function testProcessUnsupportedLogLevel()
    {
        $this->expectException(\InvalidArgumentException::class);
        $processor = new CommonProcessor('component-name');

        $processor(['datetime' => new DateTime('2019-05-29 07:40:18'), 'level' => 5]);
    }
}