<?php
/**
 * TMS: Maintenance Manager
 */

namespace Eternity\Tests\Unit\Logger\Processors;

use Eternity\Logger\Processors\EntityIdProcessor;
use Eternity\Tests\Unit\Logger\Stubs\StubEntity;
use Eternity\Tests\Unit\Logger\Stubs\StubEntityConvertableToString;
use Eternity\Tests\Unit\Logger\Stubs\StubEntityWithId;
use Eternity\Tests\Unit\Logger\Stubs\StubEntityWithLabel;
use Eternity\Tests\Unit\Logger\Stubs\StubPublicEntity;
use PHPUnit\Framework\TestCase;

/**
 * Class EntityIdProcessor
 */
class EntityIdProcessorTest extends TestCase
{
    /**
     * @return array
     */
    public function dataProvider()
    {
        return [
            [
                ['context' => ['entity' => new StubPublicEntity()]],
                'abcdef',
                "By 'entity' field in context (entity implements PublicEntity interface)"
            ],
            [
                ['context' => ['entity' => new StubEntityWithLabel()]],
                'abcdef',
                "By 'entity' field in context (entity has 'getLabel' method)"
            ],
            [
                ['context' => ['entity' => new StubEntityWithId()]],
                'stub-id',
                "By 'entity' field in context (entity with id)"
            ],
            [
                ['context' => ['entity' => new StubEntity()]],
                '',
                "By 'entity' field in context (entity w/o id or primary key)"
            ],
            [
                ['context' => ['entity-id' => 'abcdef']],
                'abcdef',
                "By 'entity-id' field in context"
            ],
            [
                ['context' => ['entity' => new StubEntityConvertableToString()],],
                'Stub entity convertable to string',
                "By entity convertable to string"
            ],
            [
                ['context' => ['exception' => new \Exception("Exception message")]],
                '',
                "Be exception without entity"
            ],
            [
                ['message' => "Some message which does not contain any entity id"],
                '',
                'Default empty value when no information about entity id'
            ],
        ];
    }

    /**
     * @dataProvider dataProvider
     *
     * @param array $record
     * @param $entityId
     * @param string $message
     * @throws \Exception
     */
    public function testProcessor(array $record, $entityId, string $message)
    {
        $processor = new EntityIdProcessor();

        $this->assertEquals(
            $record + ['entity-id' => $entityId],
            $processor($record),
            $message
        );
    }

    /**
     */
    public function testBadEntityThrowsException()
    {
        $this->expectExceptionMessage("Entity passed to log must be an object");
        $this->expectException(\Exception::class);
        $processor = new EntityIdProcessor();

        $processor(['context' => ['entity' => 'non-object']]);
    }
}