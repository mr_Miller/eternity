<?php
/**
 * TMS: Maintenance Manager
 */

namespace Eternity\Tests\Unit\Logger\Processors;

use Eternity\Logger\Processors\StackTraceProcessor;
use PHPUnit\Framework\TestCase;

/**
 * Class StackTraceProcessorTest
 */
class StackTraceProcessorTest extends TestCase
{
    /**
     * @return array
     */
    public function providerProcessor(): array
    {
        return [
            [[], ['has-stack-trace' => false]],
            [
                ['exception' => 'message'],
                ['has-stack-trace' => false]
            ],
            [
                ['exception' => new \Exception()],
                ['has-stack-trace' => true]
            ],
            [
                ['exception' => new \Exception('message', 0, new \Exception())],
                ['has-stack-trace' => false],
            ],

        ];
    }

    /**
     * @dataProvider providerProcessor
     * @param array $context
     * @param array $asserts
     */
    public function testProcessor(array $context, array $asserts)
    {
        $record = ['context' => $context];

        $processor = new StackTraceProcessor();
        $record = $processor($record);

        if ($asserts['has-stack-trace'])
        {
            $this->assertArrayHasKey('stack-trace', $record);
            $this->assertIsString($record['stack-trace']);
        }
        else
        {
            $this->assertArrayNotHasKey('stack-trace', $record);
        }
    }
}