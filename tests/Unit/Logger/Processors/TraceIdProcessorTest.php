<?php
/**
 * TMS: Maintenance Manager
 */

namespace Eternity\Tests\Unit\Logger\Processors;

use Eternity\Logger\Processors\TraceIdProcessor;
use Eternity\Trace\TraceId;
use PHPUnit\Framework\TestCase;

/**
 * Class TraceIdProcessorTest
 */
class TraceIdProcessorTest extends TestCase
{
    /**
     * @var \Eternity\Trace\TraceId
     */
    private $traceId;

    /**
     *
     * @throws \Eternity\Trace\Exceptions\TraceIdException
     */
    protected function setUp(): void
    {
        parent::setUp();
        $this->traceId = new TraceId();
        $this->traceId->generate();
    }

    /**
     * @throws \Eternity\Trace\Exceptions\TraceIdException
     */
    public function testProcessor()
    {
        $record = [];
        $processor = new TraceIdProcessor($this->traceId);

        $this->assertEquals(
            $record + ['trace-id' => $this->traceId->value()],
            $processor($record)
        );
    }
}