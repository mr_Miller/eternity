<?php
/**
 * TMS: Maintenance Manager
 */

namespace Eternity\Tests\Unit\Logger\Processors;

use Eternity\Logger\Processors\EntityTypeProcessor;
use Eternity\Tests\Unit\Logger\Stubs\StubEntity;
use Eternity\Tests\Unit\Logger\Stubs\StubPublicEntity;
use PHPUnit\Framework\TestCase;

/**
 * Class EntityTypeTest
 */
class EntityTypeProcessorTest extends TestCase
{
    /**
     * @return array
     */
    public function dataProvider()
    {
        return [
            [
                ['context' => ['entity' => new StubPublicEntity()]],
                'publicEntityType',
                "By 'entity' field in context (entity implements PublicEntity interface)"
            ],
            [
                ['context' => ['entity' => new StubEntity()]],
                'stubEntity',
                "By 'entity' field in context"
            ],
            [
                ['context' => ['entity-type' => 'stubEntity']],
                'stubEntity',
                "By 'entity-type' field in context"
            ],
            [
                ['message' => "Some message which does not contain any information related to entity"],
                '',
                'Default empty value when no information related to entity'
            ]
        ];
    }

    /**
     * @dataProvider dataProvider
     *
     * @param array $record
     * @param $entityType
     * @param string $message
     * @throws \Exception
     */
    public function testProcessor(array $record, $entityType, string $message)
    {
        $processor = new EntityTypeProcessor();

        $this->assertEquals(
            $record + ['entity-type' => $entityType],
            $processor($record),
            $message
        );
    }

    /**
     */
    public function testBadEntityThrowsException()
    {
        $this->expectExceptionMessage("Entity passed to log must be an object");
        $this->expectException(\Exception::class);
        $processor = new EntityTypeProcessor();

        $processor(['context' => ['entity' => 'non-object']]);
    }
}