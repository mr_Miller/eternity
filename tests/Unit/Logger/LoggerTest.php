<?php

namespace Eternity\Tests\Unit\Logger;

use Eternity\Logger\Config\PhpConfig;
use Eternity\Logger\Formatters\PipeFormatter;
use Eternity\Logger\Interfaces\LogConfig;
use Eternity\Logger\Logger;
use Eternity\Logger\Messages\ExceptionLogMessage;
use Eternity\Tests\Unit\Logger\Stubs\StubConfigWithoutHandlers;
use Eternity\Tests\Unit\Logger\Stubs\StubLogMessage;
use Eternity\Trace\TraceId;
use Monolog\Handler\NullHandler;
use Monolog\Handler\StreamHandler;
use PHPUnit\Framework\TestCase;

/**
 * Class LoggerTest
 */
class LoggerTest extends TestCase
{
    public const PATH_TO_LOG = __DIR__ . '/monolog.log';
    public const COMPONENT_NAME = 'gw';

    /**
     * @var Logger
     */
    private $logger;

    /**
     * @var \Eternity\Trace\TraceId
     */
    private $traceId;

    /**
     * @throws \Exception
     */
    public function setUp(): void
    {
        $this->dropFile(self::PATH_TO_LOG);
        $this->traceId = new TraceId();
        $this->traceId->generate();
        $this->logger = new Logger('general', $this->traceId, $this->getConfigMock());
    }

    /**
     * Returns Config mock
     * @return \Eternity\Logger\Interfaces\LogConfig
     */
    private function getConfigMock(): LogConfig
    {
        $phpConfig = $this->createMock(PhpConfig::class);
        $phpConfig->method('handlers')
            ->willReturn([
                'handler1' => (new StreamHandler(self::PATH_TO_LOG, Logger::DEBUG))
                    ->setFormatter(
                        new PipeFormatter(
                            "%timestamp%|%component%|%severity%|%msg-code%|%msg%|%trace-id%|%entity-type%|%entity-id%|%stack-trace%",
                            \DateTimeInterface::ISO8601
                        )
                    ),
            ]);
        $phpConfig->method('debug')
            ->willReturn(true);
        $phpConfig->method('channel')
            ->willReturn(self::COMPONENT_NAME);

        return $phpConfig;
    }

    public function tearDown(): void
    {
        $handlers = $this->logger->getHandlers();
        foreach ($handlers as $handler) {
            if ($handler instanceof StreamHandler && $handler->getStream()) {
                fclose($handler->getStream());
            }
        }

        $this->dropFile(self::PATH_TO_LOG);
    }

    /**
     * @param string $filepath
     */
    private function dropFile(string $filepath)
    {
        if (is_file($filepath)) {
            unlink($filepath);
        }
    }

    /**
     * @return array
     */
    public function levelDataProvider()
    {
        return [
            [Logger::DEBUG, 'DEBUG'],
            [Logger::INFO, 'INFO'],
            [Logger::NOTICE, 'NOTICE'],
            [Logger::WARNING, 'WARN'],
            [Logger::ERROR, 'ERROR'],
            [Logger::CRITICAL, 'CRITICAL'],
            [Logger::ALERT, 'ALERT'],
            [Logger::EMERGENCY, 'EMERGENCY'],
        ];
    }

    /**
     * @dataProvider levelDataProvider
     *
     * @param int $level
     * @param string $levelName
     */
    public function testGetLevelName(int $level, string $levelName)
    {
        $this->assertEquals($levelName, Logger::getLevelName($level));
    }

    /**
     * @dataProvider levelDataProvider
     *
     * @param int $level
     * @param string $levelName
     * @throws \InvalidArgumentException
     */
    public function testLevelByName(int $level, string $levelName)
    {
        $this->assertEquals($level, Logger::getLevelByName($levelName));
    }

    /**
     */
    public function testUndefinedLevel()
    {
        $this->expectExceptionMessage("Log level is not defined, use one of: DEBUG, INFO, NOTICE, WARN, ERROR, CRITICAL, ALERT, EMERGENCY");
        $this->expectException(\Exception::class);
        Logger::getLevelName(23543543);
    }

    /**
     */
    public function testUndefinedLevelName()
    {
        $this->expectExceptionMessage("No log level represented as 'crit', use one of: DEBUG, INFO, NOTICE, WARN, ERROR, CRITICAL, ALERT, EMERGENCY");
        $this->expectException(\Exception::class);
        Logger::getLevelByName('crit');
    }

    /**
     * @throws \Exception
     */
    public function testError()
    {
        $logMessage = new StubLogMessage();
        $this->logger->error($logMessage, ['entity-type' => 'instance', 'entity-id' => 234]);
        $this->assertStringEndsWith(
            '|' . self::COMPONENT_NAME . '|ERROR|' . $logMessage->code() . '|' . $logMessage->message() . '|' . $this->traceId->value() . '|instance|234|' . PHP_EOL,
            file_get_contents(self::PATH_TO_LOG)
        );
    }

    /**
     * @throws \Exception
     */
    public function testWarning()
    {
        $logMessage = new StubLogMessage();
        $this->logger->warning($logMessage, ['entity-type' => 'instance', 'entity-id' => 234]);
        $this->assertStringEndsWith(
            '|gw|WARN|99999999999|Stub message|' . $this->traceId->value() . '|instance|234|' . PHP_EOL,
            file_get_contents(self::PATH_TO_LOG)
        );
    }

    /**
     * @throws \Exception
     */
    public function testInfo()
    {
        $this->logger->info(new StubLogMessage(), ['entity-type' => 'instance', 'entity-id' => 234]);

        $this->assertStringEndsWith(
            '|gw|INFO|99999999999|Stub message|' . $this->traceId->value() . '|instance|234|' . PHP_EOL,
            file_get_contents(self::PATH_TO_LOG)
        );
    }

    /**
     * @throws \Exception
     */
    public function testDebug()
    {
        $this->logger->debug(new StubLogMessage(), ['entity-type' => 'instance', 'entity-id' => 234]);

        $this->assertStringEndsWith(
            '|gw|DEBUG|99999999999|Stub message|' . $this->traceId->value() . '|instance|234|' . PHP_EOL,
            file_get_contents(self::PATH_TO_LOG)
        );
    }

    public function testNotice()
    {
        $this->logger->notice(new StubLogMessage(), ['entity-type' => 'instance', 'entity-id' => 234]);

        $this->assertStringEndsWith(
            '|gw|NOTICE|99999999999|Stub message|' . $this->traceId->value() . '|instance|234|' . PHP_EOL,
            file_get_contents(self::PATH_TO_LOG)
        );
    }

    public function testAlert()
    {
        $this->logger->alert(new StubLogMessage(), ['entity-type' => 'instance', 'entity-id' => 234]);

        $this->assertStringEndsWith(
            '|gw|ALERT|99999999999|Stub message|' . $this->traceId->value() . '|instance|234|' . PHP_EOL,
            file_get_contents(self::PATH_TO_LOG)
        );
    }

    public function testCritical()
    {
        $this->logger->critical(new StubLogMessage(), ['entity-type' => 'instance', 'entity-id' => 234]);

        $this->assertStringEndsWith(
            '|gw|CRITICAL|99999999999|Stub message|' . $this->traceId->value() . '|instance|234|' . PHP_EOL,
            file_get_contents(self::PATH_TO_LOG)
        );
    }

    public function testEmergency()
    {
        $this->logger->emergency(new StubLogMessage(), ['entity-type' => 'instance', 'entity-id' => 234]);

        $this->assertStringEndsWith(
            '|gw|EMERGENCY|99999999999|Stub message|' . $this->traceId->value() . '|instance|234|' . PHP_EOL,
            file_get_contents(self::PATH_TO_LOG)
        );
    }

    /**
     * @throws \Exception
     */
    public function testDefaultHandler()
    {
        $this->assertEquals(
            [new NullHandler()],
            (new Logger('general', $this->traceId, new StubConfigWithoutHandlers()))->getHandlers()
        );
    }

    /**
     * @return array
     */
    public function exceptionDataProvider(): array
    {
        return [
            [
                new \Exception('Exception message', 12345678, new \Exception('Previous exception message', 87654321)),
                [
                    'Exception message',
                    'Previous exception message',
                ],
            ],
        ];
    }

    /**
     * @dataProvider exceptionDataProvider
     *
     * @param \Throwable $exception
     * @param array $expected
     */
    public function testAddRecordExceptions(\Throwable $exception, array $expected)
    {
        $this->logger->addRecord(
            Logger::DEBUG,
            new ExceptionLogMessage($exception),
            [
                'exception' => $exception,
            ]
        );

        $log = file_get_contents(self::PATH_TO_LOG);

        foreach ($expected as $message) {
            $this->assertStringContainsString($message, $log);
        }
    }
}