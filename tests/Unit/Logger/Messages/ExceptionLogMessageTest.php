<?php

namespace Eternity\Tests\Unit\Logger\Messages;

use Eternity\Http\Contracts\ExtendedResponse;
use Eternity\Http\Dto\ErrorDto;
use Eternity\Http\Exceptions\ResponseException;
use Eternity\Http\Response\Response;
use Eternity\Logger\Messages\ExceptionLogMessage;
use PHPUnit\Framework\TestCase;

/**
 * Class ExceptionLogMessageTest
 */
class ExceptionLogMessageTest extends TestCase
{
    /**
     * @return array
     */
    public function codeDataProvider(): array
    {
        return [
            [new \Exception("Test message"), 0],
            [new \Exception("Test message", 123), 123],
            [
                new ResponseException($this->getResponseMock(), 'Title', 'Detail'),
                500
            ],
            [
                new ResponseException($this->getResponseWithoutCodeMock(), 'Title', 'Detail'),
                500
            ]
        ];
    }

    /**
     *
     * @return \Eternity\Http\Contracts\ExtendedResponse
     */
    private function getResponseMock(): ExtendedResponse
    {
        $mock = $this->createMock(Response::class);
        $mock->method('getError')->willReturn(ErrorDto::fromArray([
            'status' => 400,
            'code'   => 11111,
            'title'  => 'Some title',
            'type'   => 'SomeExceptionType',
            'detail' => 'Some detail'
        ]));

        return $mock;
    }

    /**
     * @return \Eternity\Http\Contracts\ExtendedResponse
     */
    private function getResponseWithoutCodeMock(): ExtendedResponse
    {
        $mock = $this->createMock(Response::class);
        $mock->method('getError')->willReturn(ErrorDto::fromArray([
            'status' => 400,
            'code'   => 0,
            'title'  => 'Some title',
            'type'   => 'SomeExceptionType',
            'detail' => 'Some detail'
        ]));

        return $mock;
    }

    /**
     * @return \Eternity\Http\Contracts\ExtendedResponse
     */
    private function getValidationResponseMock(): ExtendedResponse
    {
        $mock = $this->createMock(Response::class);

        $mock->method('getError')->willReturn(ErrorDto::fromArray([
            'status'            => 400,
            'code'              => 11111,
            'title'             => 'Some title',
            'type'              => 'SomeExceptionType',
            'detail'            => 'Some detail',
            'validation_errors' => [
                [
                    'name' => [
                        'Name is required'
                    ]
                ]
            ],
        ]));

        return $mock;
    }

    /**
     * @return array
     */
    public function messageDataProvider()
    {
        return [
            [new \Exception("Test message"), "Test message"],
            [
                new ResponseException($this->getResponseMock(), 'Response title', 'Response detail'),
                'Response title. Response detail. Type: ResponseException.'
            ],
            [
                new ResponseException($this->getValidationResponseMock(), 'Response title', 'Response detail'),
                'Response title. Response detail. Type: ResponseException.'
            ],
        ];
    }

    /**
     * @dataProvider codeDataProvider
     *
     * @param \Throwable $exception
     * @param int $code
     */
    public function testCode(\Throwable $exception, int $code)
    {
        $this->assertEquals(
            $code,
            (new ExceptionLogMessage($exception))->code()
        );
    }

    /**
     * @dataProvider messageDataProvider
     *
     * @param \Throwable $exception
     * @param string $message
     */
    public function testMessage(\Throwable $exception, string $message)
    {
        $this->assertEquals(
            $message,
            (new ExceptionLogMessage($exception))->message()
        );
    }

    public function testException()
    {
        $exception = new \Exception("Exception message");

        $message = new ExceptionLogMessage($exception);

        $this->assertEquals($exception, $message->exception());
    }
}