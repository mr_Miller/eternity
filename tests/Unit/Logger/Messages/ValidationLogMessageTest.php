<?php

namespace Eternity\Tests\Unit\Logger\Messages;

use Eternity\Exceptions\ValidationException;
use Eternity\Logger\Messages\ValidationLogMessage;
use PHPUnit\Framework\TestCase;

/**
 * Class ValidationLogMessageTest
 * @package Eternity\Tests\Unit\Logger\Messages
 */
class ValidationLogMessageTest extends TestCase
{
    /**
     * @return void
     */
    public function testCode()
    {
        $exception = new ValidationException(['name' => ['Name is required']], 'Title', 'Detail');

        $this->assertEquals(422, (new ValidationLogMessage($exception))->code());
    }

    /**
     * @return void
     */
    public function testMessage()
    {
        $exception = new ValidationException(['name' => ['Name is required']], 'Title', 'Detail');

        $this->assertEquals(
            'Title. Detail. Type: ValidationException. Validation errors: {"name":["Name is required"]}.',
            (new ValidationLogMessage($exception))->message()
        );
    }

    /**
     * @return void
     */
    public function testException()
    {
        $exception = new ValidationException(['name' => ['Name is required']], 'Title', 'Detail');

        $message = new ValidationLogMessage($exception);

        $this->assertEquals($exception, $message->exception());
    }
}