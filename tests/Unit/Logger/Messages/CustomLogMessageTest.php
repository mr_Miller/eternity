<?php

namespace Eternity\Tests\Unit\Logger\Messages;

use Eternity\Logger\Messages\CustomLogMessage;
use PHPUnit\Framework\TestCase;

/**
 * Class CustomLogMessageTest
 * @package Eternity\Tests\Unit\Logger\Messages
 */
class CustomLogMessageTest extends TestCase
{
    /**
     * @var CustomLogMessage
     */
    private $message;

    public function setUp(): void
    {
        $this->message = new CustomLogMessage("Custom message", 12345);
    }

    public function testCode()
    {
        $this->assertEquals(12345, $this->message->code());
    }

    public function testCodeEmptyCode()
    {
        $this->assertEquals(0, (new CustomLogMessage("Custom message"))->code());
    }

    public function testMessage()
    {
        $this->assertEquals("Custom message", $this->message->message());
    }
}