<?php

namespace Eternity\Tests\Unit\Logger\Messages;

use Eternity\Exceptions\ErrorCodes;
use Eternity\Logger\Messages\LogMessage;
use PHPUnit\Framework\TestCase;

/**
 * Class LogMessageTest
 */
class LogMessageTest extends TestCase
{
    public function testCodeWithoutMessageReturnsMessageOnlyWithCode()
    {
        $this->assertEquals('[[99999999999]] ', (string) new LogMessage(99999999999));
    }

    public function testToString()
    {
        $this->assertEquals(
            "[[10301]] Db query error",
            (string) new LogMessage(ErrorCodes::DB_QUERY_ERROR)
        );
    }
}