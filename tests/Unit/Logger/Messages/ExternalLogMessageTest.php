<?php

namespace Eternity\Tests\Unit\Logger\Messages;

use Eternity\Http\Contracts\ExtendedResponse;
use Eternity\Http\Dto\ErrorDto;
use Eternity\Http\Exceptions\ResponseException;
use Eternity\Http\Response\Response;
use Eternity\Logger\Messages\ExternalLogMessage;
use PHPUnit\Framework\TestCase;

/**
 * Class ExternalLogMessageTest
 * @package Eternity\Tests\Unit\Logger\Messages
 */
class ExternalLogMessageTest extends TestCase
{
    /**
     * @return array
     */
    public function codeDataProvider(): array
    {
        return [
            [
                new ResponseException($this->getResponseMock(), 'Title', 'Detail'),
                11111
            ],
            [
                new ResponseException($this->getResponseWithoutCodeMock(), 'Title', 'Detail'),
                400
            ]
        ];
    }

    /**
     * @return array
     */
    public function messageDataProvider(): array
    {
        return [
            [
                new ResponseException($this->getResponseMock(), 'Response title', 'Response detail'),
                'Response title. Response detail. External "Some title". Details: "Some detail". Type: SomeExceptionType.'
            ],
            [
                new ResponseException($this->getValidationResponseMock(), 'Response title', 'Response detail'),
                'Response title. Response detail. External "Some title". Details: "Some detail". Validation errors: [{"name":["Name is required"]}]. Type: ValidateExceptionType.'
            ],
        ];
    }

    /**
     *
     * @return \Eternity\Http\Contracts\ExtendedResponse
     */
    private function getResponseMock(): ExtendedResponse
    {
        $mock = $this->createMock(Response::class);
        $mock->method('getError')->willReturn(ErrorDto::fromArray([
            'status' => 400,
            'code'   => 11111,
            'title'  => 'Some title',
            'type'   => 'SomeExceptionType',
            'detail' => 'Some detail'
        ]));

        return $mock;
    }

    /**
     * @return \Eternity\Http\Contracts\ExtendedResponse
     */
    private function getResponseWithoutCodeMock(): ExtendedResponse
    {
        $mock = $this->createMock(Response::class);
        $mock->method('getError')->willReturn(ErrorDto::fromArray([
            'status' => 400,
            'code'   => 0,
            'title'  => 'Some title',
            'type'   => 'SomeExceptionType',
            'detail' => 'Some detail'
        ]));

        return $mock;
    }

    /**
     * @return \Eternity\Http\Contracts\ExtendedResponse
     */
    private function getValidationResponseMock(): ExtendedResponse
    {
        $mock = $this->createMock(Response::class);

        $mock->method('getError')->willReturn(ErrorDto::fromArray([
            'status'            => 400,
            'code'              => 11111,
            'title'             => 'Some title',
            'type'              => 'ValidateExceptionType',
            'detail'            => 'Some detail',
            'validation_errors' => [
                [
                    'name' => [
                        'Name is required'
                    ]
                ]
            ],
        ]));

        return $mock;
    }

    /**
     * @dataProvider codeDataProvider
     *
     * @param \Eternity\Http\Exceptions\ResponseException $exception
     * @param int $code
     */
    public function testCode(ResponseException $exception, int $code)
    {
        $this->assertEquals(
            $code,
            (new ExternalLogMessage($exception))->code()
        );
    }

    /**
     * @dataProvider messageDataProvider
     *
     * @param \Eternity\Http\Exceptions\ResponseException $exception
     * @param string $message
     */
    public function testMessage(ResponseException $exception, string $message)
    {
        $this->assertEquals(
            $message,
            (new ExternalLogMessage($exception))->message()
        );
    }

    public function testException()
    {
        $exception = new ResponseException($this->getValidationResponseMock());

        $message = new ExternalLogMessage($exception);

        $this->assertEquals($exception, $message->exception());
    }
}