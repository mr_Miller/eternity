<?php

namespace Eternity\Tests\Unit\Events;

use Eternity\Events\Consumer;
use Eternity\Events\Microservices\PushNotification;
use Eternity\Microservices;
use PHPUnit\Framework\TestCase;

/**
 * Class ConsumersTest
 * @group ConsumersTest
 * @package Eternity\Tests\Unit\Events
 */
class ConsumersTest extends TestCase
{
    /**
     * @throws \Eternity\Events\Exceptions\EventException
     */
    public function testGetSuccess()
    {
        $consumer = new Consumer(['test-event-type'=> [Microservices::MARKETPLACE]]);
        $this->assertEquals(Microservices::MARKETPLACE, $consumer->get('test-event-type')[0]);
    }

    public function testPushEventConsumerSuccess()
    {
        $consumer = new Consumer();
        $this->assertEquals([Microservices::NOTIFICATION], $consumer->get(PushNotification::class));
    }
}