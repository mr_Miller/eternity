<?php

namespace Eternity\Tests\Unit\Events;

use Eternity\Events\Events;
use Eternity\Events\Microservices\Notification\Push;
use PHPUnit\Framework\TestCase;

/**
 * Class EventsTest
 * @group EventsTest
 * @package Eternity\Tests\Unit\Events\Microservices
 */
class EventsTest extends TestCase
{
    public function testPushEventSuccess(): void
    {
        // Check type
        $this->assertEquals(Events::PUSH_ID_NOTIFICATION, Events::getType(Push::class));

        // Check
        $this->assertEquals(Push::class, Events::getClass(Events::PUSH_ID_NOTIFICATION));
    }
}