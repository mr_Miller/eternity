<?php

namespace Eternity\Tests\Unit\Events\Microservices\Notification;

use Eternity\Definitions\Notification\Firebase\PriorityDefinition;
use Eternity\Events\Microservices\Notification\Push;
use PHPUnit\Framework\TestCase;

/**
 * Class PushTest
 * @group PushTest
 * @package Eternity\Tests\Unit\Events\Microservices\Notification
 */
class PushTest extends TestCase
{
    public const DATA = [
        'id'             => 'some_push_id',
        'uid'            => 5,
        'params'         => ['balance' => 100],
        'priority'       => PriorityDefinition::HIGH,
        'data'           => ['some_data' => 1],
        'android_config' => ['android'],
        'apple_config'   => ['apple'],
    ];

    public function testFromArraySuccess(): void
    {
        $push = new Push();
        $push->fromArray(self::DATA);
        $this->assertEquals(self::DATA, $push->toArray());
        $this->assertCount(
            count(self::DATA),
            $push->toArray(),
            'Push event structure is changed, add Unit tests if needed'
        );
    }

    public function testCreateSuccess(): void
    {
        $push = Push::create(
            self::DATA['id'],
            self::DATA['uid'],
            self::DATA['params'],
            self::DATA['priority'],
            self::DATA['data'],
            self::DATA['android_config'],
            self::DATA['apple_config'],
        );

        $this->assertEquals(self::DATA, $push->toArray());
    }

    public function testIdSuccess(): void
    {
        $push = new Push();
        $push->fromArray(['id' => self::DATA['id']]);
        $this->assertEquals(self::DATA['id'], $push->id());
        $this->assertIsString($push->id());
    }

    public function testUidSuccess(): void
    {
        $push = new Push();
        $push->fromArray(['uid' => self::DATA['uid']]);
        $this->assertEquals(self::DATA['uid'], $push->uid());
    }

    public function testParamsSuccess(): void
    {
        $push = new Push();
        $push->fromArray(['params' => self::DATA['params']]);
        $this->assertEquals(self::DATA['params'], $push->params());
    }

    public function testPrioritySuccess(): void
    {
        $push = new Push();
        $push->fromArray(['priority' => self::DATA['priority']]);
        $this->assertEquals(self::DATA['priority'], $push->priority());
    }

    public function testDataSuccess(): void
    {
        $push = new Push();
        $push->fromArray(['data' => self::DATA['data']]);
        $this->assertEquals(self::DATA['data'], $push->data());
    }

    public function testAppleConfigSuccess(): void
    {
        $push = new Push();
        $push->fromArray(['apple_config' => self::DATA['apple_config']]);
        $this->assertEquals(self::DATA['apple_config'], $push->appleConfig());
    }

    public function testAndroidConfigSuccess(): void
    {
        $push = new Push();
        $push->fromArray(['android_config' => self::DATA['android_config']]);
        $this->assertEquals(self::DATA['android_config'], $push->androidConfig());
    }
}