<?php

namespace Eternity\Tests\Unit\Events\Microservices\Notification;

use Eternity\Events\Microservices\Notification\Push;
use Eternity\Events\Microservices\Notification\PushMultiple;
use Monolog\Test\TestCase;

/**
 * Class PushMultipleTest
 * @group PushMultipleTest
 * @package Eternity\Tests\Unit\Events\Microservices\Notification
 */
class PushMultipleTest extends TestCase
{
    public function testTraversableSuccess(): void
    {
        $originalPush = Push::create(1, 2);
        $pushMultiple = new PushMultiple();
        $pushMultiple->add($originalPush);

        foreach ($pushMultiple as $push) {
            /** @var Push $push */
            $this->assertEquals($originalPush->serialize(), $push->serialize());
        }
    }

    public function testSerializeSuccess(): void
    {
        $originalPush = Push::create(1, 2);
        $originalPushMultiple = new PushMultiple();
        $originalPushMultiple->add($originalPush);

        $serialized = $originalPushMultiple->serialize();

        $newPushMultiple = new PushMultiple();
        $newPushMultiple->unserialize($serialized);

        $this->assertEquals($newPushMultiple->serialize(), $serialized);
    }
}