# Eternity 

Project with components to help build services oriented structure

Eternity isolated components:
- [Logger](src/Logger)
- [Connector](src/Components/Connector)

## Installation
##### 1. Add into `composer.json`
```json
"require": {
        ..........
        "eternity/eternity": "version",
        ..........
    },
..........
"repositories": [
        {
            "type": "vcs",
            "url": "git@bitbucket.org:mr_Miller/eternity.git"
        }
    ]
```
##### 2. Create auth.json file, don't forget to set correct `CONSUMER_KEY` and `CONSUMER_SECRET`:
```jsons
{
    "bitbucket-oauth": {
        "bitbucket.org": {
            "consumer-key": CONSUMER_KEY,
            "consumer-secret": CONSUMER_SECRET,
        }
    }
}
```

## Changes
When applying changes Eternity package version in `composer.json` must be changed.

- `MAJOR` version when you make numerous incompatible changes or when new release is ready.
- `MINOR` version when you make backwards incompatible changes
- `PATCH` version when you add functionality in a backwards compatible manner or when you make backwards compatible bug fixes.

Example of PATCH changes: `1.1.5` -> `1.1.6`
Example of Minor changes (backwards compatibility is broken): `1.1.6` -> `1.2.0`

## Reviewers
Reviewers should set proper tag same as package version in `composer.json`
Example: 
If version is `1.2.5`, tag name must be `1.2.5`

The condition of success package updating is that the tag and version in `composer.json` must be __equal__

## Testing
To run tests:
```
php vendor/bin/phpunit tests/Unit
```
